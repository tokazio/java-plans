/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import classes.TconfigUI;
import evrLib.gc.Tconfiguration;

/**
 * @author rpetit
 */
public interface IconfigVue {

    /**
     * fonction abstraite qui retourne la configuration
     *
     * @return la configuration
     */
    Tconfiguration getConfig();

    /**
     * @return
     */
    TconfigUI getConfigUI();

}
