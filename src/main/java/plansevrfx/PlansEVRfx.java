/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package plansevrfx;

import classes.TconfigUI;
import classes.Toptions;
import controllers.CmainForm;
import esyoLib.Formsfx.Tapplication;
import esyoLib.Formsfx.Tform;
import evrLib.gc.TgescomFile;
import evrLib.gc.TstlParts;
import javafx.application.Application;
import javafx.stage.Stage;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

/**
 * @author rpetit
 */
public class PlansEVRfx extends Application {

    /**
     * liste des pièces STL (CONSTANTE)
     */
    public static TstlParts stlParts;
    /**
     * liste des pièces gescom (CONSTANTE)
     */
    public static TgescomFile gescom;
    /**
     * options
     */
    public static Toptions options;
    /**
     * Application
     */
    public static Tapplication application;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * @param c
     */
    public static void selectChantier(TconfigUI c) {
        ((CmainForm) application.getMainForm().getController()).selectChantier(c);
    }

    /**
     * @param primaryStage
     * @throws IOException
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     * @throws SAXException
     * @throws ParserConfigurationException
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        //
        options = new Toptions("options.txt");
        //nouvelle application (esyo) EN PREMIER!!
        application = new Tapplication(primaryStage, this);
        //Charge les pièces STL
        stlParts = new TstlParts("stlParts.xml");
        //Charge les références GESCOM
        gescom = new TgescomFile("gescom.txt");
        //
        System.out.println(CmainForm.class.getName());
        //Nouvelle fenêtre avec son titre

        Tform f = application.newForm("Main", "fxmls/mainForm.fxml", null).setTitle("Plans");
        //Cette fenêtre est la fenêtre principale
        application.setMainForm(f);
        //
        f.getController().manualIni();
        //Affiche la fenêtre
        f.refresh().show();
    }
}
