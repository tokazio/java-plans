/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evrLib.binpack;

import esyoLib.XML.TxmlObject;

/**
 * @author rpetit
 */
@Deprecated
public class Tmorceau extends TxmlObject {
    //Longueur en mm
    private double Flongueur = 0;
    //Nom de la portion à laquelle le morceau appartient
    private String Fportion = "";
    //Type de morceau
    private String Ftype = "";
    //Famille
    private String Ffamille = "";
    //Code
    private String Fcode = "";

    /**
     * Constructeur
     *
     * @param lg Longueur du morceau en mm
     */
    public Tmorceau(double lg) {
        Flongueur = lg;
    }

    /**
     * Constructeur
     *
     * @param lg      Longueur du morceau en mm
     * @param type    Type du morceau
     * @param portion Nom (String) de la portion à laquelle le morceau appartient
     * @param famille Famille
     */
    public Tmorceau(double lg, String type, String portion, String famille, String code) {
        setLongueur(lg);
        Ftype = type;
        Fportion = portion;
        Ffamille = famille;
        Fcode = code;
    }

    /**
     * Constructeur
     *
     * @param morceau Un morceau à copier pour créer le nouveau morceau
     */
    public Tmorceau(Tmorceau morceau) {
        Flongueur = morceau.getLongueur();
        Ftype = morceau.getType();
        Fportion = morceau.getPortion();
        Ffamille = morceau.getFamille();
        Fcode = morceau.getCode();
    }

    /**
     * @return Nom (String) de la portion à laquelle le morceau appartient
     */
    public final String getPortion() {
        return Fportion;
    }

    /**
     * Défini le nom (String) de la portion à laquelle le morceau appartient
     *
     * @param s Nom (String) de portion
     */
    public final void setPortion(String s) {
        Fportion = s;
    }

    /**
     * @return Code (String)
     */
    public final String getCode() {
        return Fcode;
    }

    /**
     * Défini le code (String)
     *
     * @param s Code (String)
     */
    public final void setCode(String s) {
        Fcode = s;
    }

    /**
     * @return Type de morceau (String)
     */
    public final String getType() {
        return Ftype;
    }

    /**
     * Défini le type de morceau (String)
     *
     * @param s Type de morceau (String)
     */
    public final void setType(String s) {
        Ftype = s;
    }

    /**
     * @return La longueur du morceau en mm
     */
    public final double getLongueur() {
        return Flongueur;
    }

    /**
     * Défini la longueur du morceau en mm
     *
     * @param lg Longueur en mm
     */
    public final void setLongueur(double lg) {
        Flongueur = lg;
    }

    /**
     * Compare la longueur du morceau avec un autre
     *
     * @param morceau Morceau auquelle comparé celui-ci
     * @return 0 si même longueur, -1 si plus petit et 1 si plus grand
     */
    public final int compareTo(Tmorceau morceau) {
        if (getLongueur() < morceau.getLongueur()) {
            return -1;
        }
        if (getLongueur() > morceau.getLongueur()) {
            return 1;
        }
        return 0;
    }

    /**
     * @return La famille
     */
    public final String getFamille() {
        return Ffamille;
    }

    /**
     * Définir la famille
     *
     * @param s Famille (String)
     */
    public final void setFamille(String s) {
        Ffamille = s;
    }

    @Override
    public void onLoaded() {

    }
}
