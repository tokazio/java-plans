/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evrLib.binpack;

import esyoLib.Utils.TstringList;
import esyoLib.XML.TxmlObject;
import evrLib.gc.Tbarre;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map.Entry;

/**
 * @author rpetit
 */
public class Tbinpacker extends TxmlObject {
    //liste des morceaux
    private final ArrayList<Tmorceau> Fmorceaux;
    //liste des barres
    private final ArrayList<Tbarre> Fbarres;
    //largeur de la lame de scie
    public double SCIE;
    //Longueurs des pertes (total des 2 côtes)
    public double PERTES;
    //Longueur de la 1ère coupe d'alignement
    public double PREMIER;
    //Longueur de la barre
    private double LGBARRE;
    //Nom de ?
    private String Fnom;
    //Marge ?
    private double Fmarge;
    //Mode de calcul: 0->ffd, 1->bfd(par défaut)
    private int Fmode = 1;

    /**
     * Constructreur
     */
    public Tbinpacker() {
        SCIE = 5;
        LGBARRE = 6500;
        PERTES = 40;
        PREMIER = 5;
        Fmorceaux = new ArrayList();
        Fbarres = new ArrayList();
    }

    /**
     * @return La marge ?
     */
    public final double getMarge() {
        return Fmarge;
    }

    /**
     * Défini la marge ?
     *
     * @param d La marge ?
     */
    public final void setMarge(double d) {
        Fmarge = d;
    }

    /**
     * Défini le mode de calcul bfd (Best Fit Decreasing)
     * Mode par défaut
     */
    public final void bfd() {
        Fmode = 1;
    }

    /**
     * Défini le mode de calcul ffd (First Fit Decreasing)
     */
    public final void ffd() {
        Fmode = 0;
    }

    //Renvoi la 1ère barre qui à la place pour accepter le morceau
    private Tbarre getFirstBarre(Tmorceau morceau) {
        for (Tbarre Fbarre : Fbarres) {
            if ((Fbarre.getChute() - SCIE - PERTES) >= morceau.getLongueur()) {
                return Fbarre;
            }
        }
        // sinon crée une barre vide
        return newBarre(morceau);
    }

    //Renvoi la barre qui peut accueillir le morceau avec la chute minimale
    private Tbarre getBestBarre(Tmorceau morceau) {
        HashMap<Tbarre, Double> m;
        m = new HashMap();
        //liste des barres qui peuvent accueillir le morceau (chute > taille morceau)
        for (Tbarre Fbarre : Fbarres) {
            if ((Fbarre.getChute() - SCIE - PERTES) >= morceau.getLongueur()) {
                m.put(Fbarre, Fbarre.getChute() - SCIE - PERTES - morceau.getLongueur());
            }
        }
        //Parmis ces barres, laquelle peut accueillir le morceau avec la chute minimale
        Tbarre b = null;
        double s = LGBARRE;
        for (Entry<Tbarre, Double> entry : m.entrySet())
            if (entry.getValue() < s) b = entry.getKey();
        if (b != null) {
            //System.out.println("Placé dans "+b);
            return b;
        }
        // sinon crée une barre vide
        return newBarre(morceau);
    }

    //Crée une nouvelle barre avec le morceau en paramètre
    private Tbarre newBarre(Tmorceau morceau) {
        if (morceau.getLongueur() == 0) {
            return null;
        }
        Tbarre b = new Tbarre(this, LGBARRE);
        Fbarres.add(b);
        // ajoute tout de suite un morceau de 5mm
        b.add(new Tmorceau(PREMIER));
        return b;
    }

    /**
     * Efface la liste des morceaux et des barres
     */
    protected void clear() {

        Fmorceaux.clear();
        Fbarres.clear();
    }

    /**
     * Effectue le binpacking
     */
    public final void binpack() {
        Fbarres.clear();
        if (Fmorceaux.isEmpty()) {
            return;
        }
        // morceaux trop grand?
        for (int i = 0; i < Fmorceaux.size(); i++) {
            while (Fmorceaux.get(i).getLongueur() + SCIE + PERTES + PREMIER > LGBARRE) {
                //double lg, String type, String portion, String famille
                Fmorceaux.add(new Tmorceau(LGBARRE - SCIE - PERTES - PREMIER, Fmorceaux.get(i).getType(), Fmorceaux.get(i).getPortion(), Fmorceaux.get(i).getFamille(), Fmorceaux.get(i).getCode()));
                Fmorceaux.get(i).setLongueur(Fmorceaux.get(i).getLongueur() - (LGBARRE - SCIE - PERTES - PREMIER));
            }
        }
        // trier morceaux dans l'ordre croissant
        //Fmorceaux.Sort(TComparer<Tmorceau>.Construct( function(const l, r: Tmorceau): integer begin Result := 0; if l.longueur > r.longueur then Result := -1; if l.longueur < r.longueur then Result := 1; end));
        sortMorceaux();
        //calcul selon le mode choisis
        switch (Fmode) {
            case 1:
                BFD();
                break;
            default:
                FFD();
        }
    }

    //Calcul First fit decreasing
    private void FFD() {
        // pour chaque morceau
        for (int i = 0; i < Fmorceaux.size(); i++) {
            // trouve une barre ayant de la place, si pas de barre en crée une
            Tbarre b = getFirstBarre(Fmorceaux.get(i));
            // ajoute le morceau à la barre trouvé
            b.add(new Tmorceau(Fmorceaux.get(i)));
        }
    }

    //Calcul Best fit decreasing
    private void BFD() {
        // pour chaque morceau
        for (int i = 0; i < Fmorceaux.size(); i++) {
            // trouve une barre ayant de la place, si pas de barre en crée une
            Tbarre b = getBestBarre(Fmorceaux.get(i));
            // ajoute le morceau à la barre trouvé
            b.add(new Tmorceau(Fmorceaux.get(i)));
        }
    }

    //Trie décroissant des morceaux selon leurs longueur en mm
    private void sortMorceaux() {
        Collections.sort(Fmorceaux, new Comparator<Tmorceau>() {
            @Override
            public int compare(Tmorceau o1, Tmorceau o2) {
                return -o1.compareTo(o2);
            }
        });
    }

    /**
     * Charge une liste de morceau à binpacker
     *
     * @param text Liste de morceau à binpacker (String). Morceaux séparé par un saut de ligne du type longueur=qtt(options/paramètres séparées par virgule)
     */
    public final void loadStrings(String text) {
        clear();
        TstringList l = new TstringList();
        l.assign(text);
        l.setText(l.getText().replaceAll("x", "="));
        for (int i = 0; i < l.size(); i++) {
            for (int j = 0; j < getNbr(l.valueFromIndex(i)); j++) {
                //longueur,type,portion,famille,code
                Fmorceaux.add(new Tmorceau(Double.parseDouble(l.nameFromIndex(i)), getParam(l.valueFromIndex(i), 0), getParam(l.valueFromIndex(i), 1), getParam(l.valueFromIndex(i), 2), getParam(l.valueFromIndex(i), 3)));
            }
        }
    }

    //Renvoi le nombre (la qtt) depuis une chaîne représentant un morceau
    private double getNbr(String s) {
        double v;
        try {
            v = Double.parseDouble(s);
        } catch (NumberFormatException ex) {
            v = getNbr(s.substring(0, s.indexOf("(")));
        }
        return v;
    }

    //Retourne le paramètre n° index depuis une chaîne représentant un morceau
    private String getParam(String s, int index) {
        int i = s.indexOf("(");
        if (i >= 0) {
            s = s.substring(i + 1, s.length() - 1);
            String[] ts = s.split("\\,");
            if (index > ts.length - 1) return "!#" + index;
            return ts[index];
        }
        return "";
    }

    /**
     * @return Le nombre de barre à arrondis 0.5 près
     */
    public final double getNombreDeBarres() {
        double r = 0;
        for (Tbarre b : Fbarres) {
            // si la chutte est plus petite qu'une demi longueur, j'ajoute 0.5 barre
            if ((b.getLongueur() - b.getChute()) <= (b.getLongueur() / 2)) {
                r += 0.5; // (b.longueur / 2)
            } // sinon j'jaoute 1 barre complète
            else {
                r += 1; // b.longueur;
            }
        }
        return r;
    }

    /**
     * @return Le nombre de morceaux
     */
    public final int getNombreDeMorceaux() {
        return Fmorceaux.size();
    }

    /**
     * @return La liste des barres
     */
    public final ArrayList<Tbarre> getBarres() {
        return Fbarres;
    }

    /**
     * @return Le nom ?
     */
    public final String getNom() {
        return Fnom;
    }

    /**
     * Défini le nom
     *
     * @param nom Nom ? (String)
     */
    public final void setNom(String nom) {
        Fnom = nom;
    }

    /**
     * @return Retourne les infos de définition du calcul (taille de SCIE, PERTE et PREMIER) sous forme de phrase
     */
    public final String getDefText() {
        return "Trait de scie de " + SCIE + "mm, pertes de " + PERTES + "mm et coupe d'ajustement (1er morceau) de " + PREMIER + "mm";
    }

    /**
     * @return La Longueur de barre en mm
     */
    public final double getLGBARRE() {
        return LGBARRE;
    }

    /**
     * Défini la longueur des barres
     *
     * @param d Longueur de barre en mm
     */
    public final void setLGBARRE(double d) {
        LGBARRE = d;
    }

    @Override
    public void onLoaded() {

    }
}
