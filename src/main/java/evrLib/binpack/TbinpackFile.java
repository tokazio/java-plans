/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evrLib.binpack;

import esyoLib.Utils.TarrayList;
import esyoLib.XML.TxmlObject;
import evrLib.gc.Tpotelet;

/**
 * @author rpetit
 */
@Deprecated
public class TbinpackFile extends TxmlObject {

    private final TarrayList<Tpotelet> Fpotelets = new TarrayList(Tpotelet.class);
    private double Flongueur;
    private double FnbrTraverses;
    private boolean Fmaincourante;
    private String Fgamme;
    private boolean FnoDerPot;
    private boolean FnoPremPot;
    private boolean FsansPotelet;
    private String FtraverseRef;
    private String FpoteletRef;
    private String FmcRef;
    private String FcacheRef;
    private String FbalRef;
    private double FcalculatedTotBal;
    private double FhauteurBal;
    private double FlgBarre;
    private boolean FisBal;
    private boolean FisCable;
    private String Fnom;

    /**
     *
     */
    public TbinpackFile() {

    }

    /**
     * @return Avec ou sans potelet (boolean)
     */
    public final boolean getSansPotelet() {
        return FsansPotelet;
    }

    /**
     * @return Avec ou sans 1er potelet (boolean)
     */
    public final boolean getNoPremPot() {
        return FnoPremPot;
    }

    /**
     * @return Avec ou sans dernier potelet (boolean)
     */
    public final boolean getNoDerPot() {
        return FnoDerPot;
    }

    /**
     * @return Liste des potelets
     */
    public final TarrayList getPotelets() {
        return Fpotelets;
    }

    public final boolean getIsCable() {
        return FisCable;
    }

    /**
     * Ouvrir depuis le fichier aFileName
     * @param aFileName Chemin de fichier à ouvrir
     * @throws IOException Erreur de lecture/écriture
     * @throws NoNameException Erreur de paramètre dans le fichier
     */
    /*
    public final void loadFromFile(String aFileName) throws IOException, NoNameException, Exception {
        //System.out.println("Load from file "+aFileName);
        TstringList l = new TstringList(aFileName);
        //System.out.println(l.valueFromName("Fpotelets"));
    //    Fpotelets.loadFromString(l.valueFromName("Fpotelets"));
        Fmaincourante = l.valueFromName("Fmaincourante").equals("true");
        Flongueur = Double.parseDouble(l.valueFromName("Flongueur"));
        FnbrTraverses = Double.parseDouble(l.valueFromName("FnbrTraverses"));
        Fgamme = l.valueFromName("Fgamme");
        FnoDerPot = l.valueFromName("FnoDerPot").equals("true");
        FnoPremPot = l.valueFromName("FnoPremPot").equals("true");
        FsansPotelet = l.valueFromName("Fsanspotelet").equals("true");
        try{
            FtraverseRef = l.valueFromName("FtraverseRef");
            FpoteletRef = l.valueFromName("FpoteletRef");
            FmcRef = l.valueFromName("FmcRef");
            FcacheRef = l.valueFromName("FcacheRef");
            FbalRef = l.valueFromName("FbalRef");
            FlgBarre = Double.parseDouble(l.valueFromName("FLGBARRE"));
            Fnom=l.valueFromName("Fnom");
        }catch(Exception ex){
            //compatibilité fichier
            System.out.println("Type de fichier trop ancien pour être lu avec cette version du binpacker. Ouvrez les fichiers plans et recalculez.");
        }
        FcalculatedTotBal = Double.parseDouble(l.valueFromName("FcalculatedTotBal"));
        FhauteurBal = Double.parseDouble(l.valueFromName("FhauteurBal"));
        
        FisBal=l.valueFromName("FisBal").equals("true");
        FisCable=l.valueFromName("FtraversesCable").equals("true");
    }
    */

    /**
     * @return Avec ou sans main courante (boolean)
     */
    public final boolean getMainCourante() {
        return Fmaincourante;
    }

    /**
     * @return La longueur en mm
     */
    public final double getLongueur() {
        return Flongueur;
    }

    /**
     * @return Nombre de traverses
     */
    public final double getNbrTraverses() {
        return FnbrTraverses;
    }

    /**
     * @return Nom de la gamme (String)
     */
    public final String getGamme() {
        return Fgamme;
    }

    /**
     * @return Nombre total de balustre calculé
     */
    public final double getCalculatedTotBal() {
        return FcalculatedTotBal;
    }

    /**
     * @return Hauteur de balustre
     */
    public final double getHauteurBal() {
        return FhauteurBal;
    }

    /**
     * @return Référence du profil balustre
     */
    public final String getBalRef() {
        return FbalRef;
    }

    /**
     * @return Référence du profil traverse
     */
    public final String getTravRef() {
        return FtraverseRef;
    }

    /**
     * @return Référence du profil main courante
     */
    public final String getMCRef() {
        return FmcRef;
    }

    /**
     * @return Référence du profil potelet
     */
    public final String getPotRef() {
        return FpoteletRef;
    }

    /**
     * @return Référence du profil cache rainure
     */
    public final String getCacheRef() {
        return FcacheRef;
    }

    /**
     * @return Longueur de la barre en mm
     */
    public final double getLgBarre() {
        return FlgBarre;
    }

    /**
     * @return Avec ou sans balustre (boolean)
     */
    public final boolean getIsBal() {
        return FisBal;
    }

    public String getNom() {
        return Fnom;
    }

    @Override
    public void onLoaded() {

    }

}
