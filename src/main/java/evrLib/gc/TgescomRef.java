/*
 * Copyright (C) 2014 rpetit
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package evrLib.gc;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

/**
 * @author rpetit
 */
public class TgescomRef {

    private String ref = "";
    private String designation = "";
    private double prixVente = 0;
    private String famille = "";

    public TgescomRef() {

    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public double getPrixVente() {
        return prixVente;
    }

    public void setPrixVente(double prixVente) {
        this.prixVente = prixVente;
    }

    public void setPrixVente(String s) {
        s = s.trim();
        NumberFormat format = NumberFormat.getInstance(Locale.FRANCE);
        Number n = 0;
        try {
            n = format.parse(s);
        } catch (ParseException ex) {
            System.out.println("Erreur parse prixVente dans " + s);
        }
        this.prixVente = n.doubleValue();
    }


    public String getFamille() {
        return famille;
    }

    public void setFamille(String famille) {
        this.famille = famille;
    }

    @Override
    public String toString() {
        return "-" + this.ref + "- -" + this.designation + "- -" + this.prixVente + "- -" + this.famille + "-";
    }

}
