/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evrLib.gc;

import esyoLib.XML.TxmlObject;
import evrLib.enumeration.Epercage;
import evrLib.enumeration.Eside;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

/**
 * @author rpetit
 */
public class Tpercage extends TxmlObject {

    //code de perçage

    private Epercage Fcode = Epercage.none;
    //position du perçage en mm
    private double Fposition = -1;
    //face du perçage
    private Eside Fside = Eside.haut;

    /**
     * Constructeur
     *
     * @param code     Code du perçage (Epercage)
     * @param position Position en mm
     */
    public Tpercage(Epercage code, double position) {
        Fcode = code;
        Fside = Fcode.getSide();
        Fposition = position;
    }

    /**
     * Constructeur
     *
     * @param s Face du perçage (Eside)
     */
    public Tpercage(Eside s) {
        Fside = s;
    }

    /**
     * Constructeur
     * <p>
     * vide pour newInstance de TxmlObject
     */
    public Tpercage() {

    }

    /**
     * @return Position du perçage en mm
     */
    public final double getPosition() {
        return Fposition;
    }

    /**
     * Défini la position du perçage
     *
     * @param d Position en mm
     */
    public final void setPosition(double d) {
        Fposition = d;
    }

    /**
     * @return Le code du perçage (Epercage)
     */
    public final Epercage getCode() {
        return Fcode;
    }

    /**
     * Défini le code de perçage (Epercage)
     *
     * @param i Code de perçage (Epercage)
     */
    public final void setCode(Epercage i) {
        Fcode = i;
    }

    /**
     * @return La face du perçage (Eside)
     */
    public final Eside getSide() {
        return Fside;
    }

    /**
     * Défini la face du perçage (Eside)
     *
     * @param s Face du perçage (Eside)
     */
    public final void setSide(Eside s) {
        Fside = s;
    }

    /**
     * @return Phrase décrivant le perçage
     */
    @Override
    public String toString() {
        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(2);
        return "Perçage " + Fcode.getCode() + "(" + Fcode.getText() + ") à " + df.format(Fposition) + "(" + Fside.text() + ")";
    }

    /**
     * Pour enregistrement du morceau (à lire avec le programme DELPHI d'édition
     * de barre)
     *
     * @return
     */
    public String toFile() {
        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(2);
        DecimalFormatSymbols custom = new DecimalFormatSymbols();
        custom.setDecimalSeparator('.');
        df.setDecimalFormatSymbols(custom);
        return Fcode.getCode() + "=" + df.format(Fposition);
    }

    @Override
    public void onLoaded() {

    }
}
