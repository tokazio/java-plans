/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evrLib.gc;

import esyoLib.Utils.TarrayList;
import esyoLib.XML.TxmlObject;
import evrLib.enumeration.Eprofil;

import java.util.ArrayList;
import java.util.Collections;

/**
 * @author rpetit
 */
public class Tprofile extends TxmlObject {

    private final TarrayList<Tpercage> Fpercages = new TarrayList(Tpercage.class);//liste des perçages
    protected double Flongueur;//longueur du profil en mm
    protected String Fportion = "";//nom de la portion
    @Deprecated
    protected String Fref = "";//référence du profilé
    protected Eprofil Fprofil = Eprofil.none;
    protected String Ffamille = "";//traverse,baluste,MC,potelet,cache

    /**
     * Constructeur
     */
    public Tprofile() {

    }

    /**
     * Constructeur
     *
     * @param longueur Longeur du profil en mm
     * @param ref      Référence du profil
     * @param portion  Portion à laquelle le profil appartient
     * @param famille  Famille
     */
    public Tprofile(double longueur, String ref, String portion, String famille) {
        Flongueur = longueur;
        Fportion = portion;
        Fref = ref;
        Ffamille = famille;
    }

    /**
     * @return Le Tprofile sous forme de String
     */
    @Override
    public String toString() {
        return "" + Flongueur + " de " + Fref + " pour " + Ffamille + " (" + Fportion + ")";
    }

    /**
     *
     * @return La chaîne représentant le Tprofile à enregistrer dans le fichier
     * de sauvegarde
     */
    /*
    @Override
    public String saveToString() {
        return Flongueur + ",type," + Fportion + "," + Ffamille;
    }
*/

    /**
     * @return La longueur du morceau de profil en mm
     */
    public final double getLongueur() {
        return Flongueur;
    }

    /**
     * @return Le nom de la portion à laquelle le profil appartient (String)
     */
    public final String getPortion() {
        return Fportion;
    }

    /**
     * Défini la portion à laquelle le profil appartient
     *
     * @param s Nom (String) de la portion à laquelle le profil appartient
     */
    public final void setPortion(String s) {
        Fportion = s;
    }

    /**
     * @return La référence du profil (String)
     */
    @Deprecated
    public final String getRef() {
        return Fref;
    }

    /**
     * Défini la référence du profil
     *
     * @param s Référence (String) du profil
     */
    @Deprecated
    public final void setRef(String s) {
        Fref = s;
    }

    /**
     * @return La famille du profil (String)
     */
    public final String getFamille() {
        return Ffamille;
    }

    /**
     * Défini la famille du profil
     *
     * @param s Famille (String) du profil
     */
    public final void setFamille(String s) {
        Ffamille = s;
    }

    /**
     * @return La liste des perçages du profil
     */
    public final ArrayList<Tpercage> getPercages() {
        return Fpercages;
    }

    //Efface les perçages
    protected void clearPercages() {
        Fpercages.clear();
    }

    /**
     * Ajoute un perçage à la liste
     *
     * @param p Un perçage
     */
    public final void addPercage(Tpercage p) {
        Fpercages.add(p);
    }

    /**
     * Trie la liste des perçages dans l'ordre croissant des positions
     */
    public final void sortPercages() {
        //par code du plus grand au plus petit
        Collections.sort(Fpercages, (Tpercage o1, Tpercage o2) -> {
            if (o1.getCode().getCode() > o2.getCode().getCode()) {
                return -1;
            }
            if (o1.getCode().getCode() < o2.getCode().getCode()) {
                return 1;
            }
            return 0;
        });
        //par position du plus petit au plus grand
        Collections.sort(Fpercages, (Tpercage o1, Tpercage o2) -> {
            if (o1.getPosition() > o2.getPosition()) {
                return 1;
            }
            if (o1.getPosition() < o2.getPosition()) {
                return -1;
            }
            return 0;
        });
    }

    @Override
    public void onLoaded() {

    }
}
