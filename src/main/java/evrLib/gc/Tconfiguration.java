/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evrLib.gc;

import esyoLib.Files.TfileUtils;
import esyoLib.Utils.TarrayList;
import esyoLib.XML.TxmlObject;
import evrLib.enumeration.Egamme;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

/**
 * @author rpetit
 */
public abstract class Tconfiguration extends TxmlObject implements Iconfiguration {

    /**
     * Noms des gammes Non enregistré dans les fichiers de configuration
     *
     * @deprecated Utiliser Egamme qui contient les noms de gamme
     */
    @Deprecated
    public final transient static String[] GammeNames = {"Baludesign", "Monoprofil", "Chrome"};
    /**
     * Position (y) du haut du vitrage/tôle par défaut dans le cas de
     * remplissage 1/3 Non enregistré dans les fichiers de configuration
     */
    public final transient static int DEFAULTPOSVERRE = 450;
    /**
     * Position (y) du haut du vitrage/tôle par défaut dans le cas de
     * remplissage complet Non enregistré dans les fichiers de configuration
     */
    public final transient static int DEFAULTPOSVERRECOMPLET = 650;
    /**
     * Epaisseur (y) du profilé de la MC
     */
    public double FHMC;
    /**
     * Hauteur (y) de la liaison pot/mc
     */
    public double FHLIAISON;
    /**
     * Hauteur (y) de la liaison pot/mc pour potelet d'angle
     */
    public double FHLIAISONANGLE;
    /**
     * Epaisseur (y) du profilé de la traverse
     */
    public double FHTRAV;
    /**
     * Largeur (x) du profilé de potelet
     */
    public double FLARGPOT;
    /**
     * Largeur (x) du profilé de potelet
     */
    public double FLARGBAL;
    /**
     * ?
     */
    public double FAXEMC;
    /**
     * ?
     */
    public double FAXELIAISON;
    /**
     * Ecart (y) entre le sol et le bas du tube potelet
     */
    public double FHSABOT;
    /**
     * Epaisseur (y) du câble
     */
    public double FHCABLE;
    /**
     * Epaisseur (y) du bouchon de potelet
     */
    public double FHBOUCHONPOT;
    /**
     * Longueur par défaut des barres
     */
    public double FLGBARRE;
    //--------------------------------------------------------------------------
    //Constantes
    //--------------------------------------------------------------------------
    /**
     * Epaisseur (z) du potelet
     */
    public double FEPPOT;
    /**
     * Epaisseur (z) de la traverse
     */
    public double FEPTRAV;
    //--------------------------------------------------------------------------
    // POUR VERIFICATIONS EN AUTOMATIQUE
    //--------------------------------------------------------------------------
    public double MINHFINIE;//selon gamme hauteur mc+fixmc+sabot
    public double MINESPPOT = 0;
    public double MAXESPPOT;//selon gamme 1300 balu et chrome et 2000 mono
    public double MAXESPBAL = 110;
    public double MINPOSVERRE = 450;
    public double MAXVERRESOL = 110;
    public double LARGPINCEVERRE = 23;
    public double LARGPINCETOLE = 12;
    public double MINTRAVSOL = 116;
    public double MAXESPTRAV = 110;
    //liste des potelets
    protected TarrayList<Tpotelet> Fpotelets = new TarrayList(Tpotelet.class);
    //liste des traverses
    protected TarrayList<Ttraverse> Ftraverses = new TarrayList(Ttraverse.class);
    //liste des balustres
    protected TarrayList<Tbalustre> Fbalustres = new TarrayList(Tbalustre.class);
    //liste des mc
    protected TarrayList<Tmc> Fmc = new TarrayList(Tmc.class);
    //liste des caches
    protected TarrayList<Tcache> Fcaches = new TarrayList(Tcache.class);
    // selon gamme
    protected String FtraverseRef;
    protected String FpoteletRef;
    protected String FmcRef;
    protected String FcacheRef;
    protected String FbalRef;
    protected String FpercagePince;
    protected String FpercageTravA;
    protected String FpercageTravX;
    protected String FpercageTravC;
    protected double FsolDesaxeVertAncrageA;
    protected double FsolDesaxeVertAncrageB;
    protected double FsolDesaxeVertAncrageC;
    protected double FsolDesaxeHoriAncrageA;
    protected double FsolDesaxeHoriAncrageB;
    protected double FsolDesaxeHoriAncrageC;
    protected double FfacDesaxeVertAncrageA;
    protected double FfacDesaxeVertAncrageB;
    protected double FfacDesaxeHoriAncrageA;
    protected double FfacDesaxeHoriAncrageB;
    protected double FfacHauteurPlatine;
    protected double FaxeZ;
    protected int FrotPot;
    protected int FrotPotAngleDroit;
    protected int FrotPotAngleGauche;
    //nomenclature 3D
    private Tnomenclature3D nomenclature3d = null;
    //nomenclature
    private Tnomenclature nomenclature = null;
    //---------------------------------------------------------------------
    // A ENTRER------------------------------------------------------------
    //---------------------------------------------------------------------
    private boolean FMCmurale;
    private int FhauteurScellement;
    //---------------------------------------------------------------------
    private int Flongueur;
    //---------------------------------------------------------------------
    private Egamme Fgamme = Egamme.none;
    //---------------------------------------------------------------------
    private boolean Fmaincourante;
    //---------------------------------------------------------------------
    private boolean Fsanspotelet;
    //---------------------------------------------------------------------
    private String Fnom = "";
    //---------------------------------------------------------------------
    private int FhauteurFinie;
    //---------------------------------------------------------------------
    private int FespacementPotMax;
    //--------------------------------------------------------------------------
    private int FposVerre;
    //--------------------------------------------------------------------------
    private int FdebordVerreSol;
    //--------------------------------------------------------------------------
    private int FaxePinceHaut;
    //--------------------------------------------------------------------------
    private boolean FverreIscomplet;
    //--------------------------------------------------------------------------
    private int FaxePinceBas;
    //--------------------------------------------------------------------------
    private boolean FisScellement;
    //--------------------------------------------------------------------------
    private boolean FisVerre;
    //--------------------------------------------------------------------------
    private boolean FisBal;
    //--------------------------------------------------------------------------
    private int FespacementBalMax;
    //--------------------------------------------------------------------------
    private int FhauteurBal;
    //--------------------------------------------------------------------------
    private boolean FisFacade;
    //--------------------------------------------------------------------------
    private int FnbrTraverses;
    //--------------------------------------------------------------------------
    private int FdebordTravBas;
    //--------------------------------------------------------------------------
    private double FespTravSerre;
    //--------------------------------------------------------------------------
    private double FnbrTravSerre;
    //--------------------------------------------------------------------------
    private double FposTravHaute;
    //--------------------------------------------------------------------------
    private double FposTravBasse;
    //--------------------------------------------------------------------------
    private int FfacDecalHauteurPlatine;
    //--------------------------------------------------------------------------
    private int FfacDeportDecalHauteurPlatine;
    //--------------------------------------------------------------------------
    private boolean FtraversesApplique;
    //--------------------------------------------------------------------------
    private boolean FtraversesAxe;
    //--------------------------------------------------------------------------
    private boolean FtraversesCable;
    //--------------------------------------------------------------------------
    private int Fepdalle;
    //--------------------------------------------------------------------------
    private double FswitchDeportFac;
    //--------------------------------------------------------------------------
    private double FfacDecalZ;
    private double FfacDeportDecalZ;
    //--------------------------------------------------------------------------
    private double FtravDecalZ;
    //--------------------------------------------------------------------------
    private int FdecalZ;
    //--------------------------------------------------------------------------
    private double FfacAxeVertPlatine;
    //--------------------------------------------------------------------------
    private boolean FinversePotBalGauche;
    //--------------------------------------------------------------------------
    private boolean FinversePotBalDroite;
    //--------------------------------------------------------------------------
    // CALCULE -----------------------------------------------------------------
    //--------------------------------------------------------------------------
    @Deprecated
    private double FcalculatedNbrPot;
    //--------------------------------------------------------------------------
    private double FcalculatedHauteurTraverses;
    //--------------------------------------------------------------------------
    private double FcalculatedEntraxeTraverse;
    //--------------------------------------------------------------------------
    private double FcalculatedEntraxeTraverseSerre;
    //--------------------------------------------------------------------------
    private double FcalculatedEntraxeTraverseBas;
    //--------------------------------------------------------------------------
    private double FcalculatedHauteurSousMC;
    //--------------------------------------------------------------------------
    private double FcalculatedPotPosZ;
    //--------------------------------------------------------------------------
    private double FcalculatedAncragePosZ;
    //--------------------------------------------------------------------------
    private double FcalculatedNbrBal;
    //--------------------------------------------------------------------------
    private double FcalculatedTotBal;
    //--------------------------------------------------------------------------
    private double FcalculatedEntraxeBalReel;
    //--------------------------------------------------------------------------
    private double FcalculatedEspBalReel;
    //--------------------------------------------------------------------------
    private double FcalculatedDebBasBal;

    /**
     * @param index
     * @return
     * @deprecated A remplacer par getPotelets().get(?)
     */
    @Deprecated
    public final Tpotelet getPotelet(int index) {
        if (index > Fpotelets.size() - 1) {
            System.out.println("Le potelet " + index + " n'existe pas dans la liste (" + Fpotelets.size() + " max)");
            return null;
        }
        return Fpotelets.get(index);
    }

    /**
     * @param index
     * @return
     * @deprecated A remplacer par getTraverses().get(?)
     */
    @Deprecated
    public final Ttraverse getTraverse(int index) {
        if (index > Ftraverses.size() - 1) {
            System.out.println("La traverse " + index + " n'existe pas dans la liste (" + Ftraverses.size() + " max)");
            return null;
        }
        return Ftraverses.get(index);
    }

    /**
     * @return la liste des potelets
     */
    public final TarrayList<Tpotelet> getPotelets() {
        return Fpotelets;
    }

    /**
     * @return la liste des traverses
     */
    public final TarrayList getTraverses() {
        return Ftraverses;
    }

    /**
     * @return la liste des MC
     */
    public final TarrayList getMC() {
        return Fmc;
    }

    /**
     * @return la liste des balustres
     */
    public final TarrayList getBalustres() {
        return Fbalustres;
    }

    /**
     * @return la liste des caches
     */
    public final TarrayList getCaches() {
        return Fcaches;
    }

    /**
     * Fonction de calcul spécifique à la configuration
     *
     * @return Chaînage
     */
    public abstract Tconfiguration calcul();

    /**
     * @return true si la MC est directement au mur [FMCmurale]
     */
    public final boolean getMCmurale() {
        return FMCmurale;
    }

    /**
     * @param b
     */
    public final void setMCmurale(boolean b) {
        FMCmurale = b;
    }

    /**
     * @return la hauteur (y) du scellement [FhauteurScellement]
     */
    public final int getHauteurScellement() {
        return FhauteurScellement;
    }

    /**
     * @param i
     */
    public final void setHauteurScellement(int i) {
        FhauteurScellement = i;
    }

    /**
     * @param s
     */
    public final void setHauteurScellement(String s) {
        if (s.isEmpty()) {
            s = "0";
        }
        setHauteurScellement(Integer.parseInt(s));
    }

    /**
     * @return la longueur totale de la portion [Flongueur]
     */
    public final int getLongueur() {
        return Flongueur;
    }

    /**
     * @param i
     */
    public final void setLongueur(int i) {
        Flongueur = i;
    }

    /**
     * @param s
     */
    public final void setLongueur(String s) {
        if (s.isEmpty()) {
            s = "0";
        }
        setLongueur((int) Double.parseDouble(s));
    }

    /**
     * @return la gamme du type Egamme [Fgamme]
     */
    public final Egamme getGamme() {
        return Fgamme;
    }

    /**
     * @param e
     * @throws java.io.IOException
     * @throws javax.xml.parsers.ParserConfigurationException
     * @throws org.xml.sax.SAXException
     */
    public final void setGamme(Egamme e) throws IOException, ParserConfigurationException, SAXException {
        System.out.print("Tconfiguration::setGamme->La config actuelle est " + Fgamme);
        if (Fgamme != e) {
            System.out.println("-> changer pour " + e);
            Fgamme = e;
            loadGamme();
            loadNomenclature3D();
            loadNomenclature();
        } else {
            System.out.println("-> ne pas changer!");
        }
    }

    /**
     * @param s
     * @throws java.io.StreamCorruptedException
     * @throws java.lang.NoSuchMethodException
     * @throws javax.xml.parsers.ParserConfigurationException
     * @throws org.xml.sax.SAXException
     */
    public final void setGamme(String s) throws NoSuchMethodException, IOException, ParserConfigurationException, SAXException {
        setGamme(Egamme.fromString(s));
    }

    /**
     * @throws IOException
     * @throws SAXException
     * @throws ParserConfigurationException
     */
    @Override
    public void onLoaded() throws IOException, SAXException, ParserConfigurationException {
        //loadGamme() ?
        loadNomenclature3D();
        loadNomenclature();
    }

    /**
     * @return true si il y a une MC [Fmaincourante]
     */
    public final boolean getMainCourante() {
        return Fmaincourante;
    }

    /**
     * @param b
     */
    public final void setMainCourante(boolean b) {
        Fmaincourante = b;
    }

    /**
     * @return true si pas de potelet [Fsanspotelet]
     */
    public final boolean getSansPotelet() {
        return Fsanspotelet;
    }

    /**
     * @param b
     */
    public final void setSansPotelet(boolean b) {
        Fsanspotelet = b;
    }

    /**
     * @return le nom de la portion [Fnom]
     */
    public final String getNom() {
        return Fnom;
    }

    /**
     * @param s
     */
    public final void setNom(String s) {
        Fnom = s;
    }

    /**
     * @return la hauteur finie (du sol à au dessus de la MC) [FhauteurFinie] sauf si il y a un muret, du haut du muret à au dessus de la MC.
     */
    public final int getHauteurFinie() {
        return FhauteurFinie;
    }

    /**
     * @param i
     */
    public final void setHauteurFinie(int i) {
        /*
         if(i<MINHFINIE){
         System.out.println("La hauteur fini doit ête au minimum de "+MINHFINIE);
         return;
         }
         */
        FhauteurFinie = i;
    }

    /**
     * @param s
     */
    public final void setHauteurFinie(String s) {
        if (s.isEmpty()) {
            s = "0";
        }
        setHauteurFinie((int) Double.parseDouble(s));
    }

    /**
     * @return espacement maximum entre potelet [FespacementPotMax]
     */
    public final int getEspacementPotMax() {
        return FespacementPotMax;
    }

    /**
     * @param i
     */
    public final void setEspacementPotMax(int i) {
        /*
         if(i<MINESPPOT && !getSansPotelet()){
         System.out.println("L'espacement potelet doit ête au minimum de "+MINESPPOT);
         return;
         }
         if(i>MAXESPPOT && !getSansPotelet()){
         System.out.println("L'espacement potelet devrait ête au maximum de "+MAXESPPOT);
         }
         */
        FespacementPotMax = i;
    }

    /**
     * @param s
     */
    public final void setEspacementPotMax(String s) {
        if (s.isEmpty()) {
            s = "0";
        }
        setEspacementPotMax((int) Double.parseDouble(s));
    }

    /**
     * @return position du haut du vitrage depuis le sol ou le haut du muret [FposVerre]
     */
    public final int getPosVerre() {
        return FposVerre;
    }

    /**
     * @param i
     */
    public final void setPosVerre(int i) {
        /*
         if(i<MINPOSVERRE && getIsVerre()){
         System.out.println("Le vitrage devrait arriver au minimum à "+MINPOSVERRE+" du sol");
         }
         if(i>getHauteurFinie() && getIsVerre()){
         System.out.println("Le vitrage ne peut être plus haut que la hauteur finie du garde corps ("+getHauteurFinie()+")");
         return;
         }
         */
        FposVerre = i;
    }

    /**
     * @param s
     */
    public final void setPosVerre(String s) {
        if (s.isEmpty()) {
            s = "0";
        }
        setPosVerre((int) Double.parseDouble(s));
    }

    /**
     * @return position du bas du vitrage depuis le sol ou le haut du muret [FdebordVerreSol]
     */
    public final int getDebordVerreSol() {
        return FdebordVerreSol;
    }

    /**
     * @param i
     */
    public final void setDebordVerreSol(int i) {
        /*
         if(i>MAXVERRESOL && getIsVerre()){
         System.out.println("Le vitrage devrait être au maximum à "+MAXVERRESOL+" du sol");
         }
         if(i<0 && getIsVerre()){
         System.out.println("Le vitrage ne peut être dans le sol");
         return;
         }
         */
        FdebordVerreSol = i;
    }

    /**
     * @param s
     */
    public final void setDebordVerreSol(String s) {
        if (s.isEmpty()) {
            s = "0";
        }
        setDebordVerreSol((int) Double.parseDouble(s));
    }

    /**
     * @return
     */
    public final int getAxePinceHaut() {
        return FaxePinceHaut;
    }

    /**
     * @param i
     */
    public final void setAxePinceHaut(int i) {
        /* pinces qui se croisent
         if(i>){
         System.out.println("Le vitrage devrait être au maximum à "+MAXVERRESOL+" du sol");
         }
         */
        /*
         if(i<LARGPINCEVERRE && getIsVerre()){
         System.out.println("La pince du haut est en dehors du vitrage");
         }
         */
        FaxePinceHaut = i;
    }

    /**
     * @param s
     */
    public final void setAxePinceHaut(String s) {
        if (s.isEmpty()) {
            s = "0";
        }
        setAxePinceHaut((int) Double.parseDouble(s));
    }

    /**
     * @return true si le remplissage verre/tôle est complet (utilisera le débord entre le haut du verre/tôle et le bas de la main courante)
     */
    public final boolean getVerreIsComplet() {
        return FverreIscomplet;
    }

    /**
     * @param b
     */
    public final void setVerreIscomplet(boolean b) {
        /*
         if(!getIsVerre()){
         System.out.println("Le remplissage verre/tôle n'est pas activé, impossible de passer en mode complet");
         return;
         }
         */
        FverreIscomplet = b;
        /*
         if(b){
         setPosVerre(DEFAULTPOSVERRECOMPLET);
         }else{
         setPosVerre(DEFAULTPOSVERRE);
         }
         */
    }

    /**
     * @return
     */
    public final int getAxePinceBas() {
        return FaxePinceBas;
    }

    /**
     * @param i
     */
    public final void setAxePinceBas(int i) {
        /* pinces qui se croisent
         if(i>){
         System.out.println("Le vitrage devrait être au maximum à "+MAXVERRESOL+" du sol");
         }
         */
        /*
         if(i<LARGPINCEVERRE && getIsVerre()){
         System.out.println("La pince du bas est en dehors du vitrage");
         }
         */
        FaxePinceBas = i;
    }

    public final void setAxePinceBas(String s) {
        if (s.isEmpty()) {
            s = "0";
        }
        setAxePinceBas((int) Double.parseDouble(s));
    }

    /**
     * @return true si la fixation est par scellement [FisScellement]
     */
    public final boolean getIsScellement() {
        return FisScellement;
    }

    /**
     * @param b
     */
    public final void setIsScellement(boolean b) {
        FisScellement = b;
        //if(b) FisFacade=false;
    }

    /**
     * @return true si le remplissage est verre ou tôle [FisVerre]
     */
    public final boolean getIsVerre() {
        return FisVerre;
    }

    /**
     * @param b
     */
    public final void setIsVerre(boolean b) {
        FisVerre = b;
        /*
         if(!b){
         setPosVerre(0);
         }else{
         setPosVerre(DEFAULTPOSVERRE);
         }
         setIsBal(false);
         */
    }

    /**
     * @return true si le remplissage est balustre [FisBal]
     */
    public final boolean getIsBal() {
        return FisBal;
    }

    /**
     * @param b
     */
    public final void setIsBal(boolean b) {
        FisBal = b;
        //FisVerre=false;
    }

    /**
     * @return espacement maximum entre les balustres [FespacementBalMax]
     */
    public final int getEspacementBalMax() {
        return FespacementBalMax;
    }

    /**
     * @param d
     */
    public final void setEspacementBalMax(int d) {
        /*
         if(d<0 && getIsBal()){
         System.out.println("Les balustes ne peuvent pas se croiser");
         return;
         }
         if(d>MAXESPBAL && getIsBal()){
         System.out.println("L'espacement des balustres devrait ête au maximum de "+MAXESPBAL);
         }
         */
        FespacementBalMax = d;
    }

    /**
     * @param s
     */
    public final void setEspacementBalMax(String s) {
        if (s.isEmpty()) {
            s = "0";
        }
        setEspacementBalMax((int) Double.parseDouble(s));
    }

    /**
     * @return hauteur des balustres [FhauteurBal]
     */
    public final int getHauteurBal() {
        return FhauteurBal;
    }

    public final void setHauteurBal(int i) {
        FhauteurBal = i;
    }

    public final void setHauteurBal(String s) {
        if (s.isEmpty()) {
            s = "0";
        }
        setHauteurBal((int) Double.parseDouble(s));
    }

    /**
     * @return true si la fixation est en façade [FisFacade]
     */
    public final boolean getIsFacade() {
        return FisFacade;
    }

    /**
     * @param b
     */
    public final void setIsFacade(boolean b) {
        FisFacade = b;
        //if(b) FisScellement=false;
    }

    /**
     * @return nombre de traverse(s) [FnbrTraverses]
     */
    public final int getNbrTraverses() {
        return FnbrTraverses;
    }

    /**
     * @param i
     */
    public final void setNbrTraverses(int i) {
        if (i < 0) {
            i = 0;
        }
        FnbrTraverses = i;
    }

    /**
     * @param s
     */
    public final void setNbrTraverses(String s) {
        if (s.isEmpty()) {
            s = "0";
        }
        setNbrTraverses((int) Double.parseDouble(s));
    }

    /**
     * @return distance entre le sol ou le haut du muret et l'axe de la traverse du bas [FdebordTravBas]
     */
    public final int getDebordTravBas() {
        return FdebordTravBas;
    }

    /**
     * @param i
     */
    public final void setDebordTravBas(int i) {
        /*
         if(i<MINTRAVSOL){
         System.out.println("L'espacement au sol des traverses doit être supérieur à "+MINTRAVSOL);
         return;
         }
         if(i>MAXESPTRAV+FHTRAV){
         System.out.println("L'espacement des traverses doit être au maximum de "+MAXESPTRAV+FHTRAV);
         }
         */
        FdebordTravBas = i;
    }

    /**
     * @param s
     */
    public final void setDebordTravBas(String s) {
        if (s.isEmpty()) {
            s = "0";
        }
        setDebordTravBas((int) Double.parseDouble(s));
    }

    /**
     * @return espacement des traverses serrées [FespTravSerre]
     */
    public final double getEspTravSerre() {
        return FespTravSerre;
    }

    /**
     * @param i
     */
    public final void setEspTravSerre(double i) {
        FespTravSerre = i;
    }

    /**
     * @param s
     */
    public final void setEspTravSerre(String s) {
        if (s.isEmpty()) {
            s = "0";
        }
        setEspTravSerre(Double.parseDouble(s));
    }

    /**
     * @return nombre de traverses serrées (sur le nombre de traverse totale) [FnbrTravSerre] à partir du bas
     */
    public final double getNbrTravSerre() {
        return FnbrTravSerre;
    }

    /**
     * @param i
     */
    public final void setNbrTravSerre(double i) {
        FnbrTravSerre = i;
    }

    /**
     * @param s
     */
    public final void setNbrTravSerre(String s) {
        if (s.isEmpty()) {
            s = "0";
        }
        setNbrTravSerre(Double.parseDouble(s));
    }

    /**
     * @return position à l'axe de la traverse haute supportant les balustres [FposTravHaute]
     */
    public final double getPosTravHaute() {
        return FposTravHaute;
    }

    /**
     * @param i
     */
    public final void setPosTravHaute(double i) {
        FposTravHaute = i;
    }

    /**
     * @param s
     */
    public final void setPosTravHaute(String s) {
        if (s.isEmpty()) {
            s = "0";
        }
        setPosTravHaute(Double.parseDouble(s));
    }

    /**
     * @return position à l'axe de la traverse basse supportant les balustres [FposTravBasse]
     */
    public final double getPosTravBasse() {
        return FposTravBasse;
    }

    /**
     * @param i
     */
    public final void setPosTravBasse(double i) {
        FposTravBasse = i;
    }

    /**
     * @param s
     */
    public final void setPosTravBasse(String s) {
        if (s.isEmpty()) {
            s = "0";
        }
        setPosTravBasse(Double.parseDouble(s));
    }

    /**
     * @return décalage entre le bas du tube dans la platine de façade et l'axe de fixation de la platine de façade [FfacDecalHauteurPlatine]
     */
    public final int getFacDecalHauteurPlatine() {
        return FfacDecalHauteurPlatine;
    }

    /**
     * @return décalage entre le bas du tube dans la platine de façade déportée et l'axe de fixation de la platine de façade déportée [FfacDeportDecalHauteurPlatine]
     */
    public final int getFacDeportDecalHauteurPlatine() {
        return FfacDeportDecalHauteurPlatine;
    }

    public final boolean getTraversesApplique() {
        return FtraversesApplique;
    }

    public final void setTraversesApplique(boolean b) {
        FtraversesApplique = b;
        //if(b) FtraversesAxe=false;
    }

    /**
     * @return true si les traverses sont dans l'axe des potelets [FtraversesAxe]
     */
    public final boolean getTraversesAxe() {
        return FtraversesAxe;
    }

    /**
     * @param b
     */
    public final void setTraversesAxe(boolean b) {
        FtraversesAxe = b;
        //if(b) FtraversesApplique=false;
    }

    /**
     * @return true si les traverses sont en câble [FtraversesCable]
     */
    public final boolean getTraversesCable() {
        return FtraversesCable;
    }

    /**
     * @param b
     */
    public final void setTraversesCable(boolean b) {
        FtraversesCable = b;
        //if(b) FtraversesAxe=true;
        //setTraversesProfile(false);
    }

    /**
     * @return épaisseur de la dalle [Fepdalle]
     */
    public final int getEpdalle() {
        return Fepdalle;
    }

    /**
     * @param i
     */
    public final void setEpdalle(int i) {
        Fepdalle = i;
    }

    /**
     * @param s
     */
    public final void setEpdalle(String s) {
        if (s.isEmpty()) {
            s = "0";
        }
        setEpdalle((int) Double.parseDouble(s));
    }

    /**
     * @return à partie de combien on passe en platine de façade déportée
     */
    public final double getSwitchDeportFac() {
        return FswitchDeportFac;
    }

    /**
     * @return distance entre le nez de dalle et l'axe des potelets
     * si FdecalZ est >0 alors on à le FfacDeportDecalZ
     * si FdecalZ est <=0 alors on à le FfacDecalZ
     */
    public final double getPlatFacDecalZ() {
        if (getDecalZ() > FswitchDeportFac) {
            return FfacDeportDecalZ;
        } else {
            return FfacDecalZ;
        }
    }

    /**
     * @return distance entre le nez de dalle et l'axe des potelets [FdecalZ]
     */
    public final double getTravDecalZ() {
        return FtravDecalZ;
    }

    /**
     * @return distance entre le nez de dalle et l'axe des potelets [FdecalZ]
     */
    public final int getDecalZ() {
        return FdecalZ;
    }

    /**
     * @param i
     */
    public final void setDecalZ(int i) {
        FdecalZ = i;
    }

    /**
     * @param s
     */
    public final void setDecalZ(String s) {
        if (s.isEmpty()) {
            s = "0";
        }
        setDecalZ((int) Double.parseDouble(s));
    }

    /**
     * @return
     */
    public final double getFacAxeVertPlatine() {
        return FfacAxeVertPlatine;
    }

    /**
     * @param d
     */
    public final void setFacAxeVertPlatine(double d) {
        FfacAxeVertPlatine = d;
    }

    /**
     * @param s
     */
    public final void setFacAxeVertPlatine(String s) {
        if (s.isEmpty()) {
            s = "0";
        }
        setFacAxeVertPlatine(Double.parseDouble(s));
    }

    /**
     * @return inverse le potelet de gauche avec un balustre [FinversePotBalGauche]
     */
    public final boolean getInversePotBalGauche() {
        return FinversePotBalGauche;
    }

    /**
     * @param b
     */
    public final void setInversePotBalGauche(boolean b) {
        FinversePotBalGauche = b;
    }

    /**
     * @return inverse le potelet de droite avec un balustre [FinversePotBalDroite]
     */
    public final boolean getInversePotBalDroite() {
        return FinversePotBalDroite;
    }

    /**
     * @param b
     */
    public final void setInversePotBalDroite(boolean b) {
        FinversePotBalDroite = b;
    }

    /**
     * @return le nombre de potelet calculé
     * @deprecated A remplacer par getAllPotNbr (nombre total de potelet) ou getActifPotNbr (nombre de potelet 'actif')
     */
    @Deprecated
    public final double getCalculatedNbrPot() {
        return FcalculatedNbrPot;
    }

    @Deprecated
    protected final void setCalculatedNbrPot(double d) {
        FcalculatedNbrPot = d;
    }

    /**
     * @return nombre total de potelet
     */
    public final double getAllPotNbr() {
        return Fpotelets.size();
    }

    /**
     * @return nombre de potelet 'actif'
     */
    public final double getActifPotNbr() {
        int a = 0;
        return Fpotelets.stream().filter((p) -> (p.getActif())).map((_item) -> 1).reduce(a, Integer::sum);
    }

    /**
     * @return la hauteur (y) calculée des traverses
     */
    public final double getCalculatedHauteurTraverses() {
        return FcalculatedHauteurTraverses;
    }

    protected final void setCalculatedHauteurTraverses(double d) {
        FcalculatedHauteurTraverses = d;
    }

    /**
     * @return l'entraxe calculé entre les traverses
     */
    public final double getCalculatedEntraxeTraverse() {
        return FcalculatedEntraxeTraverse;
    }

    protected final void setCalculatedEntraxeTraverse(double d) {
        FcalculatedEntraxeTraverse = d;
    }

    /**
     * @return l'entraxe calculé entre les traverses serré
     * @deprecated est l'entraxe donné par FentraxeTravSerre
     */
    @Deprecated
    public final double getCalculatedEntraxeTraverseSerre() {
        return FcalculatedEntraxeTraverseSerre;
    }

    protected final void setCalculatedEntraxeTraverseSerre(double d) {
        FcalculatedEntraxeTraverseSerre = d;
    }

    /**
     * @return entraxe calculé de la traverse du bas
     */
    public final double getCalculatedEntraxeTraverseBas() {
        return FcalculatedEntraxeTraverseBas;
    }

    protected final void setCalculatedEntraxeTraverseBas(double d) {
        FcalculatedEntraxeTraverseBas = d;
    }

    /**
     * @return hauteur entre le sol ou le haut du muret et le dessous de la MC
     */
    public final double getCalculatedHauteurSousMC() {
        return FcalculatedHauteurSousMC;
    }

    protected final void setCalculatedHauteurSousMC(double d) {
        FcalculatedHauteurSousMC = d;
    }

    /**
     * @return
     */
    public final double getCalculatedPotPosZ() {
        return FcalculatedPotPosZ;
    }

    protected final void setCalculatedPotPosZ(double d) {
        FcalculatedPotPosZ = d;
    }

    /**
     * @return
     */
    public final double getCalculatedAncragePosZ() {
        return FcalculatedAncragePosZ;
    }

    protected final void setCalculatedAncragePosZ(double d) {
        FcalculatedAncragePosZ = d;
    }

    /**
     * @return nombre de balustre entre 2 potelets
     */
    public final double getCalculatedNbrBal() {
        return FcalculatedNbrBal;
    }

    protected final void setCalculatedNbrBal(double d) {
        FcalculatedNbrBal = d;
    }

    /**
     * @return nombre total de balustre
     */
    public final double getCalculatedTotBal() {
        return FcalculatedTotBal;
    }

    protected final void setCalculatedTotBal(double d) {
        FcalculatedTotBal = d;
    }

    /**
     * @return entraxe réel entre 2 balustres
     */
    public final double getCalculatedEntraxeBalReel() {
        return FcalculatedEntraxeBalReel;
    }

    protected final void setCalculatedEntraxeBalReel(double d) {
        FcalculatedEntraxeBalReel = d;
    }

    /**
     * @return espacement réel entre 2 balustres
     */
    public final double getCalculatedEspBalReel() {
        return FcalculatedEspBalReel;
    }

    protected final void setCalculatedEspBalReel(double d) {
        FcalculatedEspBalReel = d;
    }

    /**
     * @return
     */
    public final double getCalculatedDebBasBal() {
        return FcalculatedDebBasBal;
    }

    protected final void setCalculatedDebBasBal(double d) {
        FcalculatedDebBasBal = d;
    }
    // ---------------------------------------------------------------------
    //--------------------------------------------------------------------------
    //--------------------------------------------------------------------------
    //--------------------------------------------------------------------------

    /**
     * pas final car overridden dans TconfigurationXXXX
     *
     * @throws java.lang.IllegalAccessException
     */
    public void print() throws IllegalArgumentException, IllegalAccessException {
        System.out.println("POTELETS:");
        System.out.println("Nombre de potelet: " + Fpotelets.size());
        Fpotelets.stream().forEach((p) -> {
            System.out.println(p.toString());
        });
        System.out.println("Position Z potelet: " + FcalculatedPotPosZ);
        System.out.println("Position ancrage Z: " + FcalculatedAncragePosZ);
        System.out.println("TRAVERSES:");
        System.out.println("(obsolète)Hauteur traverse: " + FcalculatedHauteurTraverses);
        System.out.println("(obsolète)Entraxe traverse: " + FcalculatedEntraxeTraverse);
        System.out.println("(obsolète)Entraxe traverse basse: " + FcalculatedEntraxeTraverseBas);
        System.out.println("MC:");
        System.out.println("Hauteur sous MC: " + FcalculatedHauteurSousMC);
        System.out.println("BALUSTRES:");
        System.out.println("Nombre de balustre entre potelet: " + FcalculatedNbrBal);
        System.out.println("Nombre total de balustre: " + FcalculatedTotBal);
        System.out.println("Entraxe balustre réel: " + FcalculatedEntraxeBalReel);
        System.out.println("Espacement balustre réel: " + FcalculatedEspBalReel);
        System.out.println("Débord bas balustre: " + FcalculatedDebBasBal);
    }

    //
    private void loadGamme() throws IOException, ParserConfigurationException, SAXException {
        String f = getGamme().getNom() + ".txt";
        this.loadFromFile(f);
        System.out.println("Gamme " + f + " chargée.");
    }

    /**
     * @throws javax.xml.parsers.ParserConfigurationException
     * @throws org.xml.sax.SAXException
     * @throws java.io.IOException
     */
    public final void loadNomenclature3D() throws ParserConfigurationException, SAXException, IOException {
        File f = new File(getGamme().getNom() + ".3d");
        if (!f.canRead()) {
            System.out.println("(i) Fichier de la nomenclature 3D '" + f.getPath() + "' introuvable.");
        } else {
            this.nomenclature3d = new Tnomenclature3D(f.getPath());
            System.out.println("(i) Nomenclature 3D " + f.getPath() + " chargée.");
        }
    }

    /**
     * @throws javax.xml.parsers.ParserConfigurationException
     * @throws org.xml.sax.SAXException
     * @throws java.io.IOException
     */
    public final void loadNomenclature() throws ParserConfigurationException, SAXException, IOException {
        File f = new File(getGamme().getNom() + ".nom");
        if (!f.canRead()) {
            System.out.println("(i) Fichier de la nomenclature '" + f.getPath() + "' introuvable.");
        } else {
            this.nomenclature = new Tnomenclature(f.getPath());
            System.out.println("(i) Nomenclature " + f.getPath() + " chargée.");
        }
    }

    /**
     *
     */
    @Override
    public void doAfterLoad() {
        //calcul();
    }

    /**
     *
     */
    @Override
    public void doBeforeSave() {
        //calcul();
    }

    /**
     * @return le code de l'ancrage (SCELL,FAC DEPORT,FAC,SOL)
     * @deprecated utiliser un type enum
     */
    @Deprecated
    public final String getCodeAncrage() {
        if (getIsScellement()) {
            return "SCELL";
        }
        if (getIsFacade()) {
            if (getDecalZ() > 0) {
                return "FAC DEPORT";
            }
            return "FAC";
        }
        return "SOL";
    }

    /**
     * @return le code d'option (SANS MC)
     */
    public final String getCodeOption() {
        if (!getMainCourante()) {
            return "SANS MC";
        }
        return "";
    }

    /**
     * @return le code de traverse: xT,xC,xX où x est le nombre et T pour tube en applique, C pour câble et X pour tube dans l'axe
     */
    public final String getCodeTraverse() {
        if (getNbrTraverses() <= 0) {
            return "";
        }
        String t = "T";
        if (getTraversesCable()) {
            t = "C";
        }
        if (getTraversesAxe() && !getTraversesCable()) {
            t += " X";
        }
        return getNbrTraverses() + t;
    }

    /**
     * @return le code de remplissage: V pour Verre
     */
    public final String getCodeRemplissage() {
        if (getIsVerre()) {
            return "V";
        }
        //if(getIsTole()) return "T";
        return "";
    }

    /**
     * @return le code du potelet: POTELET + code de la gamme + code ancrage + code option + code traverse + code remplissage
     */
    public final String getCodePotelet() {
        String p = "POTELET";
        //code gamme
        p += " " + getGamme().getCode();
        //ancrage
        p += " " + getCodeAncrage();
        //option
        p += " " + getCodeOption();
        //traverses
        p += " " + getCodeTraverse();
        //remplissage
        p += " " + getCodeRemplissage();
        //
        return p;
    }

    /**
     * Compte le nombre de potelet selon leur type (G,D,M,AG,AD)
     */
    public final void comptePotelets() {
        int pM = 0;
        int pG = 0;
        int pD = 0;
        int pAG = 0;
        int pAD = 0;
        for (Tpotelet Fitem : Fpotelets) {
            switch (Fitem.getType()) {
                case "M":
                    pM++;
                    break;
                case "G":
                    pG++;
                    break;
                case "D":
                    pD++;
                    break;
                case "AG":
                    pAG++;
                    break;
                case "AD":
                    pAD++;
                    break;
            }
        }
        System.out.println(pG + " potelet de début gauche");
        System.out.println(pAG + " potelet d'angle gauche");
        System.out.println(pM + " potelet intermédiaires");
        System.out.println(pAD + " potelet d'angle droite");
        System.out.println(pD + " potelet de début droite");
    }

    /**
     *
     */
    public abstract void calculPercages();

    /**
     * @param aFilename
     * @return
     * @throws IOException
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     */
    @Override
    public Object saveToFile(String aFilename) throws IOException, IllegalArgumentException, IllegalAccessException {
        String d = TfileUtils.extractFilePath(aFilename, "\\");
        for (Tpotelet pot : getPotelets()) {
            String f = d + "/" + getNom() + " " + pot.getCode() + " " + pot.getType() + ".txt";
            pot.toFile(f);
        }
        return super.saveToFile(aFilename);
    }

    /**
     * @return la référence du profilé potelet
     */
    public final String getPotRef() {
        return FpoteletRef;
    }

    /**
     * @return la référence du profilé cache rainure
     */
    public final String getCacheRef() {
        return FcacheRef;
    }

    /**
     * @return la référence du profilé MC
     */
    public final String getMCRef() {
        return FmcRef;
    }

    /**
     * @return la référence du profilé traverse
     */
    public final String getTravRef() {
        return FtraverseRef;
    }

    /**
     * @return la référence du profilé balustre
     */
    public final String getBalRef() {
        return FbalRef;
    }

    /**
     * @return
     */
    public final Tnomenclature3D getNomenclature3D() {
        return this.nomenclature3d;
    }

    /**
     * @return
     */
    public final Tnomenclature getNomenclature() {
        return this.nomenclature;
    }
}
