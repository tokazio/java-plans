/*
 * Copyright (C) 2014 rpetit
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package evrLib.gc;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * @author rpetit
 */
public class TgescomFile {

    private ArrayList<TgescomRef> refs;


    public TgescomFile(String aFileName) throws IOException {
        ini();
        loadFromFile(aFileName);
    }

    public TgescomFile() {
        ini();
    }

    private void ini() {
        refs = new ArrayList();
    }

    /**
     * Ouvre la liste depuis le fichier 'filename'
     *
     * @param filename Nom du fichier
     * @throws FileNotFoundException Fichier inexistant
     * @throws IOException           Erreur de lecture du fichier
     */
    public final void loadFromFile(String filename) throws FileNotFoundException, IOException {
        FileReader fr = new FileReader(filename);
        load(new BufferedReader(fr));
    }

    //
    private void load(BufferedReader br) throws IOException {
        String ligne = br.readLine();
        while (ligne != null) {
            TgescomRef r = new TgescomRef();
            //ref
            //18
            r.setRef(ligne.substring(0, 17));
            //design
            //69
            r.setDesignation(ligne.substring(18, 86));
            //prix vente
            //14
            r.setPrixVente(ligne.substring(87, 100));
            //famille
            //10
            r.setFamille(ligne.substring(101));
            refs.add(r);
            ligne = br.readLine();
        }
        br.close();
    }

    /**
     *
     */
    public void print() {
        for (TgescomRef r : refs) {
            System.out.println(r.toString());
        }
    }

    /**
     * @param aRef
     * @return
     */
    public TgescomRef getFromRef(String aRef) {
        for (TgescomRef r : refs) {
            if (r.getRef().equals(aRef)) {
                return r;
            }
        }
        return null;
    }
}
