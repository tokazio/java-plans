/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package evrLib.gc;

import esyoLib.XML.TxmlObject;
import evrLib.binpack.Tbinpacker;
import evrLib.binpack.Tmorceau;

import java.util.ArrayList;

/**
 * @author rpetit
 */
public class Tbarre extends TxmlObject {
    //L'objet Tbinpacker auquelle appartient la barre
    private Tbinpacker Fparent;
    //Longueur de la barre en mm
    private double Flongueur;
    //Liste des morceaux
    private ArrayList<Tmorceau> Fmorceaux;
    //Longueur de la chute en mm
    private double Fchute;
    //
    private int codeProfil = 0;

    /**
     * Constructeur
     *
     * @param parent   L'objet Tbinpacker auquelle appartient la barre
     * @param longueur Longueur de la barre en mm
     */
    public Tbarre(Tbinpacker parent, double longueur) {
        this.Fparent = parent;
        this.Fmorceaux = new ArrayList();
        this.Flongueur = longueur;
    }

    //Additionne toutes les longueurs des morceaux pour les retirer à celle de la barre et obtenir la longueur de la chute
    private void calculChute() {
        double ltot = 0;
        ltot = this.Fmorceaux.stream().map((Fmorceaux1) -> Fmorceaux1.getLongueur() + this.Fparent.SCIE).reduce(ltot, (accumulator, _item) -> accumulator + _item);
        this.Fchute = this.Flongueur - ltot;
    }

    /**
     * @return Longueur de la barre en mm
     */
    public final double getLongueur() {
        return this.Flongueur;
    }

    /**
     * @return La liste des morceaux dans la barre
     */
    public final ArrayList<Tmorceau> getMorceaux() {
        return this.Fmorceaux;
    }

    /**
     * @return Longueur de la chute en mm
     */
    public final double getChute() {
        return this.Fchute;
    }

    /**
     * Ajoute un morceau à la liste
     *
     * @param morceau Un morceau
     */
    public final void add(Tmorceau morceau) {
        if (morceau.getLongueur() == 0) return;
        this.Fmorceaux.add(morceau);
        calculChute();
    }

    public int getCodeProfil() {
        return this.codeProfil;
    }

    @Override
    public void onLoaded() {

    }

}