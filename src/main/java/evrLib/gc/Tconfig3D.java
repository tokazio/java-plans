/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evrLib.gc;

import esyoLib.STL.STLaxis;
import esyoLib.jfx3d.*;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import org.w3c.dom.Element;

/**
 * Crée la vue 3D (avec une config et une liste de pièces mises en caches)
 * La config doit absolument contenit une nomenclature3D (même vide)
 *
 * @author rpetit
 */
public class Tconfig3D {

    //Texture métal
    private static PhongMaterial metal = new PhongMaterial();
    //Texture béton
    private static PhongMaterial beton = new PhongMaterial();

    static {
        Tconfig3D.metal.setDiffuseMap(new Image("file:///chrome.jpg"));
    }

    static {
        Tconfig3D.beton.setDiffuseColor(Color.DARKGRAY);
        Tconfig3D.beton.setSpecularColor(Color.DARKGRAY);
    }

    //forme 3D qui englobe tout (world)
    private T3dForme w = new T3dForme();
    //coordonnées
    private double x, y, z;
    //Configuration
    private Tconfiguration config;
    //liste de pièces mises en caches
    private TstlParts PARTS;

    /**
     * @param aConfig une configuration
     * @param parts   une liste de pièces mises en caches
     * @throws java.lang.Exception Erreur ?
     */
    public Tconfig3D(TconfigurationPlat aConfig, TstlParts parts) throws Exception {
        this.PARTS = parts;
        if (aConfig == null) {
            throw new Exception("La configuration pour la vue 3D est null!");
        }
        this.config = aConfig;
        if (aConfig.getNomenclature3D() == null) {
            throw new Exception("La nomenclature 3D de la configuration pour la vue 3D est null!");
        }
        build();
    }

    /**
     * Construit la vue 3D selon les infos de la nomenclature3D
     */
    private void build() throws Exception {
        //
        final double LARG = 300;
        //décalage en z
        z = getConfig().getDecalZ();
        if (getConfig().getIsFacade()) {
            z = -getConfig().getPlatFacDecalZ();
        }
        //création du sol
        T3dBox bx;
        //sol
        bx = new T3dBox(getConfig().getLongueur(), getConfig().getEpdalle(), LARG);
        bx.setTranslateY(-getConfig().getEpdalle() / 2);
        bx.setTranslateZ(LARG / 2);
        bx.setMaterial(beton);
        w.put(bx, 0, 0, 0, 0, 0, 0);

        //création du mur à gauche
        if (getConfig().getFixMurGauche()) {
            bx = new T3dBox(200, getConfig().getHauteurFinie() + 200, LARG);
            bx.setMaterial(beton);
            //w.put(bx, getConfig().getLongueur() / 2 + 100, (getConfig().getHauteurFinie() / 2) - 100, 500 - getConfig().getDecalZ()+c, 0, 0, 0);
        }
        //création du mur à droite
        if (getConfig().getFixMurDroite()) {
            bx = new T3dBox(200, getConfig().getHauteurFinie() + 200, LARG);
            bx.setMaterial(beton);
            //w.put(bx, -(getConfig().getLongueur() / 2 + 100), (getConfig().getHauteurFinie() / 2) - 100, 500 - c, 0, 0, 0);
        }
        //création du muret
        if (getConfig().getIsMuret()) {
            bx = new T3dBox(getConfig().getLongueur(), getConfig().getDecalSol() + 200, 200);
            bx.setMaterial(beton);
            //w.put(bx, 0, (getConfig().getDecalSol() / 2) - 100, 100 - c, 0, 0, 0);
        }
        //x de départ (à gauche)
        double x0 = (getConfig().getLongueur() / 2) - getConfig().getDebordPotGauche();
        x = x0;
        y = 0;
        double a, b;
        // ---------------------------
        //potelets
        // ---------------------------
        if (!getConfig().getSansPotelet()) {

            // épaisseur sabot (si sabot)
            if (getConfig().getIsFacade()) {
                a = 0;
            } else {
                a = getConfig().FHSABOT;
            }
            // hauteur scellement si scellement
            if (getConfig().getIsScellement()) {
                a = -getConfig().getHauteurScellement();
            }
            // hauteur/profondeur platine facade si facade
            if (getConfig().getIsFacade()) {
                a = -(getConfig().getFacDecalHauteurPlatine() + getConfig().getFacAxeVertPlatine());
            }
            //hauteur sol fini si muret
            if (getConfig().getIsMuret()) {
                a = getConfig().getDecalSol() + a;
            }

            double h;
            for (int i = 0; i < getConfig().getPotelets().size(); i++) {
                if (getConfig().getPotelets().get(i).getActif()) {
                    h = getConfig().getPotelets().get(i).getLongueur();
                    //tube potelet
                    switch (getConfig().getGamme()) {
                        case baludesign:
                            w.put(new T3dCylinder((getConfig().FLARGPOT / 2), h), x, (h / 2) + a, z, 0, 0, 0);
                            break;
                        case chrome:
                            w.put(new T3dExtrude(new T3dMesh("STL/chrome/INPOTH40EP2.STL", -20, 0, 20, 0, 0, 0), STLaxis.Y_AXIS, h), x, h / 2 + getConfig().FHSABOT + a, z, 0, 0, 0);
                            //cache rainure
                            /*
                             try {
                             w.put(new T3dExtrude(new T3dMesh("STL/chrome/INCACHERAIN.STL", 0, 0, 0, 0, 0, 0), STLaxis.Y_AXIS, h-100), x, (h-100) / 2 + getConfig().FHSABOT, 0, 0, 0, 0);
                             } catch (Exception ex) {
                             Logger.getLogger(Tvue3D.class.getName()).log(Level.SEVERE, null, ex);
                             }
                             */
                            break;
                        case monoprofil:
                            w.put(new T3dExtrude(new T3dMesh("STL/Monoprofil/biprofil.STL", -45.5, 0, 26.5, 0, 0, 0), STLaxis.Y_AXIS, h), x, h / 2 + getConfig().FHSABOT, z, 0, 0, 0);
                            break;
                    }

                    //--------------------------------------------------------
                    //sabot sol/façade/scellement
                    //--------------------------------------------------------
                    String s = "sol";
                    if (getConfig().getIsFacade()) {
                        s = "facade";
                        if (getConfig().getDecalZ() > getConfig().getSwitchDeportFac()) {
                            s += "deport";
                        }
                    }
                    if (getConfig().getIsScellement()) {
                        s = "scellement";
                    }
                    if (getConfig().getIsFacade()) {
                        y = a;
                    } else {
                        y = 0;
                    }
                    Tnomenclature3D.execute(getConfig().getNomenclature3D().get("//fixations/" + s + "/stl/*"), new TcallbackOnElement() {
                        @Override
                        public void execute(Element e) throws Exception {
                            w.put(PARTS.get(e.getNodeName()), x, y, z, 0, 0, 0);
                        }
                    });
                    //--------------------------------------------------------
                    //liaison mc
                    //--------------------------------------------------------
                    y = h + a;
                    if (getConfig().getMainCourante()) {
                        Tnomenclature3D.execute(getConfig().getNomenclature3D().get("//potelets/mc/stl/*"), new TcallbackOnElement() {
                            @Override
                            public void execute(Element e) throws Exception {
                                w.put(PARTS.get(e.getNodeName()), x, y, z, 0, 0, 0);
                            }
                        });
                    }
                    //--------------------------------------------------------
                    //liaison trav
                    //--------------------------------------------------------
                    if (!getConfig().getVerreIsComplet() && !getConfig().getIsBal()) {
                        s = "milieu";
                        if (i == 0) {
                            s = "debut";
                        }
                        if (i == getConfig().getPotelets().size() - 1) {
                            s = "fin";
                        }

                        String t = "tube";
                        if (getConfig().getTraversesCable()) {
                            t = "cable";
                        }
                        String u = "app";
                        if (getConfig().getTraversesAxe()) {
                            u = "axe/" + s;
                        }
                        for (int j = 0; j < getConfig().getTraverses().size(); j++) {
                            y = ((Ttraverse) getConfig().getTraverses().get(j)).getPosition();
                            Tnomenclature3D.execute(getConfig().getNomenclature3D().get("//potelets/traverses/" + t + "/" + u + "/stl/*"), new TcallbackOnElement() {
                                @Override
                                public void execute(Element e) throws Exception {
                                    w.put(PARTS.get(e.getNodeName()), x, y, z, 0, 0, 0);
                                }
                            });
                        }
                    }
                    //--------------------------------------------------------
                    //liaison verre/tole
                    //--------------------------------------------------------
                    if (getConfig().getIsVerre()) {
                        s = "milieu";
                        if (i == 0) {
                            s = "debut";
                        }
                        if (i == getConfig().getPotelets().size() - 1) {
                            s = "fin";
                        }
                        y = getConfig().getCalculatedDebordVerreSol() + getConfig().getAxePinceBas() + getConfig().getCalculatedEntraxePinces();
                        Tnomenclature3D.execute(getConfig().getNomenclature3D().get("//potelets/tole/" + s + "/stl/*"), new TcallbackOnElement() {
                            @Override
                            public void execute(Element e) throws Exception {
                                w.put(PARTS.get(e.getNodeName()), x, y, z, 0, 0, 0);
                            }
                        });
                        y = getConfig().getCalculatedDebordVerreSol() + getConfig().getAxePinceBas();
                        Tnomenclature3D.execute(getConfig().getNomenclature3D().get("//potelets/tole/" + s + "/stl/*"), new TcallbackOnElement() {
                            @Override
                            public void execute(Element e) throws Exception {
                                w.put(PARTS.get(e.getNodeName()), x, y, z, 0, 0, 0);
                            }
                        });
                    }
                }
                x = x - getConfig().getCalculatedEntraxePotReel();
            }
        }
        // ---------------------------
        //mc
        // ---------------------------
        if (getConfig().getMainCourante()) {
            switch (getConfig().getGamme()) {
                case baludesign:
                case chrome:
                    T3dCylinder cyl = new T3dCylinder(getConfig().FHMC / 2, getConfig().getLongueur());
                    cyl.setMaterial(metal);
                    w.put(cyl, 0, getConfig().getCalculatedHauteurFinie() - (getConfig().FHMC / 2), z, 0, 0, 90);
                    break;
                case monoprofil:
                    w.put(new T3dExtrude(new T3dMesh("STL/Monoprofil/monoprofil.STL", -45.5, 0, 13.5, 0, 90, 90), STLaxis.Y_AXIS, getConfig().getLongueur()), 0, getConfig().getCalculatedHauteurFinie() - getConfig().FHMC / 2, z, 0, 0, 0);
                    break;
            }
        }
        //angles mc droite
        if (getConfig().getAngleDroite() && getConfig().getMainCourante()) {
            double x1 = -(getConfig().getLongueur() / 2 - z);
            double y1 = getConfig().getCalculatedHauteurFinie() - getConfig().FHMC / 2;
            double z1 = z;
            Tnomenclature3D.execute(getConfig().getNomenclature3D().get("//angle/mc/droite/stl/*"), new TcallbackOnElement() {
                @Override
                public void execute(Element e) throws Exception {
                    w.put(PARTS.get(e.getNodeName()), x1, y1, z1, 0, 180, 0);
                }
            });
            Tnomenclature3D.execute(getConfig().getNomenclature3D().get("//angle/mc/milieu/stl/*"), new TcallbackOnElement() {
                @Override
                public void execute(Element e) throws Exception {
                    w.put(PARTS.get(e.getNodeName()), x1, y1, z1, 0, 180, 0);
                }
            });
        }
        //angles mc gauche
        if (getConfig().getAngleGauche() && getConfig().getMainCourante()) {
            double x1 = getConfig().getLongueur() / 2 - z;
            double y1 = getConfig().getCalculatedHauteurFinie() - getConfig().FHMC / 2;
            double z1 = z;
            Tnomenclature3D.execute(getConfig().getNomenclature3D().get("//angle/mc/gauche/stl/*"), new TcallbackOnElement() {
                @Override
                public void execute(Element e) throws Exception {
                    w.put(PARTS.get(e.getNodeName()), x1, y1, z1, 0, 0, 0);
                }
            });
            Tnomenclature3D.execute(getConfig().getNomenclature3D().get("//angle/mc/milieu/stl/*"), new TcallbackOnElement() {
                @Override
                public void execute(Element e) throws Exception {
                    w.put(PARTS.get(e.getNodeName()), x1, y1, z1, 0, 0, 0);
                }
            });
        }

        // ---------------------------
        // VERRE/TOLE
        // ---------------------------
        x = x0;
        if (getConfig().getIsVerre()) {
            double l = getConfig().getCalculatedEntraxePotReel() - getConfig().FLARGPOT;
            l -= getConfig().LARGPINCEVERRE * 2;
            double y = getConfig().getCalculatedPosVerre() - (getConfig().getCalculatedHauteurVerre() / 2);
            x -= l / 2 + getConfig().FLARGPOT / 2 + getConfig().LARGPINCEVERRE;
            for (int i = 0; i < getConfig().getPotelets().size() - 1; i++) {
                T3dBox box = new T3dBox(l, getConfig().getCalculatedHauteurVerre(), 8.8);
                w.put(box, x, y, z, 0, 0, 0);
                PhongMaterial verre = new PhongMaterial();
                verre.setDiffuseColor(Color.AQUA);
                verre.setSpecularColor(Color.WHITE);
                box.setMaterial(verre);
                //box.setDrawMode(DrawMode.LINE);
                x -= getConfig().getCalculatedEntraxePotReel();
            }
        }
        // ---------------------------
        //Traverses balustres
        // ---------------------------
        if (getConfig().getIsBal()) {
            switch (getConfig().getGamme()) {
                case baludesign:
                case chrome:
                    T3dCylinder cyl = new T3dCylinder(getConfig().FHTRAV / 2, getConfig().getLongueur());
                    cyl.setMaterial(metal);
                    w.put(cyl, 0, getConfig().getPosTravBasse() - (getConfig().FHTRAV / 2), z, 0, 0, 90);
                    w.put(cyl, 0, getConfig().getPosTravHaute() - (getConfig().FHTRAV / 2), z, 0, 0, 90);
                    break;
                case monoprofil:
                    w.put(new T3dExtrude(new T3dMesh("STL/Monoprofil/monoprofil.STL", -45.5, 0, 13.5, 0, 90, 90), STLaxis.Y_AXIS, getConfig().getLongueur()), 0, getConfig().getPosTravBasse(), z - getConfig().FEPTRAV / 2 + getConfig().FEPPOT / 2, 90, 0, 0);
                    w.put(new T3dExtrude(new T3dMesh("STL/Monoprofil/monoprofil.STL", -45.5, 0, 13.5, 0, 90, 90), STLaxis.Y_AXIS, getConfig().getLongueur()), 0, getConfig().getPosTravHaute(), z - getConfig().FEPTRAV / 2 + getConfig().FEPPOT / 2, 90, 0, 0);
                    break;
            }
        }
        // ---------------------------
        // Balustres
        // ---------------------------
        if (getConfig().getIsBal()) {
            x = x0 - getConfig().getCalculatedEntraxeBalReel();
            for (int j = 0; j < getConfig().getPotelets().size() - 1; j++) {
                for (int i = 0; i < getConfig().getCalculatedNbrBal(); i++) {
                    switch (getConfig().getGamme()) {
                        case baludesign:
                        case chrome:
                            //?
                            break;
                        case monoprofil:
                            w.put(new T3dExtrude(new T3dMesh("STL/Monoprofil/monoprofil.STL", -45.5, 0, 13.5, 0, 90, 90), STLaxis.Y_AXIS, getConfig().getHauteurBal()), x, getConfig().getHauteurBal() / 2 + getConfig().getCalculatedDebBasBal(), z, -90, 0, 90);
                            break;
                    }
                    x -= getConfig().getCalculatedEntraxeBalReel();
                }
                x -= getConfig().getCalculatedEntraxeBalReel();
            }

        }

        // ---------------------------
        // Traverses (hors balustre)
        // ---------------------------
        if (!getConfig().getIsBal() && !getConfig().getVerreIsComplet()) {
            //tube
            a = getConfig().FHTRAV;
            //cable
            if (getConfig().getTraversesCable()) {
                a = getConfig().FHCABLE;
            }

            double z1;
            //dans l'axe
            if (getConfig().getTraversesAxe()) {
                x = getConfig().getLongueur() / 2 - (getConfig().getDebordPotGauche() + getConfig().getCalculatedEntraxePotReel() / 2);
                z1 = z;
                for (int j = 0; j < getConfig().getPotelets().size() - 1; j++) {
                    for (int i = 0; i < getConfig().getTraverses().size(); i++) {
                        //tube
                        w.put(new T3dCylinder((a / 2), getConfig().getCalculatedEntraxePotReel() - getConfig().FLARGPOT), x, ((Ttraverse) getConfig().getTraverses().get(i)).getPosition(), z1, 0, 0, 90);
                    }
                    x = x - getConfig().getCalculatedEntraxePotReel();
                }
            } else {
                z1 = z - getConfig().getTravDecalZ();
                double lg = 0;
                double ld = 0;
                for (int i = 0; i < getConfig().getTraverses().size(); i++) {
                    double y1 = ((Ttraverse) getConfig().getTraverses().get(i)).getPosition();
                    //angles
                    if (getConfig().getAngleDroite()) {
                        double x1 = -(getConfig().getLongueur() / 2 - z1);
                        lg = -z1;
                        Tnomenclature3D.execute(getConfig().getNomenclature3D().get("//angle/traverse/droite/stl/*"), new TcallbackOnElement() {
                            @Override
                            public void execute(Element e) throws Exception {
                                w.put(PARTS.get(e.getNodeName()), x1, y1, z1, 0, 180, 0);
                            }
                        });
                        Tnomenclature3D.execute(getConfig().getNomenclature3D().get("//angle/traverse/milieu/stl/*"), new TcallbackOnElement() {
                            @Override
                            public void execute(Element e) throws Exception {
                                w.put(PARTS.get(e.getNodeName()), x1, y1, z1, 0, 180, 0);
                            }
                        });
                    }
                    if (getConfig().getAngleGauche() && getConfig().getMainCourante()) {
                        double x1 = getConfig().getLongueur() / 2 - z1;
                        ld = -z1;
                        Tnomenclature3D.execute(getConfig().getNomenclature3D().get("//angle/traverse/gauche/stl/*"), new TcallbackOnElement() {
                            @Override
                            public void execute(Element e) throws Exception {
                                w.put(PARTS.get(e.getNodeName()), x1, y1, z1, 0, 180, 0);
                            }
                        });
                        Tnomenclature3D.execute(getConfig().getNomenclature3D().get("//angle/traverse/milieu/stl/*"), new TcallbackOnElement() {
                            @Override
                            public void execute(Element e) throws Exception {
                                w.put(PARTS.get(e.getNodeName()), x1, y1, z1, 0, 180, 0);
                            }
                        });
                    }
                    //tube
                    w.put(new T3dCylinder((a / 2), getConfig().getLongueur() + lg + ld), (ld - lg) / 2, y1, z1, 0, 0, 90);
                }
            }
        }
    }

    /**
     * @return la configuration
     */
    public final TconfigurationPlat getConfig() {
        return (TconfigurationPlat) this.config;
    }

    /**
     * @return la forme 3D qui englobe tout
     */
    public final T3dForme getForme() {
        return this.w;
    }

}
