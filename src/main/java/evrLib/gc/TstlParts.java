/*
 * Copyright (C) 2014 rpetit
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package evrLib.gc;

import esyoLib.STL.STLaxis;
import esyoLib.jfx3d.T3dExtrude;
import esyoLib.jfx3d.T3dMesh;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;

/**
 * @author rpetit
 */
public class TstlParts {

    private Document doc = null;
    private HashMap<String, T3dMesh> liste = new HashMap();

    /**
     * @param aFileName
     */
    public TstlParts(String aFileName) throws ParserConfigurationException, SAXException, IOException {
        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = builderFactory.newDocumentBuilder();
        doc = builder.parse(new FileInputStream(aFileName));
        cache();
    }

    private void cache() {
        Element e = doc.getDocumentElement();
        NodeList l = e.getChildNodes();
        for (int i = 0; i < l.getLength(); i++) {
            if (l.item(i).getNodeType() == Node.ELEMENT_NODE) {
                //if(!liste.containsKey(l.item(i).getNodeName())){
                if (!liste.containsKey(((Element) l.item(i)).getAttribute("code"))) {
                    if (((Element) l.item(i)).hasAttribute("stl")) {
                        try {
                            File f = new File(((Element) l.item(i)).getAttribute("stl"));
                            if (f.canRead()) {
                                //new T3dExtrude("stl/monoprofil/monoprofil.stl",STLaxis.Y_AXIS, h)
                                switch (l.item(i).getNodeName()) {
                                    case "part":
                                        liste.put(((Element) l.item(i)).getAttribute("code"), new T3dMesh(((Element) l.item(i)).getAttribute("stl"), Double.parseDouble(((Element) l.item(i)).getAttribute("x")), Double.parseDouble(((Element) l.item(i)).getAttribute("y")), Double.parseDouble(((Element) l.item(i)).getAttribute("z")), Double.parseDouble(((Element) l.item(i)).getAttribute("rx")), Double.parseDouble(((Element) l.item(i)).getAttribute("ry")), Double.parseDouble(((Element) l.item(i)).getAttribute("rz"))));
                                        break;
                                    case "profil":
                                        liste.put(((Element) l.item(i)).getAttribute("code"), new T3dExtrude(((Element) l.item(i)).getAttribute("stl"), Double.parseDouble(((Element) l.item(i)).getAttribute("x")), Double.parseDouble(((Element) l.item(i)).getAttribute("y")), Double.parseDouble(((Element) l.item(i)).getAttribute("z")), Double.parseDouble(((Element) l.item(i)).getAttribute("rx")), Double.parseDouble(((Element) l.item(i)).getAttribute("ry")), Double.parseDouble(((Element) l.item(i)).getAttribute("rz"))));
                                        break;
                                }
                            } else {
                                System.out.println("TstlParts::cache() le fichier '" + f.getPath() + "' ne peut pas être lu! Vérifier 'stlParts.xml'");
                            }
                        } catch (Exception ex) {

                        }
                    }
                }
            }
        }
    }

    /**
     * @param aPartName
     * @return
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws Exception
     */
    public T3dMesh get(String aPartName) throws IOException, ClassNotFoundException, Exception {
        if (!liste.containsKey(aPartName)) {
            //throw new Exception(aPartName+" n'est pas dans xmlParts!");
            System.out.println(aPartName + " n'est pas dans xmlParts!");
            return null;
        }
        return new T3dMesh(liste.get(aPartName));
    }

    /**
     * @param aPartName
     * @return
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws Exception
     */
    public T3dMesh get(String aPartName, STLaxis aAxis, double depth) throws IOException, ClassNotFoundException, Exception {
        if (!liste.containsKey(aPartName)) {
            //throw new Exception(aPartName+" n'est pas dans xmlParts!");
            System.out.println(aPartName + " n'est pas dans xmlParts!");
            return null;
        }
        return new T3dExtrude(liste.get(aPartName), aAxis, depth);
    }

}
