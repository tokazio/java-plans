/*
 * Copyright (C) 2014 rpetit
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package evrLib.gc;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * @author rpetit
 */
public class TnomenclatureFile {

    private final Document doc;


    /**
     * @param aFileName
     * @throws java.io.FileNotFoundException
     * @throws javax.xml.parsers.ParserConfigurationException
     * @throws org.xml.sax.SAXException
     * @throws java.io.IOException
     */
    public TnomenclatureFile(String aFileName) throws FileNotFoundException, ParserConfigurationException, SAXException, IOException {
        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;
        builder = builderFactory.newDocumentBuilder();
        doc = builder.parse(new FileInputStream(aFileName));
    }

    /**
     * @param aNodelist
     * @param aStlReplacer
     * @throws java.lang.Exception
     */
    public final static void execute(NodeList aNodelist, TcallbackOnElement aStlReplacer) throws Exception {
        for (int i = 0; i < aNodelist.getLength(); i++) {
            if (aNodelist.item(i).getNodeType() == Node.ELEMENT_NODE) {
                aStlReplacer.execute((Element) aNodelist.item(i));
            }
        }
    }

    /**
     * @param aXpath
     * @return
     */
    public final NodeList get(String aXpath) {
        XPath xpath = XPathFactory.newInstance().newXPath();
        NodeList nodes = null;
        try {
            return (NodeList) xpath.compile(aXpath).evaluate(doc, XPathConstants.NODESET);
        } catch (XPathExpressionException ex) {
            return null;
        }
    }

}
