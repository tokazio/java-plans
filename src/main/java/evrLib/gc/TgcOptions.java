/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package evrLib.gc;

import esyoLib.Utils.Toptions;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author rpetit
 */
public class TgcOptions extends Toptions {

    public String defaultPath;
    public boolean browseByGammeName;
    public boolean browseByYear;

    /**
     * @param aFileName
     * @param c
     * @throws IOException
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     */
    public TgcOptions(String aFileName, Class c) throws IOException, IllegalArgumentException, IllegalAccessException {
        super(aFileName, c);
    }

    public final String getChantierPath(String gamme) {
        String s = defaultPath;
        if (browseByGammeName) {
            s += "\\GAMME " + gamme;
        }
        if (browseByYear) {
            Date dNow = new Date();
            SimpleDateFormat ft = new SimpleDateFormat("yyyy");
            s += "\\" + ft.format(dNow);
        }
        return s + "\\";
    }

}
