/*
 * Copyright (C) 2014 rpetit
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package evrLib.gc;

import org.w3c.dom.Element;

/**
 * @author rpetit
 */
public class TconfigNomenclature {

    //
    private Tnomenclature nom;
    //
    private Tconfiguration config;

    /**
     * @param aConfig
     * @param aNomenclature
     * @param aGescom
     */
    public TconfigNomenclature(Tconfiguration aConfig, Tnomenclature aNomenclature, TgescomFile aGescom) {
        this.nom = aNomenclature;
        this.config = aConfig;
        this.nom.linkGescom(aGescom);
    }

    public final TconfigurationPlat getConfig() {
        return (TconfigurationPlat) this.config;
    }

    public final Tnomenclature getNom() {
        return this.nom;
    }

    /**
     *
     */
    public final void build() throws Exception {
        //mur à gauche
        if (getConfig().getFixMurGauche()) {

        }
        //mur à droite
        if (getConfig().getFixMurDroite()) {

        }
        // ---------------------------
        // ---------------------------
        //potelets
        // ---------------------------
        // ---------------------------
        if (!getConfig().getSansPotelet()) {

            for (int i = 0; i < getConfig().getPotelets().size(); i++) {
                if (getConfig().getPotelets().get(i).getActif()) {
                    //--------------------------------------------------------
                    //fixation
                    //--------------------------------------------------------

                    String s = "sol";
                    if (getConfig().getIsFacade()) {
                        s = "facade";
                    } else {
                        if (getConfig().getIsScellement()) {
                            s = "scellement";
                        }
                    }
                    Tnomenclature.execute(getNom().getFromGamme("//fixations/" + s + "/nomenclature/*"), new TcallbackOnElement() {
                        @Override
                        public void execute(Element e) {
                            try {
                                getNom().add(e.getNodeName(), e.getAttribute("qtt"));
                            } catch (Exception ex) {
                                System.out.println(ex.getClass().getName() + ": " + ex.getMessage());
                            }
                        }
                    });
                    //--------------------------------------------------------
                    //liaison mc
                    //--------------------------------------------------------
                    if (getConfig().getMainCourante()) {
                        Tnomenclature.execute(getNom().getFromGamme("//potelets/mc/nomenclature/*"), new TcallbackOnElement() {
                            @Override
                            public void execute(Element e) {
                                try {
                                    getNom().add(e.getNodeName(), e.getAttribute("qtt"));
                                } catch (Exception ex) {
                                    System.out.println(ex.getClass().getName() + ": " + ex.getMessage());
                                }
                            }
                        });
                    }
                    //--------------------------------------------------------
                    //liaison trav
                    //--------------------------------------------------------
                    if (!getConfig().getVerreIsComplet() && !getConfig().getIsBal()) {
                        s = "milieu";
                        if (i == 0) {
                            s = "debut";
                        }
                        if (i == getConfig().getPotelets().size() - 1) {
                            s = "fin";
                        }

                        String t = "tube";
                        if (getConfig().getTraversesCable()) {
                            t = "cable";
                        }
                        String u = "app";
                        if (getConfig().getTraversesAxe()) {
                            u = "axe/" + s;
                        }
                        for (int j = 0; j < getConfig().getTraverses().size(); j++) {
                            Tnomenclature.execute(getNom().getFromGamme("//potelets/traverses/" + t + "/" + u + "/nomenclature/*"), new TcallbackOnElement() {
                                @Override
                                public void execute(Element e) {
                                    try {
                                        getNom().add(e.getNodeName(), e.getAttribute("qtt"));
                                    } catch (Exception ex) {
                                        System.out.println(ex.getClass().getName() + ": " + ex.getMessage());
                                    }
                                }
                            });
                        }
                    }
                    //--------------------------------------------------------
                    //liaison verre/tole
                    //--------------------------------------------------------
                    if (getConfig().getIsVerre()) {
                        s = "milieu";
                        if (i == 0) {
                            s = "debut";
                        }
                        if (i == getConfig().getPotelets().size() - 1) {
                            s = "fin";
                        }
                        Tnomenclature.execute(getNom().getFromGamme("//potelets/tole/" + s + "/nomenclature/*"), new TcallbackOnElement() {
                            @Override
                            public void execute(Element e) {
                                try {
                                    getNom().add(e.getNodeName(), e.getAttribute("qtt"));
                                } catch (Exception ex) {
                                    System.out.println(ex.getClass().getName() + ": " + ex.getMessage());
                                }
                            }
                        });
                        Tnomenclature.execute(getNom().getFromGamme("//potelets/tole/" + s + "/nomenclature/*"), new TcallbackOnElement() {
                            @Override
                            public void execute(Element e) {
                                try {
                                    getNom().add(e.getNodeName(), e.getAttribute("qtt"));
                                } catch (Exception ex) {
                                    System.out.println(ex.getClass().getName() + ": " + ex.getMessage());
                                }
                            }
                        });
                    }
                }
            }

        }

        // ---------------------------
        // VERRE/TOLE
        // ---------------------------
        if (getConfig()
                .getIsVerre()) {
            for (int i = 0; i < getConfig().getPotelets().size() - 1; i++) {

            }
        }
        // ---------------------------
        //Traverses balustres
        // ---------------------------

        if (getConfig()
                .getIsBal()) {

        }
        // ---------------------------
        // Balustres
        // ---------------------------

        if (getConfig()
                .getIsBal()) {

            for (int j = 0; j < getConfig().getPotelets().size() - 1; j++) {
                for (int i = 0; i < getConfig().getCalculatedNbrBal(); i++) {
                }
            }

        }

        // ---------------------------
        // Traverses (hors balustre)
        // ---------------------------
        if (!getConfig()
                .getIsBal() && !getConfig().getVerreIsComplet()) {
            //dans l'axe
            if (getConfig().getTraversesAxe()) {
                for (int j = 0; j < getConfig().getPotelets().size() - 1; j++) {
                    for (int i = 0; i < getConfig().getTraverses().size(); i++) {

                    }
                }
            }

        }
    }

    /**
     *
     */
    public final void print() {
        System.out.println(nom.size() + " références dans la nomenclature");
        for (TnomenclatureRef ref : nom.getRefs()) {
            System.out.println(ref.getRef() + " " + ref.getQtt());
        }
    }

}
