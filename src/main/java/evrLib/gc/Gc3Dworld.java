/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evrLib.gc;

import esyoLib.jfx3d.T3dForme;
import esyoLib.jfx3d.T3dMesh;
import esyoLib.jfx3d.T3dWorld;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Shape3D;
import javafx.stage.Stage;

/**
 * @author rpetit
 */
public class Gc3Dworld extends T3dWorld {

    private final T3dForme stlGroup = new T3dForme();
    private final T3dForme solGroup = new T3dForme();
    private Color color = null;

    //
    private Shape3D selected;

    /**
     * @param aStage
     */
    public Gc3Dworld(Stage aStage) {
        super(aStage);
        setFarClip(1000.0 * 1000);
        super.add(stlGroup);
        super.add(solGroup);
    }

    @Override
    public void onKey(KeyEvent aKeyEvent) {
        if (selected != null) {
            System.out.println("Position de l'objet: " + selected.getTranslateX() + "," + selected.getTranslateY() + "," + selected.getTranslateZ());
        }
    }

    @Override
    public void onSelect(Shape3D aShape3D) {
        if (selected != null) {
            ((PhongMaterial) selected.getMaterial()).setDiffuseColor(color);
        }
        selected = aShape3D;

        if (selected != null) {
            System.out.println("Position de l'objet: " + selected.getTranslateX() + "," + selected.getTranslateY() + "," + selected.getTranslateZ());
            PhongMaterial m = ((PhongMaterial) selected.getMaterial());
            if (m != null) {
                color = m.getDiffuseColor();
                m.setDiffuseColor(Color.LIGHTGREEN);
            } else {
                color = Color.LIGHTGRAY;
                PhongMaterial n = new PhongMaterial();
                n.setDiffuseColor(Color.LIGHTGREEN);
                selected.setMaterial(n);
            }
        }
    }

    @Override
    public void onZoom(ScrollEvent aScrollEvent) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @param aShape3D
     * @param transX
     * @param transY
     * @param transZ
     * @param xRot
     * @param yRot
     * @param zRot
     * @return
     */
    public Gc3Dworld put(Shape3D aShape3D, double transX, double transY, double transZ, double xRot, double yRot, double zRot) {
        T3dForme f = new T3dForme();
        f.getChildren().addAll(aShape3D);
        f.setRotateX(xRot);
        f.setRotateY(yRot);
        f.setRotateZ(zRot);
        f.setTranslate(transX, transY, transZ);
        return add(f);
    }

    /**
     * @param aT3dForm
     * @return
     */
    @Override
    public Gc3Dworld add(T3dForme aT3dForm) {
        stlGroup.getChildren().addAll(aT3dForm);
        return this;
    }

    /**
     * @param aT3dMesh
     * @param transX
     * @param transY
     * @param transZ
     * @param xRot
     * @param yRot
     * @param zRot
     * @return
     */
    public Gc3Dworld put(T3dMesh aT3dMesh, double transX, double transY, double transZ, double xRot, double yRot, double zRot) {
        T3dForme f = new T3dForme();
        f.getChildren().addAll(aT3dMesh);
        f.setRotateX(xRot);
        f.setRotateY(yRot);
        f.setRotateZ(zRot);
        f.setTranslate(transX, transY, transZ);
        return add(f);
    }

    /**
     * @param aT3dForm
     * @param transX
     * @param transY
     * @param transZ
     * @param xRot
     * @param yRot
     * @param zRot
     * @return
     */
    public Gc3Dworld put(T3dForme aT3dForm, double transX, double transY, double transZ, double xRot, double yRot, double zRot) {
        T3dForme f = new T3dForme();
        f.getChildren().addAll(aT3dForm);
        f.setRotateX(xRot);
        f.setRotateY(yRot);
        f.setRotateZ(zRot);
        f.setTranslate(transX, transY, transZ);
        return add(f);
    }

    public void clear() {
        solGroup.getChildren().clear();
        stlGroup.getChildren().clear();
    }

    @Override
    public void onDblClick(MouseEvent me) {
        System.out.println(me.getPickResult());

        //le point 0 en 3D est au centre de la scene au départ
        //puis décalé de translateX et translateY de la camera
    }
}
