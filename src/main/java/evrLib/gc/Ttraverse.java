/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package evrLib.gc;

/**
 * @author rpetit
 */
public class Ttraverse extends Tprofile {
    //Position de la traverse en mm depuis le sol
    private double FpositionAxeY;
    //Epaisseur de la traverse en mm
    private double Fepaisseur;

    public Ttraverse() {

    }

    /**
     * Constructeur
     *
     * @param positionAxeY Position de la traverse en mm depuis le sol
     * @param epaisseur    Epaisseur de la traverse en mm
     */
    public Ttraverse(double positionAxeY, double epaisseur) {
        FpositionAxeY = positionAxeY;
        Fepaisseur = epaisseur;
    }

    @Override
    public String toString() {
        return "Position:" + FpositionAxeY + "\n" + "Epaisseur:" + Fepaisseur;
    }
    
    /*
    @Override
    public String saveToString() {
        return FpositionAxeY+","+Fepaisseur;
    }
    */

    /**
     *
     * @param s Chaîne du fichier de sauvegarde à convertir en objet
     */
    /*
    @Override
    public final void loadFromString(String s) {
        if(s.isEmpty()){
            //System.out.println("Ttraverse::fromString() Error\nLa chaîne est vide!");
            return;
        }
        String[] ts = s.split(",");
        //System.out.println(Arrays.toString(ts));
        if (ts.length != 4) {
            //System.out.println("Ttraverse::fromString() Error\nLa chaîne '"+s+"' n'est pas valide (il manque des paramètres ou l'ordre n'est pas bon)");
        } else {
            FpositionAxeY = Double.parseDouble(ts[0]);
            Fepaisseur = Double.parseDouble(ts[1]);
        }
    }
    */

    /**
     * @return Position de la traverse en mm depuis le sol
     */
    public final double getPosition() {
        return FpositionAxeY;
    }

    /**
     * @return Epaisseur de la traverse en mm
     */
    public final double getEpaisseur() {
        return Fepaisseur;
    }

}
