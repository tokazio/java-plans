/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evrLib.gc;

import esyoLib.Exceptions.NoNameException;
import esyoLib.Graphics.Tpoint2D;
import esyoLib.Utils.TstringList;
import evrLib.enumeration.Egamme;
import evrLib.enumeration.Epercage;
import evrLib.enumeration.Eprofil;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map.Entry;

/**
 * Représente un potelet
 *
 * @author rpetit
 */
public class Tpotelet extends Tprofile {

    private boolean Factif = true;//A afficher/compter ou pas
    private String Fcode = "";//code potelet
    private int Fnum = -1;//numéro de potelet
    private String Ftype = "";//type G,AG,M,AD,D
    private Tpoint2D Fposition = Tpoint2D.zero();//position x,y
    private String FnomConfig = "";
    private Egamme Fgamme = Egamme.none;
    //private int FsensSortPercages;
    //private int FmarcheNum;

    /**
     *
     */
    public Tpotelet() {
        this.Fposition = null;
    }

    /**
     * Constructeur
     *
     * @param num     Numéro du potelet
     * @param type    Type du potelet
     * @param code    Code du potelet
     * @param hauteur Hauteur du potelet en mm
     * @param aNom
     */
    public Tpotelet(int num, String type, String code, double hauteur, Egamme aGamme, String aNom) {
        this.Fnum = num;
        this.Ftype = type;
        this.Fcode = code;
        this.Flongueur = hauteur;
        this.Fposition = null;
        this.FnomConfig = aNom;
        this.Fgamme = aGamme;
    }

    /**
     * Constructeur
     *
     * @param num      Numéro du potelet
     * @param type     Type du potelet
     * @param code     Code du potelet
     * @param hauteur  Hauteur du potelet en mm
     * @param position Position du potelet depuis la gauche en mm
     * @param aGamme
     * @param aNom
     */
    public Tpotelet(int num, String type, String code, double hauteur, Tpoint2D position, Egamme aGamme, String aNom) {
        this.Fnum = num;
        this.Ftype = type;
        this.Fcode = code;
        this.Flongueur = hauteur;
        this.Fposition = position;
        this.FnomConfig = aNom;
        this.Fgamme = aGamme;
    }

    /**
     * @return Phrase décrivant le potelet
     */
    @Override
    public String toString() {
        return "Portion=" + this.Fportion + " Code=" + this.Fcode + " Type=" + this.Ftype + " Longueur=" + this.Flongueur + " Actif=" + this.Factif;
    }

    /**
     *
     * @return Texte pour l'enregistrement dans le fichier de sauvegarde
     */
    /*
    @Override
    public String saveToString() {
        String s = "";
        s = getPercages().stream().map((p) -> "*" + p.toFile()).reduce(s, String::concat);
        return this.Fnum + "," + this.Ftype + "," + this.Fcode + "," + this.Flongueur + "," + this.Factif+","+s.substring(1).replace("=", ":");
    }
    */


    /**
     * Chaîne de la forme: 0,G,POTELET CHR SCELL 2T ,553.5,true,|*|15|:|8|*|21|:|124.93|*|15|:|124.93|*|21|:|303.59|*|15|:|303.59|*|17|:|554.26|*|17|:|744.26|1,M,POTELET BALU SOL  2T V,896.26,true,|*|15|:|8|*|15|:|124.93|*|15|:|303.59|*|16|:|554.26|*|16|:|744.26|2,M,POTELET BALU SOL  2T V,896.26,true,|*|15|:|8|*|15|:|124.93|*|15|:|303.59|*|16|:|554.26|*|16|:|744.26|3,D,POTELET BALU SOL  2T V,896.26,true,|*|15|:|8|*|15|:|124.93|*|15|:|303.59|*|17|:|554.26|*|17|:|744.26|
     *
     * @param s Texte à convertir lors de la lecture du fichier de sauvegarde
     * @throws java.lang.Exception Chaîne non valide (manque des paramètres ou
     *                             ils sont faux)
     */
    /*
    @Override
    public final void loadFromString(String s) throws Exception {
        if (s.isEmpty()) {
            return;
        }
        String[] ts = s.split(",");
        if (ts.length < 4) {
            throw new Exception("Tpotelet::fromString() Error\nLa chaîne '" + s + "' n'est pas valide (il manque des paramètres ou l'ordre n'est pas bon)");
        }
        this.Fnum = Integer.parseInt(ts[0]);
        this.Ftype = ts[1];
        this.Fcode = ts[2];
        this.Flongueur = Double.parseDouble(ts[3]);
        try {
            this.Factif = Boolean.parseBoolean(ts[4]);
            //perçages
            percagesFromString(ts[5].replace(":","=").replace("*", ","));
        } catch (Exception ex) {
            //compatibilité avec les anciens fichiers qui ne prenne pas en compte les potelets actif
        }
    }
*/
    public final void percagesFromString(String s) {
        String[] ts = s.split(",");
        for (int i = 0; i < ts.length; i++) {
            addPercage(new Tpercage(Epercage.fromString(s.substring(0, s.indexOf("="))), Double.parseDouble(s.substring(s.indexOf("=") + 1))));
        }
    }

    /**
     * @return Type du potelet
     */
    public final String getType() {
        return this.Ftype;
    }

    /**
     * @return Code du potelet
     */
    public final String getCode() {
        return this.Fcode;
    }

    /**
     * @return Position du potelet depuis la gauche en mm
     */
    public final Tpoint2D getPosition() {
        return this.Fposition;
    }

    /**
     * Retourne si un potelet est actif ou non (actif, il doit être compté,
     * affiché)
     *
     * @return boolean
     */
    public final boolean getActif() {
        return this.Factif;
    }

    /**
     * Défini si le potelet est actif ou non (actif, il doit être compté,
     * affiché)
     *
     * @param b Actif ou non
     */
    public final void setActif(boolean b) {
        this.Factif = b;
    }

    /**
     *
     */
    public final Egamme getGamme() {
        return Fgamme;
    }

    /**
     * Défini les perçages du potelet
     *
     * @param p
     */
    public final void setPercages(HashMap<String, Double> p) {
        clearPercages();
        //peu ajouter le perçage (par défaut)
        //boolean canAdd = true;
        TstringList l;
        String f = "?";
        System.out.println(getCode());
        try {
            f = getCode().substring(8, getCode().substring(9).indexOf(" ") + 9) + ".perc";
            l = new TstringList(f);
        } catch (IOException e) {
            System.out.println("Fichier de paramètres des perçages '" + f + "' non trouvé!");
            return;
        }
        //1ère lecture---------------------------------------------------------------------------
        loop:
        for (int i = 0; i < l.size(); i++) {
            //la clé est un chiffre ou contient '?:'
            try {
                //clé
                String n = l.nameFromIndex(i);
                double d = 0;
                //si : dans la clé
                if (n.indexOf(":") == 1) {
                    switch (n.charAt(0)) {
                        //façade
                        case 'F':
                            //si pas potelet façade, peut pas percer
                            //canAdd=false;
                            if (getCode().indexOf("FAC") < 0) {
                                continue loop;
                            }
                            break;
                    }
                    d = Double.parseDouble(n.substring(2));
                } else {
                    d = Double.parseDouble(n);
                }
                //si la position est négative, cela veux dire que je part du bas
                //je récupère donc la côte 'P' et je lui retire 'd'
                //si P n'existe pas, je ne fait pas de perçage
                if (d < 0) {
                    if (p.containsKey("P")) {
                        d = p.get("P") + d;
                    } else {
                        //canAdd = false;
                        continue loop;
                    }
                }
                //ajoute le code à la position
                //if (canAdd) {
                this.addPercage(new Tpercage(Epercage.fromString(l.valueFromIndex(i)), d));
                //}
            } catch (Exception e) {
                //la clé est un texte->ne fait rien
            }
        }
        //---------------------------------------------------------------------------
        //Pour chaque côtes
        for (Entry<String, Double> e : p.entrySet()) {
            //si cote P, je continue
            if (e.getKey().charAt(0) == 'P') {
                continue;
            }
            //ajoute les perçages (T,V... quelques soit le type du potelet AD,AG,M,D,G)
            try {
                if (l.hasName(e.getKey().charAt(0) + "")) {
                    this.addPercage(new Tpercage(Epercage.fromString(l.valueFromName(e.getKey().charAt(0) + "")), e.getValue()));
                }
            } catch (NoNameException ex) {
                System.out.println("Perçage '" + Ftype.charAt(0) + "-" + e.getKey().charAt(0) + "' non prévu dans le fichier de paramètres des perçages (*.perc)");
            }
            //ajoute les perçages (A-T,D-T... selon le type du potelet AD,AG,M,G,D)
            //AD et AG deviennent A
            try {
                if (l.hasName(Ftype.charAt(0) + "-" + e.getKey().charAt(0))) {
                    this.addPercage(new Tpercage(Epercage.fromString(l.valueFromName(Ftype.charAt(0) + "-" + e.getKey().charAt(0))), e.getValue()));
                }
            } catch (NoNameException ex) {
                System.out.println("Perçage '" + Ftype.charAt(0) + "-" + e.getKey().charAt(0) + "' non prévu dans le fichier de paramètres des perçages (*.perc)");
            }
        }
        sortPercages();
    }

    /**
     * Enregistre le fichier du morceau pour ouverture avec editeur de barre
     * DELPHI
     *
     * @param aFileName
     * @throws IOException
     */
    public void toFile(String aFileName) throws IOException {
        /*
         filename>
         gamme>Baludesign
         nom>STD BALUDESIGN v ou t D
         longueur>897
         percages>15=8,17=175,17=705
         codeprofil>7
         */
        TstringList l = new TstringList();
        l.setNameValueSeparator('>');
        l.add("filename", aFileName);
        l.add("gamme", getGamme().getNom());
        l.add("nom", FnomConfig + " " + getCode() + " " + getType());
        l.add("longueur", getLongueur() + "");
        String s = "";
        s = getPercages().stream().map((p) -> "," + p.toFile()).reduce(s, String::concat);
        if (!s.isEmpty()) s = s.substring(1);
        l.add("percages", s);
        switch (Fgamme) {
            case monoprofil:
                l.add("codeprofil", Eprofil.bi.getCode() + "");
                break;
            case baludesign:
                l.add("codeprofil", Eprofil.al40.getCode() + "");
                break;
            case chrome:
                l.add("codeprofil", Eprofil.inpoth40.getCode() + "");
                break;
        }
        l.saveToFile(aFileName);
    }

}
