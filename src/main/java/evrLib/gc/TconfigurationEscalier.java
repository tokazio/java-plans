/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evrLib.gc;

import esyoLib.Graphics.Tpoint2D;
import esyoLib.Utils.TstringList;
import evrLib.enumeration.Eside;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author rpetit
 */
public class TconfigurationEscalier extends Tconfiguration {

    // -------------------------------------------------------------------
    // A ENTRER---------------------------------------------------------
    // -------------------------------------------------------------------
    private TstringList Fmarches; // du type: (hauteur=largeur)
    //------------------------------------------------------------------
    private double FdebPotelet;
    //------------------------------------------------------------------
    private boolean FisDevelop;
    private double FcalculatedAngleMarches;
    private double FcalculatedHauteurTotMarches;
    private double FcalculatedLongueurTotMarches;
    private double FcalculatedTanA;
    private double FcalculatedCosA;
    //------------------------------------------------------------------
    private double FcalculatedHauteurPotelet;
    //------------------------------------------------------------------
    /*
     private double FcalculatedPosPoteletPrem;
     public  double getCalculatedPosPoteletPrem(){
     return FcalculatedPosPoteletPrem;
     }
     */
    //------------------------------------------------------------------
    private double FcalculatedHauteurFinie;
    //------------------------------------------------------------------
    //info
    //------------------------------------------------------------------
    private int FinfoEspacementNbMarche;

    // -------------------------------------------------------------------
    // trigo calculé
    // -------------------------------------------------------------------

    /**
     * @throws IOException
     */
    public TconfigurationEscalier() throws IOException {
        super();
        Fmarches = new TstringList();
        try {
            loadFromFile("escalier.def");
        } catch (ParserConfigurationException | SAXException ex) {
            Logger.getLogger(TconfigurationEscalier.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public final String getMarchesString() {
        return Fmarches.toString("\n");
    }

    public final void setMarchesString(String s) {
        Fmarches.clear();
        Fmarches.fromString(s, "\n");
    }

    public final TstringList getMarches() {
        return Fmarches;
    }

    public final double getDebPotelet() {
        return FdebPotelet;
    }

    public final void setDebPotelet(String s) {
        if (s.isEmpty()) {
            s = "0";
        }
        setDebPotelet(Double.parseDouble(s));
    }

    public final void setDebPotelet(double d) {
        FdebPotelet = d;
    }

    public final boolean getIsDevelop() {
        return FisDevelop;
    }

    public final void setIsDevelop(boolean b) {
        FisDevelop = b;
    }

    public final double getCalculatedAngleMarches() {
        return FcalculatedAngleMarches;
    }

    public final double getCalculatedHauteurTotMarches() {
        return FcalculatedHauteurTotMarches;
    }

    public final double getCalculatedLongueurTotMarches() {
        return FcalculatedLongueurTotMarches;
    }

    public final double getCalculatedTanA() {
        return FcalculatedTanA;
    }

    public final double getCalculatedCosA() {
        return FcalculatedCosA;
    }

    public final int getInfoEspacementNbMarche() {
        return FinfoEspacementNbMarche;
    }

    public void setInfoEspacementNbMarche(int i) {
        FinfoEspacementNbMarche = i;
    }

    public double getCalculatedHauteurPotelet() {
        return FcalculatedHauteurPotelet;
    }

    public double getCalculatedHauteurFinie() {
        return FcalculatedHauteurFinie;
    }

    private void calcTrigo() {

    }

    private double axeMarche(int num) {
        if (num == 1) {
            return -getDebPotelet();
        } else {
            return getLongueur(num) - (getLongueurMarche(num) / 2);
        }
    }

    private double getLongueur(int num) {
        double a;
        double r = 0;
        for (int i = 1; i < num; i++) {
            if (!Fmarches.valueFromIndex(i).isEmpty()) {
                a = Double.parseDouble(Fmarches.valueFromIndex(i));
                r += a;
            }
        }

        return r;
    }

    private double getHauteur(int num) {
        double a;
        double r = 0;
        for (int i = 0; i < num; i++) {
            if (!Fmarches.nameFromIndex(i).isEmpty()) {
                a = Double.parseDouble(Fmarches.nameFromIndex(i));
                r += a;
            }
        }
        return r;
    }

    private double getLongueurMarche(int num) {
        double a = 0;
        try {
            if (!Fmarches.valueFromIndex(num - 1).isEmpty()) {
                a = Double.parseDouble(Fmarches.valueFromIndex(num - 1));
            }
        } catch (ArrayIndexOutOfBoundsException ex) {
            System.out.println("Impossible de récupérer la longueur de la marche #" + num);
        }
        return a;
    }

    @Override
    public Tconfiguration calcul() {
        //si pas de marches, pas de calcul
        if (Fmarches.size() == 0) {
            return this;
        }
        // pour eviter division par 0
        if (getEspacementPotMax() == 0) {
            return this;
        }
        //
        double a, b;
        //
        /*
        //calcul hauteur potelet du palier (cf config plat)
        //si mc
        
        FcalculatedHauteurFinie = Math.round(getHauteurFinie());
        if (getMainCourante()) {
            setCalculatedHauteurSousMC(getCalculatedHauteurFinie() - FHMC);
        } else {
            setCalculatedHauteurSousMC(getCalculatedHauteurFinie());
        }
        a = FHLIAISON;
        if (!getMainCourante()) {
            a = FHBOUCHONPOT;
        }
        // si sabot
        b = FHSABOT;
        // sinon
        if (getIsScellement() || getIsFacade()) {
            b = 0;
        }
        FcalculatedHauteurPotelet = getCalculatedHauteurSousMC() - a - b;

        if (getIsScellement()) {
            FcalculatedHauteurPotelet += getHauteurScellement();
        }
        if (getIsFacade()) {
            FcalculatedHauteurPotelet += getFacAxeVertPlatine() + getFacDecalHauteurPlatine();
        }
        */
        //hauteur totale des marches (sans la 1)
        FcalculatedHauteurTotMarches = getHauteur(Fmarches.size() - 1);
        //longueur totale des marchers (sans la 1)
        FcalculatedLongueurTotMarches = getLongueur(Fmarches.size());
        //tangante de l'angle
        FcalculatedTanA = FcalculatedHauteurTotMarches / FcalculatedLongueurTotMarches;
        //angle
        FcalculatedAngleMarches = Math.atan(FcalculatedTanA);
        //cosinus de l'angle
        FcalculatedCosA = Math.cos(Math.atan(FcalculatedTanA));
        //nombre de potelet
        double n = Math.ceil(FcalculatedLongueurTotMarches / getEspacementPotMax());
        //2 minimum ou ajout du potelet de fin
        if (n < 2) {
            n = 2;
        } else {
            n++;
        }
        //nombre de marche entre potelet
        setInfoEspacementNbMarche((int) Math.floor(Fmarches.size() / n));
        //hauteur de la fixation d'angle
        double hfixangle = (FAXEMC / FcalculatedCosA) + (FAXELIAISON / FcalculatedCosA) + (FHLIAISON - FAXELIAISON);
        //----------------------------------------------------------------------------------
        //CREE LES POTELETS
        //----------------------------------------------------------------------------------
        double hauteurPotelet;
        Fpotelets.clear();
        //potelet 'palier'
        hauteurPotelet = ((getHauteurFinie() - FAXEMC) - (-getDebPotelet() * FcalculatedTanA)) + getHauteur(1) - hfixangle;
        Fpotelets.add(new Tpotelet(0, "", getCodePotelet(), hauteurPotelet, new Tpoint2D(-getDebPotelet(), getHauteur(0)), getGamme(), getNom()));
        //potelet 1ère marche (marche 2)
        hauteurPotelet = ((getHauteurFinie() - FAXEMC) - (FLARGPOT + 110 - getDebPotelet() * FcalculatedTanA)) + getHauteur(2) - hfixangle;
        Fpotelets.add(new Tpotelet(2, "D", getCodePotelet(), hauteurPotelet, new Tpoint2D(FLARGPOT + 110 - getDebPotelet(), getHauteur(2)), getGamme(), getNom()));
        //si plus de 2 potelets
        if (n > 2) {
            //autres potelets
            int j = 2 + FinfoEspacementNbMarche + 1;
            for (int i = 1; i < n - 1; i++) {
                System.out.println("Potelet #" + i + " sur la marche #" + j);
                //autre potelets
                hauteurPotelet = ((getHauteurFinie() - FAXEMC) - (axeMarche(j) * FcalculatedTanA)) + getHauteur(j) - hfixangle;
                Fpotelets.add(new Tpotelet(j, "M", getCodePotelet(), hauteurPotelet, new Tpoint2D(axeMarche(j), getHauteur(j - 2)), getGamme(), getNom()));
                j += FinfoEspacementNbMarche + 1;
            }
        }
        //dernier potelet sur la dernière marche
        hauteurPotelet = ((getHauteurFinie() - FAXEMC) - (axeMarche(Fmarches.size()) * FcalculatedTanA)) + getHauteur(Fmarches.size()) - hfixangle;
        Fpotelets.add(new Tpotelet(Fmarches.size(), "G", getCodePotelet(), hauteurPotelet, new Tpoint2D(axeMarche(Fmarches.size()), getHauteur(Fmarches.size())), getGamme(), getNom()));

        //----------------------------------------------------------------------------------
        //Calcul des entraxes traverses
        setCalculatedHauteurSousMC(getHauteurFinie() - FHMC);
        setCalculatedHauteurTraverses(getCalculatedHauteurSousMC() - getDebordTravBas());
        setCalculatedEntraxeTraverse(Math.round(getCalculatedHauteurTraverses() / getNbrTraverses()));
        if (getNbrTraverses() > 0) {
            if (getNbrTravSerre() == 0) {
                setCalculatedEntraxeTraverseSerre(getCalculatedEntraxeTraverse());
            } else {
                setCalculatedEntraxeTraverseSerre(getEspTravSerre());
                a = getCalculatedHauteurSousMC() - getDebordTravBas() - ((getNbrTravSerre() - 1) * getEspTravSerre());
                b = getNbrTraverses() - getNbrTravSerre() + 1;
                setCalculatedEntraxeTraverse(a / b);
            }

        }
        setCalculatedEntraxeTraverseBas(getDebordTravBas());

        double e, ep, ea;
        Tpercage pDroite, pGauche, pbasGauche, pbasDroite, phautGauche, phautDroite;

        // calcTrigo(); ?? déjà fait
        // clearPercages du potelet?
        // remplissage traverses
        int j = 1;
        for (int i = 0; i < getCalculatedNbrPot(); i++) {
            b = getDebordTravBas();
            for (int k = 0; k < getNbrTraverses(); k++) {
                pDroite = new Tpercage(Eside.droite);
                pGauche = new Tpercage(Eside.gauche);
                // perçage à gauche du potelet
                a = (b) - ((Fpotelets.get(i).getPosition().x + (FLARGPOT / 2)) * FcalculatedTanA) + getHauteur(j);
                // showmessage('Hauteur: ' + floattostr(getHauteur(j)) + ' Debord: ' + floattostr(b) + ' Position: ' + Fpotelets.Names[i] + #13 + 'Potelet ' + inttostr(i) + ': h axe trav ' + inttostr(k) + ' = ' + floattostr(a));
                // perçage à droite du potelet
                e = (b) - ((Fpotelets.get(i).getPosition().x - (FLARGPOT / 2)) * FcalculatedTanA) + getHauteur(j);
                // si dernier potelet, ajoute que percage droite
                if (j < Fmarches.size() - getCalculatedNbrPot() + 1) {
                    pGauche.setPosition(Fpotelets.get(i).getLongueur() - a);
                } else {
                    pGauche.setPosition(-1);
                }
                // si 1er potelet, ajoute que percage gauche
                if (j > 1) {
                    pDroite.setPosition(Fpotelets.get(i).getLongueur() - e);
                } else {
                    pDroite.setPosition(-1);
                }
                Fpotelets.get(i).getPercages().add(pDroite);
                Fpotelets.get(i).getPercages().add(pGauche);
                //
                b = b + getCalculatedEntraxeTraverse();
            }
            j = j + FinfoEspacementNbMarche;

            if (j > Fmarches.size()) {
                j = j - 1;
            }
        }
        // remplissage verre
        if (getIsVerre()) {
            // entrace pinces à plat
            e = getPosVerre() - getDebordVerreSol() - getAxePinceHaut() - getAxePinceBas();
            // entraxe pince haute avec angle
            ep = (getAxePinceHaut() / getCalculatedCosA());
            // entraxe pince avec angle
            ea = e / FcalculatedCosA;

            j = 1;
            for (int i = 0; i < getCalculatedNbrPot(); i++) {
                pbasGauche = new Tpercage(Eside.gauche);
                phautGauche = new Tpercage(Eside.gauche);
                pbasDroite = new Tpercage(Eside.droite);
                phautDroite = new Tpercage(Eside.droite);
                // à corriger
                Fpotelets.get(i).getPercages().add(phautGauche);
                Fpotelets.get(i).getPercages().add(pbasGauche);
                Fpotelets.get(i).getPercages().add(phautDroite);
                Fpotelets.get(i).getPercages().add(pbasDroite);
                // perçage à gauche du potelet
                if (j == 1) {
                    a = (getPosVerre() + getHauteur(1)) - ((-getDebPotelet() + (FLARGPOT / 2)) * FcalculatedTanA) - ep;
                } else {
                    a = (getPosVerre() + getHauteur(j)) - ((axeMarche(i) + (FLARGPOT / 2)) * FcalculatedTanA) - ep;
                }
                b = a - ea;
                phautGauche.setPosition(a);
                pbasGauche.setPosition(a);
                // perçage à droite du potelet
                a = (getPosVerre() + getHauteur(j)) - ((axeMarche(j) - (FLARGPOT / 2)) * FcalculatedTanA) - ep;
                b = a - ea;
                phautDroite.setPosition(a);
                pbasDroite.setPosition(b);
                j = j + FinfoEspacementNbMarche;
                if (j > Fmarches.size()) {
                    j = j - 1;
                }
            }
        }
        return this;
    }

    /**
     *
     */
    @Override
    public final void doAfterLoad() {
        super.doAfterLoad();
    }

    /**
     *
     */
    @Override
    public final void doBeforeSave() {
        super.doBeforeSave();
    }

    /**
     * Si i est négatif, renvoi depuis la fin (size-i)
     *
     * @param i
     * @return
     */
    public final double getCalculatedLongueurMarche(int i) {
        if (i < 0) {
            return getLongueur(Fmarches.size() + i);
        }
        return getLongueur(Math.abs(i));
    }

    /**
     * Si i est négatif, renvoi depuis la fin (size-i)
     *
     * @param i
     * @return
     */
    public final double getCalculatedHauteurMarche(int i) {
        if (i < 0) {
            return getHauteur(Fmarches.size() + i);
        }
        return getHauteur(Math.abs(i));
    }

    @Override
    public void calculPercages() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


}
