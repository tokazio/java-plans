/*
 * Copyright (C) 2014 rpetit
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package evrLib.gc;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Nomenclature des pièces 3D (pour vue 3D)
 *
 * @author rpetit
 */
public class Tnomenclature3D {
    //Document XML
    private final Document doc;

    /**
     * Ouvre le fichier XML de nomenclature 3D
     *
     * @param aFileName Nom du fichier XML à ouvrir
     * @throws java.io.FileNotFoundException                  Fichier non trouvé
     * @throws javax.xml.parsers.ParserConfigurationException Erreur XML
     * @throws org.xml.sax.SAXException                       Erreur XML
     * @throws java.io.IOException                            Erreur d'accès fichier
     */
    public Tnomenclature3D(String aFileName) throws FileNotFoundException, ParserConfigurationException, SAXException, IOException {
        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = builderFactory.newDocumentBuilder();
        doc = builder.parse(new FileInputStream(aFileName));
    }

    /**
     * @param aNodelist    Noeud et ses enfants
     * @param aStlReplacer Action à executer sur chaque enfants
     * @throws java.lang.Exception Erreur ?
     */
    public final static void execute(NodeList aNodelist, TcallbackOnElement aStlReplacer) throws Exception {
        for (int i = 0; i < aNodelist.getLength(); i++) {
            if (aNodelist.item(i).getNodeType() == Node.ELEMENT_NODE) {
                aStlReplacer.execute((Element) aNodelist.item(i));
            }
        }
    }

    /**
     * @param aXpath Chemin du noeud (dans la racine)
     * @return Retourne le noeud et ses noeud enfants correspondant au chemin indiqué
     */
    public final NodeList get(String aXpath) {
        XPath xpath = XPathFactory.newInstance().newXPath();
        NodeList nodes = null;
        try {
            return (NodeList) xpath.compile(aXpath).evaluate(doc, XPathConstants.NODESET);
        } catch (XPathExpressionException ex) {
            return null;
        }
    }

}
