/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evrLib.gc;

import evrLib.enumeration.Egamme;
import evrLib.enumeration.Epercage;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.HashMap;

/**
 * @author rpetit
 */
public class TconfigurationPlat extends Tconfiguration {

    //---------------------------------------------------------------------
    // A ENTRER------------------------------------------------------------
    //---------------------------------------------------------------------
    //
    private int FdebordVerreHaut;
    //
    private int Fdecalsol;
    //
    private double FdebordPotGauche;
    //
    private double FdebordPotDroite;
    //
    private boolean FangleGauche;
    //
    private int FangleGaucheDir;
    //
    private boolean FfixMurGauche;
    //
    private boolean FanglePotGauche;
    //
    private boolean FtournerPotGauche;
    //
    private boolean FtournerPot;
    //
    private boolean FcoterDebGauche;
    //
    private boolean FangleDroite;
    //
    private int FangleDroiteDir;
    //
    private boolean FfixMurDroite;
    //
    private boolean FanglePotDroite;
    //
    private boolean FtournerPotDroite;
    //
    private boolean FcoterDebDroite;
    //
    private boolean FnoPremPot;
    //
    private boolean FnoDerPot;
    //
    private String FlettreA = "";
    //
    private String FlettreB = "";
    //---------------------------------------------------------------------
    // -------------------------------------------------------------
    // calculé
    //---------------------------------------------------------------------
    //---------------------------------------------------------------------
    private double FcalculatedHauteurFinie;
    private double FcalculatedDebordVerreSol;
    private double FcalculatedPosVerre;
    private double FcalculatedPosTravBas;
    private double FcalculatedHauteurVerre;
    private double FcalculatedEntraxePinces;
    private double FcalculatedHauteurPotelet;
    private double FcalculatedHauteurPoteletAngle;
    private double FcalculatedAxePercPremTrav;
    private double FcalculatedEntraxePotReel;
    private double FcalculatedEspPotReel;
    private double FcalculatedDebordPotGauche;
    private double FcalculatedDebordPotDroite;
    //
    private boolean FisMuret;

    /**
     * Constructeur
     * <p>
     * Charge le fichier 'plat.def' par défaut.
     *
     * @param loadDefault
     * @throws java.lang.Exception
     */
    public TconfigurationPlat(boolean loadDefault) throws Exception {
        super();
        if (loadDefault) {
            try {
                loadFromFile("plat.def");
            } catch (IOException | ParserConfigurationException | SAXException ex) {
                throw new Exception("Erreur lors du chargement par défaut du fichier 'plat.def' de la nouvelle configuration.\n" + ex.getMessage());
            }
        }
    }

    /**
     * @return
     */
    public final int getDebordVerreHaut() {
        return FdebordVerreHaut;
    }

    /**
     * @param i
     */
    public final void setDebordVerreHaut(int i) {
        FdebordVerreHaut = i;
    }

    /**
     * @param s
     */
    public final void setDebordVerreHaut(String s) {
        if (s.isEmpty()) {
            s = "0";
        }
        setDebordVerreHaut((int) Double.parseDouble(s));
    }

    /**
     * @return
     */
    public final int getDecalSol() {
        return Fdecalsol;
    }

    /**
     * @param i
     */
    public final void setDecalSol(int i) {
        Fdecalsol = i;
    }

    /**
     * @param s
     */
    public final void setDecalSol(String s) {
        if (s.isEmpty()) {
            s = "0";
        }
        setDecalSol(Integer.parseInt(s));
    }

    /**
     * @return
     */
    public final double getDebordPotGauche() {
        return FdebordPotGauche;
    }

    /**
     * @param d
     */
    public final void setDebordPotGauche(double d) {
        FdebordPotGauche = d;
    }

    /**
     * @param s
     */
    public final void setDebordPotGauche(String s) {
        if (s.isEmpty()) {
            s = "0";
        }
        setDebordPotGauche(Double.parseDouble(s));
    }

    /**
     * @return
     */
    public final double getDebordPotDroite() {
        return FdebordPotDroite;
    }

    /**
     * @param d
     */
    public final void setDebordPotDroite(double d) {
        FdebordPotDroite = d;
    }

    /**
     * @param s
     */
    public final void setDebordPotDroite(String s) {
        if (s.isEmpty()) {
            s = "0";
        }
        setDebordPotDroite(Double.parseDouble(s));
    }

    /**
     * @return
     */
    public final boolean getAngleGauche() {
        return FangleGauche;
    }

    /**
     * @param b
     */
    public final void setAngleGauche(boolean b) {
        FangleGauche = b;
    }

    /**
     * @return
     */
    public final int getAngleGaucheDir() {
        return FangleGaucheDir;
    }

    /**
     * @param i
     */
    public final void setAngleGaucheDir(int i) {
        FangleGaucheDir = i;
    }

    /**
     * @return
     */
    public final boolean getFixMurGauche() {
        return FfixMurGauche;
    }

    /**
     * @param b
     */
    public final void setFixMurGauche(boolean b) {
        FfixMurGauche = b;
    }

    /**
     * @return
     */
    public final boolean getAnglePotGauche() {
        return FanglePotGauche;
    }

    /**
     * @param b
     */
    public final void setAnglePotGauche(boolean b) {
        FanglePotGauche = b;
    }

    /**
     * @return
     */
    public final boolean getTournerPotGauche() {
        return FtournerPotGauche;
    }

    /**
     * @param b
     */
    public final void setTournerPotGauche(boolean b) {
        FtournerPotGauche = b;
    }

    /**
     * @return
     */
    public final boolean getTournerPot() {
        return FtournerPot;
    }

    /**
     * @param b
     */
    public final void setTournerPot(boolean b) {
        FtournerPot = b;
    }

    /**
     * @return
     */
    public final boolean getCoterDebGauche() {
        return FcoterDebGauche;
    }

    /**
     * @param b
     */
    public final void setCoterDebGauche(boolean b) {
        FcoterDebGauche = b;
    }

    /**
     * @return
     */
    public final boolean getAngleDroite() {
        return FangleDroite;
    }

    /**
     * @param b
     */
    public final void setAngleDroite(boolean b) {
        FangleDroite = b;
    }

    /**
     * @return
     */
    public final int getAngleDroiteDir() {
        return FangleDroiteDir;
    }

    /**
     * @param i
     */
    public final void setAngleDroiteDir(int i) {
        FangleDroiteDir = i;
    }

    /**
     * @return
     */
    public final boolean getFixMurDroite() {
        return FfixMurDroite;
    }

    /**
     * @param b
     */
    public final void setFixMurDroite(boolean b) {
        FfixMurDroite = b;
    }

    /**
     * @return
     */
    public final boolean getAnglePotDroite() {
        return FanglePotDroite;
    }

    /**
     * @param b
     */
    public final void setAnglePotDroite(boolean b) {
        FanglePotDroite = b;
    }

    /**
     * @return
     */
    public final boolean getTournerPotDroite() {
        return FtournerPotDroite;
    }

    /**
     * @param b
     */
    public final void setTournerPotDroite(boolean b) {
        FtournerPotDroite = b;
    }

    /**
     * @return
     */
    public final boolean getCoterDebDroite() {
        return FcoterDebDroite;
    }

    /**
     * @param b
     */
    public final void setCoterDebDroite(boolean b) {
        FcoterDebDroite = b;
    }

    /**
     * @return
     */
    public final boolean getNoPremPot() {
        return FnoPremPot;
    }

    /**
     * @param b
     */
    public final void setNoPremPot(boolean b) {
        FnoPremPot = b;
    }

    /**
     * @return
     */
    public final boolean getNoDerPot() {
        return FnoDerPot;
    }

    /**
     * @param b
     */
    public final void setNoDerPot(boolean b) {
        FnoDerPot = b;
    }

    /**
     * @return
     */
    public final String getLettreA() {
        return FlettreA;
    }

    /**
     * @param s
     */
    public final void setLettreA(String s) {
        FlettreA = s;
    }

    /**
     * @return
     */
    public final String getLettreB() {
        return FlettreB;
    }

    /**
     * @param s
     */
    public final void setLettreB(String s) {
        FlettreB = s;
    }

    /**
     * @return
     */
    public final double getCalculatedHauteurFinie() {
        return FcalculatedHauteurFinie;
    }

    /**
     * @return
     */
    public final double getCalculatedDebordVerreSol() {
        return FcalculatedDebordVerreSol;
    }

    /**
     * @return
     */
    public final double getCalculatedPosVerre() {
        return FcalculatedPosVerre;
    }

    /**
     * @return
     */
    public final double getCalculatedPosTravBas() {
        return FcalculatedPosTravBas;
    }

    /**
     * @return
     */
    public final double getCalculatedHauteurVerre() {
        return FcalculatedHauteurVerre;
    }

    /**
     * @return
     */
    public final double getCalculatedEntraxePinces() {
        return FcalculatedEntraxePinces;
    }

    /**
     * @return
     */
    public final double getCalculatedHauteurPotelet() {
        return FcalculatedHauteurPotelet;
    }

    /**
     * @return
     */
    public final double getCalculatedAxePercPremTrav() {
        return FcalculatedAxePercPremTrav;
    }

    /**
     * @return
     */
    public final double getCalculatedEntraxePotReel() {
        return FcalculatedEntraxePotReel;
    }

    /**
     * @return
     */
    public final double getCalculatedEspPotReel() {
        return FcalculatedEspPotReel;
    }

    /**
     * @return
     */
    public final double getCalculatedDebordPotGauche() {
        return FcalculatedDebordPotGauche;
    }

    //---------------------------------------------------------------------
    //---------------------------------------------------------------------
    //---------------------------------------------------------------------
    //---------------------------------------------------------------------

    /**
     * @return
     */
    public final double getCalculatedDebordPotDroite() {
        return FcalculatedDebordPotDroite;
    }

    /**
     * @throws java.lang.IllegalAccessException
     */
    @Override
    public final void print() throws IllegalArgumentException, IllegalAccessException {
        super.print();
        System.out.println("\n spécifique à la configuration 'à plat':\n");
        System.out.println("Hauteur finie: " + FcalculatedHauteurFinie);
        System.out.println("VERRE/TOLE:");
        System.out.println("Debord verre/tôle sol: " + FcalculatedDebordVerreSol);
        System.out.println("Position verre/tôle: " + FcalculatedPosVerre);
        System.out.println("Hauteur verre/tôle: " + FcalculatedHauteurVerre);
        System.out.println("Entraxe pince: " + FcalculatedEntraxePinces);
        System.out.println("POTELETS:");
        System.out.println("Hauteur potelet: " + FcalculatedHauteurPotelet);
        System.out.println("Hauteur potelet d'angle: " + FcalculatedHauteurPoteletAngle);
        System.out.println("Entraxe potelet réel: " + FcalculatedEntraxePotReel);
        System.out.println("Espacement potelet réel: " + FcalculatedEspPotReel);
        System.out.println("TRAVERSES:");
        System.out.println("Axe perçage traverse haut: " + FcalculatedAxePercPremTrav);
    }

    /**
     * @return
     */
    @Override
    public Tconfiguration calcul() {

        System.out.println("TconfigurationPlat::calcul()");

        double l;
        double cnbrpot;
        // pour eviter division par 0
        if (getEspacementPotMax() == 0) {
            return this;
        }
        // Calcul le nombre de poteau et l'espacement-------------------------------------------------
        l = (getLongueur() - FdebordPotGauche - FdebordPotDroite);
        cnbrpot = (Math.ceil(l / getEspacementPotMax()));
        // 2 poteaux mini ou j'ajoute le poteau de fin
        if (cnbrpot == 0) {
            cnbrpot = 2;
        } else {
            cnbrpot = cnbrpot + 1;
        }
        // calcul de l'entraxe réel
        FcalculatedEntraxePotReel = (getLongueur() - FdebordPotGauche - FdebordPotDroite) / (cnbrpot - 1);
        // calcul de l'espacement reel
        FcalculatedEspPotReel = FcalculatedEntraxePotReel - FLARGPOT;
        // si pas calcul du premier/dernier poteau
        // if inputs.notpremier then Fnbr_poto:=Fnbr_poto-1;
        // if inputs.notdernier then Fnbr_poto:=Fnbr_poto-1;
        setCalculatedPotPosZ(FaxeZ + getDecalZ());
        //=====================================================================
        //BALUSTRES
        //=====================================================================
        Fbalustres.clear();
        if (getIsBal()) {
            // espacement balustre + largeur balustre
            l = getEspacementBalMax() + FLARGBAL;
            // divise l'espacement réel poteau par l'espacement balustre (+ largeur balustre) pour avoir le nombre réel de répétition
            setCalculatedNbrBal(Math.ceil((FcalculatedEspPotReel + FLARGPOT) / l) - 1);
            if (getCalculatedNbrBal() > 0) {
                // calcul l'entraxe réel
                setCalculatedEntraxeBalReel((FcalculatedEspPotReel + FLARGPOT) / (getCalculatedNbrBal() + 1));
                // calcul l'espacement réel
                setCalculatedEspBalReel(getCalculatedEntraxeBalReel() - FLARGBAL);
            }
            // si plus d'un espace entre poteau, multiplie pour avoir le nombre total
            if (cnbrpot > 2) {
                setCalculatedTotBal(getCalculatedNbrBal() * (cnbrpot - 1));
            } else {
                setCalculatedTotBal(getCalculatedNbrBal());
            }
            // si pas de potelet, ajoute le nombre de potelet en balustre
            if (getSansPotelet()) {
                setCalculatedTotBal(getCalculatedTotBal() + cnbrpot);
            } else {
                //si pas de dernier pot, ajoute un balustre
                if (this.getNoDerPot()) {
                    setCalculatedTotBal(getCalculatedTotBal() + 1);
                }
                //si pas de 1er pot, ajoute un balustre
                if (this.getNoPremPot()) {
                    setCalculatedTotBal(getCalculatedTotBal() + 1);
                }
            }
            //ajoute les balustre dans la liste
            Tbalustre b = new Tbalustre();
            //fixation traverse du haut
            double a = (getHauteurBal() - Math.abs(getPosTravHaute() - getPosTravBasse())) / 2;
            //a=77.5
            b.addPercage(new Tpercage(Epercage.code9, a));
            //System.out.println("haut: ("+getHauteurBal()+"-Math.abs("+getPosTravHaute()+"-"+getPosTravBasse()+")) / 2="+a);
            //fixation traverse du bas 842.5
            b.addPercage(new Tpercage(Epercage.code9, getHauteurBal() - a));
            //System.out.println("bas: "+getHauteurBal()+"-"+a+"="+(getHauteurBal() - a));
            System.out.println(getCalculatedTotBal() + " balustres");
            for (int i = 0; i < getCalculatedTotBal(); i++) {
                Fbalustres.add(b);
            }
        }

        if (getInversePotBalGauche()) {
            FcalculatedDebordPotGauche = FdebordPotGauche + getCalculatedEntraxeBalReel();
        } else {
            FcalculatedDebordPotGauche = FdebordPotGauche;
        }
        if (getInversePotBalDroite()) {
            FcalculatedDebordPotDroite = FdebordPotGauche + getCalculatedEntraxeBalReel();
        } else {
            FcalculatedDebordPotDroite = FdebordPotDroite;
        }

        double a, a2, b;
        FcalculatedHauteurFinie = Math.round(getHauteurFinie() + Fdecalsol);
        FcalculatedDebordVerreSol = Math.round(getDebordVerreSol() + Fdecalsol);
        FcalculatedPosVerre = Math.round(getPosVerre() + Fdecalsol);
        if (getMainCourante()) {
            setCalculatedHauteurSousMC(FcalculatedHauteurFinie - FHMC);
        } else {
            setCalculatedHauteurSousMC(FcalculatedHauteurFinie);
        }
        if (getVerreIsComplet()) {
            FcalculatedPosVerre = Math.round(getCalculatedHauteurSousMC() - FdebordVerreHaut);
            FcalculatedHauteurVerre = getCalculatedHauteurSousMC() - getDebordVerreSol() - getDebordVerreHaut() - getDecalSol();
        } else {
            FcalculatedHauteurVerre = getPosVerre() - getDebordVerreSol();
        }
        FcalculatedEntraxePinces = FcalculatedHauteurVerre - getAxePinceHaut() - getAxePinceBas();
        //=====================================================================
        //TRAVERSES
        //=====================================================================
        Ftraverses.clear();
        if (getNbrTraverses() > 0) {
            //épaisseur traverse
            double c = FHTRAV;
            if (getTraversesCable()) {
                c = FHCABLE;
            }
            //
            double nbTrav;
            double h;
            //espace entre dessous de la MC et dessus du vitrage/de la traverse en bas
            if (getIsVerre()) {
                setCalculatedHauteurTraverses(getCalculatedHauteurSousMC() - getPosVerre() - Fdecalsol);
                nbTrav = getNbrTraverses();
                h = getCalculatedHauteurTraverses();
            } else {
                setCalculatedHauteurTraverses(getCalculatedHauteurSousMC() - getDebordTravBas() - Fdecalsol);
                nbTrav = getNbrTraverses() - 1;
                h = getCalculatedHauteurTraverses() - FHTRAV / 2;
            }
            if (getNbrTravSerre() > 0) {
                setCalculatedHauteurTraverses(getCalculatedHauteurSousMC() - getDebordTravBas() - Fdecalsol - ((getNbrTravSerre() - 1) * getEspTravSerre()));
                nbTrav = getNbrTraverses() - getNbrTravSerre();
                h = getCalculatedHauteurTraverses() - c / 2;
            }

            //retire l'épaisseur des traverses
            a = h - (nbTrav * c);
            //divise par le nombre d'espacement pour avoir l'espacement exacte entre chaques traverses, espacement identique
            double espacement = a / (nbTrav + 1);
            //System.out.println("Espacement: " + espacement);
            //a devient la position à l'axe des traverses
            //crée la 1ère traverse (haute)
            a = getCalculatedHauteurSousMC() - espacement - c / 2;
            //System.out.println("1ère: " + getCalculatedHauteurSousMC() + "-" + espacement + "-" + (c / 2) + "=" + a);
            Ftraverses.add(new Ttraverse(a, c));
            //crée les traverses suivante
            if (nbTrav > 1) {
                for (int i = 0; i < nbTrav - 1; i++) {
                    b = a;
                    a = a - espacement - c;
                    //System.out.println((i + 2) + "ème: " + b + "-" + espacement + "-" + c + "=" + a);
                    Ftraverses.add(new Ttraverse(a, c));
                }
            }
            //trav serrée
            if (getNbrTravSerre() > 0) {
                a = a - espacement - c;
                for (int i = 0; i < getNbrTravSerre() - 1; i++) {
                    Ftraverses.add(new Ttraverse(a, c));
                    a = a - getEspTravSerre();
                }
            }
            //dernière trav
            if (!getIsVerre()) {
                a = getDebordTravBas() + getDecalSol();
                Ftraverses.add(new Ttraverse(a, c));
            }
        } else if (getGamme().equals(Egamme.monoprofil)) {
            Ttraverse t = new Ttraverse();
            if (getIsBal()) {
                double x = getDebordPotGauche();
                for (int i = 0; i < getCalculatedTotBal(); i++) {
                    t.addPercage(new Tpercage(Epercage.code9, x));
                    x += getCalculatedEntraxeBalReel();
                }
            }
            Ftraverses.add(t);
            Ftraverses.add(t);
        }
        //=====================================================================
        //=====================================================================
        // si mc
        a = FHLIAISON;
        if (!getMainCourante()) {
            a = FHBOUCHONPOT;
        }
        // si mc
        a2 = FHLIAISONANGLE;
        if (!getMainCourante()) {
            a2 = FHBOUCHONPOT;
        }
        // si sabot
        b = FHSABOT;
        // sinon
        if (getIsScellement() || getIsFacade()) {
            b = 0;
        }
        FcalculatedHauteurPotelet = getCalculatedHauteurSousMC() - a - b;

        FcalculatedHauteurPoteletAngle = getCalculatedHauteurSousMC() - a2 - b;

        if (getIsScellement()) {
            FcalculatedHauteurPotelet += getHauteurScellement();
            FcalculatedHauteurPoteletAngle += getHauteurScellement();
        }
        if (getIsFacade()) {
            FcalculatedHauteurPotelet += getFacAxeVertPlatine() + getFacDecalHauteurPlatine();
            FcalculatedHauteurPoteletAngle += getFacAxeVertPlatine() + getFacDecalHauteurPlatine();
        }
        if (getIsMuret()) {
            FcalculatedHauteurPotelet += -getDecalSol();
            FcalculatedHauteurPoteletAngle += -getDecalSol();
        }

        FcalculatedAxePercPremTrav = getCalculatedEntraxeTraverse() - FHLIAISON;
        // test;

        setCalculatedDebBasBal((getCalculatedHauteurSousMC() - Fdecalsol - getHauteurBal()) / 2);

        if (getIsVerre()) {
            FcalculatedPosTravBas = getPosVerre();
        } else {
            FcalculatedPosTravBas = 0;
        }
        //=====================================================================
        //POTELETS
        //=====================================================================
        Fpotelets.clear();
        String t = "";
        if (getAnglePotGauche()) {
            t = "AG";
        } else {
            t = "G";
        }
        //1er potelet
        Fpotelets.add(new Tpotelet(0, t, getCodePotelet(), getCalculatedHauteurPotelet(), getGamme(), getNom()));
        Fpotelets.first().setActif(!getNoPremPot());
        //potelets intermédiaires
        for (int i = 1; i < cnbrpot - 1; i++) {
            Fpotelets.add(new Tpotelet(i, "M", getCodePotelet(), getCalculatedHauteurPotelet(), getGamme(), getNom()));
        }
        if (getAnglePotDroite()) {
            t = "AD";
        } else {
            t = "D";
        }
        //dernier potelet
        Fpotelets.add(new Tpotelet((int) cnbrpot - 1, t, getCodePotelet(), getCalculatedHauteurPotelet(), getGamme(), getNom()));
        Fpotelets.last().setActif(!getNoDerPot());
        //
        calculPercages();
        //
        return this;
    }

    /**
     *
     */
    @Override
    public final void doAfterLoad() {
        super.doAfterLoad();
    }

    /**
     *
     */
    @Override
    public final void doBeforeSave() {
        super.doBeforeSave();
    }

    /**
     * @return
     */
    @Override
    public String toString() {
        return new StringBuffer(" Longueur : ")
                .append(getLongueur())
                .append(" Nom : ")
                .append(getNom()).toString();
    }

    /**
     * @return
     */
    public final boolean getIsMuret() {
        return FisMuret;
    }

    /**
     * @param b
     */
    public final void setIsMuret(boolean b) {
        FisMuret = b;
    }

    /**
     * @return
     */
    private TconfigurationPlat getConfig() {
        return this;
    }

    /**
     *
     */
    @Override
    public final void calculPercages() {
        //position des éléments nécessitant perçage
        HashMap<String, Double> positions = new HashMap();
        // ---------------------------
        // ---------------------------
        // COTES PERCAGES
        // ---------------------------
        // ---------------------------
        double a = 0;
        //c est la colone sur laquelle afficher la cote
        double c = 0;
        // épaisseur mc
        double b = 0;
        //épaisseur liaison 0 si pas de mc
        double d = 0;

        if (getConfig().getMainCourante()) {
            d = getConfig().FHLIAISON;
            b = d + getConfig().FHMC;
        } else {
            d = getConfig().FHBOUCHONPOT;
            b = d;
        }
        //a est le point de départ du haut du potelet
        a = getConfig().getCalculatedHauteurFinie() - b;
        //cote percage balustres
        /*
         if (getConfig().getIsBal()) {
         c = 2;
         // balustre trav haute
         p1 = new rpoint(ox - (c * e), oy - z(getConfig().getCalculatedDebBasBal() + getConfig().getHauteurBal() + getConfig().getDecalSol()));
         p2 = new rpoint(ox - (c * e), oy - z(getConfig().getDecalSol() + getConfig().getPosTravHaute()));
         cotevt(p2, p1, (getConfig().getHauteurBal() - (getConfig().getPosTravHaute() - getConfig().getPosTravBasse()) - getConfig().FHTRAV) / 2 + (getConfig().FHTRAV / 2));
         //
         c = 3;
         // balustre trav basse
         p1 = new rpoint(ox - (c * e), oy - z(getConfig().getCalculatedDebBasBal() + getConfig().getHauteurBal() + getConfig().getDecalSol()));
         p2 = new rpoint(ox - (c * e), oy - z(getConfig().getDecalSol() + getConfig().getPosTravBasse()));
         cotevt(p2, p1, (getConfig().getHauteurBal() - (getConfig().getPosTravHaute() - getConfig().getPosTravBasse()) - getConfig().FHTRAV) / 2 + (getConfig().FHTRAV / 2) + (getConfig().getPosTravHaute() - getConfig().getPosTravBasse()));
         //
         c = 4;
         if (!getConfig().getSansPotelet()) {
         // traverse haute
         p1 = new rpoint(ox - (c * e), oy - z(a));
         p2 = new rpoint(ox - (c * e), oy - z(getConfig().getDecalSol() + getConfig().getPosTravHaute()));
         cotevt(p2, p1, getConfig().getCalculatedHauteurSousMC() - getConfig().getPosTravHaute() - getConfig().FHLIAISON - getConfig().getDecalSol());
         //
         c = 5;
         // traverse basse
         p1 = new rpoint(ox - (c * e), oy - z(a));
         p2 = new rpoint(ox - (c * e), oy - z(getConfig().getDecalSol() + getConfig().getPosTravBasse()));
         cotevt(p2, p1, getConfig().getCalculatedHauteurSousMC() - getConfig().getPosTravBasse() - getConfig().FHLIAISON - getConfig().getDecalSol());
         //
         c = 6;
         // perçage 56 monoprofil
         p1 = new rpoint(ox - (c * e), oy - z(a));
         p2 = new rpoint(ox - (c * e), oy - z(getConfig().FHSABOT + 56));
         cotevt(p2, p1, getConfig().getCalculatedHauteurPotelet() - 56);
         }
         }
         */
        //cote percage traverses
        if (!getConfig().getSansPotelet() && !getConfig().getVerreIsComplet()) {
            if (!getConfig().getIsBal()) {
                c = -1;
                for (int i = 0; i < getConfig().getTraverses().size(); i++) {
                    //
                    b = getConfig().getTraverse(i).getPosition();
                    positions.put("T" + i, (a - b));
                    c--;
                }
                c = getConfig().getTraverses().size();
            }
        }

        //cote percage verre tole
        if (getConfig()
                .getIsVerre()) {
            // pince haut
            b = getConfig().getCalculatedDebordVerreSol() + getConfig().getAxePinceBas() + getConfig().getCalculatedEntraxePinces();
            positions.put("V0", (a - b));
            c++;
            // pince bas
            b = getConfig().getCalculatedDebordVerreSol() + getConfig().getAxePinceBas();
            positions.put("V1", (a - b));
            c++;
        }

        //cotes potelet
        if (!getConfig()
                .getSansPotelet()) {
            positions.put("P", getConfig().getCalculatedHauteurPotelet());
        }

        this.Fpotelets.stream().forEach((p) -> {
            p.setPercages(positions);
        });
    }

    /**
     * @return
     */
    public final double getCalculatedHauteurPoteletAngle() {
        return FcalculatedHauteurPoteletAngle;
    }

}
