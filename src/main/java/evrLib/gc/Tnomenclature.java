/*
 * Copyright (C) 2014 rpetit
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package evrLib.gc;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

/**
 * @author rpetit
 */
public class Tnomenclature {

    //
    public static TnomenclatureFile nomenclatureGamme;
    //
    private ObservableList<TnomenclatureRef> refs = FXCollections.observableArrayList();
    //
    private TgescomFile gescom;


    public Tnomenclature() {

    }

    /**
     * @param aFileName
     * @throws javax.xml.parsers.ParserConfigurationException
     * @throws org.xml.sax.SAXException
     * @throws java.io.IOException
     */
    public Tnomenclature(String aFileName) throws ParserConfigurationException, SAXException, IOException {
        nomenclatureGamme = new TnomenclatureFile(aFileName);
    }

    /**
     * @param aNodelist
     * @param aNomReplacer
     * @throws java.lang.Exception
     */
    public final static void execute(NodeList aNodelist, TcallbackOnElement aNomReplacer) throws Exception {
        for (int i = 0; i < aNodelist.getLength(); i++) {
            if (aNodelist.item(i).getNodeType() == Node.ELEMENT_NODE) {
                aNomReplacer.execute((Element) aNodelist.item(i));
            }
        }
    }

    public void linkGescom(TgescomFile aGescom) {
        this.gescom = aGescom;
    }

    /**
     * @return
     */
    public final ObservableList<TnomenclatureRef> getRefs() {
        return this.refs;
    }

    public int size() {
        return this.refs.size();
    }

    /**
     * @return
     */
    public Tnomenclature clear() {
        refs.clear();
        return this;
    }

    public Tnomenclature add(String aRef, String qtt) throws Exception {
        return add(aRef, Double.parseDouble(qtt));
    }

    /**
     * @param aRef
     * @param qtt
     * @return
     */
    public Tnomenclature add(String aRef, double qtt) throws Exception {
        //recherche si c'est un ensemble
        NodeList s = getFromGamme("//ensembles/" + aRef + "/*");
        if (s != null && s.getLength() > 0) {
            //System.out.println(aRef+" est un sous ensemble------------------------");
            //si s'en est un, ajoute le sous ensemble
            add(s, qtt);
            //System.out.println("------------------");
        } else {
            //sinon ajute la référence
            //si elle existe
            TnomenclatureRef ref = exists(aRef);
            if (ref != null) {
                //System.out.println("Ajout de "+aRef+" x "+qtt);
                //additionne les qtt
                ref.add(qtt);
            } else {
                //System.out.println("Crée "+aRef+" x "+qtt);
                //sinon la crée
                if (gescom != null) {
                    refs.add(new TnomenclatureRef(gescom.getFromRef(aRef), qtt));
                } else {
                    refs.add(new TnomenclatureRef(aRef, qtt));
                }
            }
        }
        return this;
    }

    //ajoute un sous ensemble
    private void add(NodeList aSousEnsemble, double qtt) throws Exception {
        execute(aSousEnsemble, new TcallbackOnElement() {
            @Override
            public void execute(Element e) {
                //System.out.println("\t ..."+e.getNodeName());
                try {
                    add(e.getNodeName(), Double.parseDouble(e.getAttribute("qtt")) * qtt);
                } catch (Exception ex) {
                    System.out.println(ex.getClass().getName() + ": " + ex.getMessage());
                }
            }
        });
    }

    /**
     * @param aRef
     * @return
     */
    public TnomenclatureRef exists(String aRef) {
        for (TnomenclatureRef ref : refs) {
            if (ref.getRef().equals(aRef)) {
                return ref;
            }
        }
        return null;
    }

    public NodeList getFromGamme(String aRef) {
        return nomenclatureGamme.get(aRef);
    }

}
