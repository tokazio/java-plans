/*
 * Copyright (C) 2014 rpetit
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package evrLib.gc;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * @author rpetit
 */
public class TnomenclatureRef {

    private StringProperty ref;
    private StringProperty designation;
    private DoubleProperty qtt;
    private DoubleProperty prixVente;


    public TnomenclatureRef(TgescomRef aRef, double qtt) {
        this.ref = new SimpleStringProperty(aRef.getRef());
        this.designation = new SimpleStringProperty(aRef.getDesignation());
        this.qtt = new SimpleDoubleProperty(qtt);
        this.prixVente = new SimpleDoubleProperty(aRef.getPrixVente());
    }

    /**
     * @param aRef
     * @param qtt
     */
    public TnomenclatureRef(String aRef, double qtt) {
        this.ref = new SimpleStringProperty(aRef);
        this.qtt = new SimpleDoubleProperty(qtt);
    }

    /**
     * @param qtt
     * @return
     */
    public TnomenclatureRef add(double qtt) {
        this.qtt.set(this.qtt.get() + qtt);
        return this;
    }

    /**
     * @return
     */
    public StringProperty refProperty() {
        return this.ref;
    }

    /**
     * @return
     */
    public String getRef() {
        return ref.get();
    }

    /**
     * @param ref
     * @return
     */
    public TnomenclatureRef setRef(String ref) {
        this.ref.set(ref);
        return this;
    }

    /**
     * @return
     */
    public StringProperty designationProperty() {
        return this.designation;
    }

    /**
     * @return
     */
    public String getDesignation() {
        return designation.get();
    }

    /**
     * @param s
     * @return
     */
    public TnomenclatureRef setDesignation(String s) {
        this.designation.set(s);
        return this;
    }

    /**
     * @return
     */
    public DoubleProperty qttProperty() {
        return this.qtt;
    }

    /**
     * @return
     */
    public DoubleProperty prixVenteProperty() {
        return this.prixVente;
    }

    /**
     * @return
     */
    public double getQtt() {
        return qtt.get();
    }

    /**
     * @param qtt
     * @return
     */
    public TnomenclatureRef setQtt(double qtt) {
        this.qtt.set(qtt);
        return this;
    }

    /**
     * @return
     */
    public double getPrixVente() {
        return prixVente.get();
    }

    /**
     * @param d
     * @return
     */
    public TnomenclatureRef setPrixVente(double d) {
        this.prixVente.set(d);
        return this;
    }


}
