/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evrLib.gc;

import org.w3c.dom.Element;

/**
 * Appelé lors de la traversé de document XML pour executer une action sur les noeuds
 *
 * @author RomainPETIT
 */
public abstract class TcallbackOnElement {

    /**
     * Constructeur
     */
    public TcallbackOnElement() {

    }

    /**
     * @param e Noeud avec lequel agir (Element)
     * @throws java.lang.Exception Erreur avec le noeud (Element)
     */
    public abstract void execute(Element e) throws Exception;

}
