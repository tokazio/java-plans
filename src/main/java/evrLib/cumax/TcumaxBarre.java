/*
 * Copyright (C) 2014 rpetit
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package evrLib.cumax;

import esyoLib.Utils.TstringList;

import java.util.ArrayList;

/**
 * @author rpetit
 */
public class TcumaxBarre {

    private String nom;
    private int code;
    private String description;
    private double longueur;
    private ArrayList<TcumaxMorceau> morceaux = new ArrayList();

    public TcumaxBarre(String aNom, int aCodeProfil, String aDescription, double aLongueur) {
        this.nom = aNom;
        this.code = aCodeProfil;
        this.description = aDescription;
        this.longueur = aLongueur;
    }

    public TcumaxBarre() {

    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getLongueur() {
        return longueur;
    }

    public void setLongueur(double longueur) {
        this.longueur = longueur;
    }

    public void openMorceaux(String s) {
        TstringList l = new TstringList();
        l.fromString(s, ",");
        for (int i = 0; i < l.size(); i++) {
            TcumaxMorceau m = new TcumaxMorceau(s);
            morceaux.add(m);
            /*
             if not fileexists(extractfilepath(application.ExeName) + 'morceaux/' + tmp[I] + '.txt') then
             begin
             // si le morceau n'existe pas
             Val(tmp[I], J, code);
             if code <> 0 then
             begin
             showmessage('Impossible d''ouvrir le morceau ' + tmp[I]);
             end
             else
             begin
             // si c'est une longueur simple
             m := Tmorceau.create;
             m.nom := tmp[I];
             m.longueur := strtoint(tmp[I]);
             morceaux.AddObject(m.nom, m);
             end;
             end
             else
             begin
             // si le morceau existe
             m := Tmorceau.create(extractfilepath(application.ExeName) + 'morceaux/' + tmp[I] + '.txt');
             morceaux.AddObject(m.nom, m)
             end;
             */
        }

    }

    @Override
    public String toString() {
        TstringList tmp = new TstringList();
        tmp.add("nom", nom);
        tmp.add("longueur", Math.round(longueur) + "");
        tmp.add("codeprofil", code + "");
        tmp.add("description", description);
        String m = "";
        for (TcumaxMorceau o : morceaux) {
            m += ",\"\"" + o.toString() + "\"\"";
        }
        tmp.add("morceaux", "5," + m.substring(1));
        return tmp.toString("@");
    }

    public void add(TcumaxMorceau m) {
        this.morceaux.add(m);
    }
}
