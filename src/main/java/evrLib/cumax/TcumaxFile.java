/*
 * Copyright (C) 2014 rpetit
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package evrLib.cumax;

import esyoLib.Exceptions.NoNameException;
import esyoLib.Utils.TstringList;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author rpetit
 */
public class TcumaxFile {

    private ArrayList<TcumaxBarre> barres = new ArrayList();

    public TcumaxFile() {

    }

    public void loadFromFile(String aFileName) throws IOException {
        TstringList l = new TstringList();
        l.setNameValueSeparator('>');
        l.loadFromFile(aFileName);

        for (int i = 0; i < l.size(); i++) {
            TstringList btmp = new TstringList();
            btmp.fromString(l.valueFromIndex(i), "@");
            try {
                TcumaxBarre b = new TcumaxBarre(btmp.valueFromName("nom"), Integer.parseInt(btmp.valueFromName("codeprofil")), btmp.valueFromName("description"), Integer.parseInt(btmp.valueFromName("longueur")));
                b.openMorceaux(btmp.valueFromName("morceaux"));
                barres.add(b);
            } catch (NoNameException ex) {
                Logger.getLogger(TcumaxFile.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void saveToFile(String aFileName) throws IOException {
        TstringList l = new TstringList();
        l.setNameValueSeparator('>');
        int i = 0;
        for (TcumaxBarre barre : barres) {
            l.add("Barre " + i, barre.toString());
            i++;
        }
        l.print();
        //l.saveToFile(aFileName);
    }

    public void add(TcumaxBarre b) {
        this.barres.add(b);
    }


}
