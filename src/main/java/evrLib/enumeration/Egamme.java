/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evrLib.enumeration;

import esyoLib.XML.IxmlSavable;
import esyoLib.XML.TxmlTag;
import org.w3c.dom.Node;

/**
 * @author rpetit
 */
public enum Egamme implements IxmlSavable {

    none("Aucune", "NONE"),
    baludesign("Baludesign", "BALU"),
    monoprofil("Monoprofil", "MONO"),
    chrome("Chrome", "CHR");

    //Nom complet de la gamme (Baludesign,Monoprofil,Chrome)
    private String nom = "";
    //Code de la gamme (BALU,MONO,CHR)
    private String code = "";

    /**
     * Constructeur
     *
     * @param aNom  Nom complet de la gamme (Baludesign,Monoprofil,Chrome)
     * @param aCode Code de la gamme (BALU,MONO,CHR)
     */
    Egamme(String aNom, String aCode) {
        this.nom = aNom;
        this.code = aCode;
    }

    /**
     * Retourne le 'Egamme' coresspondant au texte 's'
     *
     * @param s Nom complet à convertir en Egamme
     * @return Valeur Egamme du nom complet passé en paramètre
     */
    public static Egamme fromString(String s) {
        for (Egamme c : Egamme.values()) {
            if (c.getNom().equals(s)) {
                return c;
            }
        }
        return Egamme.none;
    }

    /**
     * @return Le nom complet de la gamme (Baludesign,Monoprofil,Chrome)
     */
    public final String getNom() {
        return this.nom;
    }

    /**
     * @return Le code de la gamme (BALU,MONO,CHR)
     */
    public final String getCode() {
        return this.code;
    }

    /**
     * Affichage dans une liste ou une combobox par exemple
     *
     * @return Retourne le nom à afficher (nom complet)
     */
    @Override
    public final String toString() {
        return this.nom;
    }

    /**
     * @return Retourne le nom à enregistrer (nom complet)
     */
    public final String saveToString() {
        return this.nom;
    }

    /**
     * Texte du tag XML à enregistrer
     *
     * @param aTag Tag XML
     * @return le tag XML
     */
    @Override
    public TxmlTag saveToXML(TxmlTag aTag) {
        aTag.text(this.nom + "");
        return aTag;
    }

    /**
     * 'Egamme' corespondant au texte du tag XML à charger
     *
     * @param noeud Tag XML
     * @return Chaînage
     * @throws Exception
     */
    @Override
    public Egamme loadFromXML(Node noeud) throws Exception {
        return Egamme.fromString(noeud.getTextContent());
    }
}
