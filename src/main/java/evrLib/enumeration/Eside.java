/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evrLib.enumeration;

import esyoLib.XML.IxmlSavable;
import esyoLib.XML.TxmlTag;
import org.w3c.dom.Node;

/**
 * @author rpetit
 */
public enum Eside implements IxmlSavable {
    none("Aucun", "A"),
    droite("Droite", "D"),
    haut("Haut", "H"),
    bas("Bas", "B"),
    gauche("Gauche", "G"),
    gaucheDroite("Gauche & Droite", "X");

    //nom
    private String Ftext = "";
    //code
    private String Fcode = "";

    /**
     * Constructeur
     *
     * @param aText Nom
     * @param aCode Code
     */
    Eside(String aText, String aCode) {
        Ftext = aText;
        Fcode = aCode;
    }

    /**
     * @param s Nom à convertir en Eside
     * @return Valeur Eside du nom passé en paramètre
     */
    public static Eside fromString(String s) {
        //System.out.println("Eside::fromString(" + s + ")");
        for (Eside c : Eside.values()) {
            if (c.Ftext.equals(s)) {
                return c;
            }
        }
        return Eside.values()[0];
    }

    /**
     * @return Le nom
     */
    public final String text() {
        return Ftext;
    }

    /**
     * @return Le code
     */
    public final String code() {
        return Fcode;
    }

    /**
     * @return Retourne le nom
     */
    @Override
    public final String toString() {
        return Ftext;
    }

    @Override
    public TxmlTag saveToXML(TxmlTag aTag) {
        aTag.text(Ftext + "");
        return aTag;
    }

    @Override
    public Eside loadFromXML(Node noeud) throws Exception {
        return Eside.fromString(noeud.getTextContent());
    }
}
