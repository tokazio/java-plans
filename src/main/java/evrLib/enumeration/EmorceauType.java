/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package evrLib.enumeration;

/**
 * @author rpetit
 */
public enum EmorceauType {

    none, poto, potodeb, potofin, potodebAngle, potofinAngle, mc, trav, boismc, cacherain, bal, baltrav

}
