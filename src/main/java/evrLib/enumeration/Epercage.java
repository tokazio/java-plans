/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evrLib.enumeration;

import esyoLib.XML.IxmlSavable;
import esyoLib.XML.TxmlTag;
import org.w3c.dom.Node;

/**
 * @author rpetit
 */
public enum Epercage implements IxmlSavable {

    none("Aucun", 0, Eside.none),
    code1("BIPROFIL     - Perçages double Ø7    vertical   axe-27", 1, Eside.haut),
    code2("BIPROFIL     - Perçages double Ø7    vertical   axe+27", 2, Eside.haut),
    code3("CHROME40     - Perçages double Ø7    vertical   débouchant (rainure)", 3, Eside.haut),
    code4("MONOPROFIL   - Perçages        Ø5    vertical   débouchant", 4, Eside.haut),
    code5("?            - Perçages              vertical   débouchant (Tube 50) !attention longueur forêt!", 5, Eside.haut),
    code6("BIPROFIL     - Perçages        Ø7-11 horizontal avant + arrière", 6, Eside.gaucheDroite),
    code7("CHROME40     - Perçages        Ø5    vertical   débouchant", 7, Eside.haut),
    code8("BIPROFIL     - Perçages 15mm   Ø7    vertical", 8, Eside.haut),
    code9("MONOPROFIL   - Perçages 15mm   Ø7    vertical", 9, Eside.haut),
    code10("CHROME40     - Perçages        Ø7    horizontal avant", 10, Eside.gauche),
    code11("CHROME40     - Perçages        Ø7    horizontal avant + arrière", 11, Eside.gaucheDroite),
    code12("?            - Perçages              vertical   1ère paroie (Tube 50)", 12, Eside.haut),
    code13("BIPROFIL     - Pointage        Ø7    horizontal arrière", 13, Eside.droite),
    code14("BIPROFIL     - Pointage        Ø7    horizontal avant + arrière", 14, Eside.gaucheDroite),
    code15("BALUDESIGN40 - Perçages        Ø5    vertical   1ère paroie", 15, Eside.haut),
    code16("BALUDESIGN40 - Perçages        Ø9    horizontal avant + arrière", 16, Eside.gaucheDroite),
    code17("BALUDESIGN40 - Perçages        Ø9    horizontal avant", 17, Eside.gauche),
    code18("BALUDESIGN40 - Perçages        Ø9    horizontal arrière", 18, Eside.droite),
    code19("BIPROFIL     - Pointage        Ø7    horizontal avant", 19, Eside.gauche),
    code20("BALUDESIGN40 - Pointage        Ø9    horizontal avant", 20, Eside.gauche),
    code21("BALUDESIGN40 - Pointage        Ø9    horizontal arrière", 21, Eside.droite),
    code22("?            - Perçages 15mm   Ø7    vertical   axe-35 (Monoprofil Colnel)", 22, Eside.haut),
    code23("?            - Perçages 15mm   Ø7    vertical   axe+35 (Monoprofil Colnel)", 23, Eside.haut),
    code24("CHROME50     - Perçages 15mm   Ø9    horizontal avant", 24, Eside.gauche),
    code25("CHROME50     - Perçages 15mm   Ø9    horizontal avant + arrière", 25, Eside.gaucheDroite),
    code26("CHROME40     - Perçages        Ø7    horizontal arrière", 26, Eside.droite),
    code27("BALUDESIGN40 - Pointage        Ø9    horizontal avant + arrière", 27, Eside.gaucheDroite),
    code28("BALUDESIGN40 - Perçages        Ø5    vertical   tout", 28, Eside.haut),
    code29("BALUDESIGN40 - Perçages Ø5 horizontal avant + arrière", 29, Eside.gaucheDroite),
    code30("BALUDESIGN40 - Perçages Ø5 horizontal avant", 30, Eside.gauche),
    code31("BALUDESIGN40 - Perçages Ø5 horizontal arrière", 31, Eside.droite);

    //Texte de description
    private String text = "";
    //Face du perçage (Eside)
    private Eside side = Eside.haut;
    //Code perçage en machine
    private int code = 0;

    /**
     * Constructeur
     *
     * @param aText Texte de description
     * @param aCode Code de perçage en machine
     * @param aSide Face du perçage (Eside)
     */
    Epercage(String aText, int aCode, Eside aSide) {
        this.text = aText;
        this.side = aSide;
        this.code = aCode;
    }

    /**
     * @param s Nom à convertir en Egamme
     * @return Valeur Egamme du nom passé en paramètre
     */
    public static Epercage fromString(String s) {
        for (Epercage c : Epercage.values()) {
            if (c.getCode() == Integer.parseInt(s)) {
                return c;
            }
        }
        return Epercage.values()[0];
    }

    /**
     * @return Le code machine du perçage
     */
    public final int getCode() {
        return this.code;
    }

    /**
     * @return Le nom du perçage
     */
    public final String getText() {
        return this.text;
    }

    /**
     * @return La face (Eside) du percage
     */
    public final Eside getSide() {
        return this.side;
    }

    /**
     * Texte du tag XML à enregistrer
     *
     * @param aTag Tag XML
     * @return le tag XML
     */
    @Override
    public TxmlTag saveToXML(TxmlTag aTag) {
        aTag.text(this.code + "");
        return aTag;
    }

    /**
     * 'Epercage' corespondant au texte du tag XML à charger
     *
     * @param noeud Tag XML
     * @return Chaînage
     * @throws Exception
     */
    @Override
    public Epercage loadFromXML(Node noeud) throws Exception {
        return Epercage.fromString(noeud.getTextContent());
    }

}
