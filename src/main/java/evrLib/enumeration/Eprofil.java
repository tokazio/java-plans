/*
 * Copyright (C) 2014 rpetit
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package evrLib.enumeration;

import esyoLib.XML.IxmlSavable;
import esyoLib.XML.TxmlTag;
import org.w3c.dom.Node;

/**
 * @author rpetit
 */
public enum Eprofil implements IxmlSavable {
    /*
     Choisir...
     01- Monoprofil (à plat vers le bas)
     02- Biprofil
     03- Cache rainure
     04- Tube MC diamètre 50
     05- Poteau diamètre 40 (rainure en bas)
     06- Tube Traverse diamètre 16
     07- Poteau diamètre 40 ep 4
     */

    none("Aucun", 0, Egamme.none, "none"),
    mono("Monoprofil (à plat vers le bas)", 1, Egamme.monoprofil, "MONO"),
    bi("Biprofil", 2, Egamme.monoprofil, "BI"),
    incache("Cache rainure", 3, Egamme.chrome, "INCACHE"),
    inmc50("Tube MC diamètre 50 gamme chrome", 4, Egamme.chrome, "INMC50"),
    inpoth40("Poteau diamètre 40 (rainure en bas)", 5, Egamme.chrome, "INPOTH40"),
    intrav16("Tube Traverse diamètre 16 gamme chrome", 6, Egamme.chrome, "INTRAV16"),
    al40("Poteau diamètre 40 ep 4", 7, Egamme.baludesign, "AL40"),
    al50("Tube MC diamètre 50 gamme baludesign", 4, Egamme.baludesign, "AL50"),
    al16("Tube Traverse diamètre 16 gamme baludesign", 6, Egamme.baludesign, "AL16");

    //Texte de description
    private String text = "";
    //Code du profilé en machine
    private int code = 0;
    //Gamme du profilé
    private Egamme gamme = Egamme.none;
    //Référence du profilé
    private String ref = "";

    /**
     * @param aText
     * @param aCode
     * @param aGamme
     * @param aRef
     */
    Eprofil(String aText, int aCode, Egamme aGamme, String aRef) {
        this.text = aText;
        this.gamme = aGamme;
        this.code = aCode;
        this.ref = aRef;
    }

    /**
     * @param s Nom à convertir en 'Eprofil'
     * @return Valeur du code du profilé passé en paramètre
     */
    public static Eprofil fromString(String s) {
        for (Eprofil c : Eprofil.values()) {
            if (c.getCode() == Integer.parseInt(s)) {
                return c;
            }
        }
        return Eprofil.values()[0];
    }

    /**
     * @param s Nom à convertir en Eprofil
     * @return Valeur du code du profilé passé en paramètre
     */
    public static Eprofil fromRef(String s) {
        for (Eprofil c : Eprofil.values()) {
            if (c.getRef() == null ? s == null : c.getRef().equals(s)) {
                return c;
            }
        }
        return Eprofil.values()[0];
    }

    /**
     * @return Le code machine du profilé
     */
    public final int getCode() {
        return this.code;
    }

    /**
     * @return Le nom du profilé
     */
    public final String getText() {
        return this.text;
    }

    /**
     * @return La référence du profilé
     */
    public final String getRef() {
        return this.ref;
    }

    /**
     * @return La gamme du profilé
     */
    public final Egamme getGamme() {
        return this.gamme;
    }

    /**
     * Texte du tag XML à enregistrer
     *
     * @param aTag Tag XML
     * @return le tag XML
     */
    @Override
    public TxmlTag saveToXML(TxmlTag aTag) {
        aTag.text(this.code + "");
        return aTag;
    }

    /**
     * 'Eprofil' corespondant au texte du tag XML à charger
     *
     * @param noeud Tag XML
     * @return Chaînage
     * @throws Exception
     */
    @Override
    public Eprofil loadFromXML(Node noeud) throws Exception {
        return Eprofil.fromString(noeud.getTextContent());
    }
}
