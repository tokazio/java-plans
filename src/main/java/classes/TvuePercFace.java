/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import esyoLib.Formsfx.Tform;
import esyoLib.Formsfx.TformException;
import esyoLib.Graphics.Tpoint2D;
import evrLib.gc.TconfigurationPlat;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author rpetit
 */
public class TvuePercFace extends TconfigVue2D {

    /**
     * @param c
     * @throws esyoLib.Formsfx.TformException
     */
    public TvuePercFace(TconfigUI c) throws TformException {
        super(c);
    }

    /**
     * @param c
     * @param zoom
     * @throws esyoLib.Formsfx.TformException
     */
    public TvuePercFace(TconfigUI c, String zoom) throws TformException {
        super(c, zoom);
    }

    /**
     * @return
     */
    @Override
    public String titleView() {
        return "Perçages";
    }

    /**
     * @return
     */
    @Override
    public TconfigurationPlat getConfig() {
        return (TconfigurationPlat) super.getConfig();
    }

    /**
     *
     */
    @Override
    public void draw() {
        //largeur de la vue
        double w = getCanvasWidth();
        //hauteur de la vue
        double h = getCanvasHeight();
        //origine x potelet
        double ox = 1.2 * (w / 4);
        //origine x potelet d'angle
        double ox2 = 3.8 * (w / 4);
        //origine y potelet
        double oy = 3 * (h / 4);
        //variables
        double a, a2, b, c;
        //efface
        clear(Color.WHITE);
        //
        Tpoint2D p1, p2;
        //coeff écartement côtes
        int e = 50;
        //-----------------------------------------------------------------------
        /*
         style.setFontColor(Color.VIOLET);
         centerText("Codes perçages: Avant/Dessus/Arrière", w / 2, 10);
         */
        style.setFontColor(Color.GRAY);
        // ---------------------------
        // SOL
        // ---------------------------
        style.normal();
        line(0, oy, w, oy);
        centerText("Sol", ox - 100, oy - 10);
        // ---------------------------
        // SOL FINI
        // ---------------------------
        if (getConfig().getDecalSol() > 0) {
            style.solfini();
            line(ox, oy - z(getConfig().getDecalSol()), w, oy - z(getConfig().getDecalSol()));
            String t = "Sol fini";
            if (getConfig().getIsMuret()) {
                t = "Muret";
            }
            centerText(t, ox - 100, oy - 10 - z(getConfig().getDecalSol()));
        }
        // ---------------------------
        // MC
        // ---------------------------
        if (getConfig().getMainCourante()) {
            style.profile();
            rectangle(ox - 100, oy - z(getConfig().getCalculatedHauteurFinie()), w, oy - z(getConfig().getCalculatedHauteurFinie() - getConfig().FHMC));
        }
        // ---------------------------
        // VERRE/TOLE
        // ---------------------------
        if (getConfig().getIsVerre()) {
            style.verre();
            rectangle(ox + z(getConfig().FLARGPOT), oy - z(getConfig().getCalculatedDebordVerreSol() + getConfig().getCalculatedHauteurVerre()), ox2 - z(getConfig().FLARGPOT), oy - z(getConfig().getCalculatedDebordVerreSol()));
        }
        // ---------------------------
        // Traverses balustre (derrière potelet)
        // ---------------------------
        if (getConfig().getIsBal()) {
            a = getConfig().FLARGPOT / 2;
            // trav haute
            b = getConfig().getPosTravHaute() + getConfig().getDecalSol();
            style.profile();
            rectangle(ox + z(a) - 100, oy - z(b + (getConfig().FHTRAV / 2)), w, oy - z(b - (getConfig().FHTRAV / 2)));
            style.axe();
            line(ox + z(a + 2 * getConfig().getCalculatedEntraxeBalReel()), oy - z(b), w, oy - z(b));
            // trav basse
            b = getConfig().getPosTravBasse() + getConfig().getDecalSol();
            style.profile();
            rectangle(ox + z(a) - 100, oy - z(b + (getConfig().FHTRAV / 2)), w, oy - z(b - (getConfig().FHTRAV / 2)));
            style.axe();
            line(ox + z(a + 2 * getConfig().getCalculatedEntraxeBalReel()), oy - z(b), w, oy - z(b));
        }
        // ---------------------------
        // Potelet
        // ---------------------------
        if (!getConfig().getSansPotelet()) {
            // épaisseur sabot (si sabot)
            a = -getConfig().FHSABOT;
            // hauteur scellement si scellement
            if (getConfig().getIsScellement()) {
                a = getConfig().getHauteurScellement();
            }
            // hauteur platine facade si facade
            if (getConfig().getIsFacade()) {
                a = getConfig().getFacDecalHauteurPlatine() + getConfig().getFacAxeVertPlatine();
            }
            //hauteur sol fini si muret
            if (getConfig().getIsMuret()) {
                a = -getConfig().getDecalSol() + a;
            }
            // épaisseur mc+liaison si mc
            b = getConfig().FHMC + getConfig().FHLIAISON;
            if (!getConfig().getMainCourante()) {
                b = 0;
            }
            // dessin du potelet
            style.profile();
            rectangle(ox + z(-getConfig().FLARGPOT / 2), oy - z((getConfig().getCalculatedHauteurFinie() - b)), ox + z(getConfig().FLARGPOT / 2), oy + z(a));

            // dessin du potelet d'angle

            leftText("Potelet d'angle", ox2 - 50, oy - z(getConfig().getCalculatedHauteurFinie()) - 30);

            // épaisseur mc+liaison si mc
            b = getConfig().FHMC + getConfig().FHLIAISONANGLE;
            if (!getConfig().getMainCourante()) {
                b = 0;
            }
            rectangle(ox2 + z(-getConfig().FLARGPOT / 2), oy - z((getConfig().getCalculatedHauteurFinie() - b)), ox2 + z(getConfig().FLARGPOT / 2), oy + z(a));

            // axe épaisseur sabot
            /*
             if (!getConfig().getIsScellement() && !getConfig().getIsFacade()) {
             style.axe();
             line(ox + z(-getConfig().FLARGPOT / 2), oy + z(-getConfig().FHSABOT), w, oy + z(-getConfig().FHSABOT));
             }
             */
        }
        // ---------------------------
        // Traverses (hors balustre)
        // ---------------------------
        if (!getConfig().getIsBal() && !getConfig().getVerreIsComplet()) {

            //départ gauche traverse
            b = -100;
            if (getConfig().getTraversesAxe()) {
                b = (getConfig().FLARGPOT / 2);
            }
            //
            for (int i = 0; i < getConfig().getTraverses().size(); i++) {
                a = getConfig().getTraverse(i).getPosition();
                c = getConfig().getTraverse(i).getEpaisseur();
                //traverse
                style.profile();
                rectangle(ox + z(b), oy - z(a + (c / 2)), w, oy - z(a - (c / 2)));
                // axe
                style.axe();
                line(0, oy - z(a), w, oy - z(a));
            }

            /*
             //départ bas
             a = getConfig().getCalculatedPosTravBas() + getConfig().getCalculatedEntraxeTraverseBas() + getConfig().getDecalSol();
             //épaisseur traverse
             c = getConfig().FHTRAV;
             if (getConfig().getTraversesCable()) {
             c = getConfig().FHCABLE;
             }
             //départ gauche traverse
             b = -100;
             if (getConfig().getTraversesAxe()) {
             b = (getConfig().FLARGPOT / 2);
             }
             //traverses en bas (serre)
             for (int i = 0; i < getConfig().getNbrTravSerre() - 1; i++) {
             //traverse
             style.profile();
             rectangle(ox + z(b), oy - z(a + (c / 2)), w, oy - z(a - (c / 2)));
             // axe
             style.axe();
             line(0, oy - z(a), w, oy - z(a));
             //suivante
             a += getConfig().getCalculatedEntraxeTraverseSerre();
             }
             //traverses en haut
             double f;
             if (getConfig().getNbrTravSerre() == 0) {
             f = getConfig().getNbrTraverses();
             } else {
             f = getConfig().getNbrTraverses() - getConfig().getNbrTravSerre() + 1;
             }
             for (int i = 0; i < f; i++) {
             //traverse
             style.profile();
             rectangle(ox + z(b), oy - z(a + (c / 2)), w, oy - z(a - (c / 2)));
             // axe
             style.axe();
             line(0, oy - z(a), w, oy - z(a));
             //suivante
             a += getConfig().getCalculatedEntraxeTraverse();
             }
             */
        }
        // ---------------------------
        // BALUSTRES
        // ---------------------------

        if (getConfig()
                .getIsBal()) {
            a = (getConfig().FLARGPOT / 2);
            for (int i = 0; i < getConfig().getCalculatedNbrBal(); i++) {
                style.profile();
                rectangle(
                        ox + a + z(i * getConfig().getCalculatedEntraxeBalReel()),
                        oy - z(getConfig().getCalculatedDebBasBal() + getConfig().getHauteurBal() + getConfig().getDecalSol()),
                        ox + a + z(i * getConfig().getCalculatedEntraxeBalReel() + getConfig().FLARGBAL),
                        oy - z(getConfig().getCalculatedDebBasBal() + getConfig().getDecalSol())
                );
            }
        }
        // ---------------------------
        // PLATINE FACADE
        // ---------------------------

        if (getConfig()
                .getIsFacade()) {
            /*
             try
             DXF(0, z(FfacDecalHauteurPlatine), extractfilepath(application.exename) + 'files\' + GammeNames[ord(Fgamme)] + '\fac\dxf\platine fac.dxf');
             except

             end;
             */
        }
        // ---------------------------
        // ---------------------------
        // axes
        // ---------------------------
        // ---------------------------

        style.axe();

        if (getConfig()
                .getIsVerre() || getConfig().getVerreIsComplet()) {
            // pince haut axe
            a = getConfig().getCalculatedDebordVerreSol() + getConfig().getAxePinceBas() + getConfig().getCalculatedEntraxePinces();
            line(ox, oy - z(a), w, oy - z(a));
            // percages
            //style.percage();
            //line(w/2, h/2-z(getConfig().getCalculatedDebordVerreSol() + getConfig().getAxePinceBas() + getConfig().getCalculatedEntraxePinces()), w+e, h/2-z(getConfig().getCalculatedDebordVerreSol() + getConfig().getAxePinceBas() + getConfig().getCalculatedEntraxePinces()) - e);
            //textOut(e, -z(getConfig().getcalculatedDebordVerreSol() + getConfig().getAxePinceBas() + getConfig().getCalculatedEntraxePinces()) - e - 5, getConfig().getPercagePince());
            // pince bas axe
            style.axe();
            a = getConfig().getCalculatedDebordVerreSol() + getConfig().getAxePinceBas();
            line(ox, oy - z(a), w, oy - z(a));
            // percages
            //style.percage();
            //line(w/2, h/2-z(getConfig().getCalculatedDebordVerreSol() + getConfig().getAxePinceBas()), w/2+e, h/2-z(getConfig().getCalculatedDebordVerreSol() + getConfig().getAxePinceBas()) + e);
            //textOut(e, -z(FcalculatedDebordVerreSol + FaxePinceBas) + e, FpercagePince);
        }
        // ---------------------------
        // ---------------------------
        // cotes
        // ---------------------------
        // ---------------------------
        // hauteur finie
        p1 = new Tpoint2D(ox + (7 * e), oy - z(getConfig().getDecalSol()));
        p2 = new Tpoint2D(ox + (7 * e), oy - z(getConfig().getCalculatedHauteurFinie()));

        cotevt(p2, p1, getConfig().getCalculatedHauteurFinie() - getConfig().getDecalSol());

        //hauteur finie si decalage du sol fini
        if (getConfig().getDecalSol() > 0) {
            p1 = new Tpoint2D(ox + (8 * e), oy - z(0));
            p2 = new Tpoint2D(ox + (8 * e), oy - z(getConfig().getCalculatedHauteurFinie()));
            cotevt(p2, p1, getConfig().getCalculatedHauteurFinie());
        }

        if (getConfig()
                .getMainCourante()) {
            // hauteur sous MC
            p1 = new Tpoint2D(ox + (6 * e), oy - z(getConfig().getDecalSol()));
            p2 = new Tpoint2D(ox + (6 * e), oy - z(getConfig().getCalculatedHauteurSousMC()));
            cotevt(p2, p1, getConfig().getCalculatedHauteurSousMC() - getConfig().getDecalSol());
            // hauteur MC
            p1 = new Tpoint2D(ox + (6 * e), oy - z(getConfig().getCalculatedHauteurFinie()));
            p2 = new Tpoint2D(ox + (6 * e), oy - z(getConfig().getCalculatedHauteurFinie() - getConfig().FHMC));
            cotevt(p1, p2, getConfig().FHMC);
        }

        if (getConfig()
                .getIsBal()) {
            a = 5;
            // traverse balustre haute depuis sol
            if (!getConfig().getSansPotelet()) {
                p1 = new Tpoint2D(ox + a * e, oy - z(getConfig().getDecalSol()));
                p2 = new Tpoint2D(ox + a * e, oy - z(getConfig().getPosTravHaute() + getConfig().getDecalSol()));
                cotevt(p1, p2, getConfig().getPosTravHaute());
            }
            // traverse balustre haute depuis sous mc
            if (getConfig().getMainCourante()) {
                p1 = new Tpoint2D(ox + a * e, oy - z(getConfig().getCalculatedHauteurSousMC()));
                p2 = new Tpoint2D(ox + a * e, oy - z(getConfig().getPosTravHaute() + getConfig().getDecalSol()));
                cotevt(p1, p2, getConfig().getCalculatedHauteurSousMC() - getConfig().getPosTravHaute() - getConfig().getDecalSol());
            }
            //
            a = 4;
            // traverse balustre basse depuis sol
            if (!getConfig().getSansPotelet()) {
                p1 = new Tpoint2D(ox + a * e, oy - z(getConfig().getDecalSol()));
                p2 = new Tpoint2D(ox + a * e, oy - z(getConfig().getPosTravBasse() + getConfig().getDecalSol()));
                cotevt(p1, p2, getConfig().getPosTravBasse());
            }
            // traverse balustre basse depuis sous mc
            if (getConfig().getMainCourante()) {
                p1 = new Tpoint2D(a * e, oy - z(getConfig().getCalculatedHauteurSousMC()));
                p2 = new Tpoint2D(a * e, oy - z(getConfig().getPosTravBasse() + getConfig().getDecalSol()));
                cotevt(p1, p2, getConfig().getCalculatedHauteurSousMC() - getConfig().getPosTravBasse() - getConfig().getDecalSol());
            }
            //
            a = 2;
            // entraxe traverse balustre
            p1 = new Tpoint2D(ox + a * e, oy - z(getConfig().getPosTravHaute() + getConfig().getDecalSol()));
            p2 = new Tpoint2D(ox + a * e, oy - z(getConfig().getPosTravBasse() + getConfig().getDecalSol()));
            cotevt(p1, p2, getConfig().getPosTravHaute() - getConfig().getPosTravBasse());
            // debord balustre traverse basse
            p1 = new Tpoint2D(ox + a * e, oy - z(getConfig().getCalculatedDebBasBal() + getConfig().getDecalSol()));
            p2 = new Tpoint2D(ox + a * e, oy - z(getConfig().getPosTravBasse() + getConfig().getDecalSol() - getConfig().FHTRAV / 2));
            cotevt(p1, p2, (getConfig().getHauteurBal() - (getConfig().getPosTravHaute() - getConfig().getPosTravBasse()) - getConfig().FHTRAV) / 2);
            // debord balustre traverse haute
            p1 = new Tpoint2D(ox + a * e, oy - z(getConfig().getCalculatedDebBasBal() + getConfig().getHauteurBal() + getConfig().getDecalSol()));
            p2 = new Tpoint2D(ox + a * e, oy - z(getConfig().getPosTravHaute() + getConfig().getDecalSol() + getConfig().FHTRAV / 2));
            cotevt(p1, p2, (getConfig().getHauteurBal() - (getConfig().getPosTravHaute() - getConfig().getPosTravBasse()) - getConfig().FHTRAV) / 2);
            //
            a = 3;
            // hauteur balustre
            p1 = new Tpoint2D(ox + z(getConfig().getCalculatedEntraxeBalReel()), oy - z(getConfig().getCalculatedDebBasBal() + getConfig().getDecalSol()));
            p2 = new Tpoint2D(ox + z(getConfig().getCalculatedEntraxeBalReel()), oy - z(getConfig().getCalculatedDebBasBal() + getConfig().getHauteurBal() + getConfig().getDecalSol()));
            cotevt(p1, p2, getConfig().getHauteurBal());
            // balustre depuis sol
            if (!getConfig().getSansPotelet()) {
                p1 = new Tpoint2D(ox + z(getConfig().getCalculatedEntraxeBalReel()), oy - z(getConfig().getCalculatedDebBasBal() + getConfig().getDecalSol()));
                p2 = new Tpoint2D(ox + z(getConfig().getCalculatedEntraxeBalReel()), oy - z(getConfig().getDecalSol()));
                cotevt(p1, p2, getConfig().getCalculatedDebBasBal());
            }
            // balustre depuis sous mc
            if (getConfig().getMainCourante()) {
                p1 = new Tpoint2D(ox + z(getConfig().getCalculatedEntraxeBalReel()), oy - z(getConfig().getCalculatedDebBasBal() + getConfig().getHauteurBal() + getConfig().getDecalSol()));
                p2 = new Tpoint2D(ox + z(getConfig().getCalculatedEntraxeBalReel()), oy - z(getConfig().getCalculatedHauteurSousMC()));
                cotevt(p1, p2, getConfig().getCalculatedHauteurSousMC() - getConfig().getCalculatedDebBasBal() - getConfig().getHauteurBal() - getConfig().getDecalSol());
            }
        }

        //cotes traverse (hors balustre)
        if (!getConfig().getIsBal() && !getConfig().getVerreIsComplet() && getConfig().getTraverses().size() > 0) {
            //1ère traverse
            a = getConfig().getCalculatedHauteurSousMC();
            p1 = new Tpoint2D(ox + (4 * e), oy - z(a));
            b = getConfig().getTraverse(0).getPosition();
            p2 = new Tpoint2D(ox + (4 * e), oy - z(b));
            cotevt(p2, p1, a - b);
            //intermédiaires
            for (int i = 0; i < getConfig().getTraverses().size() - 1; i++) {
                a = getConfig().getTraverse(i).getPosition();
                p1 = new Tpoint2D(ox + (4 * e), oy - z(a));
                b = getConfig().getTraverse(i + 1).getPosition();
                p2 = new Tpoint2D(ox + (4 * e), oy - z(b));
                cotevt(p2, p1, a - b);
            }
            //dernière traverse
            if (getConfig().getIsVerre()) {
                a = getConfig().getPosVerre();
            } else {
                a = 0;//getConfig().getDebordTravBas();
            }
            p1 = new Tpoint2D(ox + (4 * e), oy - z(a));
            b = getConfig().getTraverse(getConfig().getTraverses().size() - 1).getPosition();
            p2 = new Tpoint2D(ox + (4 * e), oy - z(b));
            cotevt(p2, p1, b - a);

            /*
             //départ bas
             a = getConfig().getCalculatedPosTravBas() + getConfig().getCalculatedEntraxeTraverseBas() + getConfig().getDecalSol();
             //cote sol à 1ère
             if (getConfig().getNbrTraverses()>0) {
             p1 = new rpoint(ox + (4 * e), oy - z(a));
             p2 = new rpoint(ox + (4 * e), oy - z(getConfig().getCalculatedPosTravBas() + getConfig().getDecalSol()));
             cotevt(p1, p2, getConfig().getCalculatedEntraxeTraverseBas());
             }
             //traverses basse (serre)
             for (int i = 0; i < getConfig().getNbrTravSerre() - 1; i++) {
             p1 = new rpoint(ox + (4 * e), oy - z(a));
             a += getConfig().getCalculatedEntraxeTraverseSerre();
             p2 = new rpoint(ox + (4 * e), oy - z(a));
             cotevt(p2, p1, getConfig().getCalculatedEntraxeTraverseSerre());
             }
             //traverses en haut
             double f;
             if (getConfig().getNbrTravSerre() == 0) {
             f = getConfig().getNbrTraverses();
             } else {
             f = getConfig().getNbrTraverses() - getConfig().getNbrTravSerre() + 1;
             }
             for (int i = 0; i < f; i++) {
             p1 = new rpoint(ox + (4 * e), oy - z(a));
             a += getConfig().getCalculatedEntraxeTraverse();
             p2 = new rpoint(ox + (4 * e), oy - z(a));
             cotevt(p2, p1, getConfig().getCalculatedEntraxeTraverse());
             }
             */
        }

        //cotes verre/tole
        if (getConfig()
                .getIsVerre()) {
            if (getConfig().getVerreIsComplet()) {
                // debord verre haut
                p1 = new Tpoint2D(ox + (4 * e), oy - z(getConfig().getCalculatedPosVerre()));
                p2 = new Tpoint2D(ox + (4 * e), oy - z(getConfig().getCalculatedHauteurSousMC()));
                cotevt(p2, p1, getConfig().getCalculatedHauteurSousMC() - getConfig().getCalculatedPosVerre());
            } else {
                // hauteur traverses
                p1 = new Tpoint2D(ox + (5 * e), oy - z(getConfig().getCalculatedPosVerre()));
                p2 = new Tpoint2D(ox + (5 * e), oy - z(getConfig().getCalculatedPosVerre() + getConfig().getCalculatedHauteurTraverses()));
                cotevt(p2, p1, getConfig().getCalculatedHauteurTraverses());
            }
            // Position verre
            p1 = new Tpoint2D(ox + (5 * e), oy - z(getConfig().getDecalSol()));
            p2 = new Tpoint2D(ox + (5 * e), oy - z(getConfig().getCalculatedPosVerre()));
            cotevt(p2, p1, getConfig().getCalculatedPosVerre() - getConfig().getDecalSol());
            // debord verre bas
            p1 = new Tpoint2D(ox + (4 * e), oy - z(getConfig().getDecalSol()));
            p2 = new Tpoint2D(ox + (4 * e), oy - z(getConfig().getCalculatedDebordVerreSol()));
            cotevt(p2, p1, getConfig().getCalculatedDebordVerreSol() - getConfig().getDecalSol());
            // hauteur verre
            p1 = new Tpoint2D(ox + (4 * e), oy - z(getConfig().getCalculatedDebordVerreSol()));
            p2 = new Tpoint2D(ox + (4 * e), oy - z(getConfig().getCalculatedDebordVerreSol() + getConfig().getCalculatedHauteurVerre()));
            cotevt(p2, p1, getConfig().getCalculatedHauteurVerre());
            // pince bas
            p1 = new Tpoint2D(ox + (3 * e), oy - z(getConfig().getCalculatedDebordVerreSol()));
            p2 = new Tpoint2D(ox + (3 * e), oy - z(getConfig().getCalculatedDebordVerreSol() + getConfig().getAxePinceBas()));
            cotevt(p2, p1, getConfig().getAxePinceBas());
            // entraxe pince
            p1 = new Tpoint2D(ox + (3 * e), oy - z(getConfig().getCalculatedDebordVerreSol() + getConfig().getAxePinceBas()));
            p2 = new Tpoint2D(ox + (3 * e), oy - z(getConfig().getCalculatedDebordVerreSol() + getConfig().getAxePinceBas() + getConfig().getCalculatedEntraxePinces()));
            cotevt(p2, p1, getConfig().getCalculatedEntraxePinces());
            // pince haut
            p1 = new Tpoint2D(ox + (3 * e), oy - z(getConfig().getCalculatedDebordVerreSol() + getConfig().getAxePinceBas() + getConfig().getCalculatedEntraxePinces()));
            p2 = new Tpoint2D(ox + (3 * e), oy - z(getConfig().getCalculatedDebordVerreSol() + getConfig().getAxePinceBas() + getConfig().getAxePinceHaut() + getConfig().getCalculatedEntraxePinces()));
            cotevt(p2, p1, getConfig().getAxePinceHaut());
        }

        // décalage
        if (getConfig()
                .getDecalSol() > 0) {
            p1 = new Tpoint2D(ox + (3 * e), oy);
            p2 = new Tpoint2D(ox + (3 * e), oy - z(getConfig().getDecalSol()));
            cotevt(p2, p1, getConfig().getDecalSol());
        }
        //scellement
        if (getConfig().getIsScellement()) {
            if (getConfig().getIsMuret()) {
                a = getConfig().getDecalSol();
            } else {
                a = 0;
            }
            p1 = new Tpoint2D(ox + (2 * e), oy - z(a));
            p2 = new Tpoint2D(ox + (2 * e), oy - z(a - getConfig().getHauteurScellement()));
            cotevt(p2, p1, getConfig().getHauteurScellement());
        }
        // ---------------------------
        // ---------------------------
        // COTES PERCAGES
        // ---------------------------
        // ---------------------------

        //c est la colone sur laquelle afficher la cote
        c = 0;
        // épaisseur mc
        b = 0;
        //épaisseur liaison 0 si pas de mc
        double d = 0;

        if (getConfig()
                .getMainCourante()) {
            d = getConfig().FHLIAISON;
            b = d + getConfig().FHMC;
        } else {
            d = getConfig().FHBOUCHONPOT;
            b = d;
        }
        //a est le point de départ du haut du potelet
        a = getConfig().getCalculatedHauteurFinie() - b;

        if (getConfig()
                .getMainCourante()) {
            d = getConfig().FHLIAISONANGLE;
            b = d + getConfig().FHMC;
        } else {
            d = getConfig().FHBOUCHONPOT;
            b = d;
        }
        //a2 est le point de départ du haut du potelet d'angle
        a2 = getConfig().getCalculatedHauteurFinie() - b;

        // cote épaisseur liaison
        if (!getConfig()
                .getSansPotelet() && getConfig().getMainCourante()) {
            p1 = new Tpoint2D(ox - e, oy - z(a));
            p2 = new Tpoint2D(ox - e, oy - z((a + getConfig().FHLIAISON)));
            cotevt(p2, p1, getConfig().FHLIAISON);
        }

        // cote épaisseur liaison d'angle
        if (!getConfig()
                .getSansPotelet() && getConfig().getMainCourante()) {
            p1 = new Tpoint2D(ox2 - e, oy - z(a2));
            p2 = new Tpoint2D(ox2 - e, oy - z((a2 + getConfig().FHLIAISONANGLE)));
            cotevt(p2, p1, getConfig().FHLIAISONANGLE);
        }

        //
        style.cote();
        //cote percage balustres

        if (getConfig().getIsBal()) {
            c = 2;
            // balustre trav haute
            p1 = new Tpoint2D(ox - (c * e), oy - z(getConfig().getCalculatedDebBasBal() + getConfig().getHauteurBal() + getConfig().getDecalSol()));
            p2 = new Tpoint2D(ox - (c * e), oy - z(getConfig().getDecalSol() + getConfig().getPosTravHaute()));
            cotevt(p2, p1, (getConfig().getHauteurBal() - (getConfig().getPosTravHaute() - getConfig().getPosTravBasse()) - getConfig().FHTRAV) / 2 + (getConfig().FHTRAV / 2));
            //
            c = 3;
            // balustre trav basse
            p1 = new Tpoint2D(ox - (c * e), oy - z(getConfig().getCalculatedDebBasBal() + getConfig().getHauteurBal() + getConfig().getDecalSol()));
            p2 = new Tpoint2D(ox - (c * e), oy - z(getConfig().getDecalSol() + getConfig().getPosTravBasse()));
            cotevt(p2, p1, (getConfig().getHauteurBal() - (getConfig().getPosTravHaute() - getConfig().getPosTravBasse()) - getConfig().FHTRAV) / 2 + (getConfig().FHTRAV / 2) + (getConfig().getPosTravHaute() - getConfig().getPosTravBasse()));
            //
            c = 4;
            if (!getConfig().getSansPotelet()) {
                // traverse haute
                p1 = new Tpoint2D(ox - (c * e), oy - z(a));
                p2 = new Tpoint2D(ox - (c * e), oy - z(getConfig().getDecalSol() + getConfig().getPosTravHaute()));
                cotevt(p2, p1, getConfig().getCalculatedHauteurSousMC() - getConfig().getPosTravHaute() - getConfig().FHLIAISON - getConfig().getDecalSol());
                //
                c = 5;
                // traverse basse
                p1 = new Tpoint2D(ox - (c * e), oy - z(a));
                p2 = new Tpoint2D(ox - (c * e), oy - z(getConfig().getDecalSol() + getConfig().getPosTravBasse()));
                cotevt(p2, p1, getConfig().getCalculatedHauteurSousMC() - getConfig().getPosTravBasse() - getConfig().FHLIAISON - getConfig().getDecalSol());
                //
                c = 6;
                // perçage 56 monoprofil
                p1 = new Tpoint2D(ox - (c * e), oy - z(a));
                p2 = new Tpoint2D(ox - (c * e), oy - z(getConfig().FHSABOT + 56));
                cotevt(p2, p1, getConfig().getCalculatedHauteurPotelet() - 56);
            }
        }

        //cote percage traverses
        if (!getConfig().getSansPotelet() && !getConfig().getVerreIsComplet()) {
            if (!getConfig().getIsBal()) {
                c = -1;
                for (int i = 0; i < getConfig().getTraverses().size(); i++) {
                    //haut du potelet
                    p1 = new Tpoint2D(ox + (c * e), oy - z(a));
                    //
                    b = getConfig().getTraverse(i).getPosition();
                    p2 = new Tpoint2D(ox + (c * e), oy - z(b));
                    cotevt(p2, p1, a - b);
                    c--;
                }
                c = getConfig().getTraverses().size();
            }
        }

        //cote percage traverses potelet d'angle
        if (!getConfig().getSansPotelet() && !getConfig().getVerreIsComplet()) {
            if (!getConfig().getIsBal()) {
                c = -1;
                for (int i = 0; i < getConfig().getTraverses().size(); i++) {
                    //haut du potelet
                    p1 = new Tpoint2D(ox2 + (c * e), oy - z(a2));
                    //
                    b = getConfig().getTraverse(i).getPosition();//+(getConfig().FHLIAISON-getConfig().FHLIAISONANGLE);
                    p2 = new Tpoint2D(ox2 + (c * e), oy - z(b));
                    cotevt(p2, p1, a2 - b);
                    c--;
                }
                c = getConfig().getTraverses().size();
            }
        }
        double c2 = c;
        //cote percage verre tole
        if (getConfig()
                .getIsVerre()) {
            // pince haut
            p1 = new Tpoint2D(ox - (c * e) - e, oy - z(a));
            b = getConfig().getCalculatedDebordVerreSol() + getConfig().getAxePinceBas() + getConfig().getCalculatedEntraxePinces();
            p2 = new Tpoint2D(ox - (c * e) - e, oy - z(b));
            cotevt(p1, p2, a - b);
            c++;
            // pince bas
            p1 = new Tpoint2D(ox - (c * e) - e, oy - z(a));
            b = getConfig().getCalculatedDebordVerreSol() + getConfig().getAxePinceBas();
            p2 = new Tpoint2D(ox - (c * e) - e, oy - z(b));
            cotevt(p1, p2, a - b);
            c++;
        }
        c = c2;
        //cote percage verre tole potelet d'angle
        if (getConfig()
                .getIsVerre()) {
            // pince haut
            p1 = new Tpoint2D(ox2 - (c * e) - e, oy - z(a2));
            b = getConfig().getCalculatedDebordVerreSol() + getConfig().getAxePinceBas() + getConfig().getCalculatedEntraxePinces();
            p2 = new Tpoint2D(ox2 - (c * e) - e, oy - z(b));
            cotevt(p1, p2, a2 - b);
            c++;
            // pince bas
            p1 = new Tpoint2D(ox2 - (c * e) - e, oy - z(a2));
            b = getConfig().getCalculatedDebordVerreSol() + getConfig().getAxePinceBas();
            p2 = new Tpoint2D(ox2 - (c * e) - e, oy - z(b));
            cotevt(p1, p2, a2 - b);
            c++;
        }

        //cotes potelet
        if (!getConfig()
                .getSansPotelet()) {
            p1 = new Tpoint2D(ox - (c * e) - e, oy - z(a));
            p2 = new Tpoint2D(ox - (c * e) - e, oy - z(a - getConfig().getCalculatedHauteurPotelet()));
            cotevt(p1, p2, getConfig().getCalculatedHauteurPotelet());
        }

        //cotes potelet d'angle
        if (!getConfig()
                .getSansPotelet()) {
            p1 = new Tpoint2D(ox2 - (c * e) - e, oy - z(a2));
            p2 = new Tpoint2D(ox2 - (c * e) - e, oy - z(a2 - getConfig().getCalculatedHauteurPoteletAngle()));
            cotevt(p1, p2, getConfig().getCalculatedHauteurPoteletAngle());
        }

        Date dNow = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy");

        style.setFontColor(Color.BLACK);
        centerText("Côtes en mm / Sans échelle", w - 200, h - 20);
        Image logo = new Image(getClass().getResourceAsStream("/logo.png"));
        getGC().drawImage(logo, 10, 20, 60, 60);
        leftText(getConfig().getNom() + " le " + ft.format(dNow), 80, 30);
        leftText("Schéma des perçages", 80, 50);
        leftText("Vue de face depuis l'extérieur", 80, 70);
    }

    /**
     * @param f
     */
    @Override
    public void onCreateForm(Tform f) {
        super.onCreateForm(f);
        f.setPos(600, java.awt.GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds().height - getForm().getHeight()).setTitle("Perçages: vue de face").show();
    }

    @Override
    public void onDragged(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onClick(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onDblClick(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onPressed(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onReleased(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onMove(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
