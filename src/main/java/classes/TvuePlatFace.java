/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import esyoLib.Formsfx.Tform;
import esyoLib.Formsfx.TformException;
import esyoLib.Graphics.Tpoint2D;
import evrLib.gc.TconfigurationPlat;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * @author rpetit
 */
public class TvuePlatFace extends TconfigVue2D {

    /**
     * @param c
     * @throws esyoLib.Formsfx.TformException
     */
    public TvuePlatFace(TconfigUI c) throws TformException {
        super(c);
    }

    /**
     * @param c
     * @param zoom
     * @throws esyoLib.Formsfx.TformException
     */
    public TvuePlatFace(TconfigUI c, String zoom) throws TformException {
        super(c, zoom);
    }

    /**
     * @return
     */
    @Override
    public String titleView() {
        return "Positionnement-face";
    }

    /**
     * @return
     */
    @Override
    public TconfigurationPlat getConfig() {
        return (TconfigurationPlat) super.getConfig();
    }

    /**
     *
     */
    @Override
    public void draw() {
        double w = getCanvasWidth();
        double h = getCanvasHeight();
        //efface
        clear(Color.WHITE);
        //
        Tpoint2D p1, p2;
        //estimation de la largeur
        double rm = ((getConfig().getAllPotNbr() - 1) * getConfig().getCalculatedEntraxePotReel() + getConfig().getDebordPotGauche() + getConfig().getDebordPotDroite()) / 2;
        double m = z(rm);
        //point gauche
        double rd = -rm + getConfig().getDebordPotGauche();
        double d = z(rd);
        double rinvd = -m + getConfig().getCalculatedDebordPotGauche();
        double invd = z(rinvd);
        //point droite
        double rf = rm - getConfig().getDebordPotDroite();
        double f = z(rf);
        double rinvf = m - getConfig().getCalculatedDebordPotDroite();
        double invf = z(rinvf);
        //SOL====================================================================
        style.normal();
        line(-m + w / 2, 150 + h / 2, -m + w / 2, 0 + h / 2);
        line(-m + w / 2, 0 + h / 2, m + w / 2, 0 + h / 2);
        line(m + w / 2, 0 + h / 2, m + w / 2, 150 + h / 2);
        //INFOS====================================================================
        double a;
        //compte le nombre de potelet et affiche si >0---------
        a = getConfig().getActifPotNbr();
        if (getConfig().getFixMurGauche()) {
            a = a - 1;
        }
        if (getConfig().getFixMurDroite()) {
            a = a - 1;
        }
        if (!getConfig().getSansPotelet()) {
            centerText((int) a + " potelets", 0 + w / 2, 125 + h / 2);
        }
        //compte le nombre de potelet d'angle et affiche si >0 ---
        a = 0;
        if (getConfig().getAnglePotGauche()) {
            a = a + 1;
        }
        if (getConfig().getAnglePotDroite()) {
            a = a + 1;
        }
        if (a > 0) {
            centerText("(" + (int) a + " d'angle)", 0 + w / 2, 140 + h / 2);
        }
        //-----------------------------------------
        if (getConfig().getIsBal()) {
            centerText((int) getConfig().getCalculatedTotBal() + " balustres (Espacement de " + getConfig().getCalculatedEspBalReel() + ")", 0 + w / 2, 155 + h / 2);
        }

        // côté gauche
        // premier potelet (si pas fixé au mur)
        if (!getConfig().getFixMurGauche()) {
            //
            String l = "";

            if (!getConfig().getLettreA().isEmpty()) {
                l = " " + getConfig().getLettreA();
            }

            // dessine le 1er potelet
            if (getConfig().getAngleGauche()) {
                centerText("Angle" + l, d + w / 2, -25 + h / 2);
            } else if (getConfig().getAnglePotGauche()) {
                centerText("Potelet d'angle" + l, d + w / 2, -25 + h / 2);
            }
        } else {
            centerText("Fixé au mur", d + w / 2, -25 + h / 2);
        }
        // dernier potelet (si pas fixé au mur)
        if (!getConfig().getFixMurDroite()) {
            //
            String l = "";

            if (!getConfig().getLettreB().isEmpty()) {
                l = " " + getConfig().getLettreB();
            }

            // dessine le dernier potelet
            if (getConfig().getAngleDroite()) {
                centerText("Angle" + l, w / 2 + f, -25 + h / 2);
            } else if (getConfig().getAnglePotDroite()) {
                centerText("Potelet d'angle" + l, w / 2 + f, -25 + h / 2);
            }
        } else {
            centerText("Fixé au mur", f + w / 2, -25 + h / 2);
        }
        //AXES==============================================================================================
        double rl = d - z(getConfig().getCalculatedEntraxeBalReel());
        int n = (int) (getConfig().getCalculatedTotBal()) + getConfig().getPotelets().size() - 1;
        if (getConfig().getSansPotelet()) {
            n -= 2;
        }

        // balustre en arrière plan
        if (getConfig().getIsBal()) {
            getGC().setStroke(Color.ORANGE);
            for (int k = 0; k <= n; k++) {
                rl += z(getConfig().getCalculatedEntraxeBalReel());
                /*
                //1er balustre #0, si pas inversé on ne dessine pas (c'est le potelet) sauf si pas de potelet
                if (!getConfig().getInversePotBalGauche() && k == 0 && !getConfig().getSansPotelet()) {
                    continue;
                }
                //2ème balustre #1, si inversé on ne le dessine pas (c'est le potelet)
                if (getConfig().getInversePotBalGauche() && k == 1 && !getConfig().getSansPotelet()) {
                    continue;
                }
                //dernier balustre #max, si pas inversé on ne dessine pas (c'est le potelet)
                if (!getConfig().getInversePotBalDroite() && k == n && !getConfig().getSansPotelet()) {
                    continue;
                }
                //avant dernier balustre #max-1, si inversé on ne le dessine pas (c'est le potelet)
                if (getConfig().getInversePotBalDroite() && k == n - 1 && !getConfig().getSansPotelet()) {
                    continue;
                }
                */
                getGC().strokeLine(
                        w / 2 + rl,//d + z(i * getConfig().getCalculatedEntraxePotReel()) + z(k * getConfig().getCalculatedEntraxeBalReel()),
                        h / 2 - z(getConfig().getCalculatedHauteurFinie()),
                        w / 2 + rl,//+ d + z(i * getConfig().getCalculatedEntraxePotReel()) + z(k * getConfig().getCalculatedEntraxeBalReel()),
                        h / 2
                );
            }
        }
        // potelet en 1er plan
        style.axe();
        double e = 0;
        ArrayList<Double> pots = new ArrayList();
        ArrayList<Double> rpots = new ArrayList();

        for (int i = 0; i < getConfig().getPotelets().size(); i++) {
            //si le potelet est actif, je le dessine
            if (getConfig().getPotelets().get(i).getActif()) {
                e = 0;
                //1er potelet inversé, je ne le dessine pas
                if (i == 0 && getConfig().getInversePotBalGauche()) {
                    e = getConfig().getCalculatedEntraxeBalReel();
                }
                //dernier potelet inversé , je ne le dessine pas
                if (i == getConfig().getPotelets().size() - 1 && getConfig().getInversePotBalDroite()) {
                    e = -getConfig().getCalculatedEntraxeBalReel();
                }

                if (!getConfig().getSansPotelet() || getConfig().getMCmurale()) {
                    pots.add((d + z(i * getConfig().getCalculatedEntraxePotReel()) + w / 2) + z(e));
                    rpots.add(getConfig().getCalculatedEntraxePotReel());
                    a = 80 + h / 2;
                    if (getConfig().getMCmurale()) {
                        a = h / 2 - z(getConfig().getCalculatedHauteurFinie()) + 80;
                    }
                    line((d + z(i * getConfig().getCalculatedEntraxePotReel()) + w / 2) + z(e), h / 2 - z(getConfig().getCalculatedHauteurFinie()), (d + z(i * getConfig().getCalculatedEntraxePotReel()) + w / 2) + z(e), a);
                }
            }
        }
        //COTES======================================================================================================
        //retire le dernier pot
        if (rpots.size() >= 2) {
            rpots.remove(rpots.size() - 1);
            //correction potelets inversés
            if (getConfig().getInversePotBalGauche()) {
                rpots.set(0, rpots.get(0) - getConfig().getCalculatedEntraxeBalReel());
            }
            if (getConfig().getInversePotBalDroite()) {
                rpots.set(rpots.size() - 1, rpots.get(rpots.size() - 1) - getConfig().getCalculatedEntraxeBalReel());
            }

            //insert le point de départ avant le 1er potelet
            if (getConfig().getCoterDebGauche()) {
                pots.add(0, w / 2 - m);
                rpots.add(0, getConfig().getCalculatedDebordPotGauche());
            }
            //insert le point de fin
            if (getConfig().getCoterDebDroite()) {
                pots.add(w / 2 + m);
                //
                rpots.add(getConfig().getCalculatedDebordPotDroite());
            }

        }
        //potelets
        for (int i = 0; i < pots.size() - 1; i++) {
            if (!getConfig().getSansPotelet() || getConfig().getMCmurale()) {
                a = h / 2 + 50;
                if (getConfig().getMCmurale()) {
                    a = h / 2 - z(getConfig().getCalculatedHauteurFinie()) + 50;
                }
                p1 = new Tpoint2D(pots.get(i), a);
                p2 = new Tpoint2D(pots.get(i + 1), a);
                cotehz(p1, p2, rpots.get(i));
            }
        }
        //
        if (getConfig().getMCmurale()) {
            centerText("Fixation main courante murale", w / 2, h / 2 - z(getConfig().getCalculatedHauteurFinie()) + 80);
        }
        // portion
        p1 = new Tpoint2D(w / 2 - m, h / 2 + 100);
        p2 = new Tpoint2D(w / 2 + m, h / 2 + 100);
        cotehz(p1, p2, getConfig().getLongueur());
        // balustre
        if (getConfig().getIsBal()) {
            p1 = new Tpoint2D(w / 2 + d, h / 2 - 100);
            p2 = new Tpoint2D(w / 2 + d + z(getConfig().getCalculatedEntraxeBalReel()), h / 2 - 100);
            cotehz(p1, p2, getConfig().getCalculatedEntraxeBalReel());
        }

        Date dNow = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy");

        style.setFontColor(Color.BLACK);
        centerText("Côtes en mm / Sans échelle", w - 200, h - 20);
        Image logo = new Image(getClass().getResourceAsStream("/logo.png"));
        getGC().drawImage(logo, 10, 20, 60, 60);
        leftText(getConfig().getNom() + " le " + ft.format(dNow), 80, 30);
        leftText("Schéma des positionnements des ancrages potelets (à titre indicatif)", 80, 50);
        leftText("Vue de face depuis l'extérieur", 80, 70);
    }

    /**
     * @param f
     */
    @Override
    public void onCreateForm(Tform f) {
        super.onCreateForm(f);
        f.setPos(600, 100).setTitle("Positionnement: vue de face").show();
    }

    @Override
    public void onDragged(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onClick(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onDblClick(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onPressed(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onReleased(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onMove(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
