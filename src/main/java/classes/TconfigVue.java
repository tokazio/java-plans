/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import esyoLib.Formsfx.Tform;
import esyoLib.Formsfx.TformException;
import esyoLib.Graphics.Tvue;
import evrLib.gc.Tconfiguration;
import interfaces.IconfigVue;

/**
 * @author rpetit
 */
public abstract class TconfigVue extends Tvue implements IconfigVue {
    //
    private TconfigUI configUI;

    /**
     * @param aConfigurationUI
     */
    public TconfigVue(TconfigUI aConfigurationUI) throws TformException {
        super();
        this.configUI = aConfigurationUI;
    }

    @Override
    public Tconfiguration getConfig() {
        return this.configUI.getConfig();
    }

    /**
     * @return
     */
    @Override
    public TconfigUI getConfigUI() {
        return this.configUI;
    }

    @Override
    public void onCreateForm(Tform f) {
        //lie l'objet à la fenêtre
        this.configUI.attach(f.getController());
    }
}
