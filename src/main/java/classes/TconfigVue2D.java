/*
 * Copyright (C) 2014 rpetit
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package classes;

import esyoLib.Formsfx.Tform;
import esyoLib.Formsfx.TformException;
import esyoLib.Graphics.Tvue2D;
import evrLib.gc.Tconfiguration;
import interfaces.IconfigVue;

/**
 * @author rpetit
 */
public abstract class TconfigVue2D extends Tvue2D implements IconfigVue {
    //
    private TconfigUI configUI;

    /**
     * Constructeur
     * <p>
     * Version simplifié, ne nécessite que le 'TonfigUI'
     *
     * @param aConfigurationUI Configuration du chantier (TconfigUI)
     * @throws esyoLib.Formsfx.TformException
     */
    public TconfigVue2D(TconfigUI aConfigurationUI) throws TformException {
        super();
        this.configUI = aConfigurationUI;
    }

    /**
     * Constructeur
     *
     * @param aConfigurationUI Configuration du chantier (TconfigUI)
     * @param aZoomText        Texte de zoom (A/B)
     * @throws esyoLib.Formsfx.TformException
     */
    public TconfigVue2D(TconfigUI aConfigurationUI, String aZoomText) throws TformException {
        super();
        this.configUI = aConfigurationUI;
        setZoom(aZoomText);
    }

    /**
     * Calcul du zoom en automatique
     */
    @Override
    public final void autoZoom() {
        try {
            if (getConfig().getLongueur() > 0) {
                setZoom(frac(pxScreenToMm(getCanvas().getWidth()) / getConfig().getLongueur()));
            }
        } catch (Exception e) {

        }
    }

    /**
     * @param f
     */
    @Override
    public void onCreateForm(Tform f) {
        //lie l'objet à la fenêtre
        this.configUI.attach(f.getController());
        //
        f.setObj(this);
    }

    /**
     * @return
     */
    @Override
    public Tconfiguration getConfig() {
        return this.configUI.getConfig();
    }

    /**
     * @return
     */
    @Override
    public TconfigUI getConfigUI() {
        return this.configUI;
    }

}
