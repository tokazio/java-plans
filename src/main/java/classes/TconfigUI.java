/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import esyoLib.Files.TfileUtils;
import esyoLib.Formsfx.*;
import esyoLib.Graphics.Tvue;
import esyoLib.Graphics.Tvue2D;
import esyoLib.XML.Txml;
import esyoLib.XML.TxmlObject;
import evrLib.enumeration.Egamme;
import evrLib.gc.Tconfiguration;
import javafx.stage.FileChooser;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;
import plansevrfx.PlansEVRfx;

import javax.xml.parsers.ParserConfigurationException;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.io.StreamCorruptedException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static plansevrfx.PlansEVRfx.application;

/**
 * Squelette de la fenêtre d'entrée des informations de configuration
 *
 * @author rpetit
 */
public abstract class TconfigUI extends TxmlObject {

    //Liste des vues de la config
    protected final transient List<Tvue> vues = new ArrayList<>();
    //liste des controleurs des vues
    private final transient List<Tcontroller> drawingCtrls = new ArrayList<>();
    //fenêtre
    protected Tform form;
    //chemin du fichier du chantier (de la configuration)
    protected String fileName = "";
    //configuration
    protected Tconfiguration cfg;
    //
    private String className;
    //edited?
    private boolean edited = false;

    /**
     * Constructeur
     *
     * @throws IOException
     * @throws java.lang.InstantiationException
     * @throws java.lang.IllegalAccessException
     * @throws java.lang.NoSuchMethodException
     * @throws java.lang.reflect.InvocationTargetException
     * @throws java.lang.ClassNotFoundException
     */
    public TconfigUI() throws IOException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, ClassNotFoundException {
        ini();
        open();
        setEdited();
    }

    /**
     * Constructeur avec non du fichier de configuration à ouvrir
     *
     * @param aFileName Nom du fichier de la configuration
     * @throws IOException
     * @throws java.lang.InstantiationException
     * @throws java.lang.IllegalAccessException
     * @throws java.lang.reflect.InvocationTargetException
     * @throws java.lang.NoSuchMethodException
     * @throws java.lang.ClassNotFoundException
     * @throws java.io.StreamCorruptedException
     * @throws javax.xml.parsers.ParserConfigurationException
     * @throws org.xml.sax.SAXException
     */
    public TconfigUI(String aFileName) throws IOException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, ClassNotFoundException, StreamCorruptedException, ParserConfigurationException, SAXException {
        ini();
        openFromFile(aFileName);
    }

    /**
     * Crée la fenêtre d'entrée des informations
     *
     * @param aFileName
     * @return
     * @throws IOException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     * @throws InvocationTargetException
     * @throws NoSuchMethodException
     * @throws ClassNotFoundException
     * @throws StreamCorruptedException
     * @throws ParserConfigurationException
     * @throws SAXException
     */
    public static TconfigUI createFrom(String aFileName) throws Exception, IOException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, ClassNotFoundException, StreamCorruptedException, ParserConfigurationException, SAXException {
        Document x = Txml.loadFromFile(aFileName);
        Element r = x.getDocumentElement();
        String className = r.getNodeName();
        //System.out.println("Dans le fichier '" + aFileName + "' class= " + className);
        if (className.isEmpty()) {
            throw new Exception("Tchantier::createNew(" + aFileName + ") Error\n" + "Le fichier ne contient pas le champ 'class='");
        }
        switch (className) {
            //case "TconfigurationPlat"://version delphi
            case "plansevrfx.TconfigurationPlat"://version java
                //case "plansevrfx/TconfigurationPlat"://version java
            case "evrLib.gc.TconfigurationPlat"://version java xml
                return new TconfigUIPlat(aFileName);
            //case "TconfigurationEsc"://version delphi
            //case "plansevrfx/TconfigurationEscalier"://version java
            case "plansevrfx.TconfigurationEscalier"://version java
            case "evrLib.gc.TconfigurationEscalier"://version java xml
                return new TconfigUIEscalier(aFileName);
        }
        throw new Exception("la classe '" + className + "' du fichier '" + aFileName + "' n'existe pas dans TconfigUI");
    }

    /**
     * Fonction abstraite appelée pour ouvrir la configuration
     */
    public abstract void open();

    /**
     * Fonction abstraite appelée à l'initialisation Doit créer le bon type
     * descendant de Tconfiguration et le placer dans this.cfg
     */
    public abstract void onIni();

    //initialisation
    private void ini() throws IOException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, ClassNotFoundException {
        onIni();
    }

    /**
     * Indique qu'un paramètre à été changé
     * <p>
     * Change le titre dans la barre de la fenêtre (*...)
     */
    public final void setEdited() {
        this.edited = true;
        this.form.setTitle("*" + getName());
        this.form.canClose = false;
    }

    /**
     * @return si un paramètre à été changé
     */
    public final boolean getEdited() {
        return this.edited;
    }

    //annule si un paramètre à été changé
    private void notEdited() {
        this.edited = false;
        this.form.setTitle(getName());
        this.form.canClose = true;
    }

    /**
     * @return la fenêtre (Tform)
     */
    public Tform getForm() {
        return this.form;
    }

    /**
     * Ouvre la configuration depuis un fichier
     *
     * @param aFileName fichier à ouvrir
     * @throws IOException
     * @throws java.io.StreamCorruptedException
     * @throws java.lang.NoSuchMethodException
     * @throws java.lang.reflect.InvocationTargetException
     * @throws javax.xml.parsers.ParserConfigurationException
     * @throws org.xml.sax.SAXException
     */
    public final void openFromFile(String aFileName) throws IOException, StreamCorruptedException, NoSuchMethodException, InvocationTargetException, ParserConfigurationException, SAXException {
        this.fileName = aFileName;
        this.cfg.loadFromFile(aFileName);
        open();
    }

    /**
     * @return nom de la configuration
     */
    public final String getName() {
        if (this.fileName.isEmpty()) {
            return "Nouveau";
        }
        String[] s = this.fileName.split("\\\\");
        return s[s.length - 1];
    }

    /**
     * Enregistre la configuration dans un fichier
     * <p>
     * Ouvre une boîte de dialogue
     *
     * @return @throws IOException
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     */
    public final String saveToFile() throws IOException, IllegalArgumentException, IllegalAccessException {
        FileChooser fileChooser = new FileChooser();
        //titre
        fileChooser.setTitle("Enregistrer-sous...");
        //défini le nom du fichier selon le nom de la config
        //fileChooser.setInitialFileName(getConfigPlat().getNom());
        fileChooser.setInitialFileName(getConfig().getNom());
        //pré selection du dossier
        String f = getForm().getApplication().getActualFolder();
        if (!TfileUtils.isValidFolder(f)) {
            if (getConfig().getGamme() != null) {
                String g = getConfig().getGamme().toString();
                f = PlansEVRfx.options.getChantierPath(g);
            }
        }
        if (TfileUtils.isValidFolder(f)) {
            fileChooser.setInitialDirectory(new File(f));
        }
        //extensions
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("PLA files (*.pla)", "*.pla");
        fileChooser.getExtensionFilters().add(extFilter);
        //affiche
        File file = fileChooser.showSaveDialog(getForm().getStage());
        //récup file
        if (file != null) {
            this.fileName = file.getPath();
            save();
        }
        return this.fileName;
    }

    /**
     * Exporte la vue dans un fichier PNG
     *
     * @return chemin du fichier enregistré
     * @throws java.io.IOException
     * @throws java.lang.NoSuchFieldException
     * @throws java.lang.IllegalAccessException
     */
    public final String exportAsPng() throws IOException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Exporter les plans en PNG...");
        //défini le nom du fichier selon le nom de la config
        //fileChooser.setInitialFileName(getConfigPlat().getNom());
        fileChooser.setInitialFileName(getConfig().getNom());
        if (!this.fileName.isEmpty()) {
            fileChooser.setInitialDirectory(new File(this.fileName).getParentFile());
        }
        //Set extension filter
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.png");
        fileChooser.getExtensionFilters().add(extFilter);
        //Show save file dialog
        final File file = fileChooser.showSaveDialog(getForm().getStage());
        //récup file
        if (file != null) {
            for (Tvue v : this.vues) {
                ((Tvue2D) v).exportAsPng(file.getPath());
            }
        }
        if (file != null) {
            return file.getPath();
        }
        return "?!";
    }

    /**
     * Enregistre dans le fichier déjà choisis
     *
     * @return chemin du fichier enregistré
     * @throws IOException
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     */
    public final String save() throws IOException, IllegalArgumentException, IllegalAccessException {
        if (getFileName().isEmpty()) {
            saveToFile();
        } else {
            notEdited();
            this.cfg.saveToFile(this.fileName);
        }
        return this.fileName;
    }

    /**
     * @return nom du fichier où la configuration est enregistrée
     */
    public final String getFileName() {
        return this.fileName;
    }

    /**
     * Défini le nom du fichier où la configuration est enregistrée
     *
     * @param s nom du fichier où la configuration est enregistrée
     */
    public final void setFileName(String s) {
        this.fileName = s;
    }

    /**
     * @return la configuration
     */
    public final Tconfiguration getConfig() {
        return this.cfg;
    }

    /**
     * Ferme la fenêtre
     */
    public final void close() {
        this.form.close();
    }

    /**
     * Ferme toutes les vues liées
     */
    public final void doClose() {
        closeViews();
    }

    /**
     * Demande confirmation à la fermeture
     * <p>
     * Enregistré avant de fermer?
     * <p>
     * Bug sur 'Annuler'!
     *
     * @return @throws IOException
     * @throws esyoLib.Formsfx.TformException
     * @throws java.lang.IllegalAccessException
     */
    public final boolean confirmClose() throws IOException, TformException, IllegalArgumentException, IllegalAccessException, TformFmxlNotFoundException, TformFmxlLoadException {
        //si à enregistrer, demande?
        if (this.edited) {
            //TconfirmCancelForm f = new TconfirmCancelForm(Fform.getName() + "\nEnregistrer avant de fermer le chantier '" + FcfgPlat.getNom() + "' ?");
            TconfirmCancelForm f = new TconfirmCancelForm(this.form.getName() + "\nEnregistrer avant de fermer le chantier '" + this.cfg.getNom() + "' ?", application);
            switch (f.isConfirmed()) {
                case crCancel:
                    return false;
                case crYes:
                    save();
                    return true;
                case crNo:
                    return true;
            }
        }
        return true;
    }

    /**
     * Ferme toutes les vues liées
     */
    public final void closeViews() {
        //ferme toutes les fenêtres
        while (this.vues.size() > 0) {
            this.vues.get(0).close();
            this.vues.remove(0);
        }
    }

    /**
     * Dessine les différentes vues
     */
    public final void draw() {
        this.vues.stream().forEach((Fvue) -> {
            try {
                Fvue.draw();
            } catch (Exception ex) {
                Logger.getLogger(TconfigUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

    /**
     * Exporte les informations de la configuration dans un PDF
     * <p>
     * Ouvre une boîte de dialogue
     */
    @Deprecated
    public final void exportPDF() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Exporter la fiche...");
        //défini le nom du fichier selon le nom de la config
        //fileChooser.setInitialFileName(getConfigPlat().getNom());
        fileChooser.setInitialFileName(getConfig().getNom());
        if (!this.fileName.isEmpty()) {
            fileChooser.setInitialDirectory(new File(this.fileName).getParentFile());
        }

        //Set extension filter
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("PDF files (*.pdf)", "*.pdf");
        fileChooser.getExtensionFilters().add(extFilter);
        //Show save file dialog
        File file = fileChooser.showSaveDialog(getForm().getStage());
        //récup file
        /*
         Tconfig3 n'existe plus...
         if (file != null) {
         DBobj3 o = new Tconfig3(Fcfg);
         Tdoc fd = new Tdoc(o);
         try {
         fd.convert("chantier.html", file.getPath(),true);
         } catch (Exception ex) {
         Logger.getLogger(TconfigUI.class.getName()).log(Level.SEVERE, null, ex);
         }
         }
         */
    }

    /**
     * Place la fenêtre et les vues au 1er plan
     */
    public final void toFront() {
        this.vues.stream().forEach(Fvue -> {
            if (Fvue != null) {
                Fvue.toFront();
            }
        });
        this.form.toFront().setFocus();
    }

    /**
     * Réduis la fenêtre et les vues
     */
    public final void minimize() {
        this.vues.stream().forEach((Fvue) -> {
            Fvue.minimize();
        });
        this.form.minimize();
    }

    /**
     * Agrandis la fenêtre et les vues
     */
    public final void maximize() {
        this.vues.stream().forEach((Fvue) -> {
            Fvue.maximize();
        });
        this.form.maximize();
    }

    /**
     * Lie les controleur (Tcontroller) des différentes vues
     *
     * @param c controleur (Tcontroller) de vue
     */
    public final void attach(Tcontroller c) {
        if (!this.drawingCtrls.contains(c)) {
            this.drawingCtrls.add(c);
        }
    }

    /**
     * Actualise la configuration
     * <p>
     * _ Actualise la fenêtre des paramètres _ recalcul _ redessine les vues
     *
     * @throws java.lang.Exception
     */
    public final void refresh() throws Exception {
        System.out.println("TconfigUI::refresh");
        //refresh inputs
        getForm().getController().refresh();
        //calcul
        this.cfg.calcul();
        draw();
        //refresh vues
        this.drawingCtrls.stream().forEach((FdrawingCtrl) -> {
            try {
                FdrawingCtrl.refresh();
            } catch (Exception ex) {
                Logger.getLogger(TconfigUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

    /**
     * Change la gamme et actualise
     *
     * @param gamme gamme (Egamme)
     */
    public final void changeGamme(Egamme gamme) {
        try {
            this.cfg.setGamme(gamme);
        } catch (IOException | ParserConfigurationException | SAXException ex) {
            application.showError(ex.getClass().getName(), ex.getMessage());
        }
        //refresh();
    }

    /**
     * Affiche le dossier dans lequel à été enregistré la configuration dans
     * l'explorateur de fichier de l'OS
     */
    public final void openFolder() {
        if (!this.fileName.isEmpty()) {
            try {
                Desktop.getDesktop().open(new File(this.fileName).getParentFile());
            } catch (IOException ex) {
                application.showError(ex.getClass().getName(), ex.getMessage());
            }
        }
    }

    @Override
    public void onLoaded() {

    }

}
