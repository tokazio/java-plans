/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package classes;

import esyoLib.Formsfx.TformException;
import evrLib.gc.TconfigurationEscalier;
import org.xml.sax.SAXException;
import plansevrfx.PlansEVRfx;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StreamCorruptedException;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author rpetit
 */
public class TconfigUIEscalier extends TconfigUI {

    /**
     * Constructeur
     *
     * @throws IOException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     * @throws InvocationTargetException
     * @throws NoSuchMethodException
     * @throws ClassNotFoundException
     */
    public TconfigUIEscalier() throws IOException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, ClassNotFoundException {
        super();
    }

    /**
     * Constructeur
     *
     * @param aFileName Fichier de configuration à ouvrir
     * @throws IOException
     * @throws java.lang.InstantiationException
     * @throws java.lang.IllegalAccessException
     * @throws java.lang.reflect.InvocationTargetException
     * @throws java.lang.NoSuchMethodException
     * @throws java.lang.ClassNotFoundException
     * @throws java.io.StreamCorruptedException
     * @throws javax.xml.parsers.ParserConfigurationException
     * @throws org.xml.sax.SAXException
     */
    public TconfigUIEscalier(String aFileName) throws IOException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, ClassNotFoundException, StreamCorruptedException, ParserConfigurationException, SAXException {
        super(aFileName);
    }

    /**
     * Ouvre la configuration
     */
    @Override
    public final void open() {
        //crée la fenêtre d'inputs
        this.form = PlansEVRfx.application.newForm(getName(), "fxmls/inputsEscForm.fxml", null).setTitle("Plan");
        //lie l'objet à la fenêtre
        this.form.setObj(this);
        //
        this.form.getController().manualIni();
        //met à jour la fenêtre,affiche la fenêtre,place le fenêtre en 1er
        this.form.setPos(0, 100);
        this.form.setSize(600, java.awt.GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds().height - 100);
        if (this.fileName.isEmpty()) {
            this.form.setTitle("Nouveau");
        } else {
            this.form.setTitle(getName());
        }
        this.form.show();
    }

    /**
     * Initilisation
     * <p>
     * Crée les vues
     */
    @Override
    public final void onIni() {
        try {
            this.cfg = new TconfigurationEscalier();
        } catch (IOException ex) {
            Logger.getLogger(TconfigUIPlat.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            this.vues.add(new TvueEscFace(this, "auto"));
        } catch (TformException ex) {
            Logger.getLogger(TconfigUIEscalier.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
