/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import esyoLib.Files.TstringObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Options pour le logiciel 'Plans'
 *
 * @author rpetit
 */
public class Toptions extends TstringObject {

    /**
     * chemin par défaut pour enregistrer les configs
     */
    public String defaultPath;
    /**
     * Naviguer dans des dossiers portant le nom de gammes
     */
    public boolean browseByGammeName;
    /**
     * Naviguer dans des dossiers portant le nom de l'année
     */
    public boolean browseByYear;

    /**
     * Constructeur
     *
     * @param aFileName Fichier des options
     */
    public Toptions(String aFileName) {
        try {
            this.loadFromFile(aFileName);
        } catch (IOException ex) {
            try {
                this.saveToFile(aFileName);
            } catch (IOException | IllegalArgumentException | IllegalAccessException ex1) {
                Logger.getLogger(Toptions.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }

    /**
     * @param gamme Gamme du chantier
     * @return le chemin du dossier de chantier selon la gamme et l'année
     */
    public final String getChantierPath(String gamme) {
        String s = defaultPath;
        if (browseByGammeName) {
            s += "\\GAMME " + gamme;
        }
        if (browseByYear) {
            Date dNow = new Date();
            SimpleDateFormat ft = new SimpleDateFormat("yyyy");
            s += "\\" + ft.format(dNow);
        }
        return s + "\\";
    }

}
