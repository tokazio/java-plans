/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import esyoLib.Formsfx.Tform;
import esyoLib.Formsfx.TformException;
import esyoLib.Graphics.Tpoint2D;
import evrLib.gc.TconfigurationEscalier;
import evrLib.gc.Tpotelet;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

/**
 * @author rpetit
 */
public class TvueEscFace extends TconfigVue2D {

    /**
     * @param c
     * @throws esyoLib.Formsfx.TformException
     */
    public TvueEscFace(TconfigUI c) throws TformException {
        super(c);
    }

    /**
     * @param c
     * @param zoom
     * @throws esyoLib.Formsfx.TformException
     */
    public TvueEscFace(TconfigUI c, String zoom) throws TformException {
        super(c, zoom);
    }

    /**
     * @return
     */
    @Override
    public String titleView() {
        return "Positionnement-face";
    }

    /**
     * @return
     */
    @Override
    public TconfigurationEscalier getConfig() {
        return (TconfigurationEscalier) super.getConfig();
    }

    /**
     *
     */
    @Override
    public void draw() {
        //largeur de la vue
        double w = getCanvasWidth();
        //hauteur de la vue
        double h = getCanvasHeight();
        //origine x
        double ox = 3 * (w / 4);
        //origine y
        double oy = 2 * (h / 6);
        //variables
        double a, b, c, d;
        double y, solbas;
        String t;
        //efface
        clear(Color.WHITE);
        //
        Tpoint2D p1, p2, p3, p4;
        //coeff écartement côtes
        int e = 50;
        //-----------------------------------------------------------------------
        style.normal();
        style.setFontColor(Color.GRAY);
        // bas
        //b = oy+z(getConfig().getCalculatedHauteurTotMarches()) + e;
        //----------------------------------------------------------------------
        // sol
        //----------------------------------------------------------------------
        //x de départ
        a = ox + z(Double.parseDouble(getConfig().getMarches().valueFromIndex(0)));
        /*
         if (getConfig().getIsPalier()) {
         y = oy - z(getConfig().getHauteurPalier());
         line(ox, oy, ox, y);
         line(ox, y, w, y);
         //y de départ
         b = oy;
         } else {
         */
        line(ox, oy, w, oy);
        //y de départ
        b = oy;
        //}
        //point de départ a,b        
        for (int i = 0; i < getConfig().getMarches().size(); i++) {
            if (getConfig().getMarches().valueFromIndex(i).isEmpty() || getConfig().getMarches().nameFromIndex(i).isEmpty()) {
                continue;
            }
            c = a - z(Double.parseDouble(getConfig().getMarches().valueFromIndex(i)));
            line(a, b, c, b);
            centerText("" + (i + 1), c + (z(Double.parseDouble(getConfig().getMarches().valueFromIndex(i))) / 2), b + (z(Double.parseDouble(getConfig().getMarches().nameFromIndex(i))) / 2));
            d = b + z(Double.parseDouble(getConfig().getMarches().nameFromIndex(i)));
            line(c, b, c, d);
            a = c;
            b = d;
        }
        //line(lx,ly,x, y);
        line(a, b, 0, b);
        solbas = b;
        //mc
        if (getConfig().getMainCourante()) {
            style.profile();
            y = getConfig().getHauteurFinie();
            rectangle(ox, oy - z(y), w, oy - z(y + getConfig().FHMC));
            d = getConfig().getCalculatedLongueurTotMarches();
            b = getConfig().getCalculatedTanA() * d;
            line(ox, oy - z(y), ox - z(d), oy - z(y - b));
            line(ox, oy - z(y - getConfig().FHMC), ox - z(d), oy - z(y - getConfig().FHMC - b));
        }
        //----------------------------------------------------------------------
        // axes
        //----------------------------------------------------------------------
        style.axe();
        //0
        line(ox, oy, ox, solbas);
        //potelets
        for (int i = 0; i < getConfig().getPotelets().size(); i++) {
            Tpotelet p = getConfig().getPotelet(i);
            line(ox - z(p.getPosition().x), solbas, ox - z(p.getPosition().x), oy + z(p.getPosition().y) - z(p.getLongueur()));
        }
        // traverses
        style.axe();
        if (!getConfig().getIsBal() && !getConfig().getVerreIsComplet()) {
            //épaisseur traverse
            c = getConfig().FHTRAV;
            if (getConfig().getTraversesCable()) {
                c = getConfig().FHCABLE;
            }
            y = getConfig().getDebordTravBas();
            d = getConfig().getCalculatedLongueurTotMarches();
            b = getConfig().getCalculatedTanA() * d;
            //traverses en bas (serre)
            for (int i = 0; i < getConfig().getNbrTravSerre() - 1; i++) {
                //palier
                line(ox, oy - z(y), w, oy - z(y));
                //escalier
                line(ox - z(d), oy - z(y - b), ox - z(0), oy - z(y));
                //suivante
                y += getConfig().getCalculatedEntraxeTraverseSerre();
            }
            //traverses en haut
            double f;
            if (getConfig().getNbrTravSerre() == 0) {
                f = getConfig().getNbrTraverses();
            } else {
                f = getConfig().getNbrTraverses() - getConfig().getNbrTravSerre() + 1;
            }
            for (int i = 0; i < f; i++) {
                //palier
                line(ox, oy - z(y), w, oy - z(y));
                //escalier
                line(ox - z(d), oy - z(y - b), ox - z(0), oy - z(y));
                //suivante
                y += getConfig().getCalculatedEntraxeTraverse();
            }
        }
        //----------------------------------------------------------------------
        // cotes
        //----------------------------------------------------------------------
        style.cote();
        //hauteur finie MC
        if (getConfig().getMainCourante()) {
            y = getConfig().getHauteurFinie();
            p1 = new Tpoint2D(ox + e * 3, oy);
            p2 = new Tpoint2D(ox + e * 3, oy - z(y));
            cotevt(p1, p2, getConfig().getHauteurFinie());
        }
        //potelets
        a = 0;
        b = 0;
        p1 = new Tpoint2D(ox - z(a), solbas + z(b));
        c = -getConfig().getDebPotelet();
        d = 0;
        p2 = new Tpoint2D(ox - z(c), solbas + z(d));
        for (int i = 0; i < getConfig().getPotelets().size(); i++) {
            Tpotelet p = getConfig().getPotelet(i);
            c = a;
            a = p.getPosition().x;
            p3 = new Tpoint2D(ox - z(a), solbas + z(b));
            cotehz(p2, p3, a - c);
            p2 = p3;
        }
        // cote largeur totale
        p1 = new Tpoint2D(ox, solbas + e);
        p2 = new Tpoint2D(ox - z(getConfig().getCalculatedLongueurTotMarches()), solbas + e);
        cotehz(p2, p1, getConfig().getCalculatedLongueurTotMarches());
        /*
        // cote développée 1
        p1 = new rpoint(getConfig().getCalculatedLongueurMarche(1), 0);
        p2 = new rpoint(getConfig().getCalculatedLongueurTotMarches(), getConfig().getCalculatedHauteurMarche(-1));
        d = Math.sqrt(Math.pow(p2.x - p1.x, 2) + Math.pow(p2.y - p1.y, 2));
        line(ox - z(p1.x), oy + z(p1.y), ox - z(p2.x), oy + z(p2.y));
        centerText("" + d, 400, 400);
        // cote développée 2
        p1 = new rpoint(0, 0);
        p2 = new rpoint(getConfig().getCalculatedLongueurTotMarches(), getConfig().getCalculatedHauteurTotMarches());
        d = Math.sqrt(Math.pow(p2.x - p1.x, 2) + Math.pow(p2.y - p1.y, 2));
        line(ox - z(p1.x), oy + z(p1.y), ox - z(p2.x), oy + z(p2.y));
        centerText("" + d, 400, 440);
        */
        //
        //centerText("" + (getConfig().getCalculatedLongueurTotMarches() / (Math.cos(Math.PI / 6))), 400, 420);
        // cote hauteur totale
        /*
         if(getConfig().getIsPalier())
         {
         p1 = new rpoint(z(2 * FespPotPrem), 0);
         p2 = new rpoint(z(2 * FespPotPrem), z(getHauteur(FcalculatedNbrMarches)));
         }
         else
         {
         p1 = new rpoint(z(2 * FespPotPrem), z(strToFloat(Fmarches.Names[0])));
         p2 = new rpoint(z(2 * FespPotPrem), solbas);
         }
         cotevt(p1, p2, round(FcalculatedHauteurTotMarches));
         */
        //----------------------------------------------------------------------
        // legende
        //----------------------------------------------------------------------
        y = 400;
        centerText("      h      l", 100, y);
        y = y + textHeight("q'");
        for (int i = 0; i < getConfig().getMarches().size(); i++) {
            t = "" + (i + 1) + ": " + getConfig().getMarches().nameFromIndex(i) + " " + getConfig().getMarches().valueFromIndex(i);
            centerText(t, 100, y);
            y = y + textHeight(t);
        }
    }

    /**
     * @param f
     */
    @Override
    public void onCreateForm(Tform f) {
        f.setPos(600, java.awt.GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds().height - getForm().getHeight()).setTitle("Perçages: vue de face").show();
    }

    @Override
    public void onDragged(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onClick(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onDblClick(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onPressed(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onReleased(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onMove(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
