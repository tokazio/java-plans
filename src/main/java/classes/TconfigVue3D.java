/*
 * Copyright (C) 2014 rpetit
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package classes;

import esyoLib.Formsfx.Tform;
import esyoLib.Formsfx.TformException;
import esyoLib.Graphics.Tvue3D;
import evrLib.gc.Gc3Dworld;
import evrLib.gc.Tconfig3D;
import evrLib.gc.Tconfiguration;
import evrLib.gc.TconfigurationPlat;
import interfaces.IconfigVue;
import plansevrfx.PlansEVRfx;

/**
 * @author rpetit
 */
public class TconfigVue3D extends Tvue3D implements IconfigVue {
    //
    private TconfigUI configUI;

    /**
     * @param aConfigurationUI
     * @throws esyoLib.Formsfx.TformException
     */
    public TconfigVue3D(TconfigUI aConfigurationUI) throws TformException {
        super();
        this.configUI = aConfigurationUI;
    }

    /**
     * @return
     */
    @Override
    public Tconfiguration getConfig() {
        return this.configUI.getConfig();
    }

    /**
     * @return
     */
    @Override
    public TconfigUI getConfigUI() {
        return this.configUI;
    }

    /**
     *
     */
    @Override
    public void draw() {
        //début du chrono
        long startTime = System.nanoTime();
        getWorld().clear();
        Tconfig3D ddd = null;
        try {
            ddd = new Tconfig3D(((TconfigurationPlat) getConfig()), PlansEVRfx.stlParts);
            getWorld().put(ddd.getForme(), 0, 0, 0, 0, 0, 0);
        } catch (Exception ex) {
            PlansEVRfx.application.showError(ex.getClass().getName(), ex.getMessage());
        }
        //
        double r = ((double) (System.nanoTime() - startTime) / 1000000000);
        System.out.println("Dessin 3D en " + r + "ms");
    }

    /**
     * @param f
     */
    @Override
    public void onCreateForm(Tform f) {
        //lie l'objet à la fenêtre
        this.configUI.attach(f.getController());
        //
        f.setObj(this);
        setWorld(new Gc3Dworld(f.getStage()));
        f.setPos(600, java.awt.GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds().height - getForm().getHeight()).setTitle("3D").show();
    }

    /**
     * @return
     */
    @Override
    public String titleView() {
        return "3D";
    }

}
