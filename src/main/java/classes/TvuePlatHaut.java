/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package classes;

import esyoLib.Formsfx.Tform;
import esyoLib.Formsfx.TformException;
import esyoLib.Graphics.Tpoint2D;
import evrLib.gc.TconfigurationPlat;
import javafx.scene.input.MouseEvent;

/**
 * @author rpetit
 */
public class TvuePlatHaut extends TconfigVue2D {
    /**
     * @param c
     * @throws esyoLib.Formsfx.TformException
     */
    public TvuePlatHaut(TconfigUI c) throws TformException {
        super(c);
    }

    /**
     * @return
     */
    @Override
    public String titleView() {
        return "Positionnement-dessus";
    }

    /**
     * @return
     */
    @Override
    public TconfigurationPlat getConfig() {
        return (TconfigurationPlat) super.getConfig();
    }

    /**
     * @param f
     */
    @Override
    public void onCreateForm(Tform f) {
        f.setPos(800, 550).setTitle("Positionnement: vue de dessus").show();
    }

    /**
     *
     */
    @Override
    public void draw() {
        //System.out.println("draw TvuePlatHaut");
        double w = getCanvasWidth();
        double h = getCanvasHeight();
        //efface
        getGC().clearRect(0, 0, w, h);
        Tpoint2D p1, p2;
        //estimation de la largeur
        double rm = ((getConfig().getCalculatedNbrPot() - 1) * getConfig().getCalculatedEntraxePotReel() + getConfig().getDebordPotGauche() + getConfig().getDebordPotDroite()) / 2;
        double m = z(rm);
        //point gauche
        double rd = -rm + getConfig().getDebordPotGauche();
        double d = z(rd);
        double rinvd = -m + getConfig().getCalculatedDebordPotGauche();
        double invd = z(rinvd);
        //point droite
        double rf = m - getConfig().getDebordPotDroite();
        double f = z(rf);
        double rinvf = m - getConfig().getCalculatedDebordPotDroite();
        double invf = z(rinvf);
        //SOL====================================================================
        style.normal();
        line(-m + w / 2, 150 + h / 2, -m + w / 2, 0 + h / 2);
        line(-m + w / 2, 0 + h / 2, m + w / 2, 0 + h / 2);
        line(m + w / 2, 0 + h / 2, m + w / 2, 150 + h / 2);
        //INFOS====================================================================
        centerText(getConfig().getNom(), w / 2, 10);
        centerText("Vue de dessus", w / 2, 30);
    }

    @Override
    public void onDragged(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onClick(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onDblClick(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onPressed(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onReleased(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onMove(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
