/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import esyoLib.Formsfx.Tform;
import esyoLib.Formsfx.TformException;
import evrLib.gc.TconfigNomenclature;
import plansevrfx.PlansEVRfx;

/**
 * @author rpetit
 */
public class TvueNomenclature extends TconfigVue {

    /**
     * @param aConfigurationUI
     * @throws esyoLib.Formsfx.TformException
     */
    public TvueNomenclature(TconfigUI aConfigurationUI) throws TformException {
        super(aConfigurationUI);
    }

    /**
     * @return
     */
    @Override
    public String titleView() {
        return "Nomenclature";
    }

    /**
     *
     */
    @Override
    public void draw() {
        //lie la tableview à la nomenclature
        getForm().getController().manualIni();
        //nomenclature selon la config
        TconfigNomenclature n = new TconfigNomenclature(getConfig(), getConfig().getNomenclature().clear(), PlansEVRfx.gescom);
        try {
            n.build();
        } catch (Exception ex) {
            PlansEVRfx.application.showError(ex);
        }
    }

    /**
     * @param f
     */
    @Override
    public void onCreateForm(Tform f) {
        super.onCreateForm(f);
        //
        f.setObj(this);
        //
        f.setPos(600, java.awt.GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds().height - getForm().getHeight()).setTitle("Nomenclature").show();
    }

    /**
     * Création de la fenêtre
     *
     * @return
     */
    @Override
    public Tform createForm() {
        //crée la fenêtre de dessin
        Tform f = PlansEVRfx.application.newForm("", "fxmls/nomenclatureForm.fxml", getConfigUI().getForm());
        setForm(f);
        //lie l'objet à la fenêtre
        getConfigUI().attach(getForm().getController());
        getForm().setObj(this);
        getForm().getController().manualIni();
        onCreateForm(getForm());
        return f;
    }
}
