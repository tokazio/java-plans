/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.DB;

import esyoLib.Utils.Tobject;

import java.lang.reflect.InvocationTargetException;
import java.sql.*;
import java.util.ArrayList;

/**
 * @author RomainPETIT
 */
public abstract class DBobj extends Tobject implements IDBobj {

    //Connexion au serveur

    //Liste des couples champs/valeurs
    private final ArrayList<DBset> properties = new ArrayList<>();
    //liste des PK
    private final ArrayList<String> pks = new ArrayList<>();
    //Affiche les messages dans la console
    private final boolean trace = false;
    //liste des champs
    private final ArrayList<String> fs = new ArrayList<>();
    //liste des FK
    private final ArrayList<String> fks = new ArrayList<>();
    protected DBserver server = null;
    //Table dans la db (à définir en héritage dans le constructeur)
    protected String table = "";
    //Données sur les champs du résultat
    private ResultSetMetaData rsmd = null;
    //à changé?
    private boolean changed = false;

    /**
     * @param aServer
     * @throws esyoLib.DB.DBException
     */
    public DBobj(DBserver aServer) throws DBException {
        if (aServer == null) {
            throw new DBException("DBobj::constructor Impossible de créer un objet sans connexion au serveur!");
        }
        if (this.trace) {
            System.out.println(this.getClass().getSimpleName() + "(DBobj)::constructor avec server=" + this.server);
        }
        this.server = aServer;
    }

    /**
     * Retourne true si fieldName est dans le ResultSet sinon retourne false
     *
     * @param rs        ResultSet
     * @param fieldName Un nom de champs
     * @return true si fieldName est dans le ResultSet sinon retourne false
     */
    protected static boolean hasField(ResultSet rs, String fieldName) {
        try {
            rs.findColumn(fieldName);
            return true;
        } catch (SQLException ex) {
            return false;
        }
    }

    /**
     * @param field
     * @return
     */
    public static DBvalueFactory linkCell(String field) {
        return new DBvalueFactory(field);
    }

    /**
     * Recharge l'objet depuis la db Le flag 'changed' et placé à false (l'objet
     * est à jour par rapport à la db)
     *
     * @return
     */
    public final DBobj reload() {
        if (this.changed) {
            load();
            this.changed = false;
        }
        return this;
    }

    /**
     * Crée la liste des couples champs/valeurs depuis le ResultSet passé en
     * paramètre
     *
     * @param rs un ResultSet
     * @return
     * @throws SQLException
     */
    public final DBobj set(ResultSet rs) throws SQLException {
        this.properties.clear();
        this.rsmd = rs.getMetaData();
        if (this.trace) {
            System.out.println("DBobj::set>set...");
        }
        for (int i = 1; i <= rsmd.getColumnCount(); i++) {
            if (this.trace) {
                System.out.println("DBobj::set>set " + rsmd.getColumnLabel(i) + " with " + rs.getString(i));
            }
            this.properties.add(new DBset(rsmd.getColumnLabel(i), rs.getString(i)));
        }
        if (this.trace) {
            System.out.println("DBobj::set>...");
        }
        return this;
    }

    /**
     * Défini une nouvelle valeur pour la clé Si la clé n'existe pas, elle est
     * ajoutée
     *
     * @param field
     * @param val
     * @return
     */
    public final DBobj set(String field, String val) {
        for (DBset propertie : this.properties) {
            try {
                if (propertie.getName().equals(field)) {
                    propertie.setValue(val);
                    return this;
                }
            } catch (DBException ex) {

            }
        }
        //
        this.properties.add(new DBset(field, val));
        return this;
    }

    /**
     * Ajoute une paire clé/valeur si la clé existe déjà, ne fait rien
     *
     * @param field
     * @param val
     * @return
     */
    public final DBobj add(String field, String val) {
        //vérifier si le field existe déjà, si oui, ne fait rien
        for (DBset propertie : this.properties) {
            try {
                if (propertie.getName().equals(field)) {
                    return this;
                }
            } catch (DBException ex) {

            }
        }
        //si non ajouter
        this.properties.add(new DBset(field, val));
        return this;
    }

    /**
     * Ajoute les clé/valeur d'un DBset Ne vérifie pas la pré existance des clés
     *
     * @param s
     * @return
     */
    public final DBobj add(DBset s) {
        this.properties.add(s);
        return this;
    }

    /**
     * Retourne la valeur selon le nom du champs
     *
     * @param aFieldName nom du champ
     * @return valeur selon le nom du champ
     */
    public final String get(String aFieldName) {
        if (aFieldName.contains(".")) {
            String[] s = aFieldName.split("\\.");
            String d = "";
            for (int i = 1; i < s.length; i++) {
                if (i > 1) {
                    d += ".";
                }
                d += s[i];
            }
            return getFrom(s[0], d);
        } else {
            for (DBset s : this.properties) {
                try {
                    if (s.getName().equals(aFieldName)) {
                        return s.getValue();
                    }
                } catch (DBException ex) {
                    return ex.getMessage();
                }
            }
            return "!?" + aFieldName + "?!";
        }
    }

    /**
     * Retourne la valeur selon le nom du champs
     *
     * @param aFieldName nom du champ
     * @return valeur selon le nom du champ
     */
    public final String getEscape(String aFieldName) {
        String s = get(aFieldName);
        try {
            if (s.contains("'")) {
                s = s.replace("'", "\\'");
            }
        } catch (Exception e) {

        }
        return s;
    }

    /**
     * @param aListName
     * @param aFieldName
     * @return
     */
    public abstract String getFrom(String aListName, String aFieldName);

    /**
     * Envoi la requête au serveur et consrtuit la liste des coulples
     * champs/valeurs avec la réponse
     *
     * @param SQL Texte de la requête
     * @return
     * @throws SQLException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     * @throws InvocationTargetException
     * @throws NoSuchMethodException
     * @throws ClassNotFoundException
     */
    public final DBobj query(String SQL) throws SQLException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, ClassNotFoundException {
        if (trace) {
            System.out.println("DBobj::query(" + SQL + ")");
        }
        ResultSet rs = this.server.getConnection().createStatement().executeQuery(SQL);
        try {
            rs.first();
            set(rs);
        } catch (SQLException ex) {
            System.out.println("SQL Error: " + SQL + "\n" + ex.getMessage());
        }
        return this;
    }

    /**
     * Renvoi une chaîne avec les couples champs/valeurs résultats séparés par ,
     *
     * @return chaîne avec les couples champs/valeurs résultats séparés par ,
     */
    @Override
    public String toString() {
        String s = "";
        s = this.properties.stream().map((propertie) -> propertie.toString() + ",").reduce(s, String::concat);
        return s;
    }

    /**
     * INSERT les propriétés dans Ftable
     *
     * @return TRUE si l'insert s'est bien passé, sinon false
     */
    public final boolean insert() {
        String SQL = "";
        PreparedStatement q = null;
        try {
            SQL = "INSERT INTO " + this.table + "(";
            String t = "";
            for (int i = 0; i < this.properties.size(); i++) {
                SQL += "`" + this.properties.get(i).getName() + "`";
                t += "?";
                if (i < this.properties.size() - 1) {
                    SQL += ",";
                    t += ",";
                }
            }
            SQL += ") VALUES(" + t + ");";
            q = this.server.getConnection().prepareStatement(SQL);
            for (int i = 0; i < this.properties.size(); i++) {
                if (this.properties.get(i).getValue().isEmpty() || this.properties.get(i).getValue().toLowerCase().equals("null")) {
                    q.setNull(i + 1, Types.VARCHAR);
                } else {
                    q.setString(i + 1, this.properties.get(i).getValue());
                }
            }
            q.executeUpdate();
        } catch (DBException | SQLException ex) {
            System.out.println("DBobj::insert->Error lors de '" + SQL + "'\n" + ex.getMessage());
            return false;
        } finally {
            try {
                q.close();
            } catch (SQLException ex) {
                System.out.println("DBobj::Close query error lors de '" + SQL + "'\n" + ex.getMessage());
            }
        }
        return true;
    }

    /**
     * UPDATE les propriétés dans Ftable
     *
     * @return TRUE si l'update s'est bien passé, sinon false
     */
    public final boolean update() {
        //System.out.println(pks.toString());
        String SQL = "";
        PreparedStatement q = null;
        try {
            SQL = "UPDATE " + this.table + " SET ";
            String t = "";
            for (int i = 0; i < this.properties.size(); i++) {
                if (this.fs.contains(this.properties.get(i).getName()) && !this.pks.contains(this.properties.get(i).getName()) && !this.properties.get(i).getValue().equals("null")) {
                    SQL += "`" + this.properties.get(i).getName() + "`='" + this.properties.get(i).getValue() + "'";
                    if (i < this.properties.size() - 1) {
                        SQL += ",";
                    }
                }
            }
            SQL += " WHERE ";
            //pks
            for (int i = 0; i < this.pks.size(); i++) {
                SQL += "`" + this.pks.get(i) + "`='" + get(this.pks.get(i)) + "'";
                if (i < this.pks.size() - 1) {
                    SQL += ",";
                }
            }
            if (trace) {
                System.out.println(SQL);
            }
            //
            this.server.getConnection().createStatement().executeUpdate(SQL);
        } catch (DBException | SQLException ex) {
            System.out.println("DBobj::update->Error lors de '" + SQL + "'\n" + ex.getMessage());
            return false;
        }
        this.changed = true;
        return true;
    }

    /**
     * @param aTableName
     * @throws esyoLib.DB.DBTableException
     */
    public final void setTable(String aTableName) throws DBTableException {
        this.table = aTableName;
        this.pks.clear();
        this.fks.clear();
        this.fs.clear();
        try {
            ResultSet rs = this.server.getConnection().createStatement().executeQuery("SHOW INDEX FROM " + this.table + " WHERE key_name='PRIMARY'");
            rs.beforeFirst();
            while (rs.next()) {
                this.pks.add(rs.getString("column_name"));
            }
        } catch (SQLException ex) {
            throw new DBTableException("DBobj::setTable() Erreur à la récupération des clées primaires pour " + this.table + ". \n" + ex.getMessage());
        }
        //récupérer les FK
        try {
            ResultSet rs = this.server.getConnection().createStatement().executeQuery("SHOW INDEX FROM " + this.table + " WHERE key_name<>'PRIMARY'");
            rs.beforeFirst();
            while (rs.next()) {
                this.fks.add(rs.getString("column_name"));
            }
        } catch (SQLException ex) {
            throw new DBTableException("DBobj::setTable() Erreur à la récupération des clées éteangères pour " + this.table + ". \n" + ex.getMessage());
        }
        //récupère les champs
        try {
            ResultSet rs = this.server.getConnection().createStatement().executeQuery("SHOW COLUMNS FROM " + this.table);
            rs.beforeFirst();
            while (rs.next()) {
                this.fs.add(rs.getString("field"));
            }
        } catch (SQLException ex) {
            throw new DBTableException("DBobj::setTable() Erreur à la récupération des champs pour " + this.table + ". \n" + ex.getMessage());
        }
    }

    /**
     *
     */
    protected final void clear() {
        this.properties.clear();
    }
}
