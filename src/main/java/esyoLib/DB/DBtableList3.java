/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.DB;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @param <T>
 * @author RomainPETIT
 */
public abstract class DBtableList3<T extends DBobj3> {

    //
    private static DBserver con = null;
    //
    //Liste des lignes de données (observées par la vue)
    private final ObservableList<T> data;
    //
    private Class classe = null;
    //
    private TableView<T> view = null;
    //
    private HashMap<String, String> filters = new HashMap();
    //
    private Callback<TableColumn, TableCell> cellFactory
            = new Callback<TableColumn, TableCell>() {
        @Override
        public TableCell call(TableColumn p) {
            TableCell cell = new TableCell<Object, String>() {
                @Override
                public void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(empty ? null : getString());
                    setGraphic(null);
                }

                private String getString() {
                    return getItem() == null ? "" : getItem();
                }
            };

            cell.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    if (event.getClickCount() > 1) {
                        //System.out.println("double clicked!");
                        //TableCell c = (TableCell) event.getSource();
                        //System.out.println("Cell text: " + c.getText());
                        onDblClick(view.getSelectionModel().getSelectedItem());
                    }
                }
            });
            return cell;
        }
    };

    /**
     * @param aTableView
     * @param aClass
     */
    public DBtableList3(TableView<T> aTableView, Class<? extends DBobj3> aClass) {
        //Initialise la liste de donnée
        this.data = FXCollections.observableArrayList();
        //Mémorise la vue choisie
        this.view = aTableView;
        //lie les données à la vue
        this.view.setItems(this.data);
        //Mémorise la classe des objets des lignes
        this.classe = aClass;
        //Prépare la 1ère requête
        Object o = null;
        try {
            o = aClass.newInstance();
            //DBtable3 t = ((DBobj3) o).getTable();
            //Fait la requête
            show(((DBobj3) o).list());
            //select(selectString(t.getName()));
        } catch (InstantiationException | IllegalAccessException | SQLException ex) {
            System.out.println("DBtableList3::Constructeur->" + ex.getClass().getName() + "\n" + ex.getMessage() + "n'a peut être pas de constructeur sans paramètre de défini.");
        } catch (Exception ex) {
            System.out.println("DBtableList3::Constructeur->" + ex.getClass().getName() + "\n" + ex.getMessage());
        }

    }

    /**
     * @param aCon
     */
    public static void iniCon(DBserver aCon) {
        con = aCon;
    }

    /**
     * @param s
     * @return
     * @throws SQLException
     * @throws esyoLib.DB.DBException
     */
    @Deprecated
    public ResultSet select(String s) throws SQLException, DBException {
        DBobj3 o = null;
        try {
            o = (DBobj3) this.classe.newInstance();
            ResultSet rs = o.select(s);
            show(rs);
            return rs;
        } catch (InstantiationException | IllegalAccessException | SQLException ex) {
            System.out.println("DBlist3::select->" + ex.getClass().getName() + "\n" + ex.getMessage());
            return null;
        }
    }

    /**
     * @param s
     */
    public void filter(String s) {
        Object o = null;
        try {
            o = this.classe.newInstance();
        } catch (InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(DBtableList3.class.getName()).log(Level.SEVERE, null, ex);
            return;
        }
        String q = "";
        //récupère le SELECT complet depuis le T(DBobj3)
        try {
            q = ((DBobj3) o).getListString();
        } catch (Exception ex) {
            Logger.getLogger(DBtableList3.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("Filter---------");
        System.out.println("q=" + q);
        int whereEndPos = -1;
        boolean hasGROUPBY = q.lastIndexOf("GROUP BY ") >= 0;
        boolean hasORDERBY = q.lastIndexOf("ORDER BY ") >= 0;
        int wherePos = q.lastIndexOf("WHERE ");
        //si GROUP BY, je me positionne devant
        //sinon si ORDER BY, je me positionne devant
        //sinon je me positionne à la fin
        if (hasGROUPBY) {
            whereEndPos = q.lastIndexOf("GROUP BY ");
        } else {
            if (hasORDERBY) {
                whereEndPos = q.lastIndexOf("ORDER BY ");
            } else {
                whereEndPos = q.length();
            }
        }
        //si pas de WHERE, wherePos et whereEndPos = la fin du SELECT ? FROM ? JOIN ?
        if (wherePos < 0) {
            wherePos = whereEndPos;
        }
        //* indique la position 'whereEndPos' selon les cas
        //                           |<- wherePos
        //                                      |<-whereEndPos
        //           a               |     b    |       c (reste)
        //    SELECT ? FROM ? JOIN ? | WHERE ?  | *GROUP BY ? ORDER BY ?
        //    SELECT ? FROM ? JOIN ? | WHERE ?  | *ORDER BY ?
        //    SELECT ? FROM ? JOIN ? | WHERE ?* |
        //(*) SELECT ? FROM ? JOIN ? |          |

        //par défaut, je garde le SELECT (a) et pas de reste (b)
        String a = q;
        String b = "";
        String c = "";
        //

        //je garde le SELECT ? FROM ? JOIN ?
        a = q.substring(0, wherePos);
        //le WHERE
        if (whereEndPos > wherePos) {
            b = q.substring(wherePos + 6, whereEndPos);
        }
        // après le where
        c = q.substring(whereEndPos);


        System.out.println("a=" + a);
        System.out.println("b=" + b);
        System.out.println("c=" + c);

        //si j'ai un filtre
        if (!s.isEmpty()) {
            if (b.isEmpty()) {
                b = "WHERE " + s;
            } else {
                b += "AND " + s;
            }
        }
        //filtres permanents (si il y en a)--
        if (this.filters.size() > 0) {
            String t = "";
            if (this.filters.size() > 0) {
                for (Entry<String, String> e : filters.entrySet()) {
                    t += " AND " + e.getKey() + "='" + e.getValue() + "'";
                }
                t = t.substring(5);
            }
            if (b.isEmpty()) {
                b = "WHERE " + t;
            } else {
                b += "AND " + t;
            }
        }
        //------------------
        //a:SELECT ? FROM ? JOIN ?
        //b:WHERE ?
        //c:GROUP BY ? / ORDER BY ?
        q = a + " " + b + " " + c;
        System.out.println("q=" + q);
        System.out.println("---------");
        this.data.clear();
        try {
            show(((DBobj3) o).select(q));
        } catch (Exception ex) {
            System.out.println("DBlist3::filter->" + ex.getClass().getName() + "\n" + ex.getMessage());
        }
    }

    public void show(ResultSet rs) throws SQLException {
        makeCols(rs.getMetaData());
        makeRows(rs);
    }

    private void makeRows(ResultSet rs) throws SQLException {
        rs.beforeFirst();
        while (rs.next()) {
            //ajoute la ligne au données
            //rs n'est pas directement passé car détruit à la fermeture de la requête ou de la connection
            //copié vers un objet du type T
            T o = newObj(rs);
            this.data.add(o);
        }
    }

    //
    private T newObj(ResultSet rs) {
        Class[] types = {ResultSet.class};
        Constructor constructor = null;
        try {
            constructor = this.classe.getConstructor(types);
        } catch (NoSuchMethodException | SecurityException ex) {
            System.out.println("DBlist3::newObj->" + ex.getClass().getName() + "\n" + ex.getMessage());
        }
        Object[] args = new Object[1];
        args[0] = rs;
        Object instanceOfMyClass = null;
        try {
            instanceOfMyClass = constructor.newInstance(args);
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            System.out.println("DBlist3::newObj->" + ex.getClass().getName() + "\n" + ex.getMessage());
        }
        return (T) instanceOfMyClass;
    }

    //N'affichera pas les champs commençant par 'pk_' ou 'fk_' ni sql_table
    private void makeCols(ResultSetMetaData rsmd) throws SQLException {
        //efface les colones de la vue
        this.view.getColumns().clear();
        //colone vide
        TableColumn t;
        /*
         //si une ico est en paramètre
         if (!this.ico.isEmpty()) {
         //nouvelle colone
         t = new TableColumn("");
         //de type 'icone'
         t.setCellValueFactory(new IcoValueFactory());
         //défini l'image dans la case
         t.setCellFactory(new IcoCellFactory(Tgraphics.image(this.ico)));
         //ajoute la colone à la vue
         view.getColumns().add(t);
         }
         */
        //pour chaque champs de l'en-tête
        int m = rsmd.getColumnCount();
        for (int i = 1; i <= m; i++) {
            if (rsmd.getColumnLabel(i).indexOf("pk_") != 0) {// && rsmd.getColumnLabel(i).indexOf("fk_")!=0) && !rsmd.getColumnLabel(i).equals("sql_table")){
                //nouvelle colone
                t = new TableColumn(rsmd.getColumnLabel(i));
                //de type 'champs'
                t.setCellValueFactory(new DBvalueFactory3(rsmd.getColumnLabel(i)));
                //détection du double clic pour chaques cellules
                t.setCellFactory(this.cellFactory);
                //ajoute la colone à la vue
                this.view.getColumns().add(t);
            }
        }
    }

    /**
     * Fonction à éxécuté lors du changement de selection dans la vue
     * (TableView)
     *
     * @param o
     */
    public abstract void onDblClick(T o);

    //public abstract String selectString(String tableName);

    /**
     * $f la valeur du filtre
     *
     * @param aTextField
     * @param aFilter
     */
    public void addFilter(TextField aTextField, String aFilter) {
        DBtableList3 liste = this;
        aTextField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String filtre) {
                String f;
                if (filtre.equals("*") || filtre.isEmpty()) {
                    f = "";
                } else {
                    f = aFilter.replace("$f", filtre);
                }
                liste.filter(f);
            }
        });
    }

    /**
     * Ajoute des paires champs/valeur qui seront utilisé dans le WHERE de la
     * requête appelé dans load()
     *
     * @param aFieldName Nom du champs
     * @param aValue     Sa valeur
     * @return
     */
    public DBtableList3 addFilter(String aFieldName, String aValue) {
        this.filters.put(aFieldName, aValue);
        this.filter("");
        return this;
    }

    /**
     * Efface les filtres
     *
     * @return
     */
    public DBtableList3 clearFilter() {
        this.filters.clear();
        return this;
    }

}
