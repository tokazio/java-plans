/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package esyoLib.DB;

/**
 * @author romainpetit
 */
public class DBException extends Exception {

    /**
     *
     */
    public DBException() {
        super();
    }

    /**
     * @param s Message de l'exception
     */
    public DBException(String s) {
        super(s);
    }
}
