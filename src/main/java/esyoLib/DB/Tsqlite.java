/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.DB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * https://tokazio@bitbucket.org/xerial/sqlite-jdbc
 *
 * @author romainpetit
 */
public class Tsqlite extends DBserver {
    //Afficher les informations
    private static final boolean trace = false;

    /**
     * Nombre de résultat de la dernière requête SELECT
     */
    private static int nbresults = 0;

    /**
     * @param database
     * @throws Exception
     */
    public Tsqlite(String database) throws Exception {
        super("", "", database, "", "", false);
    }

    /**
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    @Override
    public void load() throws InstantiationException, IllegalAccessException, ClassNotFoundException {
        //chargement du pilote sqlite
        Class.forName("org.sqlite.JDBC").newInstance();
    }

    /**
     * @param connected
     * @throws DBconnexionException
     */
    @Override
    public void afterConnect(boolean connected) throws DBconnexionException {
        if (Tsqlite.trace) {
            if (connected) {
                System.out.println("Connecté à sqLite.");
            } else {
                System.out.println("NON connecté à sqLite!");
            }
        }
    }

    /**
     * @return
     */
    @Override
    public String makeString() {
        return "jdbc:sqlite:" + getDatabase();
    }

    /**
     * @return
     * @throws DBconnexionException
     * @throws SQLException
     */
    @Override
    public Connection connexion() throws DBconnexionException, SQLException {
        String s = makeString();
        if (Tsqlite.trace) {
            System.out.println(s);
        }
        return DriverManager.getConnection(s);
    }

    /**
     * @param aSQLquery
     * @return
     * @throws SQLException
     * @throws DBException
     */
    @Override
    public ResultSet select(String aSQLquery) throws SQLException, DBException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @param aSQLquery
     * @return
     * @throws SQLException
     */
    @Override
    public boolean update(String aSQLquery) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @return
     */
    @Override
    public int getResultsCount() {
        return Tsqlite.nbresults;
    }
}
