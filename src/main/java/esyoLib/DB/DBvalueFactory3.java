/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.DB;

import javafx.beans.NamedArg;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TableColumn;
import javafx.util.Callback;

/**
 * @param <S>
 * @param <T>
 * @author rpetit
 */
public class DBvalueFactory3<S, T> implements Callback<TableColumn.CellDataFeatures<S, T>, ObservableValue<T>> {
    //
    private final String field;

    /**
     * @param field
     */
    public DBvalueFactory3(@NamedArg("field") String field) {
        this.field = field;
    }

    /**
     * @param param
     * @return
     */
    @Override
    public ObservableValue<T> call(TableColumn.CellDataFeatures<S, T> param) {
        return getCellData((T) param.getValue());
    }

    //
    private ObservableValue<T> getCellData(T rowData) {
        return (ObservableValue<T>) new SimpleStringProperty(((DBobj3) rowData).get(field));
    }

}
