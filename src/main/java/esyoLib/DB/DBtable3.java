/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.DB;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * @author rpetit
 */
public class DBtable3 {

    //Connection du DBserver
    private static DBserver con = null;
    //Liste des clés primaires
    private final ArrayList<String> pkList = new ArrayList();
    //Liste des champs
    private final ArrayList<String> dbFieldList = new ArrayList();
    //Liste des clés étrangères
    private final ArrayList<String> fkList = new ArrayList();
    //Nom de la table
    private String table = "";
    //Affiche les informations
    private Boolean trace = false;
    //
    //private static ChainedList<String> fields = new ChainedList();
    //
    //private static ChainedList<String> joins = new ChainedList();
    //
    //private static ChainedList<String> orders = new ChainedList();

    /**
     * Constructeur PRIVE
     * <p>
     * Utiliser DBtable3.create("*nom-table*",new DBtable3createCallBack()) pour instancier une table.
     * <p>
     * Ceci car la table doit être définie dans un champs static et ce constructeur renvois des erreures qu'il faut gérer.
     * Cela est donc fait danc la fonction statique 'create' qui appellera une fonction de callback pour la gestion de l'erreur.
     * <p>
     * Objet représentant un table d'une base de donnée. Permet d'utiliser une
     * seule connection statique pour cette table. Liste les clés primaires et
     * étrangère ansi que tout les champs. Permet de faire un SELECT sur la
     * table, il sera automatiquement ajouté les clés primaires au champs du
     * résultats sous la forme *table*.pk_*nom-clé*
     * <p>
     * SELECT ? FROM ? JOIN ? ON ? WHERE ? GROUP BY ? ORDER BY ? sera
     * automatiquement transformé en SELECT ?,*table*.pk_? FROM ? JOIN ? ON ?
     * WHERE ? GROUP BY ? ORDER BY ?
     * <p>
     * en mode trace, le nombre de résultat et le temps de la requête sont
     * affiché dans la console
     *
     * @param aTableName Un nom de table
     * @throws java.sql.SQLException
     * @throws esyoLib.DB.DBException
     */
    private DBtable3(String aTableName) throws SQLException, DBException {
        if (aTableName.isEmpty()) {
            throw new DBException("Le nom de la table ne peut pas être vide!");
        }
        this.table = aTableName;
        setPK();
    }

    /**
     * Permet de créer l'instance d'une table en gérant les exceptions.
     *
     * @param aTableName Nom de la table
     * @param aCallBack  Fonction appelée en cas d'erreur (lui est passé l'objet Exception)
     * @return Nouvelle instance de DBtable3
     */
    public static DBtable3 create(String aTableName, DBtable3createCallBack aCallBack) {
        DBtable3 t;
        try {
            t = new DBtable3(aTableName);
        } catch (SQLException | DBException ex) {
            aCallBack.error(ex);
            return null;
        }
        return t;
    }

    /**
     * Initialise la connection à la base de donnée pour cette table
     *
     * @param aCon Une connection DBserver
     */
    public static void iniCon(DBserver aCon) {
        con = aCon;
    }

    /**
     * @return la connection DBserver
     * @throws esyoLib.DB.DBException si pas de connection
     */
    public static DBserver getCon() throws DBException {
        if (DBtable3.con == null) {
            throw new DBException("Vous devez initialiser la classe avec une connexion de type DBserver.");
        }
        return DBtable3.con;
    }

    //Remplit les listes des clés primaires et étrangères
    private void setPK() throws SQLException, DBException {
        //String q = "SELECT column_name AS pk FROM information_schema.key_column_usage WHERE  table_schema = SCHEMA() AND constraint_name = 'PRIMARY' AND table_name = '" + getTable() + "'";
        String q = "SHOW COLUMNS FROM " + getName();
        //long startTime = System.nanoTime();
        ResultSet rs = getCon().getConnection().createStatement().executeQuery(q);
        //double r = ((double) (System.nanoTime() - startTime) / 1000000000);
        //System.out.println("Recherche PK en " + r + "ms (" + q + ")");
        while (rs.next()) {
            switch (rs.getString("Key")) {
                case "PRI":
                    this.pkList.add(rs.getString("Field"));
                    break;
                case "MUL":
                    this.fkList.add(rs.getString("Field"));
                    break;
            }
            this.dbFieldList.add(rs.getString("Field"));
        }
    }

    /**
     * Affiche les champs clés primaires de la table dans la console
     */
    public void printPK() {
        System.out.println("=PK pour " + getName() + "======================================================================");
        this.pkList.stream().forEach((e) -> {
            System.out.println(e);
        });
        System.out.println("=======================================================================");
    }

    /**
     * Affiche les champs clés étrangères de la table dans la console
     */
    public void printFK() {
        System.out.println("=FK pour " + getName() + "======================================================================");
        this.fkList.stream().forEach((e) -> {
            System.out.println(e);
        });
        System.out.println("=======================================================================");
    }

    /**
     * @return la liste des champs sous forme de ArrayList de String
     */
    public ArrayList<String> getDBFields() {
        return this.dbFieldList;
    }

    /**
     * Affiche les champs de la table dans la console
     */
    public void printFIELDS() {
        System.out.println("=Champs pour " + getName() + "======================================================================");
        this.dbFieldList.stream().forEach((e) -> {
            System.out.println(e);
        });
        System.out.println("=======================================================================");
    }

    /**
     * @return le nom de la table
     */
    public String getName() {
        return this.table;
    }

    /**
     * Execute la requête de DELETE
     *
     * @param s La requête commençant par 'DELETE '
     * @return True si l'opération à réussi
     * @throws DBException           La requête ne commence pas par DELETE ou une erreur est
     *                               survenue
     * @throws java.sql.SQLException Erreur SQL
     */
    public boolean delete(String s) throws DBException, SQLException {
        if (s.indexOf("DELETE ") <= 0) {
            throw new DBException("Cette requête: " + s + " n'est pas un DELETE!");
        }
        getCon().getConnection().createStatement().executeUpdate(s);
        return true;
    }

    /**
     * Effectue la requête et renvoi le resultSet Les clés PRIMARY seront
     * ajoutées avec leur nom précédée de 'pk_dans l'alias La table sera ajoutée
     * avec l'alias 'sql_table' Si la requête n'est pas un 'select', une erreur
     * est levée Chronomètre la requête finale
     * <p>
     * <p>
     * SELECT b.avancement AS Avancement, a.indice AS Indice,
     * DATE_FORMAT(a.date,'%d/%m/%Y') AS Date, a.modifications AS Modification,
     * b.nom AS `Nom pièce`, b.num AS `Num pièce`, b.etat AS `Etat pièce`, c.nom
     * AS `Nom projet`, c.num AS `Num projet`, c.etat AS `Etat projet`,
     * DATE_FORMAT((SELECT d.date,num AS pk_num FROM indice d WHERE
     * d.numpiece=b.num AND d.indice='A'),'%d/%m/%Y') AS `Date de création` FROM
     * indice a LEFT JOIN piece b ON a.numpiece=b.num LEFT JOIN projet c ON
     * b.numprojet=c.num WHERE a.numpiece=573 ORDER BY a.indice desc
     *
     * @param s Requête SQL
     * @return ResultSet
     * @throws SQLException           Erreur SQL ou de connection
     * @throws esyoLib.DB.DBException La requête n'est pas valide
     */
    public ResultSet select(String s) throws SQLException, DBException {
        //si la requête ne commence pas par SELECT
        if (s.indexOf("SELECT") != 0) {
            throw new DBException("Cette requête: " + s + " n'est pas un SELECT!");
        }
        //Position du dernier FROM dans la requête
        int p = s.lastIndexOf("FROM ") + 5;
        int i = p;
        //PK vers String du type pkfield AS pk_field de toutes les PK séparées par ,
        //pour ajouter au SELECT de la requête demandée
        ArrayList<String> a = new ArrayList();
        this.pkList.stream().forEach((f) -> {
            a.add(this.table + "." + f + " AS pk_" + f);
            //a.add(f + " AS pk_" +f);
        });
        String pk = "";
        pk = a.stream().map((t) -> ',' + t).reduce(pk, String::concat);
        if (!pk.isEmpty()) {
            pk = pk.substring(1);
            pk = "," + pk;
        }
        //reformule la requête avec les PK
        String s2 = s.substring(0, i - 6) + pk + " FROM " + s.substring(p);
        //requête
        long startTime = System.nanoTime();
        ResultSet rs = getCon().getConnection().createStatement().executeQuery(s2);
        double r = ((double) (System.nanoTime() - startTime) / 1000000000);
        int c = 0;
        while (rs.next()) {
            c++;
        }
        if (this.trace) {
            System.out.println(this.table + " select: " + c + " résultats en " + r + "ms: " + s2);
        }
        rs.first();
        return rs;
    }

    /**
     * Afficher les informations
     *
     * @param trace True ou False
     */
    public void setTrace(Boolean trace) {
        this.trace = trace;
    }

}
