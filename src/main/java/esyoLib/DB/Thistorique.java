package esyoLib.DB;


import javafx.beans.property.SimpleStringProperty;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * @author RomainPETIT
 */
public class Thistorique {
    private final SimpleStringProperty Ftable;
    private final SimpleStringProperty Ftype;

    public Thistorique(String type, String table) {
        Ftable = new SimpleStringProperty(table);
        Ftype = new SimpleStringProperty(type);
    }

    public String getTable() {
        return Ftable.get();
    }

    public void setTable(String table) {
        Ftable.set(table);
    }

    public String getType() {
        return Ftype.get();
    }

    public void setType(String type) {
        Ftype.set(type);
    }

}
