/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.DB;

/**
 * @author RomainPETIT
 */
public class DBTableException extends Exception {

    /**
     *
     */
    public DBTableException() {
        super();
    }

    /**
     * @param s Message de l'exception
     */
    public DBTableException(String s) {
        super(s);
    }
}
