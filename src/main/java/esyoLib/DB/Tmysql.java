/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.DB;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Etend la classe DBserver pour fournir les méthodes/fonctions pour une connexion à un serveur mySQL.
 *
 * @author rpetit
 */
public class Tmysql extends DBserver {
    /**
     * Afficher les informations
     */
    private static final boolean trace = false;

    /**
     * Nombre de résultat de la dernière requête SELECT
     */
    private static int nbresults = 0;

    /**
     * Constructeur
     * <p>
     * Voir DBserver
     *
     * @param server
     * @param port
     * @param database
     * @param user
     * @param password
     * @param SSL
     * @throws java.lang.ClassNotFoundException
     * @throws java.lang.InstantiationException
     * @throws java.sql.SQLException
     * @throws java.lang.IllegalAccessException
     */
    public Tmysql(String server, String port, String database, String user, String password, boolean SSL) throws Exception {
        super(server, port, database, user, password, SSL);
    }

    /**
     * Constructeur
     * <p>
     * Voir DBserver
     *
     * @param filename Fichier de configuration (clé=valeur)
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws SQLException
     * @throws IllegalAccessException
     */
    public Tmysql(String filename) throws Exception {
        super(filename);
    }

    /**
     * Charge le driver mysql
     *
     * @throws java.lang.ClassNotFoundException
     * @throws java.lang.InstantiationException
     * @throws java.lang.IllegalAccessException
     */
    @Override
    public void load() throws InstantiationException, IllegalAccessException, ClassNotFoundException {
        //chargement du pilote mysql
        Class.forName("com.mysql.jdbc.Driver").newInstance();
    }

    /**
     * Crée la chaîne de connection du driver mySQL
     *
     * @return chaîne de connection du driver mySQL
     */
    @Override
    public String makeString() {
        //"jdbc:mysql://"+Fserver+":"+Fport+"/"+Fdatabase+"?user="+Fuser+"&password="+Fpassword
        String c = "jdbc:mysql://" + getServer();
        if (getPort().length() > 0) {
            c = c + ":" + getPort();
        }
        c = c + "/" + getDatabase() + "?user=" + getUser();
        if (getPassword().length() > 0) {
            c = c + "&password=" + getPassword();
        }
        return c;
    }

    /**
     * @return
     * @throws esyoLib.DB.DBconnexionException @throws Exception
     * @throws java.sql.SQLException
     */
    @Override
    public Connection connexion() throws DBconnexionException, SQLException {
        String s = makeString();
        if (Tmysql.trace) {
            System.out.println(s);
        }
        return DriverManager.getConnection(s);
    }

    /**
     * Fonction UPDATE
     * <p>
     * en mode trace, affiche le temps de réponse du serveur
     *
     * @param aSQLquery Requête SQL
     * @return True si l'opération s'est bien passée
     * @throws SQLException Erreur SQL
     */
    @Override
    public final boolean update(String aSQLquery) throws SQLException {
        long startTime = System.nanoTime();
        int i = getConnection().createStatement().executeUpdate(aSQLquery);
        double r = ((double) (System.nanoTime() - startTime) / 1000000000);
        if (Tmysql.trace) {
            System.out.println("Update en " + r + "ms (" + aSQLquery + ")");
        }
        Tmysql.compteur++;
        return i == 0;
    }

    /**
     * Effectue la requête et renvoi le resultSet
     * <p>
     * En mode trace, affiche le nombre de résultat et le temps de réponse de la requête
     *
     * @param s Requête SQL
     * @return ResultSet
     * @throws SQLException
     * @throws esyoLib.DB.DBException
     */
    @Override
    public final ResultSet select(String s) throws SQLException, DBException {
        //si la requête ne commence pas par SELECT
        if (s.indexOf("SELECT") != 0) {
            throw new DBException("Cette requête: " + s + " n'est pas un SELECT!");
        }
        //requête
        long startTime = System.nanoTime();
        ResultSet rs = getConnection().createStatement().executeQuery(s);
        double r = ((double) (System.nanoTime() - startTime) / 1000000000);
        Tmysql.nbresults = 0;
        while (rs.next()) {
            Tmysql.nbresults++;
        }
        if (Tmysql.trace) {
            System.out.println(Tmysql.nbresults + " résultats en " + r + "ms: " + s);
        }
        rs.beforeFirst();
        Tmysql.compteur++;
        return rs;
    }

    /**
     * Appelé après la connexion au serveur
     *
     * @throws DBconnexionException
     */
    @Override
    public void afterConnect(boolean connected) throws DBconnexionException {
        if (Tmysql.trace) {
            if (connected) {
                System.out.println("Connecté à mySQL.");
            } else {
                System.out.println("NON connecté à mySQL!");
            }
        }
    }

    /**
     * @return
     */
    @Override
    public int getResultsCount() {
        return Tmysql.nbresults;
    }

}
