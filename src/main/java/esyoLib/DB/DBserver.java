/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.DB;

import esyoLib.Exceptions.NoNameException;
import esyoLib.Utils.TstringList;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

/**
 * Fournis les méthodes/fonctions par défaut pour une connexion à un serveur de
 * base de donnée.
 * <p>
 * Compte le total des requêtes sur la connexion.
 *
 * @author rpetit
 */
public abstract class DBserver {

    /**
     * compteur de requête
     */
    protected static int compteur = 0;
    /**
     *
     */
    protected Connection connection = null;
    /**
     * nom du serveur
     */
    private String server = "";
    /**
     * port
     */
    private String port = "";
    /**
     * nom de la base de donnée
     */
    private String database = "";
    /**
     * Utilisateur
     */
    private String user = "";
    /**
     * Mot de passe
     */
    private String password = "";
    /**
     * SSL
     */
    private boolean SSL = false;

    /**
     * Constructeur
     * <p>
     * Permet la connexion à un serveur de donnée avec les paramètres dans un
     * fichier ascii server=? port=? database=? user=? password=?
     *
     * @param filename
     * @throws java.io.FileNotFoundException
     * @throws esyoLib.Exceptions.NoNameException
     * @throws esyoLib.DB.DBException
     * @throws esyoLib.DB.DBconnexionException
     * @throws java.lang.ClassNotFoundException
     * @throws java.lang.InstantiationException
     * @throws java.lang.IllegalAccessException
     * @throws java.sql.SQLException
     */
    public DBserver(String filename) throws IOException, NoNameException, DBException, DBconnexionException, ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        File f = new File(filename);
        if (!f.canRead()) {
            throw new FileNotFoundException("Impossible de trouver '" + f.getAbsolutePath() + "'!");
        }
        TstringList l = new TstringList(filename);
        setServer(l.valueFromName("server"));
        setPort(l.valueFromName("port"));
        setDatabase(l.valueFromName("database"));
        setUser(l.valueFromName("user"));
        setPassword(l.valueFromName("password"));
        try {
            setSSL(l.valueFromName("SSL").equals("true"));
        } catch (NoNameException ex) {

        }
        connect();
    }

    /**
     * Constructeur
     * <p>
     * Permet la connexion à un serveur de donnée avec les paramètres dans un
     * fichier ascii
     * <p>
     * Défini les paramètres et se connecte.
     *
     * @param server   server ip or name
     * @param port     server port
     * @param database database name to open
     * @param user     user
     * @param password password
     * @param SSL
     * @throws java.lang.ClassNotFoundException
     * @throws java.lang.InstantiationException
     * @throws java.sql.SQLException
     * @throws java.lang.IllegalAccessException
     * @throws esyoLib.DB.DBException
     */
    public DBserver(String server, String port, String database, String user, String password, boolean SSL) throws Exception {
        connect(server, port, database, user, password, SSL);
    }

    /**
     * Retourne le nombre de requête depuis le début de l'application
     *
     * @return
     */
    public static final int count() {
        return DBserver.compteur;
    }

    /**
     * @param rs
     * @throws SQLException
     */
    public static void print(ResultSet rs) throws SQLException {
        //entêtes colones
        ResultSetMetaData rsmd = rs.getMetaData();
        //affichage des en-tête
        for (int i = 1; i <= rsmd.getColumnCount(); i++) {
            //AlignLeft(max(rsmd.getPrecision(i), rsmd.getColumnLabel(i).length() + 1), rsmd.getColumnLabel(i));
            System.out.print(rsmd.getColumnLabel(i) + "|");
        }
        System.out.println();
        //affichage des lignes
        while (rs.next()) {
            for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                //AlignLeft(max(rsmd.getPrecision(i), rsmd.getColumnLabel(i).length() + 1), rs.getString(i));
                System.out.print(rs.getString(i) + "|");
            }
            System.out.println();
        }
    }

    /**
     * Aligne le texte à gauche et complète avec des espaces.
     *
     * @param TotalLength
     * @param Text
     */
    public static void AlignLeft(int TotalLength, String Text) {
        if (Text == null) {
            Text = "";
        }
        System.out.print(Text);
        PrintNSpaces(CalcNbrOfSpaces(TotalLength, Text.length()));
        System.out.print("|");
    }

    // Calcule le nombre d'espaces entre la longueur voulue et la longueur du texte.
    private static int CalcNbrOfSpaces(int TotalLength, int TextLength) {
        if (TextLength > TotalLength) {
            return 0;
        }
        return TotalLength - TextLength;
    }

    // Afficher n espaces.
    static void PrintNSpaces(int n) {
        while (n >= 10) {
            System.out.print("          ");
            n -= 10;
        }
        while (n >= 2) {
            System.out.print("  ");
            n -= 2;
        }
        while (n > 0) {
            System.out.printf(" ");
            n--;
        }
    }

    /**
     * @param a
     * @param b
     * @return le plus grand des deux int
     */
    public static int max(int a, int b) {
        if (a > b) {
            return a;
        } else {
            return b;
        }
    }

    /**
     * Constructeur
     * <p>
     * Permet la connexion à un serveur de donnée avec les paramètres dans
     * l'appel du constructeur
     * <p>
     * Défini les paramètres et se connecte.
     *
     * @param server   server ip or name
     * @param port     server port
     * @param database database name to open
     * @param user     user
     * @param password password
     * @throws java.lang.ClassNotFoundException
     * @throws java.lang.InstantiationException
     * @throws java.sql.SQLException
     */
    private void connect(String server, String port, String database, String user, String password, boolean SSL) throws Exception {
        setServer(server);
        setPort(port);
        setDatabase(database);
        setUser(user);
        setPassword(password);
        setSSL(SSL);
        connect();
    }

    /**
     * Retourne l'objet Connection
     *
     * @return objet Connection
     */
    public Connection getConnection() {
        return this.connection;
    }

    /**
     * Charge le driver à utiliser
     *
     * @throws java.lang.ClassNotFoundException
     * @throws java.lang.InstantiationException
     * @throws java.lang.IllegalAccessException
     */
    public abstract void load() throws InstantiationException, IllegalAccessException, ClassNotFoundException;

    /**
     * Appelé après la connection
     *
     * @param connected Connecté ou non (Connection!=null revient à true)
     * @throws esyoLib.DB.DBconnexionException Erreur de connection
     */
    public abstract void afterConnect(boolean connected) throws DBconnexionException;

    /**
     * Crée la chaîne de connexion selon le driver utilisé
     *
     * @return chaîne de connexion du driver
     */
    public abstract String makeString();

    /**
     * Etablis la connexion selon le driver utilisé
     *
     * @return La connexion du type Connection
     * @throws DBconnexionException  Erreur de connexion
     * @throws java.sql.SQLException
     */
    public abstract Connection connexion() throws DBconnexionException, SQLException;

    /**
     * Chargée d’établir la connexion au serveur
     * <p>
     * 1: Chargement du driver avec la méthode load() qu'il faut overridé 2:
     * Connexion au serveur avec les infos fournis dans le constructeur 3:
     * Mémorisation de la connexion 4: Callback de la méthode afterConnect()
     * qu'il faut overridé
     *
     * @throws java.lang.ClassNotFoundException
     * @throws java.lang.InstantiationException
     * @throws java.lang.IllegalAccessException
     * @throws esyoLib.DB.DBconnexionException
     * @throws java.sql.SQLException
     */
    public final void connect() throws ClassNotFoundException, InstantiationException, IllegalAccessException, DBconnexionException, SQLException {
        //load driver from inerited class
        load();
        //connexion
        this.connection = connexion();
        //
        afterConnect(this.connection != null);
    }

    /**
     * Déconnection du serveur
     *
     * @throws SQLException
     */
    public void disconnect() throws SQLException {
        this.connection.close();
    }

    /**
     * @return Le nom/l'adresse du serveur
     */
    public final String getServer() {
        return this.server;
    }

    /**
     * Défini l'adresse du serveur
     *
     * @param s Adresse du serveur
     */
    public final void setServer(String s) {
        this.server = s;
    }

    /**
     * @return Le port
     */
    public final String getPort() {
        return this.port;
    }

    /**
     * Défini le port du serveur
     *
     * @param s port du serveur
     * @throws esyoLib.DB.DBException
     * @throws esyoLib.DB.DBconnexionException
     */
    public final void setPort(String s) throws DBException, DBconnexionException {
        if (!s.isEmpty()) {
            try {
                Integer.parseInt(s);
                this.port = s;
            } catch (NumberFormatException e) {
                throw new DBconnexionException("Le port doit être un chiffre!");
            }
        }
    }

    /**
     * @return Le nom de la base de donnée
     */
    public final String getDatabase() {
        return this.database;
    }

    /**
     * Définie la base de donnée à ouvrir
     *
     * @param s base de donnée
     */
    public final void setDatabase(String s) {
        this.database = s;
    }

    /**
     * @return Le nom de l'utilisateur
     */
    public final String getUser() {
        return this.user;
    }

    /**
     * Défini l'utilisateur
     *
     * @param s utilisateur
     */
    public final void setUser(String s) {
        this.user = s;
    }

    /**
     * @return Le mot de passe
     */
    public final String getPassword() {
        return this.password;
    }

    /**
     * Défini le mot de passe de l'utilisateur
     *
     * @param s mot de passe
     */
    public final void setPassword(String s) {
        this.password = s;
    }

    /**
     * @return connection avec SSL oui ou non?
     */
    public final boolean getSSL() {
        return this.SSL;
    }

    /**
     * @param b SSL oui ou non?
     */
    public final void setSSL(boolean b) {
        this.SSL = b;
    }

    /**
     * Fonction SELECT retournant un ResultSet
     *
     * @param aSQLquery La requête SQL
     * @return un ResultSet
     * @throws SQLException           Erreur SQL
     * @throws esyoLib.DB.DBException
     */
    public abstract ResultSet select(String aSQLquery) throws SQLException, DBException;

    /**
     * Fonction UPDATE retournant True si l'opération à été éxécutée avec succès
     *
     * @param aSQLquery La requête SQL
     * @return True ou False
     * @throws SQLException Erreur SQL
     */
    public abstract boolean update(String aSQLquery) throws SQLException;

    /**
     * @return Nombre d'enregistrement de la dernière requête SELECT
     */
    public abstract int getResultsCount();

    /**
     * @throws java.sql.SQLException
     */
    public void close() throws SQLException {
        this.connection.close();
    }

}
