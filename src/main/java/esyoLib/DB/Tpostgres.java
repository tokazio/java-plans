/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.DB;

import esyoLib.Exceptions.NoNameException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Etend la classe DBserver pour fournir les méthodes/fonctions pour une
 * connexion à un serveur postGresSQL.
 *
 * @author rpetit
 */
public class Tpostgres extends DBserver {

    /**
     * Afficher les informations
     */
    private static final boolean trace = true;

    /**
     * Nombre de résultat de la dernière requête SELECT
     */
    private static int nbresults = 0;

    /**
     * Constructeur
     * <p>
     * Voir DBserver
     *
     * @param server
     * @param port
     * @param database
     * @param user
     * @param password
     * @param SSL
     * @throws java.lang.ClassNotFoundException
     * @throws java.lang.InstantiationException
     * @throws java.sql.SQLException
     * @throws java.lang.IllegalAccessException
     */
    public Tpostgres(String server, String port, String database, String user, String password, boolean SSL) throws Exception {
        super(server, port, database, user, password, SSL);
    }

    /**
     * Constructeur
     * <p>
     * Voir DBserver
     *
     * @param filename Fichier de configuration (clé=valeur)
     * @throws IOException
     * @throws java.io.FileNotFoundException
     * @throws esyoLib.Exceptions.NoNameException
     * @throws esyoLib.DB.DBException
     * @throws esyoLib.DB.DBconnexionException
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws SQLException
     * @throws IllegalAccessException
     */
    public Tpostgres(String filename) throws IOException, FileNotFoundException, NoNameException, DBException, DBconnexionException, ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        super(filename);
    }

    /**
     * Charge le driver postgres
     *
     * @throws java.lang.ClassNotFoundException
     * @throws java.lang.InstantiationException
     * @throws java.lang.IllegalAccessException
     */
    @Override
    public void load() throws InstantiationException, IllegalAccessException, ClassNotFoundException {
        //chargement du pilote mysql
        Class.forName("org.postgresql.Driver").newInstance();
    }

    /**
     * Crée la chaîne de connection du driver postgres
     *
     * @return la chaîne de connection du driver postgres
     */
    @Override
    public String makeString() {
        //jdbc:postgresql://localhost/test?user=fred&password=secret&ssl=true
        String c = "jdbc:postgresql://" + getServer();
        if (!getPort().isEmpty()) {
            c += ":" + getPort();
        }
        c += "/" + getDatabase() + "?user=" + getUser();
        if (getPassword().length() > 0) {
            c += "&password=" + getPassword();
        }
        if (getSSL()) {
            c += "&ssl=true";
        }
        return c;
    }

    /**
     * Effectue la requête et renvoi le resultSet
     * <p>
     * En mode trace, affiche le nombre de résultat et le temps de réponse de la
     * requête
     *
     * @param s Requête SQL
     * @return ResultSet
     * @throws SQLException
     * @throws esyoLib.DB.DBException
     */
    @Override
    public final ResultSet select(String s) throws SQLException, DBException {
        //si la requête ne commence pas par SELECT
        if (s.indexOf("SELECT") != 0) {
            throw new DBException("Cette requête: " + s + " n'est pas un SELECT!");
        }
        //requête
        long startTime = System.nanoTime();
        ResultSet rs = getConnection().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY, ResultSet.HOLD_CURSORS_OVER_COMMIT).executeQuery(s);
        double r = ((double) (System.nanoTime() - startTime) / 1000000000);
        Tpostgres.nbresults = 0;
        while (rs.next()) {
            Tpostgres.nbresults++;
        }
        if (Tpostgres.trace) {
            System.out.println(Tpostgres.nbresults + " résultats en " + r + "ms: " + s);
        }
        rs.beforeFirst();
        Tpostgres.compteur++;
        return rs;
    }

    /**
     * Fonction UPDATE
     * <p>
     * en mode trace, affiche le temps de réponse du serveur
     *
     * @param aSQLquery Requête SQL
     * @return True si l'opération s'est bien passée
     * @throws SQLException Erreur SQL
     */
    @Override
    public boolean update(String aSQLquery) throws SQLException {
        return false;
    }

    /**
     * Appelé après la connexion au serveur
     *
     * @param connected
     * @throws DBconnexionException
     */
    @Override
    public void afterConnect(boolean connected) throws DBconnexionException {
        if (Tpostgres.trace) {
            if (connected) {
                System.out.println("Connecté à PostgreSQL.");
            } else {
                System.out.println("NON connecté à PostgreSQL!");
            }
        }
    }

    /**
     * @return @throws DBconnexionException
     * @throws SQLException
     */
    @Override
    public Connection connexion() throws DBconnexionException, SQLException {
        String s = makeString();
        if (Tpostgres.trace) {
            System.out.println(s);
        }
        return DriverManager.getConnection(s);
    }

    /**
     * @return
     */
    @Override
    public int getResultsCount() {
        return Tpostgres.nbresults;
    }
}
