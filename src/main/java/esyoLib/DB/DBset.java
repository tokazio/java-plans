/*
 * Here comes the text of your license
 * Each line should be prefixed with  *
 */

package esyoLib.DB;

/**
 * @author rpetit
 */
public class DBset {
    private String Fname;
    private String Fvalue;
    private boolean FnullCheck = false;

    /**
     * Défini le nom et la valeur
     *
     * @param aName  Le nom
     * @param aValue La valeur
     */
    public DBset(String aName, String aValue) {
        init(aName, aValue);
    }

    /**
     * Défini le nom et la valeur
     *
     * @param aName     Le nom
     * @param aValue    La valeur
     * @param nullCheck
     */
    public DBset(String aName, String aValue, boolean nullCheck) {
        FnullCheck = nullCheck;
        init(aName, aValue);
    }

    private void init(String aName, String aValue) {
        setName(aName);
        setValue(aValue);
    }

    /**
     * Retourne le nom
     *
     * @return la clé
     * @throws esyoLib.DB.DBException
     */
    public final String getName() throws DBException {
        if (Fname == null && FnullCheck) throw new DBException("Name is null in DBset (value=" + Fvalue + ")");
        return Fname;
    }

    /**
     * Défini la clé
     *
     * @param s
     */
    public final void setName(String s) {
        Fname = s;
    }

    /**
     * Retourne la valeur
     *
     * @return la valeur
     * @throws esyoLib.DB.DBException
     */
    public final String getValue() throws DBException {
        if (Fvalue == null && FnullCheck) throw new DBException("Value is null in DBset (name=" + Fname + ")");
        if (Fvalue == null) Fvalue = "null";
        return Fvalue;
    }

    /**
     * Défini la valeur
     *
     * @param s
     */
    public final void setValue(String s) {
        Fvalue = s;
    }

    /**
     * Renvoi une chaîne avec le couple clé/valeur séparé par =
     *
     * @return chaîne avec le couple clé/valeur séparé par =
     */
    @Override
    public String toString() {
        try {
            return getName() + "=" + getValue();
        } catch (DBException ex) {
            return ex.getMessage();
        }
    }
}
