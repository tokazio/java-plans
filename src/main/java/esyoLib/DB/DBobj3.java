/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package esyoLib.DB;

import esyoLib.Formsfx.Tform;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author RomainPETIT
 */
public abstract class DBobj3 {
    //Objet représentant la table
    private DBtable3 table = null;
    //Liste des données
    private HashMap<String, String> data = new HashMap();


    /**
     * Constructeur vide
     */
    public DBobj3() {

    }

    /**
     * Constructeur
     *
     * @param aTable Objet de la table utilisée
     */
    public DBobj3(DBtable3 aTable) {
        this.table = aTable;
    }

    /**
     * Constructeur
     * <p>
     * Utilise un ResultSet pour créer les données.
     * Les données ne sont pas effacées, elles sont ajoutées (ici il n'y a pas encore de données).
     *
     * @param aTable Objet de la table utilisée
     * @param rs     Un ResultSet à utiliser pour les données
     */
    public DBobj3(DBtable3 aTable, ResultSet rs) {
        this.table = aTable;
        set(rs);
    }

    //Définis les données avec un ResultSet
    //Les données ne sont pas effacées, elles sont ajoutées
    private void set(ResultSet rs) {
        try {
            ResultSetMetaData rsmd = rs.getMetaData();
            for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                try {
                    data.put(rsmd.getColumnLabel(i), rs.getString(i));
                } catch (SQLException ex) {
                    System.out.println(getClass().getName() + "(DBobj3)::set -> " + ex.getMessage());
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBobj3.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Compose la requête DELETE
     * <p>
     * Utilise la table et les clés primaires pour créer la requête DELETE
     * <p>
     * DELETE *table* WHERE *pk=?* AND *pk=?*...
     *
     * @return True si l'opération s'est bien passée
     * @throws DBException
     * @throws SQLException
     */
    public final boolean delete() throws DBException, SQLException {
        String q = "DELETE FROM " + this.table.getName() + " WHERE " + this.pkFilter().replace(",", " AND ");
        return this.table.delete(q);
    }

    //retourne une chaîne composée des clés primaires et leurs valeurs, séparées par une ','
    //utilisé dans ?
    private String pkFilter() {
        String r = "";
        r = data.entrySet().stream().filter((e) -> (e.getKey().indexOf("pk_") == 0)).map((e) -> "," + e.getKey().replace("pk_", "") + "='" + e.getValue() + "'").reduce(r, String::concat);
        return r.substring(1);
    }

    /**
     * Retourne la valeur du champs dont le nom est passé en paramètre
     *
     * @param aFieldName Nom du champs (String)
     * @return Valeur du champs (String)
     */
    public final String get(String aFieldName) {
        return this.data.get(aFieldName);
    }

    /**
     * Affiche les clés primaires de la table dans la console
     *
     * @return Chaînage
     */
    public final DBobj3 printPK() {
        this.table.printPK();
        return this;
    }

    /**
     * Affiche les clés étrangères de la table dans la console
     *
     * @return Chaînage
     */
    public final DBobj3 printFK() {
        this.table.printFK();
        return this;
    }

    /**
     * Affiche les champs de la table dans la console
     *
     * @return Chaînage
     */
    public final DBobj3 printFIELDS() {
        this.table.printFIELDS();
        return this;
    }

    /**
     * Affiche les champs=valeurs
     *
     * @return Chaînage
     */
    public final DBobj3 printData() {
        System.out.println(this.toString() + "===============================>");
        data.entrySet().stream().forEach((e) -> {
            System.out.println(e.getKey() + "=" + e.getValue());
        });
        System.out.println("<===============================");
        return this;
    }

    /**
     * Fournie la requête SQL appelée dans la fonction load()
     *
     * @return requête SQL
     */
    public abstract String loadString();

    /**
     * Récupèrer la requête SQL appelée dans la fonction load()
     *
     * @return la requête SQL définie dans loadString()
     * @throws esyoLib.DB.DBException
     */
    public final String getLoadString() throws DBException {
        String r = loadString();
        if (r.isEmpty())
            throw new DBException("'" + getClass().getName() + " abstract loadString()' est vide (ne retourne pas une requête SQL).");
        return r;
    }

    /**
     * @return l'objet DBtable3 de la table associée
     */
    public final DBtable3 getTable() {
        return this.table;
    }

    /**
     * Requête SELECT (utilise la fonction select() de la table (DBtable3)
     *
     * @param s Requête SQL
     * @return le ResultSet
     * @throws SQLException
     * @throws esyoLib.DB.DBException
     */
    public final ResultSet select(String s) throws SQLException, DBException {
        return this.table.select(s);
    }

    /**
     * Requête SELECT selon la requête SQL fournie dans listString() (utilise la fonction select())
     *
     * @return le ResultSet
     * @throws SQLException
     */
    public final ResultSet list() throws Exception {
        return select(getListString());
    }

    /**
     * Fournie la requête SQL appelée dans la fonction list()
     *
     * @return requête SQL
     */
    public abstract String listString();

    /**
     * Récupèrer la requête SQL appelée dans la fonction list()
     *
     * @return la requête SQL définie dans listString()
     * @throws DBException
     */
    public final String getListString() throws DBException {
        String r = listString();
        if (r.isEmpty())
            throw new DBException("'" + getClass().getName() + " abstract listString() est vide (ne retourne pas une requête SQL).");
        return r;
    }

    /**
     * Charge les données de l'objet selon la requête fournie par loadString()
     *
     * @return Chaînage
     * @throws esyoLib.DB.DBException
     * @throws java.sql.SQLException
     */
    public final DBobj3 load() throws DBException, SQLException {
        String l = this.getLoadString();
        ResultSet rs = this.table.select(l);
        this.data.clear();
        set(rs);
        return this;
    }

    /**
     * Gestion de l'UI pour l'édition
     *
     * @return l'objet Tform de la fenêtre
     */
    public abstract Tform editForm();

    /**
     * Gestion de l'UI pour la suppression
     */
    public abstract void delForm();

    /**
     * Gestion de l'UI pour l'envois de mail
     *
     * @param aType Type de mail (pilote...)
     * @return l'objet Tform de la fenêtre
     */
    public abstract Tform mailForm(String aType);

    /**
     * Retourne <tt>true</tt> si le champs 's' existe
     *
     * @param s Nom du champs
     * @return <tt>true</tt> si le champs 's' existe
     */
    public final boolean hasKey(String s) {
        return data.containsKey(s);
    }

    /**
     * Ajoute un champs et sa valeur
     *
     * @param aFieldName Nom du champs
     * @param aValue     Valeur du champs
     * @return Chaînage
     */
    public final DBobj3 add(String aFieldName, String aValue) {
        data.put(aFieldName, aValue);
        return this;
    }
}
