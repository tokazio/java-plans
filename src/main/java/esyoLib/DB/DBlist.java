/*
 * Here comes the text of your license
 * Each line should be prefixed with  *
 */
package esyoLib.DB;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TableView;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @param <T>
 * @author romainpetit
 */
public abstract class DBlist<T> implements Idb {
    //Affiche les messages dans la console
    private static final boolean trace = false;
    //Liste des données
    protected ObservableList<T> data;
    //table
    private String table = "undefined";
    //UI tableview
    private TableView<T> tableview;
    //UI listview
    private ListView<T> listview;
    //UI combobox
    private ComboBox combobox;
    //Connexion au serveur
    private DBserver server = null;

    /**
     * @param aServer Une connexion au serveur de base de donnée.
     * @throws esyoLib.DB.DBException Non connecté au serveur de base de donnée?
     */
    public DBlist(DBserver aServer) throws DBException {
        if (aServer == null) {
            throw new DBException("DBlist::constructor: Pas de serveur!");
        }
        this.server = aServer;
        this.data = FXCollections.observableArrayList();
    }

    /**
     * @param rs
     * @param myClass
     * @param con
     * @return
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws NoSuchMethodException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     * @throws InvocationTargetException
     */
    public static DBobj newObj(ResultSet rs, Class myClass, DBserver con) throws SQLException, ClassNotFoundException, NoSuchMethodException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        if (DBlist.trace) {
            System.out.println("DBlist::newObj>class du type '" + myClass.getName() + "'");
        }
        if (DBlist.trace) {
            System.out.println("DBlist::newObj>paramètres vide");
        }
        Class[] types = {DBserver.class};
        if (DBlist.trace) {
            System.out.println("DBlist::newObj>get constructor de '" + myClass.getName() + "' avec paramètres");
        }
        Constructor constructor = myClass.getConstructor(types);
        Object[] args = new Object[1];
        args[0] = con;
        Object instanceOfMyClass = constructor.newInstance(args);
        if (DBlist.trace) {
            System.out.println("DBlist::newObj>DBobj (normalement) set avec le rs");
        }
        ((DBobj) instanceOfMyClass).set(rs).load();
        return (DBobj) instanceOfMyClass;
    }

    /**
     * Efface la liste des données
     */
    private DBlist clear() {
        this.data.clear();
        return this;
    }

    /**
     * Lie la liste des données à une ListView (javaFX)
     *
     * @param l
     * @return
     */
    public final DBlist attach(ListView l) {
        this.listview = l;
        this.listview.setItems(this.data);
        return this;
    }

    /**
     * Lie la liste des données à une TableView (javaFX)
     *
     * @param t a TableView UI (javafx)
     * @return
     */
    public final DBlist attach(TableView t) {
        this.tableview = t;
        this.tableview.setItems(this.data);
        return this;
    }

    /**
     * Lie la liste des données à une ComboBox (javaFX)
     *
     * @param c a ComboBox UI (javafx)
     * @return
     */
    public final DBlist attach(ComboBox c) {
        this.combobox = c;
        this.combobox.setItems(this.data);
        return this;
    }

    /**
     * Envoi la requête au serveur et consrtuit la liste de résultat avec la
     * réponse
     *
     * @param SQL  Texte de la requête
     * @param type Type (class) de l'objet qui contiendra le résultat
     * @return
     * @throws SQLException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     * @throws InvocationTargetException
     * @throws NoSuchMethodException
     * @throws ClassNotFoundException
     * @throws esyoLib.DB.DBException
     */
    public final DBlist query(String SQL, Class type) throws SQLException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, ClassNotFoundException, DBException {
        if (this.server == null) {
            throw new DBException("DBlist::query>Non connecté au serveur de base de donnée?");
        }
        if (DBlist.trace) {
            System.out.println("DBlist::query>clear() data");
        }
        clear();
        if (DBlist.trace) {
            System.out.println("DBlist::query>execute query\n" + SQL + "\n");
        }
        ResultSet rs = this.server.getConnection().createStatement().executeQuery(SQL);
        //compte
        if (DBlist.trace) {
            rs.beforeFirst();
            int c = 0;
            while (rs.next()) {
                c++;
            }
            System.out.println("DBlist::query> " + c + " enregistrement(s)");
        }
        //
        int i = 1;
        rs.beforeFirst();
        while (rs.next()) {
            if (DBlist.trace) {
                System.out.println("DBlist::query>#" + i + "========================");
            }
            if (DBlist.trace) {
                System.out.println("DBlist::query>nouvelle instance de '" + type.getName() + "' avec paramètres '" + this.server + "'");
            }
            if (DBlist.trace) {
                System.out.println("DBlist::newObj>ajoute dans data");
            }

            T o = (T) newObj(rs, type, this.server);
            if (o != null) {
                this.data.add(o);
            }

            i++;
        }
        if (DBlist.trace) {
            System.out.println("DBlist::query>fin (" + i + ")========================");
        }
        return this;
    }

    /**
     * Renvoi le résultat (DBobj) selon sont index dans la liste des résultats
     *
     * @param index Position de l'object dans la liste
     * @return résultat (DBobj) selon sont index dans la liste des résultats
     */
    public final T get(int index) {
        return this.data.get(index);
    }

    /**
     * Renvoi le nombre de résultat
     *
     * @return nombre de résultat
     */
    public final int count() {
        return this.data.size();
    }

    /**
     * Alias de count()
     *
     * @return nombre de résultat
     */
    public final int size() {
        return count();
    }

    /**
     * Renvoi une chaîne avec les résultats séparés par |
     *
     * @return chaîne avec les résultats séparés par |
     */
    @Override
    public String toString() {
        String s = "";
        s = this.data.stream().map((data1) -> data1.toString() + "|").reduce(s, String::concat);
        return s;
    }

    /**
     * @param aTableName Un nom de table
     * @return Chaînage <code>DBlist</code>
     */
    public final DBlist setTable(String aTableName) {
        this.table = aTableName;
        return this;
    }

}
