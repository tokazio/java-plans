/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.DB;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ComboBox;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @param <T>
 * @author RomainPETIT
 */
public class DBcomboList3<T extends DBobj3> {

    //
    private static DBserver con = null;
    //Liste des lignes de données (observées par la vue)
    private final ObservableList<T> data;
    //
    private Class classe = null;
    //
    private ComboBox<T> view = null;

    /**
     * @param aComboBox
     * @param aClass
     */
    public DBcomboList3(ComboBox<T> aComboBox, Class<? extends DBobj3> aClass) {
        //Initialise la liste de donnée
        this.data = FXCollections.observableArrayList();
        //Mémorise la vue choisie
        this.view = aComboBox;
        //lie les données à la vue
        this.view.setItems(this.data);
        //Mémorise la classe des objets des lignes
        this.classe = aClass;
        //Prépare la 1ère requête
        Object o = null;
        try {
            o = aClass.newInstance();
            //DBtable3 t = ((DBobj3) o).getTable();
            //Fait la requête
            show(((DBobj3) o).list());
            //select(selectString(t.getName()));
        } catch (InstantiationException | IllegalAccessException | SQLException ex) {
            System.out.println("DBlist3::Constructeur->" + ex.getClass().getName() + "\n" + ex.getMessage() + "n'a peut être pas de constructeur sans paramètre de défini.");
        } catch (Exception ex) {
            System.out.println("DBlist3::Constructeur->" + ex.getClass().getName() + "\n" + ex.getMessage());
        }

    }

    /**
     * @param aCon
     */
    public static void iniCon(DBserver aCon) {
        con = aCon;
    }

    public DBcomboList3 first() {
        view.getSelectionModel().selectFirst();
        return this;
    }

    public DBcomboList3 select(String aFieldName, String aValue) {
        for (int i = 0; i < data.size(); i++) {
            if (data.get(i).get(aFieldName).equals(aValue)) {
                view.getSelectionModel().select(i);
                break;
            }
        }
        return this;
    }

    /**
     * @param rs
     * @throws SQLException
     */
    public void show(ResultSet rs) throws SQLException {
        makeRows(rs);
    }

    //
    private void makeRows(ResultSet rs) throws SQLException {
        while (rs.next()) {
            //ajoute la ligne au données
            //rs n'est pas directement passé car détruit à la fermeture de la requête ou de la connection
            //copié vers un objet du type T
            T o = newObj(rs);
            data.add(o);
        }
    }

    //
    private T newObj(ResultSet rs) {
        Class[] types = {ResultSet.class};
        Constructor constructor = null;
        try {
            constructor = this.classe.getConstructor(types);
        } catch (NoSuchMethodException | SecurityException ex) {
            System.out.println("DBlist3::newObj->" + ex.getClass().getName() + "\n" + ex.getMessage());
        }
        Object[] args = new Object[1];
        args[0] = rs;
        Object instanceOfMyClass = null;
        try {
            instanceOfMyClass = constructor.newInstance(args);
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            System.out.println("DBlist3::newObj->" + ex.getClass().getName() + "\n" + ex.getMessage());
        }
        return (T) instanceOfMyClass;
    }

}
