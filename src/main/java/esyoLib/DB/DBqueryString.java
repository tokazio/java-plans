/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.DB;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map.Entry;

/**
 * @author rpetit
 */
public class DBqueryString {

    //table name
    private String table = "";
    //data
    private HashMap<String, String> data = new HashMap();

    /**
     * @param aTableName The table name
     */
    public DBqueryString(String aTableName) {
        this.table = aTableName;
    }

    /**
     * Add field/value pair to the data
     *
     * @param aFieldName A field name
     * @param aValue     The corresponding field value
     * @return
     */
    public DBqueryString add(String aFieldName, String aValue) {
        data.put(aFieldName, aValue);
        return this;
    }

    /**
     * Return the INSERT query string
     *
     * @return the INSERT query string
     */
    public String getInsertString() {
        String SQL = "INSERT INTO " + this.table + " ";
        String t = "";
        String s = "";
        for (Entry<String, String> e : data.entrySet()) {
            s += "," + e.getKey();
            t += ",'" + e.getValue() + "'";
        }
        SQL += "(" + s.substring(1) + ") VALUES(" + t.substring(1) + ");";
        return SQL;
    }

    /**
     * Return the prepared string with the table name and data
     * INSERT INTO *table*(*champs1*,*champs2*,...) VALUES(?,?,...);
     * Values are replaced with ?
     * Query must be prepared with prepare(PreparedStatement) function
     *
     * @return the prepared string with the table name and data
     */
    public String getPreparedInsertString() {
        String SQL = "INSERT INTO " + this.table + " ";
        String t = "";
        String s = "";
        for (Entry<String, String> e : data.entrySet()) {
            s += "," + e.getKey();
            t += ",?";
        }
        SQL += "(" + s.substring(1) + ") VALUES(" + t.substring(1) + ");";
        return SQL;
    }

    /**
     * Prepare the PreparedStatement with data
     *
     * @param pstt a PreparedStatement
     * @return This object
     */
    public DBqueryString prepare(PreparedStatement pstt) {
        int i = 0;
        for (Entry<String, String> e : data.entrySet()) {
            try {
                if (e.getValue().isEmpty()) {
                    pstt.setNull(i + 1, Types.VARCHAR);
                } else {
                    pstt.setString(i + 1, e.getValue());
                }
                i++;
            } catch (SQLException ex) {

            }
        }
        return this;
    }

    /**
     * @return The table name
     */
    public String getTable() {
        return this.table;
    }

    /**
     * @return Comma separated field=value
     */
    @Override
    public String toString() {
        String r = "";
        r = data.entrySet().stream().map((e) -> "," + e.getKey() + "=" + e.getValue()).reduce(r, String::concat);
        return r.substring(1);
    }

}
