/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package esyoLib.DB;

import javafx.collections.ObservableList;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * @author RomainPETIT
 */
public class DBhistorique {
    //Connexion au serveur
    private DBserver Fcon = null;

    /**
     * @param con Une connexion au serveur de base de donnée.
     * @throws esyoLib.DB.DBException Non connecté au serveur de base de donnée?
     */
    public DBhistorique(DBserver con) throws DBException {
        if (con != null) {
            Fcon = con;
        } else {
            throw new DBException("DBlist::query>Non connecté au serveur de base de donnée?");
        }
    }

    public void get(ObservableList liste) throws SQLException, DBException {
        liste.clear();
        if (Fcon != null) {
            Pattern p = Pattern.compile("\\w+=[\\w\\-\\:\\.éè]+");
            ResultSet rs = Fcon.getConnection().createStatement().executeQuery("SELECT type,UCFIRST(nomtable) as nomtable,date,textquery FROM historique ORDER BY date DESC");
            rs.beforeFirst();
            while (rs.next()) {
                System.out.println("+++++");
                try {

                    String entree = rs.getString("textquery");
                    Matcher m = p.matcher(entree);
                    while (m.find())
                        System.out.println(m.group());// entree.substring(m.start(), m.end()));
                } catch (PatternSyntaxException e) {

                }
                //regex \w+=[\w\-\:]+ pour les paires clef=valeur

                switch (rs.getString("type")) {
                    case "INSERT":
                        liste.add(new Thistorique("INSERT", rs.getString("nomtable")));
                        break;
                    case "DELETE":
                        liste.add(new Thistorique("DELETE", rs.getString("nomtable")));
                        break;
                    case "UPDATE":
                        liste.add(new Thistorique("UPDATE", rs.getString("nomtable")));
                        break;
                }

            }
        } else {
            throw new DBException("DBlist::query>Non connecté au serveur de base de donnée?");
        }
    }
}
