/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.DB;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author rpetit
 */
@Deprecated
public class DBrow {

    //

    private final HashMap<String, String> p;

    /**
     *
     */
    public DBrow() {
        p = new HashMap();
    }

    /**
     * @param rs
     */
    public DBrow(ResultSet rs) {
        p = new HashMap();
        set(rs);
    }

    /**
     * @param rsmd
     */
    public DBrow(ResultSetMetaData rsmd) {
        p = new HashMap();
        try {
            for (int i = 1; i <= rsmd.getColumnCount(); i++)
                p.put(rsmd.getColumnLabel(i), rsmd.getColumnLabel(i));
        } catch (SQLException ex) {
            Logger.getLogger(DBrow.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * @param rs
     */
    public final void set(ResultSet rs) {
        try {
            ResultSetMetaData rsmd = rs.getMetaData();
            for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                p.put(rsmd.getColumnLabel(i), rs.getString(i));
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBrow.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * @param aFieldName
     * @return
     */
    public final String get(String aFieldName) {
        return p.get(aFieldName);
    }

    public final String getPK() {
        String s = "";
        for (Entry<String, String> entry : p.entrySet())
            if (entry.getKey().indexOf("pk_") == 0) {
                s += "AND " + entry.getKey().replace("pk_", "") + "='" + entry.getValue() + "'";
            }
        return s.substring(4);
    }

}
