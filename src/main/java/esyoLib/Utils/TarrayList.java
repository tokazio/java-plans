/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.Utils;

import esyoLib.XML.IxmlSavable;
import esyoLib.XML.TxmlObject;
import esyoLib.XML.TxmlTag;
import org.w3c.dom.Node;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

/**
 * @param <E>
 * @author rpetit
 */
public class TarrayList<E> extends ArrayList<E> implements IxmlSavable {

    //Classe des éléments dans le 'ArrayList'
    private final Class Fclass;

    /**
     * Constructeur
     *
     * @param c Classe des éléments dans le 'ArrayList'
     */
    public TarrayList(Class c) {
        Fclass = c;
    }

    /**
     * @return le 1er élément
     */
    public final E first() {
        return get(0);
    }

    /**
     * @return le dernier élément
     */
    public final E last() {
        return get(size() - 1);
    }

    /**
     * @param aTag
     * @return
     */
    @Override
    public TxmlTag saveToXML(TxmlTag aTag) {
        for (int i = 0; i < size(); i++) {
            ((TxmlObject) get(i)).saveToXML(aTag.addTag(get(i).getClass().getName()).attr("id", i + ""));
        }
        return aTag;
    }

    /**
     * @param noeud Tag XML
     * @return Object
     */
    @Override
    public Object loadFromXML(Node noeud) {
        Class[] types = {Node.class};
        Method m = null;
        try {
            m = Fclass.getMethod("loadFromXML", types);
        } catch (NoSuchMethodException ex) {
            System.out.println("/!\\ Impossible de trouver la méthode loadFromXML des éléments du ArrayList (<" + Fclass.getName() + ">)");
        }
        for (int i = 0; i < noeud.getChildNodes().getLength(); i++) {
            if (noeud.getChildNodes().item(i).getNodeType() == Node.ELEMENT_NODE) {
                //crée une instance
                E o = null;
                try {
                    o = (E) Fclass.newInstance();
                } catch (Exception ex) {
                    System.out.println("/!\\ TarrayList<" + Fclass.getName() + "> " + ex.getClass().getName() + " erreur lors de l'appel à newInstance() sur l'élément " + noeud.getChildNodes().item(i).getNodeName() + " " + noeud.getChildNodes().item(i).getAttributes().getNamedItem("id") + ": " + ex.getMessage());
                }
                //loadFromXML de l'instance
                try {
                    m.invoke(o, noeud.getChildNodes().item(i));
                    //ajoute à l'ArrayList
                    add(o);
                } catch (IllegalAccessException | IllegalArgumentException ex) {
                    System.out.println("/!\\ TarrayList<" + Fclass.getName() + "> " + ex.getClass().getName() + " sur l'élément " + noeud.getChildNodes().item(i).getNodeName() + " " + noeud.getChildNodes().item(i).getAttributes().getNamedItem("id") + ": " + ex.getMessage());
                } catch (InvocationTargetException ex) {
                    System.out.println("/!\\ InvocationTargetException TarrayList<" + Fclass.getName() + "> " + ex.getTargetException().getClass().getName() + " sur l'élément " + noeud.getChildNodes().item(i).getNodeName() + " " + noeud.getChildNodes().item(i).getAttributes().getNamedItem("id") + ": " + ex.getMessage());
                }
            }
        }
        return this;
    }
}
