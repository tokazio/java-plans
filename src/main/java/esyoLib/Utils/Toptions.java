/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.Utils;

import esyoLib.Files.TstringObject;

import java.io.IOException;

/**
 * @author rpetit
 */
public class Toptions extends TstringObject {

    /**
     * @param aFileName
     * @param c
     * @throws IOException
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     */
    public Toptions(String aFileName, Class c) throws IOException, IllegalArgumentException, IllegalAccessException {
        try {
            this.loadFromFile(aFileName);
        } catch (IOException ex) {
            //impossible de charger le fichier d'option
            System.out.println("Impossible d'ouvrir '" + aFileName + "'");
            //essai de le copier depuis la classe
            if (c != null) {
                try {
                    this.loadFromStream(c.getResourceAsStream(aFileName));
                } catch (Exception e) {
                    //
                    System.out.println("Impossible d'ouvrir '" + aFileName + "' depuis la classe '" + c.getName() + "'.");
                }
            }
            try {
                this.saveToFile(aFileName);
            } catch (IOException | IllegalArgumentException | IllegalAccessException ex1) {
                throw new IOException("Impossible de trouver le fichier '" + aFileName + "'. Impossible de le créer par défaut!");
            }
        }
    }

}
