/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.Utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author rpetit
 */
public final class TdateUtils {

    public static String[] moisEnShort = {"", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    public static String[] moisFrShort = {"", "Jan", "Fev", "Mar", "Avr", "Mai", "Juin", "Juil", "Aou", "Sep", "Oct", "Nov", "Dec"};
    public static String[] jourFrLong = {"Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi"};

    /**
     * Transforme une chaîne de date du type Sat Jun 14 06:16:43 CEST 2014 en
     * chaîne de date du type 14/05/2014 06:16:43 Fuseau CEST (Date europèene
     * d'été)
     *
     * @param strDate date du type Sat Jun 14 06:16:43 CEST 2014
     * @return date du type 14/05/2014 06:16:43
     */
    public static String dateE(String strDate) {
        String[] s = strDate.split("\\s");

        int month = 0;
        String strMonth;
        for (int j = 0; j < moisEnShort.length; j++) {
            if (moisEnShort[j].equals(s[1])) {
                month = j;
                break;
            }
        }
        if (month < 1 || month > 12) {
            throw new IndexOutOfBoundsException("Problème lors de l'extraction du mois pour '" + strDate + "'");
        }
        if (month < 10) {
            strMonth = "0" + month;
        } else {
            strMonth = month + "";
        }
        return s[2] + '/' + strMonth + '/' + s[5] + ' ' + s[3];
    }

    /**
     * @param strDate
     * @return
     */
    public static String dateSQL(String strDate) {
        String[] s = strDate.split("\\s");

        int month = 0;
        String strMonth;
        for (int j = 0; j < moisEnShort.length; j++) {
            if (moisEnShort[j].equals(s[1])) {
                month = j;
                break;
            }
        }
        if (month < 1 || month > 12) {
            throw new IndexOutOfBoundsException("Problème lors de l'extraction du mois pour '" + strDate + "'");
        }
        if (month < 10) {
            strMonth = "0" + month;
        } else {
            strMonth = month + "";
        }
        return s[5] + '-' + strMonth + '-' + s[2] + ' ' + s[3];
    }

    public static String humanDate(String aDateTimeString) {
        DateTimeFormatter fmt = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
        LocalDateTime dt = LocalDateTime.parse(aDateTimeString, fmt);
        return humanDate(dt);
    }

    public static String humanDate(LocalDateTime aDateTime) {
        //aujourd'hui
        LocalDateTime aujourdhui = LocalDateTime.now();
        //si la date (sans l'heure) est aujourd'hui, j'affine
        if (aDateTime.toLocalDate().equals(aujourdhui.toLocalDate())) {
            int eh = aujourdhui.getHour() - aDateTime.getHour();
            //si c'est la même heure
            if (eh == 0) {
                int em = aujourdhui.getMinute() - aDateTime.getMinute();
                String mt;
                //si c'est la même minute
                if (em == 0) {
                    //écart sec (string)
                    int es = aujourdhui.getSecond() - aDateTime.getSecond();
                    String st;
                    if (es > 0) {
                        int s = es;
                        if (s < 10) {
                            st = "0" + s;
                        } else {
                            st = s + "";
                        }
                        //je renvoi le nombre de secondes d'écart
                        return "Il y a environ " + st + "sec";
                    } else {
                        int s = Math.abs(es);
                        if (s < 10) {
                            st = "0" + s;
                        } else {
                            st = s + "";
                        }
                        //je renvoi le nombre de secondes d'écart
                        return "Dans environ " + st + "sec";
                    }
                    //il y a x min
                } else if (em > 0) {
                    int m = em;

                    if (m < 10) {
                        mt = "0" + m;
                    } else {
                        mt = m + "";
                    }
                    //je renvoi le nombre de minutes d'écart
                    return "Il y a environ " + mt + "min";
                    //dans x min
                } else {
                    int m = Math.abs(em);
                    if (m < 10) {
                        mt = "0" + m;
                    } else {
                        mt = m + "";
                    }
                    //je renvoi le nombre de minutes d'écart
                    return "Dans environ " + mt + "min";
                }
                //si écart d'heure de 5h avant
            } else if (eh > 0 && eh < 5) {
                String ht;
                String mt;
                int m = aujourdhui.getMinute() - aDateTime.getMinute();
                int h = aujourdhui.getHour() - aDateTime.getHour();
                if (m < 0) {
                    m = Math.abs(m);
                    h = h + 1;
                }
                if (m < 10) {
                    mt = "0" + m;
                } else {
                    mt = m + "";
                }
                if (h < 10) {
                    ht = "0" + h;
                } else {
                    ht = h + "";
                }
                //je renvoi le nombre d'heure d'écart
                return "Il y a environ " + ht + "h" + mt;
                //si écart d'heure de 5h après
            } else if (eh < 0 && eh > -5) {
                String ht;
                String mt;
                int m = aDateTime.getMinute() - aujourdhui.getMinute();
                int h = aDateTime.getHour() - aujourdhui.getHour();
                if (m < 0) {
                    m = 60 + m;
                    h = h - 1;
                }
                if (m < 10) {
                    mt = "0" + m;
                } else {
                    mt = m + "";
                }
                if (h < 10) {
                    ht = "0" + h;
                } else {
                    ht = h + "";
                }
                //je renvoi le nombre d'heure d'écart
                return "Dans environ " + ht + "h" + mt;
            } else {
                //si plus de 5h, je renvoi aujourd'hui avec l'heure
                return "Aujourd'hui à " + aDateTime.toLocalTime();
            }
            //si pas aujourd'hui, hier?
        } else if (aDateTime.toLocalDate().isEqual(aujourdhui.toLocalDate().plusDays(-1))) {
            //hier
            return "Hier à " + aDateTime.toLocalTime();
            //si pas aujourd'hui, demain?
        } else if (aDateTime.toLocalDate().isEqual(aujourdhui.toLocalDate().plusDays(1))) {
            //demain
            return "Demain à " + aDateTime.toLocalTime();
            //jour à 1 semaine avant
        } else if (aDateTime.toLocalDate().isBefore(aujourdhui.toLocalDate()) && aDateTime.toLocalDate().isAfter(aujourdhui.toLocalDate().plusDays(-6))) {
            return jourFrLong[aDateTime.getDayOfWeek().getValue()] + " dernier à " + aDateTime.toLocalTime();
            //jour à 1 semaine après
        } else if (aDateTime.toLocalDate().isAfter(aujourdhui.toLocalDate()) && aujourdhui.toLocalDate().isBefore(aDateTime.toLocalDate().plusDays(6))) {
            return jourFrLong[aDateTime.getDayOfWeek().getValue()] + " prochain à " + aDateTime.toLocalTime();
        }
        //autres
        String j = jourFrLong[aDateTime.getDayOfWeek().getValue()];
        DateTimeFormatter fmt = DateTimeFormatter.ofPattern("'le " + j + "' dd/MM/yyyy 'à' HH:mm:ss");
        return aDateTime.format(fmt);
    }
}
