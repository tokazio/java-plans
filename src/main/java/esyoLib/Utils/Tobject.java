/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.Utils;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * @author rpetit
 */
public class Tobject {

    public Tobject() {
        NumberFormat nf = NumberFormat.getNumberInstance(Locale.FRENCH);
        DecimalFormat df = (DecimalFormat) nf;
    }

    /**
     * @param orig objet d'origine
     * @return une copie de l'objet
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static Object copy(Object orig) throws IOException, ClassNotFoundException {
        Object obj = null;
        // Write the object out to a byte array
        FastByteArrayOutputStream fbos = new FastByteArrayOutputStream();
        ObjectOutputStream out = new ObjectOutputStream(fbos);
        out.writeObject(orig);
        out.flush();
        out.close();
        // Retrieve an input stream from the byte array and read a copy of the object back in. 
        ObjectInputStream in = new ObjectInputStream(fbos.getInputStream());
        obj = in.readObject();
        return obj;
    }

}
