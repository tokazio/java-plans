/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.Utils;

import java.util.ArrayList;

/**
 * @param <T>
 * @author rpetit
 */
public class ChainedList<T> {

    private ArrayList<T> data;

    /**
     *
     */
    public ChainedList() {
        data = new ArrayList();
    }

    /**
     * @param s
     * @return
     */
    public ChainedList add(T s) {
        data.add(s);
        return this;
    }

    /**
     * @param o
     * @return
     */
    public ChainedList remove(T o) {
        data.remove(o);
        return this;
    }

    /**
     * @param i
     * @return
     */
    public ChainedList remove(int i) {
        data.remove(i);
        return this;
    }

    /**
     * @param i
     * @return
     */
    public T get(int i) {
        return data.get(i);
    }

    /**
     * @return
     */
    @Override
    public String toString() {
        String r = "";
        for (T o : data) {
            r += "," + o.toString();
        }
        return r.substring(1);
    }

    public ChainedList clear() {
        data.clear();
        return this;
    }

}
