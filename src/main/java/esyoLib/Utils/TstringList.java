/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.Utils;

import esyoLib.DB.DBserver;
import esyoLib.Exceptions.NoNameException;
import esyoLib.Exceptions.NoValueException;

import java.io.*;
import java.util.ArrayList;

/**
 * @author RomainPETIT
 */
public class TstringList {

    //
    private ArrayList<String> Fnames = null;
    //
    private ArrayList<String> Fvalues = null;
    //
    private String Ftext = "";
    //
    private char FnameValueSeparator = '=';
    //
    private boolean textQuote = false;

    /**
     * @param filename
     * @throws IOException
     */
    public TstringList(String filename) throws IOException {
        init();
        loadFromFile(filename);
    }

    /**
     * @param aInputStream
     * @param aEncoding
     * @throws IOException
     */
    public TstringList(InputStream aInputStream, String aEncoding) throws IOException {
        init();
        loadFromStream(aInputStream, aEncoding);
    }

    /**
     *
     */
    public TstringList() {
        init();
    }

    /**
     * Crée une liste depuis une chaine contenant les couples=valeurs séparés
     * par une ','
     *
     * @param s chaine contenant les couples=valeurs séparés par une ','
     * @return
     */
    public final static TstringList fromString(String s) {
        //System.out.println("TstringList::fromString("+s+")");
        TstringList l = new TstringList();
        l.fromString(s, ",");
        return l;
    }

    /**
     * @param b
     */
    public void useTextQuote(boolean b) {
        this.textQuote = b;
    }

    //
    private void init() {
        Fnames = new ArrayList();
        Fvalues = new ArrayList();
    }

    /**
     * La liste possède la valeur 's'
     *
     * @param s Valeur recherché
     * @return true si trouvé, sinon false
     */
    public final boolean hasValue(String s) {
        return Fvalues.stream().anyMatch((v) -> (v.equals(s)));
    }

    /**
     * La liste possède le nom/clé 's'
     *
     * @param s Nom/clé recherché
     * @return true si trouvé, sinon false (les nom/clé null sont ignoré)
     */
    public final boolean hasName(String s) {
        return Fnames.stream().filter((v) -> !(v == null)).anyMatch((v) -> (v.equals(s)));
    }

    /**
     * Ouvre la liste depuis le fichier 'filename'
     *
     * @param filename Nom du fichier
     * @throws FileNotFoundException Fichier inexistant
     * @throws IOException           Erreur de lecture du fichier
     */
    public final void loadFromFile(String filename) throws FileNotFoundException, IOException {
        FileReader fr = new FileReader(filename);
        load(new BufferedReader(fr));
    }

    /**
     * Ouvre la liste depuis un stream (UTF-8)
     *
     * @param aInputStream stream
     * @param aEncoding    encodage (UTF-8)
     * @throws UnsupportedEncodingException Erreur d'encodage
     * @throws IOException                  Erreur de lecture du stream
     */
    public final void loadFromStream(InputStream aInputStream, String aEncoding) throws UnsupportedEncodingException, IOException {
        load(new BufferedReader(new InputStreamReader(aInputStream, aEncoding)));

    }

    private void load(BufferedReader br) throws IOException {
        String ligne = br.readLine();
        while (ligne != null) {
            add(ligne);
            ligne = br.readLine();
        }
        br.close();
    }

    /**
     * Enregistre la liste dans le fichier 'filename'
     *
     * @param filename Nom du fichier
     * @throws IOException Erreur d'écriture dans le fichier
     */
    public final void saveToFile(String filename) throws IOException {
        FileWriter fw = new FileWriter(filename);
        BufferedWriter bw = new BufferedWriter(fw);
        PrintWriter pw = new PrintWriter(bw);
        for (int i = 0; i < Fvalues.size(); i++) {
            pw.println(get(i));
        }
        pw.close();
    }

    /**
     * Séparateur nom/valeur ('=' par défaut)
     *
     * @param c Caractère de séparation ('=' par défaut)
     */
    public final void setNameValueSeparator(char c) {
        FnameValueSeparator = c;
    }

    /**
     * Efface/vide la liste
     */
    public final void clear() {
        Fnames.clear();
        Fvalues.clear();
    }

    /**
     * Retourne la valeur qui possède la clé 'name'
     *
     * @param name Nom/clé de la valeur recherché
     * @return
     * @throws NoNameException
     */
    public final String valueFromName(String name) throws NoNameException {
        if (!Fnames.contains(name)) {
            throw new NoNameException(name);
        }
        return Fvalues.get(Fnames.indexOf(name));
    }

    /**
     * Retourne la valeur à la position 'index'
     *
     * @param index Position de la valeur
     * @return
     */
    public final String valueFromIndex(int index) {
        return getValue(index);
    }

    /**
     * Retourne le nom/clé qui possède la valeur 'value'
     *
     * @param value Valeur du nom/clé recherché
     * @return
     * @throws NoValueException
     */
    public final String nameFromValue(String value) throws NoValueException {
        if (!Fvalues.contains(value)) {
            throw new NoValueException(value);
        }
        return Fnames.get(Fvalues.indexOf(value));
    }

    /**
     * Retourne le nom/clé à la position 'index'
     *
     * @param index Position du nom/clé
     * @return
     */
    public final String nameFromIndex(int index) {
        return getName(index);
    }

    /**
     * Retourne la paire clé=valeur selon sa position
     *
     * @param index Position de la paire demandée
     * @return
     */
    public final String get(int index) {
        return getName(index) + FnameValueSeparator + getValue(index);
    }

    /**
     * Ajoute la valeur 's' à la liste
     *
     * @param s Valeur
     */
    public final void add(String s) {
        if (s.contains(FnameValueSeparator + "")) {
            String[] c = s.split(FnameValueSeparator + "");
            addName(c[0].trim());
            if (c.length == 1) {
                addValue("");
            } else {
                String v = "";
                for (int i = 1; i < c.length; i++) {
                    v += c[i];
                    if (i < c.length - 1) {
                        v += FnameValueSeparator;
                    }
                }
                addValue(v.trim());
            }
        } else {
            addName(null);
            addValue(s);
        }
    }

    private void addValue(String s) {
        /*
        //if (textQuote) {
            //retire les '"' du début et de la fin
            if (s.charAt(0) == '"') {
                s = s.substring(1);
                if(s.lastIndexOf("\"")==s.length()){
                    s=s.substring(0,s.length()-1);
                };
            }
        //}
        */
        Fvalues.add(s);
    }

    private void addName(String s) {
        /*
        //if (textQuote) {
            //retire les '"' du début et de la fin
            if (s.charAt(0) == '"') {
                s = s.substring(1);
                if(s.lastIndexOf("\"")==s.length()){
                    s=s.substring(0,s.length()-1);
                };
            }
        //}
        */
        Fnames.add(s);
    }

    /**
     * Ajoute la paire name/value à la liste
     *
     * @param name  Nom/clé
     * @param value Valeur
     */
    public final void add(String name, String value) {
        addName(name);
        addValue(value);
    }

    /**
     * Alias de remove
     *
     * @param index Alias de remove
     */
    public final void delete(int index) {
        Fnames.remove(index);
        Fvalues.remove(index);
    }

    /**
     * Retire un élément de la liste selon sa position
     *
     * @param index Position de l'élément dans la liste
     */
    public final void remove(int index) {
        delete(index);
    }

    /**
     * @return le nombre d'élément dans la liste
     */
    public final int size() {
        return Fvalues.size();
    }

    /**
     * Affiche les éléments dans la console
     */
    public final void print() {
        for (int i = 0; i < size(); i++) {
            System.out.println(get(i));
        }
    }

    private String getName(int i) {
        return Fnames.get(i);
    }

    private String getValue(int i) {
        return Fvalues.get(i);
    }

    /**
     * Affiche les éléments dans la console
     */
    public final void printValues() {
        for (int i = 0; i < size(); i++) {
            System.out.println(getValue(i));
        }
    }

    /**
     * Ajoute le couple key/val à la liste
     *
     * @param name
     * @param value
     */
    public final void put(String name, String value) {
        //Fnames.add(name);
        addName(name);
        //Fvalues.add(value);
        addValue(value);
    }

    /**
     * Retourne une chaine contenant les couples=valeurs séparés par une ','
     *
     * @return
     */
    @Override
    public String toString() {
        return toString(",");
    }

    /**
     * Retourne les éléments de la liste séparés par 'aSeparator'
     *
     * @param aSeparator séparateur des éléments (',' ou '\n' ou autre char)
     * @return
     */
    public String toString(String aSeparator) {
        String r = "";
        for (int i = 0; i < Fvalues.size(); i++) {
            /*
            if (textQuote) {
                r += aSeparator + "\"" + Fnames.get(i) + "\"=\"" + Fvalues.get(i) + "\"";
            } else {
                r += aSeparator + Fnames.get(i) + "=" + Fvalues.get(i);
            }
            */
            r += aSeparator + Fnames.get(i) + "=" + Fvalues.get(i);
        }
        return r.substring(aSeparator.length());
    }

    /**
     * Crée une liste depuis une chaine contenant les couples=valeurs séparés
     * par 'aSeparator'
     *
     * @param s          chaine contenant les couples=valeurs séparés par 'aSeparator'
     * @param aSeparator séparateur des éléments (',' ou '\n' ou autre char)
     */
    public void fromString(String s, String aSeparator) {
        String[] ts = s.split(aSeparator);
        for (String t : ts) {
            add(t);
        }
    }

    /**
     * Alias de setText
     *
     * @param s Alias de setText
     */
    public final void assign(String s) {
        setText(s);
    }

    /**
     * @return La version texte des éléments de la liste (plusieurs lignes)
     */
    public final String getText() {
        return Ftext;
    }

    /**
     * Défini les éléments de la liste depuis un texte (plusieurs lignes)
     *
     * @param s Le texte dont les lignes seront transformée en éléments de la
     *          liste
     */
    public final void setText(String s) {
        Ftext = s;
        clear();
        fromString(s, "\n");
    }

    public void loadFromServer(DBserver aServer) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


}
