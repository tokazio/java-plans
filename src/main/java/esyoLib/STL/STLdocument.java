/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.STL;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * @author romainpetit
 */
public class STLdocument {

    private File file;

    public void loadFromFile() {

    }

    //bug! manque une librairie?
    private int readInt(FileInputStream f) {
        return 0;
    }

    //bug! manque une librairie?
    private float readFloat(FileInputStream f) {
        return 0;
    }

    private void readBinary() throws IOException {

        FileInputStream fis = new FileInputStream(file);
        fis.skip(80); //-> Header skippen (80 Byte) 
        int number_of_triangles = readInt(fis);

        // 
        // ToDo: Check if number of triangles is OK (not negative) 
        // 
        if (number_of_triangles < 0) {
            // error
        }

        if (number_of_triangles > 0) {

            float[] triangleAttributes = new float[18];
            float[] verticesAttributes = new float[number_of_triangles * 18];

            // read data 
            float[] tri;
            for (int counter = 0; counter < number_of_triangles; counter++) {

                tri = new float[12];
                tri[9] = readFloat(fis);
                tri[10] = readFloat(fis);
                tri[11] = readFloat(fis);
                tri[0] = readFloat(fis);
                tri[1] = readFloat(fis);
                tri[2] = readFloat(fis);
                tri[3] = readFloat(fis);
                tri[4] = readFloat(fis);
                tri[5] = readFloat(fis);
                tri[6] = readFloat(fis);
                tri[7] = readFloat(fis);
                tri[8] = readFloat(fis);
                fis.skip(2);

                triangleAttributes[0] = tri[0];
                triangleAttributes[1] = tri[1];
                triangleAttributes[2] = tri[2];
                triangleAttributes[3] = tri[9];
                triangleAttributes[4] = tri[10];
                triangleAttributes[5] = tri[11];
                triangleAttributes[6] = tri[3];
                triangleAttributes[7] = tri[4];
                triangleAttributes[8] = tri[5];
                triangleAttributes[9] = tri[9];
                triangleAttributes[10] = tri[10];
                triangleAttributes[11] = tri[11];
                triangleAttributes[12] = tri[6];
                triangleAttributes[13] = tri[7];
                triangleAttributes[14] = tri[8];
                triangleAttributes[15] = tri[9];
                triangleAttributes[16] = tri[10];
                triangleAttributes[17] = tri[11];

                System.arraycopy(triangleAttributes, 0, verticesAttributes, counter * 18, 18);
            }

            fis.close();
        }
    }
}
