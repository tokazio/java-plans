/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.STL;

import java.text.DecimalFormat;

/**
 * @author rpetit
 */
public class STLvertex {

    public float x;
    public float y;
    public float z;
    private DecimalFormat df = new DecimalFormat("0,000000E000");

    /**
     * @param s
     * @param offset
     */
    //vertex 5.850000e+001 0.000000e+000 0.000000e+000
    public STLvertex(String s, int offset) {
        String[] v = s.split("\\s");
        x = Float.parseFloat(v[offset]);
        y = Float.parseFloat(v[offset + 1]);
        z = Float.parseFloat(v[offset + 2]);
    }

    /**
     * @param x
     * @param y
     * @param z
     */
    public STLvertex(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /**
     * @return Un vecteur avec x,y et z à 0
     */
    public static STLvertex zero() {
        return new STLvertex(0, 0, 0);
    }

    /**
     * dot product of 2 vectors a & b
     *
     * @param a a vector
     * @param b a vector
     * @return a double
     */
    public static double dot(STLvertex a, STLvertex b) {
        return a.x * b.x + a.y * b.y + a.z * b.z;
    }

    /**
     * @param a
     * @param b
     * @return
     */
    public static STLvertex cross(STLvertex a, STLvertex b) {
        return new STLvertex(a.y * b.z - a.z * b.y, a.z * b.x - a.x * b.z, a.x * b.y - a.y * b.x);
    }

    /**
     * Addition des vecteurs: a+b
     *
     * @param a Un vecteur
     * @param b Un vecteur
     * @return Un vecteur
     */
    public static STLvertex add(STLvertex a, STLvertex b) {
        return new STLvertex(a.x + b.x, a.y + b.y, a.z + b.z);
    }

    /**
     * Soustraction des vecteurs: a-b
     *
     * @param a Un vecteur
     * @param b Un vecteur
     * @return Un vecteur
     */
    public static STLvertex sub(STLvertex a, STLvertex b) {
        return new STLvertex(a.x - b.x, a.y - b.y, a.z - b.z);
    }

    /**
     * @return
     */
    @Override
    public String toString() {
        return df.format(x) + " " + df.format(y) + " " + df.format(z);
    }

    /**
     * @return La longueur d'un vecteur
     */
    public float length() {
        return (float) Math.sqrt((x * x) + (y * y) + (z * z));
    }

    /**
     * Normalize the vector
     *
     * @return a vector
     */
    public final STLvertex normal() {
        return new STLvertex(x / length(), y / length(), z / length());
    }

    /**
     * @param v
     * @return
     */
    public final boolean equals(STLvertex v) {
        return (this.x == v.x && this.y == v.y && this.z == v.z);
    }
}
