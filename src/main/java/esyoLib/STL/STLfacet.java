/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.STL;

/**
 * @author rpetit
 */
public class STLfacet {

    public STLvertex a;
    public STLvertex b;
    public STLvertex c;
    public STLvertex normal;

    /**
     * @param n
     * @param a
     * @param b
     * @param c
     */
    public STLfacet(STLvertex n, STLvertex a, STLvertex b, STLvertex c) {
        this.normal = n;
        this.a = a;
        this.b = b;
        this.c = c;
    }

    /**
     * @return
     */
    @Override
    public String toString() {
        return "\tfacet normal " + normal.toString() + "\n" + "\t\touter loop\n\t\t\tvertex " + a.toString() + "\n\t\t\tvertex " + b.toString() + "\n\t\t\tvertex " + c.toString() + "\n\t\t" + "endloop\n\tendfacet";
    }

    //
    public void computeNormal() {
        STLvertex d = STLvertex.sub(b, a);
        STLvertex e = STLvertex.sub(c, a);
        this.normal = STLvertex.cross(d, e);
    }
}
