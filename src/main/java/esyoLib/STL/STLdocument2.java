/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.STL;

import java.io.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import static esyoLib.Files.TfileUtils.getExtension;

/**
 * @author rpetit
 */
public class STLdocument2 {

    private final ArrayList<STLfacet> faces = new ArrayList();
    private ArrayList<String> lines = new ArrayList();
    private int fileCursor = 0;
    private String entete = "";

    /**
     * @param aFilename
     */
    public STLdocument2(String aFilename) {
        try {
            loadFromFile(aFilename);
        } catch (IOException ex) {
            Logger.getLogger(STLdocument2.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //
    private ArrayList fileToList2(String aFilename) throws IOException {
        ArrayList<String> l = new ArrayList();
        BufferedReader reader = new BufferedReader(new FileReader(aFilename));
        String line = null;
        while ((line = reader.readLine()) != null) {
            l.add(line.trim());
        }
        reader.close();
        return l;
    }

    /**
     * @param aFileName
     * @throws FileNotFoundException
     * @throws IOException
     */
    public final void loadFromFile(String aFileName) throws FileNotFoundException, IOException {
        if (!getExtension(aFileName).toUpperCase().equals(".STL")) {
            throw new IOException("Le fichier n'est pas au format STL.");
        }
        this.lines = fileToList2(aFileName);
        load();
        this.lines.clear();
    }

    //
    private void load() {
        this.fileCursor = 0;
        //en tête
        String l = this.lines.get(this.fileCursor);
        String[] v = l.split("\\s");
        this.entete = v[1];
        this.fileCursor++;
        //
        while (this.readFacet()) {

        }
    }

    //
    private boolean readFacet() {
        //facet normal 2.209394e-001 0.000000e+000 -9.752876e-001
        STLvertex n = new STLvertex(this.lines.get(this.fileCursor), 2);
        this.fileCursor++;
        //loop
        this.fileCursor++;
        //vertex
        faces.add(new STLfacet(n, readVertex(), readVertex(), readVertex()));
        //endloop
        this.fileCursor++;
        //endfacet
        this.fileCursor++;
        return !this.lines.get(this.fileCursor).equals("endsolid");
    }

    //
    private STLvertex readVertex() {
        String l = this.lines.get(this.fileCursor);
        this.fileCursor++;
        return new STLvertex(l, 1);
    }

    /**
     * @return
     */
    @Override
    public String toString() {
        String s = "solid " + entete + "\n";
        for (int i = 0; i < faces.size(); i++) {
            s += faces.get(i).toString() + "\n";
        }
        s += "endsolid";
        return s;
    }

    public void saveToFile(String aFilename) throws IOException {
        FileWriter fw = new FileWriter(aFilename);
        BufferedWriter bw = new BufferedWriter(fw);
        PrintWriter pw = new PrintWriter(bw);
        pw.println(toString());
        pw.close();
    }

    /**
     * @return
     */
    public ArrayList<STLfacet> getFaces() {
        return this.faces;
    }
}



