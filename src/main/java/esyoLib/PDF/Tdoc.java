package esyoLib.PDF;

import com.lowagie.text.DocumentException;
import esyoLib.DB.DBobj3;
import org.xhtmlrenderer.pdf.ITextRenderer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Tdoc {

    private String content = "";
    private DBobj3 informations = null;
    private String filename = "";
    private boolean replaceFileIfExists = false;
    private boolean verbose = false;

    /**
     * Utilise un fichier HTML pour le transformer en PDF
     * Remplace les valeurs des champs du type {|champ|} dans le fichier HTML
     * <p>
     * {|ON champ OUT text|} Remplace par text si le champ est true/présent/non vide
     * {|ON champ=valeur OUT text|} Remplace par text si le champ = la valeur
     * <p>
     * {|ON ? OUT text|} text peut contenir d'autres {|?|}
     *
     * @param informations
     */
    public Tdoc(DBobj3 informations) {
        //
        this.informations = informations;
    }

    private static String readFile(String path, Charset encoding) throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }

    private static void doRenderToPDF(ITextRenderer renderer, String pdf) throws IOException, DocumentException {
        renderer.layout();
        OutputStream os = new FileOutputStream(pdf);
        try {
            renderer.createPDF(os);
        } finally {
            os.close();
            os = null;
        }
    }

    public void setVerbose(boolean b) {
        this.verbose = b;
    }

    /**
     * @param modelFileName
     * @param aFileName
     * @param replaceFileIfExists
     * @throws IOException
     */
    public void convert(String modelFileName, String aFileName, boolean replaceFileIfExists) throws Exception {
        if (this.verbose) {
            System.out.println("Début de la convertion...");
        }
        File f = new File(modelFileName);
        if (modelFileName.isEmpty() || !f.exists()) {
            throw new IOException("Vous devez fournir un chemin de fichier de modèle valide.");
        }
        if (aFileName.isEmpty()) {
            throw new IOException("Vous devez fournir un chemin de fichier PDF cible valide.");
        }
        //
        this.replaceFileIfExists = replaceFileIfExists;
        //ouvre le modèle dans Fcontent, remplace les variables
        replaceValuesFrom(modelFileName);
        //transforme Fcontent en pdf
        toPDF(aFileName);
        //efface le fichier tmp
        //??
        if (this.verbose) {
            System.out.println("Terminé.");
        }
    }

    /**
     * Ouvre un fichier vers Fcontent, remplace les variables
     *
     * @param modelFileName
     * @return
     */
    private void replaceValuesFrom(String modelFileName) throws IOException {
        if (this.verbose) {
            System.out.println("Ouverture du modèle " + modelFileName);
        }
        //place le contenu du fichier modèle dans un String
        this.content = readFile(modelFileName, StandardCharsets.UTF_8);
        //remplace les variables
        if (this.verbose) {
            System.out.println("Remplacement des variables...");
        }
        replaceVars(this.content);
        replaceVars(this.content);
    }

    /**
     * Recherche et remplace les variables |??| et |ON ?? OUT ??| dans ct
     * (ct=Fcontent au départ, peu être un sous texte avec une sous variable)
     *
     * @param ct
     */
    private void replaceVars(String ct) {
        Pattern pattern = Pattern.compile("\\{\\|[^\\{\\|\\|\\}]*\\|\\}");
        Matcher matcher = pattern.matcher(ct);
        String f, v, t, a, k;
        boolean canOut = true;
        while (matcher.find()) {
            //récupère le {|?|}
            a = matcher.group().substring(2, matcher.group().length() - 2);
            //System.out.println("=>"+a+"<=");
            //si du type ON ? OUT
            if (a.contains("ON ")) {
                //=============================================================
                //ON ? (OUT ?) - Récupère le ON et vérifie si il est true
                //=============================================================
                //prend de juste après le ON jusqu'avant le OUT
                f = a.substring(3, a.indexOf("OUT ") - 1);
                //System.out.println("ON "+f);
                int g = f.indexOf("=");
                //si il y a un =
                if (g >= 0) {
                    //prend du début jusqu'avant le = -> KEY
                    k = f.substring(0, g).trim();

                    //prend du = jusqu'à la fin -> VAL
                    v = f.substring(g + 1).trim();
                    //System.out.println("test "+k +" ("+this.informations.get(k)+") = "+v);
                    //peut afficher si VAL=info[KEY]
                    canOut = this.informations.get(k).trim().equals(v.trim());
                } else {
                    //si pas de =, test booleen direct -> peut afficher si f=true
                    canOut = Boolean.valueOf(this.informations.get(f.trim()));
                }
                //=============================================================
                //(ON ?) OUT ? - Récupère le OUT
                //=============================================================
                //prend de juste après OUT+espace jusqu'à la fin
                t = a.substring(a.indexOf("OUT") + 4);//, a.length());
                //System.out.println("OUT=>"+t);
                //remplace si canOut
                if (canOut) {// v.isEmpty() || v.equals("0") || v.equals("null")) {
                    //ON à true -> remplace le {|?|} total par la valeur après le OUT
                    //remplace("{|" + a + "|}", t);
                    this.content = this.content.replace("{|" + a + "|}", t);
                    //System.out.println("OUT "+t);
                } else {
                    //sinon remplace par rien (ON à false)
                    //remplace("{|" + a + "|}", "");
                    this.content = this.content.replace("{|" + a + "|}", "");
                    //System.out.println("ON f n'est pas true, pas de OUT");
                }
            } else {
                //pas de ON -> remplace directement
                v = this.informations.get(a);
                remplace("{|" + a + "|}", v);
            }
        }
    }

    /**
     * Remplace k par v dans Fcontent
     *
     * @param k
     * @param v
     */
    private void remplace(String k, String v) {
        if (!this.informations.hasKey(k.substring(2, k.length() - 2))) {
            if (this.verbose) {
                System.out.println("/!\\ Le modèle de document demande un champ non présent dans les informations: " + k.substring(2, k.length() - 2) + ".");
            }
            return;
        }
        this.content = this.content.replace(k, v);
        //voir pour remplacer par match (!?d_presserie?!) -> \!\?.*\?\!
        if (v.contains("!?")) {
            if (this.verbose) {
                System.out.println("/!\\ Pas de valeur dans les informations pour le champs " + k + " du document PDF");
            }
        }
        if (v.isEmpty()) {
            v = "(vide)";
        }
        //System.out.println(k + "=" + v);
    }

    private void toPDF(String pdfFileName) throws Exception {
        //Charge le modèle xhtml 1.0 strict (pour export en pdf)
        ITextRenderer renderer = new ITextRenderer();
        //inclus les polices
        renderer.getFontResolver().addFont("C:\\WINDOWS\\FONTS\\ARIAL.TTF", true);
        renderer.getFontResolver().addFont("C:\\WINDOWS\\FONTS\\CALIBRI.TTF", true);
        renderer.getFontResolver().addFont("C:\\WINDOWS\\FONTS\\VERDANA.TTF", true);
        //charge Fcontent dans le moteur de rendu
        try {
            renderer.setDocumentFromString(this.content);
        } catch (Exception ex) {
            throw new Exception("Renderer HTML error\n" + ex.getMessage());
        }
        //crée le pdf
        if (this.verbose) {
            System.out.println("Création du document PDF " + pdfFileName);
        }
        File f = new File(pdfFileName);
        if (f.exists() && !this.replaceFileIfExists) {
            throw new IOException("Le fichier " + pdfFileName + " existe déjà. Utiliser l'otpion 'replaceFileIfExists' pour votre document.");
        }

        try {
            doRenderToPDF(renderer, pdfFileName);
        } catch (IOException ex) {
            throw new Exception("HTML to PDF load error\n" + ex.getMessage());
        } catch (DocumentException ex) {
            throw new Exception("HTML to PDF error\n" + ex.getMessage());
        }
    }
}
