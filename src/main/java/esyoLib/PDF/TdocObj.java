/*
 * Here comes the text of your license
 * Each line should be prefixed with  *
 */
package esyoLib.PDF;

import esyoLib.DB.DBException;
import esyoLib.DB.DBobj;
import esyoLib.DB.DBserver;
import esyoLib.Files.FileObj;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

/**
 * @author rpetit
 */
public class TdocObj extends DBobj {

    public TdocObj(DBserver aServer) throws DBException {
        super(aServer);
    }

    @Override
    public final String getFrom(String aListName, String aFieldName) {
        return "";
    }

    @Override
    public final TdocObj load() {
        return this;
    }

    public final void from(FileObj o) {
        getFields(o.getClass(), o);
    }

    private void getFields(Class aClass, FileObj o) {
        String v;
        Field[] fs = aClass.getDeclaredFields();
        for (int i = 0; i < fs.length; i++) {
            try {
                //si transient, je n'enregistre pas
                if (Modifier.isTransient(fs[i].getModifiers())) {
                    continue;
                }
                fs[i].setAccessible(true);
                try {
                    switch (fs[i].getType().getName()) {
                        case "int":
                            v = "" + fs[i].getInt(o);
                            break;
                        case "double":
                            v = "" + fs[i].getDouble(o);
                            break;
                        case "boolean":
                            v = "" + fs[i].getBoolean(o);
                            break;
                        case "java.lang.String":
                            v = "" + fs[i].get(o);
                            break;
                        default:
                            v = fs[i].get(o).toString();
                            System.out.println("from error " + aClass.getName() + "::" + fs[i].getName() + "(" + fs[i].getType().getName() + ") ->toString()");
                    }
                } catch (Exception ex) {
                    v = ex.getMessage();
                }
                //System.out.println(v);
                add(fs[i].getName(), v);
            } catch (Exception ex) {
                //System.out.println("from error\n" + ex.getMessage());
            }
        }
        if (getClass().getSuperclass() != null) {
            getFields(aClass.getSuperclass(), o);
        }
    }
}
