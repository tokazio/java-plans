/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package esyoLib.Formsfx;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import javax.xml.bind.DatatypeConverter;
import java.io.ByteArrayInputStream;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * FXML Controller class
 *
 * @author rpetit
 */
public class CErrorForm extends Tcontroller implements Initializable {
    //éléments de l'IHM
    @FXML
    private Label title;
    @FXML
    private Label text;
    @FXML
    private ImageView icon;

    /**
     * Initializes the controller class.
     *
     * @param url voir doc javafx
     * @param rb  voir doc javafx
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        String ico = "iVBORw0KGgoAAAANSUhEUgAAAEAAAABBCAYAAABhNaJ7AAAACXBIWXMAAAsTAAAL\n" +
                "EwEAmpwYAAAAIGNIUk0AAG11AABzoAAA/N0AAINkAABw6AAA7GgAADA+AAAQkOTs\n" +
                "meoAAAbrSURBVHja1Jt9kE11GMc/du1ay2KxYb2sIu8ZkkIqMxiZVGNU06CmzKQX\n" +
                "qWaEZFSSlKmJhjGppIjKSiWypLDy/hbSG1Ft3tpip8hbbn+c5+x97q9775577zln\n" +
                "731mzh/33HOel9/5/Z6X7+/5gT9UG2gP9AdGABOBV4DXgTeAacAk4BHgFqATUI8U\n" +
                "p87AKOB94CBwBgg4vM4DvwJLgXFAN6BqKhidL1/4c+BcFANPA2XAceAocBI4FeX5\n" +
                "i8BWYDzQKhkNbwFMBkrCKH8YWAM8D9wL9ALaAM2BRkADoEAMuxYYKktkWQR+ZbJs\n" +
                "uiSD4XXEsJOGkiXAPGAgcEkC/OsBfYAZwH5DxllgFtCssoy/A9hnKLUbeEi+qttU\n" +
                "CxgMFIeZYSP8NDwbeNVQ4htgGFDTB/lpwABgo6HDIqCh18JbAxuU0FPAVCC3EmZg\n" +
                "NQmbpUqf/UAPrwR2AQ4pYXuA65PAD7UD1iq9TgA3uS2kF3BMCXkPyEuiKJQFvKT0\n" +
                "+wcY4hbzK4HfFfNpsg6TkcYoPc9JVpkQtTKm/ZQUSMbuV/qeBLrHy6gGsF0xm55C\n" +
                "afgopfdv8eYKMxWThUk87SPRZKX/CiA9lpcHqZf3SsaXalQFKFJ2jHH6Yq5a96el\n" +
                "EktVagYcEVv+dlpIvaBG7SlSn+5U9hQ6yfTssvTbGFPbTLm8pnSJ+7HQKjUIvZ06\n" +
                "vkExCMgD1gHbgA4eGt9IjNktJXUsucxZsaso0kNNgD/loW0xev1r1MAdAq7wwPiG\n" +
                "AorYcgbH+P4iee9foGu4B55QzO+OkXkm8KZ6/yAWBugWNQC2KP4fS3kcC/UQZCkA\n" +
                "zAm3rnapLxhPWZsGzFVK/iSFihvGb1J8l0qSFg+tFx7HzNDeFbjgQsaXBryjlD0A\n" +
                "tE2AX55R9y9LwHiAkYrXbfqPiWp9XOeCl55n1Oht4jReYw/LXQBcmqsoN1//sVKt\n" +
                "3SwXpm26CLCV/1FCrFOqr6ZrAPgMyHHJnxQrnTLBAi3/kJuLXXRcVYEFyogfHGZi\n" +
                "9QjF/YpcNB7gRYJ7Dx1s72gLG+ly6MoQ8MTm/z3QMsrzdQ10Z1Uc3r4iulnxvwtB\n" +
                "Tuwb/TyI3xlYu0MBlWG2jFCDrDGMr+2BPq3l6weAZwCelR8XXApbkfKERcq4fcBl\n" +
                "UYxf7WEFmqsKpPkAb8uP4zIF8XAQCgmF0ptIWPtC3f/S4/K7CrBDZK1FHJ8drrLw\n" +
                "lqopeQFgpxisjfcDYl8n8rYiIcaGudN9EF5NlsNFQjc/V+HflvgKtRTLp98OH2v0\n" +
                "PFly9gCckYrNL1qiZj3LFfTlxwzIkZTW3PXdhn8bnUXKD5U7pgNAdR+MX26s+U8M\n" +
                "n9DUhwEo1j7ALmNLPV6DNY0vv1EcXgbwqbq/Q6KDl4jSbvUBmKDygPYeCa0Rxnjd\n" +
                "N5BtzIztQGOPdKmv/M9bJmjY3yPjlyoZmwnfP1BDRSTbJ+R7oE8blQmOB7haCR3t\n" +
                "srDqgt7Y/LcQvXmiJqFY/lbBAd2kgYr/ECTfPqKgJjeN/8gwpqFDR7nSGDQ3Gx+m\n" +
                "E2yxKccp7KlXkiDiYlMW8KGxpmOZzjmEQtmbca/t5qtwYX+cEtbHBeMXJ2C8TbWk\n" +
                "KNKOM9FBaEsQHp+r/+ioHMNrCaa5hUZIS8Sb1zYKpQ0k1nWmd40HmBWSjbwejhOE\n" +
                "yAQ+UAJ2uRTP6xil8nri606pQnBfoSQc1vCYEvJAHA5voXr/a5czujoGUlQcR+ne\n" +
                "VxVgMypKEPZKhuaUuhHaPFXgEZBRbIawGGiZ8v4dIz00VQm4J0bllmD1CHtZ0OSK\n" +
                "g10LXBrDezdgQf4VAr9NCba9/kzl9P85Xc9OKUP5t/M46Bl6Us2Cl0l9elDZM8fJ\n" +
                "C9nAd2rE+qaw8e2wGicDWDvfjh1zP4J7hQfxoQfXA6pO6KbqfbEy0F1WK/EeMHWb\n" +
                "Ziv9F8TDoKqRis5LIeOfI3Q3Km6gJ5/QMwGzSP5+wQlK3yPRYr5Tao11gElPp+wk\n" +
                "NV7nMWVAT7cYdyK0Z3g11l57slBd4F2lXylwo9tCWkmaawv5Bbg1CYzvSRDktIs5\n" +
                "z5o78wmFsQNYhx8LKumrT8E6G2DrsglvOtRCKE0czXnD2YyWgsprygGGY3V46A8x\n" +
                "E3/OLJVTb0J7eOwOs6eByz2Q1xjrdNgeQ+YerBNslUKZwKNSOGml/sLa7BiGBTxm\n" +
                "xME7XRztUKwGi6OGjKNY542TomBrADyO1QcU7pjsTilExgK3Y50Q7YyF0bWVKNMd\n" +
                "C7IeJfnGFv5/IDMgIXkilXhgMhrZhxsLCTZeRbouiAM7bfiTcFeZON/heLNZ4gkV\n" +
                "yGDMlK9ZqkCJiq4TgkrNFqNbpHpNniZf7ipZAg9L1JgkRddYuTcEq2utwK/i678B\n" +
                "ALuytlhLhmO1AAAAAElFTkSuQmCC";
        byte[] b = DatatypeConverter.parseBase64Binary(ico);
        ByteArrayInputStream s = new ByteArrayInputStream(b);
        Image i = new Image(s);
        icon.setImage(i);
    }

    @FXML
    private void fermer(ActionEvent event) {
        ((Node) (event.getSource())).getScene().getWindow().hide();
    }

    /**
     * Défini le titre de la fenêtre d'erreur
     *
     * @param s Titre de la fenêtre
     */
    public void setTitle(String s) {
        title.setText(s);
    }

    /**
     * Défini le texte dans le fenêtre d'erreur
     *
     * @param s Texte de la fenêtre
     */
    public void setText(String s) {
        text.setText(s);
    }

    @Override
    public void refresh() {

    }
}
