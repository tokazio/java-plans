/*
 * Here comes the text of your license
 * Each line should be prefixed with  *
 */
package esyoLib.Formsfx;

import esyoLib.DB.DBserver;
import esyoLib.Exceptions.NoNameException;
import esyoLib.Utils.TstringList;

import java.io.IOException;

/**
 * @author rpetit
 */
public class TapplicationOptions {

    /**
     *
     */
    private final DBserver server = null;
    /**
     *
     */
    private Tapplication application = null;
    /**
     *
     */
    private TstringList list = null;

    /**
     * @param aApplication
     * @param aFileName
     * @throws java.io.IOException
     */
    public TapplicationOptions(Tapplication aApplication, String aFileName) throws IOException {
        init(aApplication);
        loadFromFile(aFileName);
    }

    /**
     * @param aApplication
     * @param aServer
     */
    public TapplicationOptions(Tapplication aApplication, DBserver aServer) {
        init(aApplication);
        loadFromServer(aServer);
    }

    /**
     * @param aApplication
     */
    public TapplicationOptions(Tapplication aApplication) {
        init(aApplication);
    }

    /**
     * @param aFileName
     * @return
     * @throws IOException
     */
    public final TapplicationOptions loadFromFile(String aFileName) throws IOException {
        this.list.loadFromFile(aFileName);
        return this;
    }

    /**
     * @param aServer
     * @return
     */
    public final TapplicationOptions loadFromServer(DBserver aServer) {
        list.loadFromServer(aServer);
        return this;
    }

    /**
     * @param aApplication
     */
    private void init(Tapplication aApplication) {
        this.application = aApplication;
        this.list = new TstringList();
    }

    /**
     * @param name
     * @param value
     * @return
     */
    public final TapplicationOptions set(String name, String value) {
        list.add(name, value);
        return this;
    }

    /**
     * @param name
     * @return
     * @throws esyoLib.Exceptions.NoNameException
     */
    public final String get(String name) throws NoNameException {
        return list.valueFromName(name);
    }
}
