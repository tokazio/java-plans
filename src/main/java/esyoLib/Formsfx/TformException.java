/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package esyoLib.Formsfx;

/**
 * @author romainpetit
 */
public class TformException extends Exception {

    /**
     *
     */
    public TformException() {
        super();
    }

    /**
     * @param s Message de l'exception
     */
    public TformException(String s) {
        super(s);
    }
}
