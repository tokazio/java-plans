/*
 * Here comes the text of your license
 * Each line should be prefixed with  *
 */

package esyoLib.Formsfx;

import java.io.IOException;

import static esyoLib.Formsfx.EconfirmResult.crNo;

/**
 * @author romainpetit
 */
public class TconfirmForm extends Tform {
    /**
     * Etat de la confirmation
     */
    private EconfirmResult confirm = crNo;

    /**
     * Crée la fenêtre de confirmation avec son texte et la fenêtre parent
     *
     * @param text         Texte à afficher dans la fenêtre de confirmation
     * @param aApplication
     * @throws java.io.IOException
     * @throws esyoLib.Formsfx.TformException
     * @throws esyoLib.Formsfx.TformFmxlNotFoundException
     */
    public TconfirmForm(String text, Tapplication aApplication) throws IOException, TformException, TformFmxlNotFoundException, TformFmxlLoadException {
        super("Confirmer", "fxmls/confirmForm.fxml", aApplication);
        //Affiche l'erreur dans la console
        System.out.println("Confirmer: " + text);
        //
        setTitle("Confirmer");
        ((CConfirmForm) getController()).setText(text);
        ((CConfirmForm) getController()).setConfirm(this);
        showAndWait();
    }

    /**
     * Retourne l'état de la confirmation
     *
     * @return L'état de la confirmation
     */
    public EconfirmResult isConfirmed() {
        return this.confirm;
    }

    /**
     * @param c
     */
    public void setConfirm(EconfirmResult c) {
        this.confirm = c;
    }
}
