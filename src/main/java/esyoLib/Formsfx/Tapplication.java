/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.Formsfx;

import esyoLib.DB.DBserver;
import esyoLib.Exceptions.NoNameException;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;

/**
 * @param <T>
 * @author RomainPETIT
 */
public class Tapplication<T extends DBserver> {

    /**
     * Affiche les messages dans la console
     */
    private final boolean trace = true;
    /**
     *
     */
    private String version = "";
    /**
     * Connection au serveur de base de donnée
     */
    private T server = null;
    /**
     *
     */
    private TapplicationOptions options = null;
    /**
     *
     */
    private Stage stage = null;
    /**
     *
     */
    private String actualFolder = "";
    /**
     * Application javaFX liée
     */
    private Application application = null;
    /**
     * Fenêtre principale
     */
    private Tform mainForm;
    /**
     * Liste des fenêtres de l'application
     */
    private HashMap<String, Tform> formMap;

    /**
     * @param aStage             Stage javaFX principal de l'application
     * @param aJavaFXApplication Application javaFX
     */
    public Tapplication(Stage aStage, Application aJavaFXApplication) {
        init(aStage, aJavaFXApplication);
    }

    /**
     * @param aStage
     * @param aJavaFXApplication
     * @param aClass
     */
    public Tapplication(Stage aStage, Application aJavaFXApplication, Class<? extends DBserver> aClass) {
        init(aStage, aJavaFXApplication);
        //tente de se connecter au serveur de base de donnée
        try {
            //nouvelle connection au serveur de base de donnée selon les paramètres dans le fichier db.txt
            Class[] types = {String.class};
            Constructor constructor = aClass.getConstructor(types);
            Object[] args = new Object[1];
            args[0] = "db.txt";
            Object instanceOfMyClass = constructor.newInstance(args);
            this.server = (T) instanceOfMyClass;
        } catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            //Erreur, nouvelle fenêtre d'erreur
            showError("Erreur de connexion au serveur de la base de donnée", ex.getMessage());
        }
    }

    /**
     * @return
     */
    public static final Tapplication nullApplication() {
        return null;
    }

    /**
     * @param aStage
     * @param aJavaFXApplication
     */
    private void init(Stage aStage, Application aJavaFXApplication) {
        this.options = new TapplicationOptions(this);
        this.stage = aStage;
        this.formMap = new HashMap<>();
        this.application = aJavaFXApplication;
        //Fermeture de la dernière fenêtre ne termine pas l'application (false)
        Platform.setImplicitExit(false);
    }

    /**
     * @param aFileName
     * @throws IOException
     */
    public final void loadOptions(String aFileName) throws IOException {
        options.loadFromFile(aFileName);
    }

    /**
     * @return
     */
    public final Stage getStage() {
        return this.stage;
    }

    /**
     * Crée une fenêtre dans l'application: version simple avec titre Ne demande
     * que le nom de la fenêtre, le fichier fxml et le titre Le nom permet
     * d'accèder à la fenêtre depuis l'application, il doit être unique Le nom
     * du fichier fxml doit comprendre le nom de la classe/le nom du fichier
     * sans l'extension
     *
     * @param aFormName
     * @param aFxml
     * @param aParentForm
     * @return La fenêtre (TForm) créée
     */
    public final Tform newForm(String aFormName, String aFxml, Tform aParentForm) {
        Tform f = null;
        if (aFormName.isEmpty()) {
            aFormName = newName();
        } else {
            f = getFormByName(aFormName);
        }
        if (f == null) {
            try {
                if (aParentForm == null) {
                    f = new Tform(aFormName, aFxml, this);
                } else {
                    f = new Tform(aFormName, aFxml, aParentForm);
                }
                this.formMap.put(aFormName, f);
                if (this.trace) {
                    System.out.println("Fenêtres en cours: " + this.formMap.size());
                }
            } catch (IOException | TformException ex) {
                showError("Impossible d'afficher la fenêtre " + aFormName, "Vérifier l'addresse du contrôleur (fx:controller) dans le fichier fxml '" + aFxml + "'.\n" + ex.getMessage());
                return null;
            } catch (TformFmxlNotFoundException | TformFmxlLoadException ex) {
                showError("Impossible d'afficher la fenêtre " + aFormName, ex.getMessage());
                return null;
            }
        }
        return f;
    }

    /**
     * @return
     */
    private String newName() {
        return "Form" + this.formMap.size();
    }

    /**
     * Crée une fenêtre dans l'application: version simple avec titre Ne demande
     * que le nom de la fenêtre, le fichier fxml Le nom permet d'accèder à la
     * fenêtre depuis l'application, il doit être unique Le nom du fichier fxml
     * doit comprendre le nom de la classe/le nom du fichier sans l'extension
     *
     * @param aFormName
     * @param aFxml
     * @return La fenêtre (TForm) créée
     */
    public final Tform newForm(String aFormName, String aFxml) {
        Tform f = null;
        if (aFormName.isEmpty()) {
            aFormName = newName();
        } else {
            f = getFormByName(aFormName);
        }
        if (f == null) {
            try {
                f = new Tform(aFormName, aFxml, this);
                this.formMap.put(aFormName, f);
                if (this.trace) {
                    System.out.println("Fenêtres en cours: " + this.formMap.size());
                }
            } catch (IOException | TformException ex) {
                showError("Impossible d'afficher la fenêtre " + aFormName, "Vérifier l'addresse du contrôleur (fx:controller) dans le fichier fxml '" + aFxml + "'.\n" + ex.getMessage());
                return null;
            } catch (TformFmxlNotFoundException | TformFmxlLoadException ex) {
                showError("Impossible d'afficher la fenêtre " + aFormName, ex.getMessage());
                return null;
            }
        }
        return f;
    }

    /**
     * Crée une fenêtre modale dans l'application: version simple avec titre Le
     * nom permet d'accèder à la fenêtre depuis l'application, il doit être
     * unique Le nom du fichier fxml doit comprendre le nom de la classe/le nom
     * du fichier sans l'extension
     *
     * @param aFormName   Nom de la fenêtre, doit être UNIQUE!
     * @param aFxml       Fichier fxml du design de la fenêtre
     * @param aParentForm
     * @return La fenêtre modale (TmodalForm) créée
     */
    public final TmodalForm newModalForm(String aFormName, String aFxml, Tform aParentForm) {
        try {
            TmodalForm f = null;
            if (this.trace) {
                System.out.println("createModalForm: " + aFormName + " avec " + aFxml);
            }
            if (aParentForm == null) {
                f = new TmodalForm(aFormName, aFxml, this);
            } else {
                f = new TmodalForm(aFormName, aFxml, aParentForm);
            }
            if (this.trace) {
                System.out.println("modalForm: " + f);
            }
            this.formMap.put(aFormName, f);
            if (this.trace) {
                System.out.println("Fenêtres en cours: " + this.formMap.size());
            }
            return f;
        } catch (IOException | TformException | TformFmxlNotFoundException | TformFmxlLoadException ex) {
            showError("Impossible d'afficher la fenêtre modale " + aFormName, ex.getClass().getName() + "\n" + ex.getMessage() + "\n Il y a peut être un problème dans la fonction 'initialize' du conrôlleur.");
        }
        return null;
    }

    /**
     * Retourne la fenêtre principale
     *
     * @return La fenêtre (Tform) principale de l'application
     */
    public final Tform getMainForm() {
        return this.mainForm;
    }

    /**
     * Retourne la fenêtre par son nom
     *
     * @param formName Nom de la fenêtre
     * @return La fenêtre (Tform) formName dans l'application
     */
    public final Tform getFormByName(String formName) {
        return this.formMap.get(formName);
    }

    /**
     * Supprime la fenêtre de la liste
     *
     * @param f Fenêtre (Tform) à détruire (retirer de l'application (de la
     *          liste))
     */
    protected void destroyForm(Tform f) {
        this.formMap.remove(f.getName(), f);
        if (this.trace) {
            System.out.println("Nombre de fenêtres en cours: " + this.formMap.size());
        }
    }

    /**
     * Retourne le n° de version indiqué par setVersion(). Si cela n'a pas été fait, indique le n° de version de l'application défini par
     * javafx.application.implementation.version=0.1.1 dans
     * \nbproject\project.properties
     *
     * @return Une chaîne contenant le n° de version
     */
    public final String getVersion() {
        if (this.version.isEmpty()) {
            Package objPackage = this.application.getClass().getPackage();
            return objPackage.getImplementationVersion();
        } else {
            return this.version;
        }
    }

    /**
     * @param s Un n° de version
     * @return Chaînage <code>Tapplication</code>
     */
    public Tapplication setVersion(String s) {
        this.version = s;
        return this;
    }

    /**
     * @return Retourne le nom de l'OS
     */
    public final String getOs() {
        return System.getProperty("os.name").toLowerCase();
    }

    /**
     * @param titre Titre de l'erreur
     * @param texte Texte de l'erreur
     * @return
     */
    public final Tform showError(String titre, String texte) {
        Tform f = null;
        try {
            f = new TerrorForm(titre, texte, this);
        } catch (IOException | TformException | TformFmxlNotFoundException | TformFmxlLoadException ex) {
            System.out.println("----------------------------------------------");
            System.out.println(titre + ": \n" + texte);
            System.out.println("----------------------------------------------");
        }
        return f;
    }

    /**
     * @param ex
     * @return
     */
    public final Tform showError(Exception ex) {
        return showError(ex.getClass().getName(), ex.getMessage());
    }

    /**
     * @param f
     * @return Chaînage (form)
     * @deprecated ?
     */
    @Deprecated
    public final Tform setMainForm(Tform f) {
        this.mainForm = f;
        //pilote l'application, termine tout à la fermeture
        f.getStage().setOnCloseRequest((WindowEvent ev) -> {
            Platform.exit();
        });
        return f;
    }

    /**
     * @return
     */
    public final String getActualFolder() {
        System.out.println("Actual folder est: " + this.actualFolder);
        return this.actualFolder;
    }

    /**
     * @param s
     */
    public final void setActualFolder(String s) {
        this.actualFolder = s;
        System.out.println("Actual folder devient: " + s);
    }

    /**
     * @param s
     */
    public void setIco(String s) {
        try {
            this.stage.getIcons().add(new Image(getClass().getResourceAsStream(s)));
        } catch (Exception ex) {

        }
    }

    /**
     * @param text
     * @return
     * @throws IOException
     * @throws TformException
     * @throws TformFmxlNotFoundException
     * @throws esyoLib.Formsfx.TformFmxlLoadException
     */
    public TconfirmForm newConfirmForm(String text) throws IOException, TformException, TformFmxlNotFoundException, TformFmxlLoadException {
        return new TconfirmForm(text, this);
    }

    /**
     * @param text
     * @return
     * @throws IOException
     * @throws TformException
     * @throws TformFmxlNotFoundException
     * @throws esyoLib.Formsfx.TformFmxlLoadException
     */
    public TconfirmCancelForm newConfirmCancelForm(String text) throws IOException, TformException, TformFmxlNotFoundException, TformFmxlLoadException {
        return new TconfirmCancelForm(text, this);
    }

    /**
     * @return
     */
    public final T getServer() {
        return this.server;
    }

    /**
     * @param name
     * @param value
     */
    public void setOption(String name, String value) {
        options.set(name, value);
    }

    /**
     * @param name
     * @return
     */
    public String getOption(String name) {
        try {
            return options.get(name);
        } catch (NoNameException ex) {
            showError("!" + name + "!", "Option '" + name + "' introuvable!");
            return null;
        }
    }
}
