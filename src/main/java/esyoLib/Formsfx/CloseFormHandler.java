/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package esyoLib.Formsfx;

import javafx.event.Event;
import javafx.event.EventHandler;

/**
 * @author RomainPETIT
 */
public class CloseFormHandler implements EventHandler {

    /**
     *
     */
    private final Tform Fform;

    /**
     * @param aForm
     */
    public CloseFormHandler(Tform aForm) {
        Fform = aForm;
    }

    /**
     * Gère la fermeture d'une fenêtre
     *
     * @param event Fermeture de la fenêtre
     */
    @Override
    public void handle(Event event) {
        try {
            Fform.close();
        } catch (Exception ex) {
            //la fenêtre principale à été fermée?
        }
    }

}
