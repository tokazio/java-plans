/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.Formsfx;

import javafx.application.Platform;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import static java.awt.Toolkit.getDefaultToolkit;

/**
 * @author rpetit
 */
public class TnotifyForm extends Tform {
    /**
     * Vitesse d'animation
     */
    private static final int MOVE = 20;
    /**
     * Temps d'affichage avant de disparaître
     */
    private static final int TIME = 3000;
    /*
    Liste des fenêtres
    */
    public static ArrayList<Stage> liste = new ArrayList();

    /**
     * Constructeur
     *
     * @param aFormName    Nom de la fenêtre
     * @param aFxml        Fichier FXML de la fenêtre
     * @param aApplication
     * @throws IOException                                Erreur lecture fichier FXML
     * @throws TformException                             Erreur de fenêtre
     * @throws esyoLib.Formsfx.TformFmxlNotFoundException
     */
    public TnotifyForm(String aFormName, String aFxml, Tapplication aApplication) throws IOException, TformException, TformFmxlNotFoundException, TformFmxlLoadException {
        super(aFormName, aFxml, aApplication);
        this.getStage().initStyle(StageStyle.UNDECORATED);
        this.setSize(300, 200);
        liste.add(getStage());
    }

    /**
     * Démarre l'animation de la fenêtre
     */
    public final void start() {
        //position x de départ (à droite)
        getStage().setX(getDefaultToolkit().getScreenSize().width - getStage().getWidth());
        //position y de départ (en bas)
        getStage().setY(getDefaultToolkit().getScreenSize().height);
        //Position finale de la fenêtre (y) -60 pour la barre des tâches windows7
        double j = getDefaultToolkit().getScreenSize().height - (getStage().getHeight() * liste.size()) - 60;
        Timer timer = new Timer();
        TimerTask t = new TimerTask() {
            //départ de l'animation = position acutelle de la fenêtre
            double i = getStage().getY();

            @Override
            public void run() {
                if (i > j) {
                    getStage().setY(i);
                    i -= MOVE;
                } else {
                    pause();
                    //this.cancel();
                    timer.cancel();
                }
            }
        };
        timer.scheduleAtFixedRate(t, 0, 25);
    }

    /**
     *
     */
    private void pause() {
        Timer timer = new Timer();
        TimerTask t = new TimerTask() {
            @Override
            public void run() {
                stop();
                //this.cancel();
                timer.cancel();
            }
        };
        timer.schedule(t, TIME, 25);
    }

    /**
     * Termine l'animation de la fenêtre
     */
    public final void stop() {
        //fin y
        double j = getDefaultToolkit().getScreenSize().height;
        Timer timer = new Timer();
        TimerTask t = new TimerTask() {
            //départ de l'animation = position acutelle de la fenêtre
            double i = getStage().getY();

            @Override
            public void run() {
                if (i < j + 30) {
                    getStage().setY(i);
                    i += MOVE;
                } else {
                    liste.remove(getStage());
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            getStage().close();
                        }
                    });
                    //this.cancel();
                    timer.cancel();
                }
            }
        };
        timer.scheduleAtFixedRate(t, 0, 25);
    }
}
