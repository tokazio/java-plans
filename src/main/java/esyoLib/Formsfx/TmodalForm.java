/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.Formsfx;

import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * @author romainpetit
 */
public class TmodalForm extends Tform {

    /**
     * Crée une fenêtre modale Le nom permet d'accèder à la fenêtre depuis
     * l'application, il doit être unique Le nom du fichier fxml doit comprendre
     * le nom de la classe/le nom du fichier sans l'extension
     *
     * @param aFxml       Fichier fxml du design de la fenêtre
     * @param aFormName   Nom de la fenêtre, doit être UNIQUE!
     * @param aParentForm
     * @throws IOException
     * @throws TformException
     * @throws esyoLib.Formsfx.TformFmxlNotFoundException
     * @throws esyoLib.Formsfx.TformFmxlLoadException
     */
    public TmodalForm(String aFormName, String aFxml, Tform aParentForm) throws IOException, TformException, TformFmxlNotFoundException, TformFmxlLoadException {
        super(aFormName, aFxml, aParentForm);
        //pour modality---
        Stage s = getStage();
        if (s != null) {
            try {
                s.initOwner(aParentForm.getStage().getScene().getWindow());
                s.initModality(Modality.WINDOW_MODAL);
            } catch (Exception e) {
                System.out.println("TmodalForm::modality error pour '" + aFormName + "': " + e.getMessage());
            }
        } else {
            System.out.println("TmodalForm::modality error pour '" + aFormName + "', impossible de récupérer le 'Stage'.");
        }

    }

    /**
     * Crée une fenêtre modale Le nom permet d'accèder à la fenêtre depuis
     * l'application, il doit être unique Le nom du fichier fxml doit comprendre
     * le nom de la classe/le nom du fichier sans l'extension
     *
     * @param aFxml        Fichier fxml du design de la fenêtre
     * @param aFormName    Nom de la fenêtre, doit être UNIQUE!
     * @param aApplication
     * @throws IOException
     * @throws TformException
     * @throws esyoLib.Formsfx.TformFmxlNotFoundException
     * @throws esyoLib.Formsfx.TformFmxlLoadException
     */
    public TmodalForm(String aFormName, String aFxml, Tapplication aApplication) throws IOException, TformException, TformFmxlNotFoundException, TformFmxlLoadException {
        super(aFormName, aFxml, aApplication);
        //pour modality---
        Stage s = getStage();
        if (s != null) {
            try {
                System.out.println("application: " + getApplication());
                System.out.println("stage: " + getApplication().getStage());
                System.out.println("scene: " + getApplication().getStage().getScene());
                System.out.println("window: " + getApplication().getStage().getScene().getWindow());
                s.initOwner(getApplication().getStage().getScene().getWindow());
                s.initModality(Modality.WINDOW_MODAL);
            } catch (Exception e) {
                System.out.println("TmodalForm::modality error pour '" + aFormName + "': " + e.getMessage());
            }
        } else {
            System.out.println("TmodalForm::modality error pour '" + aFormName + "', impossible de récupérer le 'Stage'.");
        }
    }
}
