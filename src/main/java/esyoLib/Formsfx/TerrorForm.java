/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package esyoLib.Formsfx;

import javafx.stage.StageStyle;

import java.io.IOException;

/**
 * @author rpetit
 */
public class TerrorForm extends Tform {

    /**
     * @param title        Titre de l'erreur
     * @param text         Texte de l'erreur
     * @param aApplication
     * @throws java.io.IOException
     * @throws esyoLib.Formsfx.TformException
     * @throws esyoLib.Formsfx.TformFmxlNotFoundException
     */
    public TerrorForm(String title, String text, Tapplication aApplication) throws IOException, TformException, TformFmxlNotFoundException, TformFmxlLoadException {
        super("Error", "fxmls/errorForm.fxml", aApplication);
        //Affiche l'erreur dans la console
        System.out.println(title + ": " + text);
        //Défini le titre
        setTitle(title);
        //Que le boutton fermer
        getStage().initStyle(StageStyle.UTILITY);
        //Affiche le titre dans la fenêtre
        ((CErrorForm) getController()).setTitle(title);
        //Affiche le texte dans la fenêtre
        ((CErrorForm) getController()).setText(text);
        //Affiche bloquant
        showAndWait();
    }
}
