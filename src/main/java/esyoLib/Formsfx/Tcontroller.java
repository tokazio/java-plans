/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.Formsfx;

/**
 * @author rpetit
 */
public abstract class Tcontroller {


    /**
     *
     */
    protected Tform Fform;
    protected Object Fobj;
    /**
     *
     */
    private Tapplication application = null;

    /**
     *
     */
    public Tcontroller() {

    }

    /**
     * Retourne la fenêtre à laquelle le contrôleur est lié
     *
     * @return La fenêtre à laquelle le contrôleur est lié
     */
    public Tform getForm() {
        return Fform;
    }

    /**
     * Défini la fenêtre à laquelle le contrôleur est lié
     *
     * @param aForm fenêtre à laquelle le contrôleur est lié
     */
    public void setForm(Tform aForm) {
        Fform = aForm;
    }

    /**
     * Ancien refresh()
     *
     * @return Chaînage
     */
    public Tcontroller invalidate() {
        //to be overiden
        return this;
    }

    public abstract void refresh();

    /**
     * Ancien manualInit()
     */
    public void afterCreate() {
        //to be overiden
    }

    /**
     *
     */
    public void onActivate() {
        //to be overiden
    }

    /**
     *
     */
    public void onShow() {
        //to be overiden
    }

    /**
     *
     */
    public void onHide() {
        //to be overiden
    }

    /**
     *
     */
    public void onClose() {
        //to be overiden
    }

    /**
     *
     */
    public void onCloseQuery() {
        //to be overiden
    }

    /**
     *
     */
    public void onDeactivate() {
        //to be overiden
    }

    /**
     *
     */
    public void onClick() {
        //to be overiden
    }

    /**
     *
     */
    public void onDblClick() {
        //to be overiden
    }

    /**
     *
     */
    public void onCreate() {
        //to be overiden
    }

    /**
     *
     */
    public void onDestroy() {
        //to be overiden
    }

    /**
     *
     */
    public void onResize() {
        //to be overiden
    }

    /**
     *
     */
    public void onMinimized() {
        //to be overiden
    }

    /**
     *
     */
    public void onMaximized() {
        //to be overiden
    }

    /**
     *
     */
    public final void close() {
        getForm().close();
    }

    /**
     * Retourne l'application à laquelle la fenêtre appartient
     *
     * @return L'application (Tapplication) à laquelle la fenêtre appartient
     */
    public final Tapplication getApplication() {
        return this.getForm().getApplication();
    }

    public void manualIni() {
        // ?
    }

    public void setObj(Object obj) {
        if (obj != null) {
            Fobj = obj;
        }
    }
}
