/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.Formsfx;

/**
 * @author RomainPETIT
 */
public class TformFmxlLoadException extends Exception {

    /**
     * @param s
     */
    public TformFmxlLoadException(String s) {
        super(s);
    }

}
