/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package esyoLib.Formsfx;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import javax.xml.bind.DatatypeConverter;
import java.io.ByteArrayInputStream;
import java.net.URL;
import java.util.ResourceBundle;

import static esyoLib.Formsfx.EconfirmResult.*;

/**
 * FXML Controller class
 *
 * @author rpetit
 */
public class CconfirmCancelForm extends Tcontroller implements Initializable {
    //Fenêtre de confirmation lié
    private TconfirmCancelForm c;
    @FXML
    private Label text;
    @FXML
    private ImageView icon;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        String ico = "iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAACXBIWXMAAAsTAAAL\n" +
                "EwEAmpwYAAAAIGNIUk0AAG11AABzoAAA/N0AAINkAABw6AAA7GgAADA+AAAQkOTs\n" +
                "meoAAAUxSURBVHja3Jt/aJVlFMc/KySbZjpr8w9/RD/UYCholrRVA81AVlB/SIEJ\n" +
                "mf9MVBLS/UKSCLspLl0lhmuDlGT+0eYf+YdNUBeChWTSyBYIuZR+0CyX2/yR64/3\n" +
                "vNzj03u33Xvf53nu9cAL933f85zvOef58T7nPOeCfZoHVAE7gHagG+gHBoEBuQbl\n" +
                "Wbfw7JA288hDugOoBPaIQUNZXt0iq1Jk5yxNAxLA+QgjfpGebQQ2AMuAMqBcrjJ5\n" +
                "tkF42qWNKee8YEzLJcOnypC9bih7EqgG5gMFGcgtkLbVIkvLvi6YU30bXwtcU4r1\n" +
                "Aw3AXAtYc0V2v8K7Jjo4p8eBLqXIP0AdMMUB9hSgXjBD/C7RyQmtMYbjHqDYQycU\n" +
                "C7bWZY1t0H0K7GdgUQ6sQYtFl1CvfTZA7gIOKZBWYFwOLcTjgANKv0Oicyw0Bjij\n" +
                "hG/J4U/xFqXnd6J71nRECa3Lg41YndK3I1th+5WwzXm0G92s9P4sUyGrlZD38nBL\n" +
                "vlXpvzrdxrNV42PkLx1XdsxOp2EYxFwGCi0GTQB3WnRAIdCngqpRUY3yWnmMyswE\n" +
                "1gItwA9AD3BOgp6TMmTnW3DCU8qe6pGYJyvmvTHmA1rTCH8/seCEvUr+5OEYdwnT\n" +
                "v8D4mMB3ZpAD6IzZAeOBmyJ713BBRqjA9hjBZxjG9QJfAgeBr4ZxwjsxO2G7kl0S\n" +
                "xZBQIeaEmMFPyDyvjBhZDwK7IxxwEyiKUYcJKnRPRDFckJeNFubgPaPg+TTCCStj\n" +
                "1uMDkXvBfLFEgZZ6+mbPVPM0vDbFjFGqZD+rXzTJw1OeNy5/GA6osYBxSmQ36Yc9\n" +
                "ORDsTASuGA7YaAGnXmT3hA9mWdr4pEsvRqwBT1reGM0CWEUyrzfWowM6+X8K3AaN\n" +
                "JZlPfB1gm9wc8Wj88xG9/4pFvDDHsQ2V6mr0ZPzdwCXD+O8tYzYKzheodNc6Tw44\n" +
                "EdH7D1vGXCc4ZwD+kpu1npMW4bXMAW6Y2r8EcFVuljg2fnGE8a4yT88J3lUIjqaH\n" +
                "gAUOjR8D/GkY/41D/AWCOejLAW9E9P59vhxww8MUOGcYv9Px9AunwA2A38gwc5oh\n" +
                "PRLR+67PFqsE91dk7tnad0fRSsN4HwHYRr3utMnNbkfg1YYDPvLggI8Fu01/iw87\n" +
                "Aq83HNDswQGHBXurj2DoJfn+hteHjo3XwdAqX+FwiSx8JYwuXWY1HNYJkRpuf6qN\n" +
                "CrebSdba3O7UFbX2PKOGxRwHSjwBLAdecGz8HGXn0+bLi47yAm8ZX4Hj2D0gjcoD\n" +
                "XBxubtg4GAnpIaJPgaocGK8PRmpTMdg4GtP0WgoHtDlwgD4aS9nB+nC0yIISi1I4\n" +
                "4H3Lxk9iFIejIWM4TNotKFJA8mBCXzMsO+Cgmt4TR2LWtUEVFpS5l6AGoIOgCOsx\n" +
                "y8ZXkEGt0E/S4G/slci4oEKxIa0SGYAHsFes4JI6s5lmKzxmbOIgXZmyIlMhzeRH\n" +
                "iaxJ7xJjzdGBPHOCNr7VxlxqyGHjG2yuXXokdOA2jT0S3S86xd7zJjUpkN+BpTlg\n" +
                "/FJurSxpsg1oZnX3A9M9GD6dW6vZbRRVpaRSkun08Iwtgbs/TSVInmkOAV/jqbhr\n" +
                "PUFBdajIgCxECy1gLRTZAwrvsujglYolmuszhuNpgjK3igy304XSdpPI0rL7BLOY\n" +
                "HKIigiqzsxHRXi9BWWwL8Kb02ssEf5ktk9/r5V2L8PZGyDkrGEXkOJUT5PtPk/2f\n" +
                "p78VWeXkKT0KvAq8LZmfo8CPBDWB4d/nr0jvHgU+F97l0tYq/TcACBBiEkpD68UA\n" +
                "AAAASUVORK5CYII=";
        byte[] b = DatatypeConverter.parseBase64Binary(ico);
        ByteArrayInputStream s = new ByteArrayInputStream(b);
        Image i = new Image(s);
        icon.setImage(i);
    }

    /**
     * Défini le texte dans la fenêtre de confirmation
     *
     * @param s Texte à afficher dans la fenêtre
     */
    public void setText(String s) {
        text.setText(s);
    }

    /**
     * Lie la fenêtre de confirmation
     *
     * @param confirmCancelForm
     */
    public void setConfirm(TconfirmCancelForm confirmCancelForm) {
        c = confirmCancelForm;
    }

    @FXML
    private void fermer(ActionEvent event) {
        c.confirm = crNo;
        ((Node) (event.getSource())).getScene().getWindow().hide();
    }

    @FXML
    private void confirm(ActionEvent event) {
        c.confirm = crYes;
        ((Node) (event.getSource())).getScene().getWindow().hide();
    }

    @FXML
    private void cancel(ActionEvent event) {
        c.confirm = crCancel;
        ((Node) (event.getSource())).getScene().getWindow().hide();
    }

    @Override
    public void refresh() {

    }
}
