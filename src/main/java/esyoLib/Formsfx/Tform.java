/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.Formsfx;

import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.SceneAntialiasing;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;

import java.io.IOException;
import java.net.URL;

/**
 * Création de fenêtres depuis un fichier FXML
 * <p>
 * Conçu de façon 'Fluent interface' c'est à dire return this après les
 * différentes méthodes afin de pouvoir chainer les appels.
 *
 * @author rpetit
 */
public class Tform {

    /**
     * Affiche les messages dans la console
     */
    private final boolean trace = false;
    /**
     * Application à laquelle la fenêtre appartient, OBLIGATOIRE!
     */
    private final Tapplication application;
    /**
     *
     */
    public EventHandler<WindowEvent> closeQueryHandler;
    /**
     *
     */
    public boolean canClose = true;
    /**
     *
     */
    private boolean getConfirm = true;
    /**
     * Contrôleur lié à la fenêtre
     */
    private Tcontroller controller;
    /**
     * Fenêtre javaFX lié à cette fenêtre
     */
    private Stage stage;
    /**
     * Nom de la fenêtre
     */
    private String name;

    /**
     * Crée la fenêtre Le nom du fichier fxml doit comprendre le nom de la
     * classe/le nom du fichier sans l'extension
     *
     * @param aStage
     * @param aFxml
     * @param aApplication
     * @throws java.io.IOException
     * @throws esyoLib.Formsfx.TformException
     * @throws esyoLib.Formsfx.TformFmxlNotFoundException
     * @throws esyoLib.Formsfx.TformFmxlLoadException
     */
    public Tform(Stage aStage, String aFxml, Tapplication aApplication) throws IOException, TformException, TformFmxlNotFoundException, TformFmxlLoadException {
        this.application = aApplication;
        load(aStage, aFxml);
        onCreate();
    }

    /**
     * Crée la fenêtre Le nom permet d'accèder à la fenêtre depuis
     * l'application, il doit être unique Le nom du fichier fxml doit comprendre
     * le nom de la classe/le nom du fichier sans l'extension
     *
     * @param aFxml        Fichier fxml du design de la fenêtre
     * @param aFormName    Nom de la fenêtre, doit être UNIQUE!
     * @param aApplication
     * @throws java.io.IOException
     * @throws esyoLib.Formsfx.TformException
     * @throws esyoLib.Formsfx.TformFmxlNotFoundException
     * @throws esyoLib.Formsfx.TformFmxlLoadException
     */
    public Tform(String aFormName, String aFxml, Tapplication aApplication) throws IOException, TformException, TformFmxlNotFoundException, TformFmxlLoadException {
        this.name = aFormName;
        if (aApplication == null) {
            throw new TformException("L'application ne peut pas être 'null' pour la création de la fenêtre '" + aFormName + "'");
        }
        this.application = aApplication;
        load(new Stage(), aFxml);
        onCreate();
    }

    /**
     * @param aFormName
     * @param aFxml
     * @param aParentForm
     * @throws IOException
     * @throws TformException
     * @throws esyoLib.Formsfx.TformFmxlNotFoundException
     * @throws esyoLib.Formsfx.TformFmxlLoadException
     */
    public Tform(String aFormName, String aFxml, Tform aParentForm) throws IOException, TformException, TformFmxlNotFoundException, TformFmxlLoadException {
        this.name = aFormName;
        setParentForm(aParentForm);
        this.application = aParentForm.getController().getApplication();
        load(new Stage(), aFxml);
        onCreate();
    }

    /**
     * @return
     */
    public static final Tform nullForm() {
        return null;
    }

    /**
     *
     */
    private Tform load(Stage aStage, String aFxml) throws IOException, TformException, TformFmxlNotFoundException, TformFmxlLoadException {
        //détecte l'événement de fermeture de la fenêtre pour la retirer de la liste des fenêtres de l'application
        aStage.setOnHidden(new CloseFormHandler(this));
        //fichier fxml
        if (this.trace) {
            System.out.println("Tform::load>fxml url location for " + aFxml);
        }
        URL location = getClass().getClassLoader().getResource(aFxml);
        if (location == null) {
            throw new TformFmxlNotFoundException("Tform::load\nLe fichier fxml '" + aFxml + "' est introuvable!\nIndiquer son emplacement depuis la racine avec '/' et non '.'.\nPar exemple: esyo/Formsfx/aForm.fxml");
        }
        //loader fxml
        if (this.trace) {
            System.out.println("Tform::load>New fxml loader");
        }
        FXMLLoader fxmlLoader = new FXMLLoader();
        //lié au fichier
        if (this.trace) {
            System.out.println("Tform::load>fxml loader link to location");
        }
        fxmlLoader.setLocation(location);
        fxmlLoader.setBuilderFactory(new JavaFXBuilderFactory());
        //chargement du fichier fxml
        if (this.trace) {
            System.out.println("Tform::load>Load fxml file '" + fxmlLoader.getLocation().getFile() + "'");
        }
        Parent root = null;
        try {
            root = fxmlLoader.<Parent>load(location.openStream());
        } catch (Exception ex) {
            throw new TformFmxlLoadException(ex.getMessage());
        }
        //Nouvelle scene
        if (this.trace) {
            System.out.println("Tform::load>New scene");
        }
        Scene scene = new Scene(root, -1, -1, true, SceneAntialiasing.BALANCED);
        aStage.setScene(scene);
        //css
        String s = aFxml.split("\\/")[0];
        URL url = getClass().getClassLoader().getResource(s + "/gui.css");
        if (url == null) {
            System.out.println("Resource " + s + "/gui.css introuvable.");
        } else {
            String css = url.toExternalForm();
            scene.getStylesheets().add(css);
        }
        //récupère le controleur (pour utilisation depuis autres fenêtres)
        if (this.trace) {
            System.out.println("Tform::load>Get controller");
        }
        //Au centre de l'écran
        aStage.centerOnScreen();
        //Lie le controller à la form
        this.controller = fxmlLoader.getController();
        //Lie la form au controller
        getController().setForm(this);
        //Mémorise le stage
        this.stage = aStage;
        //
        if (this.trace) {
            System.out.println("Tform::load ok");
        }
        //=====================================================================
        //EVENTS===============================================================
        //=====================================================================
        //on focus / onActivate // onDeactivate
        aStage.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (newValue) {
                onActivate();
            } else {
                onDeactivate();
            }
        });
        //onShow
        aStage.addEventFilter(WindowEvent.WINDOW_SHOWN, (WindowEvent event) -> {
            onShow();
        });
        //onHide
        aStage.addEventFilter(WindowEvent.WINDOW_HIDDEN, (WindowEvent event) -> {
            onHide();
        });
        //onClose
        //voir close()
        //onCloseQuery
        this.closeQueryHandler = (WindowEvent event) -> {
            event.consume();
            close();
        };
        aStage.addEventHandler(WindowEvent.WINDOW_CLOSE_REQUEST, closeQueryHandler);

        //onClick
        //onDblClick
        //onCreate
        //voir constructor
        //onDestroy
        //onResize
        //voir setSize();
        //onMinimized
        aStage.iconifiedProperty().addListener((ObservableValue<? extends Boolean> prop, Boolean oldValue, Boolean newValue) -> {
            if (newValue) {
                onMinimized();
            } else {
                onMaximized();
            }
        });
        return this;
    }

    /**
     * Défini le style de la fenêtre
     * <p>
     * DECORATED, Defines a normal {@code Stage} style with a solid white
     * background and platform decorations. UNDECORATED, Defines a {@code Stage}
     * style with a solid white background and no decorations. TRANSPARENT,
     * Defines a {@code Stage} style with a transparent background and no
     * decorations. UTILITY, Defines a {@code Stage} style with a solid white
     * background and minimal platform decorations used for a utility window.
     * UNIFIED, Defines a {@code Stage} style with platform decorations and
     * eliminates the border between client area and decorations. The client
     * area background is unified with the decorations. This is a conditional
     * feature, to check if it is supported see
     * {@link javafx.application.Platform#isSupported(javafx.application.ConditionalFeature)}.
     * If the feature is not supported by the platform, this style downgrades to
     * {@code StageStyle.DECORATED}
     *
     * @param style Style javaFX
     * @return
     */
    public final Tform setStyle(StageStyle style) {
        getStage().initStyle(style);
        return this;
    }

    /**
     * Mise à jour de la fenêtre (passe par le contrôleur)
     *
     * @return
     */
    public final Tform invalidate() {
        try {
            getController().invalidate();
        } catch (Exception ex) {
            getApplication().showError(ex);
        }
        return this;
    }

    /**
     * @param x
     * @param y
     * @return
     */
    public final Tform setPos(double x, double y) {
        getStage().setX(x);
        getStage().setY(y);
        return this;
    }

    /**
     * @param w
     * @param h
     * @return
     */
    public final Tform setSize(double w, double h) {
        getStage().setWidth(w);
        getStage().setHeight(h);
        onResize();
        return this;
    }

    /**
     * @return
     */
    public final double getHeight() {
        return getStage().getHeight();
    }

    /**
     * @return
     */
    public final double getWidth() {
        return getStage().getWidth();
    }

    /**
     * Affiche la fenêtre
     *
     * @return
     */
    public final Tform show() {
        if (!getStage().isShowing()) {
            onShow();
            getStage().show();
        }
        return this;
    }

    /**
     * @return
     */
    public final Tform setFocus() {
        getStage().requestFocus();
        return this;
    }

    /**
     * Ferme la fenêtre Celle ci est retiré de la liste des fenêtres de
     * l'application grâce à son nom
     */
    public final void close() {
        if (this.getConfirm) {
            onCloseQuery();
            if (this.canClose) {
                doClose();
            }
        }
    }

    /**
     *
     */
    private void doClose() {
        this.getConfirm = false;
        getStage().close();
        getApplication().destroyForm(this);
        //else throw new TformException("La fenêtre n'appartient pas à une application, impossible de l'en supprimer");
        onClose();
    }

    /**
     * Affiche la fenêtre et attend (modale)
     *
     * @return
     */
    public final Tform showAndWait() {
        if (!getStage().isShowing()) {
            onShow();
            getStage().showAndWait();
        }
        return this;
    }

    /**
     * Retourne la fenêtre javaFX liée
     *
     * @return La fenêtre (Stage) javaFX liée
     */
    public final Stage getStage() {
        return this.stage;
    }

    /**
     * Retourne le contrôleur lié
     *
     * @return Le contrôleur (Tcontroller) lié
     */
    public final Tcontroller getController() {
        return this.controller;
    }

    /**
     * Passe la fenêtre en 1er plan
     *
     * @return
     */
    public final Tform toFront() {
        getStage().toFront();
        return this;
    }

    /**
     * Retourne le nom de la fenêtre
     *
     * @return Le nom (String) de la fenêtre
     */
    public final String getName() {
        return this.name;
    }

    /**
     * Retourne l'application à laquelle la fenêtre appartient
     *
     * @return L'application (Tapplication) à laquelle la fenêtre appartient
     */
    public final Tapplication getApplication() {
        return this.application;
    }

    /**
     *
     */
    private void onActivate() {
        getController().onActivate();
    }

    /**
     *
     */
    private void onShow() {
        getController().onShow();
    }

    /**
     *
     */
    private void onHide() {
        getController().onHide();
    }

    /**
     *
     */
    private void onClose() {
        getController().onClose();
    }

    /**
     *
     */
    private void onCloseQuery() {
        getController().onCloseQuery();
    }

    /**
     *
     */
    private void onDeactivate() {
        getController().onDeactivate();
    }

    /**
     *
     */
    private void onClick() {
        getController().onClick();
    }

    /**
     *
     */
    private void onDblClick() {
        getController().onDblClick();
    }

    /**
     *
     */
    private void onCreate() {
        getController().onCreate();
    }

    /**
     *
     */
    private void onDestroy() {
        getController().onDestroy();
    }

    /**
     *
     */
    private void onResize() {
        getController().onResize();
    }

    /**
     *
     */
    private void onMinimized() {
        getController().onMinimized();
    }

    /**
     *
     */
    private void onMaximized() {
        getController().onMaximized();
    }

    /**
     *
     */
    public void minimize() {
        getStage().setIconified(true);
    }

    /**
     *
     */
    public void maximize() {
        getStage().setIconified(false);
    }

    /**
     * @param form
     */
    private void setParentForm(Tform form) {
        if (form == null || form == this) {
            return;
        }
        getStage().initOwner(form.getStage());
    }

    /**
     * @return
     */
    public String getTitle() {
        return getStage().getTitle();
    }

    /**
     * @param titre Titre de la fenêtre
     * @return
     */
    public final Tform setTitle(String titre) {
        getStage().setTitle(titre);
        return this;
    }

    /**
     *
     */
    public final void afterCreate() {
        this.getController().afterCreate();
    }


    public void setObj(Object o) {
        controller.setObj(o);
    }

    public Tform refresh() {
        controller.refresh();
        return this;
    }
}
