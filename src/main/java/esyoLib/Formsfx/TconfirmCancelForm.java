/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package esyoLib.Formsfx;

import java.io.IOException;

import static esyoLib.Formsfx.EconfirmResult.crNo;

/**
 * @author rpetit
 */
public class TconfirmCancelForm extends Tform {
    /**
     * Etat de la confirmation
     */
    public EconfirmResult confirm = crNo;

    /**
     * Crée la fenêtre de confirmation avec son texte et la fenêtre parent
     *
     * @param text         Texte à afficher dans la fenêtre de confirmation
     * @param aApplication
     * @throws java.io.IOException
     * @throws esyoLib.Formsfx.TformException
     * @throws esyoLib.Formsfx.TformFmxlNotFoundException
     * @throws esyoLib.Formsfx.TformFmxlLoadException
     */
    public TconfirmCancelForm(String text, Tapplication aApplication) throws IOException, TformException, TformFmxlNotFoundException, TformFmxlLoadException {
        super("Confirmer", "fxmls/confirmCancelForm.fxml", aApplication);
        //Affiche l'erreur dans la console
        System.out.println("Confirmer: " + text);
        //
        setTitle("Confirmer");
        ((CconfirmCancelForm) getController()).setText(text);
        ((CconfirmCancelForm) getController()).setConfirm(this);
        showAndWait();
    }

    /**
     * Retourne l'état de la confirmation
     *
     * @return L'état de la confirmation
     */
    public EconfirmResult isConfirmed() {
        return confirm;
    }
}
