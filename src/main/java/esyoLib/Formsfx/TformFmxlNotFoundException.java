/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.Formsfx;

/**
 * @author romain
 */
public class TformFmxlNotFoundException extends Exception {
    /**
     * @param s
     */
    public TformFmxlNotFoundException(String s) {
        super(s);
    }
}
