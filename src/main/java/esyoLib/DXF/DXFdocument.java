/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.DXF;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.FileImageOutputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;

import static esyoLib.Files.TfileUtils.getExtension;
import static java.awt.image.BufferedImage.TYPE_INT_RGB;

/**
 * @author rpetit
 */
public class DXFdocument {

    public static double INCHPERMM = 0.0393701;
    private final ArrayList<DXFentity> entities;
    protected String ligneCmd = "";
    protected String ligneVal = "";
    private ArrayList<String> lines = new ArrayList();
    private int fileCursor = 0;
    @Deprecated
    private BufferedReader br;
    private double zoom = 1;
    //paramètres dans le fichier
    private double angleBase = 0;
    private int angleDir = 1;
    private int angleUnit = 0;
    private DXFpoint limMin = DXFpoint.zero();
    private DXFpoint limMax = DXFpoint.zero();
    private DXFpoint basePoint = DXFpoint.zero();
    private DXFpoint upperRight = DXFpoint.zero();
    private DXFpoint lowerLeft = DXFpoint.zero();
    private double textHeight = 1;
    private String version = "";
    private double scale = 1;

    /**
     *
     */
    public DXFdocument() {
        entities = new ArrayList();
    }

    /**
     * @param aFileName
     * @throws java.io.IOException
     */
    public DXFdocument(String aFileName) throws IOException {
        entities = new ArrayList();
        //this.loadFromFile(aFileName);
        this.loadFromFile2(aFileName);
    }

    /**
     * @param value
     * @return
     */
    public static final double mmToPxScreen(double value) {
        return INCHPERMM * value * 96.0;// Toolkit.getDefaultToolkit().getScreenResolution();//screen.pixelsPerInch;

    }

    public double getScale() {
        return this.scale;
    }

    public String getVersion() {
        String v = "";
        switch (this.version) {
            case "AC1006":
                v = "R10";
                break;
            case "AC1009":
                v = "R11 or R12";
                break;
            case "AC1012":
                v = "R13";
                break;
            case "AC1014":
                v = "R14";
                break;
            case "AC1015":
                v = "AutoCAD 2000";
                break;
            case "AC1018":
                v = "AutoCAD 2004";
                break;
            case "AC1021":
                v = "AutoCAD 2007";
                break;
            case "AC1024":
                v = "AutoCAD 2010";
                break;
            default:
                v = "?";
        }
        return this.version + "->" + v;
    }

    public double getTextHeight() {
        return textHeight;
    }

    public void setTextHeight(double textHeight) {
        this.textHeight = textHeight;
    }

    /**
     * @return
     */
    public final int getFileCursor() {
        return this.fileCursor;
    }

    /**
     * @param i
     */
    public final void setFileCursor(int i) {
        this.fileCursor = i;
    }

    /**
     * @return
     */
    public final String getCmd() {
        return this.ligneCmd;
    }

    /**
     * @return
     */
    public final String getVal() {
        return this.ligneVal;
    }

    /**
     * @return
     */
    @Deprecated
    public final BufferedReader getBuffer() {
        return this.br;
    }

    /**
     * @return
     */
    public final ArrayList<String> getLines() {
        return this.lines;
    }

    /**
     * @return
     */
    public final double getZoom() {
        return zoom;

    }

    /**
     * @param zoom
     */
    public final void setZoom(double zoom) {
        this.zoom = zoom;
    }

    //
    private ArrayList fileToList2(String aFilename) throws IOException {
        ArrayList<String> l = new ArrayList();
        BufferedReader reader = new BufferedReader(new FileReader(aFilename));
        String line = null;
        while ((line = reader.readLine()) != null) {
            l.add(line.trim());
        }
        reader.close();
        return l;
    }

    /**
     * @param aFileName
     * @throws FileNotFoundException
     * @throws IOException
     */
    public final void loadFromFile2(String aFileName) throws FileNotFoundException, IOException {
        if (!getExtension(aFileName).toUpperCase().equals(".DXF")) {
            throw new IOException("Le fichier n'est pas au format DXF.");
        }
        this.lines = fileToList2(aFileName);
        load();
    }

    /**
     * @param i
     * @return
     */
    public String getLine(int i) {
        return lines.get(i);
    }

    //
    private void load() {
        while (this.read1FromLines()) {
            switch (this.ligneCmd) {
                case "SPLINE":
                    try {
                        this.entities.add(new DXFspline(this));
                    } catch (Exception ex) {
                        System.out.println("SPLINE: " + ex.getClass().getName() + ": " + ex.getMessage());
                    }
                    break;
                case "LINE":
                    try {
                        this.entities.add(new DXFline(this));
                    } catch (Exception ex) {
                        System.out.println("LINE: " + ex.getClass().getName() + ": " + ex.getMessage());
                    }
                    break;
                case "LWPOLYLINE":
                    try {
                        this.entities.add(new DXFlwpolyLine(this));
                    } catch (Exception ex) {
                        System.out.println("LWPOLYLINE: " + ex.getClass().getName() + ": " + ex.getMessage());
                    }
                    break;
                case "POLYLINE":
                    try {
                        this.entities.add(new DXFpolyLine(this));
                    } catch (Exception ex) {
                        System.out.println("POLYLINE: " + ex.getClass().getName() + ": " + ex.getMessage());
                    }
                    break;
                case "ARC":
                    try {
                        this.entities.add(new DXFarc(this));
                    } catch (Exception ex) {
                        System.out.println("ARC: " + ex.getClass().getName() + ": " + ex.getMessage());
                    }
                    break;
                case "MTEXT":
                    try {
                        this.entities.add(new DXFmtext(this));
                    } catch (Exception ex) {
                        System.out.println("MTEXT: " + ex.getClass().getName() + ": " + ex.getMessage());
                    }
                    break;
                case "TEXT":
                    try {
                        this.entities.add(new DXFtext(this));
                    } catch (Exception ex) {
                        System.out.println("TEXT: " + ex.getClass().getName() + ": " + ex.getMessage());
                    }
                    break;
                case "DIMENSION":
                    break;
                case "ELLIPSE":
                    try {
                        this.entities.add(new DXFellipse(this));
                    } catch (Exception ex) {
                        System.out.println("ELLIPSE: " + ex.getClass().getName() + ": " + ex.getMessage());
                    }
                    break;
                case "CIRCLE":
                    try {
                        this.entities.add(new DXFcircle(this));
                    } catch (Exception ex) {
                        System.out.println("CIRCLE: " + ex.getClass().getName() + ": " + ex.getMessage());
                    }
                    break;
                case "$ANGBASE"://Angle direction -> 0 degrees is to the east
                    this.read1FromLines();//code 50
                    this.read1FromLines();
                    this.angleBase = Double.parseDouble(this.ligneCmd);
                    break;
                case "$ANGDIR"://1 = Clockwise angles 0 = Counterclockwise angles
                    this.read1FromLines();//code 70
                    this.read1FromLines();
                    this.angleDir = Integer.parseInt(this.ligneCmd);
                    break;
                case "$AUNITS"://Units format for angles
                    this.read1FromLines();//code 70
                    this.read1FromLines();
                    this.angleUnit = Integer.parseInt(this.ligneCmd);
                    break;
                case "$LIMMAX":
                    this.limMax = this.getPoint2();
                    break;
                case "$DIMTXT":
                    this.read1FromLines();//code 40
                    this.read1FromLines();
                    this.textHeight = Double.parseDouble(this.ligneCmd);
                    break;
                case "$ACADVER":
                    this.read1FromLines();//code
                    this.read1FromLines();
                    this.version = this.ligneCmd;
                    break;
                case "$DIMSCALE":
                    this.read1FromLines();//code
                    this.read1FromLines();
                    this.scale = Double.parseDouble(this.ligneCmd);
                    break;
                case "$LIMMIN":
                    this.limMin = this.getPoint2();
                    break;
                case "$PINSBASE":
                    this.basePoint = this.getPoint2();
                    break;
                case "$PLIMMAX":
                    //this.basePoint = this.getPoint();
                    break;
                case "$PLIMMIN":
                    //this.basePoint = this.getPoint();
                    break;
                case "$EXTMAX"://X, Y, and Z drawing extents upper-right corner (in WCS)
                    this.upperRight = this.getPoint2();
                    break;
                case "$EXTMIN"://X, Y, and Z drawing extents lower-left corner (in WCS)
                    this.lowerLeft = this.getPoint2();
                    break;
            }
        }
    }

    /**
     * @param aFileName
     * @throws FileNotFoundException
     * @throws IOException
     */
    @Deprecated
    public final void loadFromFile(String aFileName) throws FileNotFoundException, IOException {
        if (!getExtension(aFileName).toUpperCase().equals(".DXF")) {
            throw new IOException("Le fichier n'est pas au format DXF.");
        }
        FileReader fr;
        fr = new FileReader(aFileName);
        this.br = new BufferedReader(fr);
        try {
            while (this.read1()) {
                switch (this.ligneCmd) {
                    case "LINE":
                        this.entities.add(new DXFline(this));
                        break;
                    case "LWPOLYLINE":
                        this.entities.add(new DXFlwpolyLine(this));
                        break;
                    case "ARC":
                        this.entities.add(new DXFarc(this));
                        break;
                    case "MTEXT":
                        this.entities.add(new DXFmtext(this));
                        break;
                    case "DIMENSION":
                        break;
                    case "ELLIPSE":
                        this.entities.add(new DXFellipse(this));
                        break;
                    case "CIRCLE":
                        this.entities.add(new DXFcircle(this));
                        break;
                    case "$ANGBASE"://Angle direction -> 0 degrees is to the east
                        this.read1();//code 50
                        this.read1();
                        this.angleBase = Double.parseDouble(this.ligneCmd);
                        break;
                    case "$ANGDIR"://1 = Clockwise angles 0 = Counterclockwise angles
                        this.read1();//code 70
                        this.read1();
                        this.angleDir = Integer.parseInt(this.ligneCmd);
                        break;
                    case "$AUNITS"://Units format for angles
                        this.read1();//code 70
                        this.read1();
                        this.angleUnit = Integer.parseInt(this.ligneCmd);
                        break;
                    case "$DIMSCALE":

                        break;
                    case "$LIMMAX":
                        this.limMax = this.getPoint();
                        break;
                    case "$LIMMIN":
                        this.limMin = this.getPoint();
                        break;
                    case "$PINSBASE":
                        this.basePoint = this.getPoint();
                        break;
                    case "$PLIMMAX":
                        //this.basePoint = this.getPoint();
                        break;
                    case "$PLIMMIN":
                        //this.basePoint = this.getPoint();
                        break;
                    case "$EXTMAX"://X, Y, and Z drawing extents upper-right corner (in WCS)
                        this.upperRight = this.getPoint();
                        break;
                    case "$EXTMIN"://X, Y, and Z drawing extents lower-left corner (in WCS)
                        this.lowerLeft = this.getPoint();
                        break;
                }
            }
        } catch (Exception ex) {

        }
        br.close();
    }

    //
    @Deprecated
    private boolean read1() {
        try {
            this.ligneCmd = this.br.readLine().trim();
        } catch (Exception e) {
            //System.out.println("DXF::read1() error (ou fin)");
            return false;
        }
        //System.out.println(this.ligneCmd);
        return true;
    }

    //
    protected boolean read1FromLines() {
        try {
            this.ligneCmd = this.lines.get(this.fileCursor);
            this.fileCursor++;
        } catch (Exception e) {
            //System.out.println("DXF::read1() error (ou fin)");
            return false;
        }
        //System.out.println(this.ligneCmd);
        return true;
    }

    //
    @Deprecated
    protected final boolean read2() {
        try {
            this.ligneCmd = this.br.readLine().trim();
            if (this.ligneCmd.equals("0") || this.ligneCmd.equals("9")) {
                //System.out.println("<---------------------");
                return false;
            }
            this.ligneVal = this.br.readLine().trim();
        } catch (Exception e) {
            //System.out.println("DXF::read2() error (ou fin)");
            return false;
        }
        //System.out.println(this.ligneCmd+"="+this.ligneVal);
        return true;

    }

    //
    protected final boolean read2FromLines() {
        try {
            this.ligneCmd = this.lines.get(this.fileCursor);
            this.fileCursor++;
            if (this.ligneCmd.equals("0") || this.ligneCmd.equals("9")) {
                //System.out.println("<---------------------");
                return false;
            }
            this.ligneVal = this.lines.get(this.fileCursor);
            this.fileCursor++;
        } catch (Exception e) {
            //System.out.println("DXF::read2() error (ou fin)");
            return false;
        }
        //System.out.println(this.ligneCmd+"="+this.ligneVal);
        return true;
    }

    /**
     *
     */
    public final void print() {
        System.out.println("Version: " + this.getVersion());
        System.out.println("Scale: " + this.scale);
        //1 = Clockwise angles 0 = Counterclockwise angles
        switch (this.angleDir) {
            case 1:
                System.out.println("Horaire");
                break;
            case 0:
                System.out.println("Anti horaire");
                break;
        }
        System.out.println("Taille de la page: " + this.limMax.getX() + "x" + this.limMax.getY());
        System.out.println("Point d'insertion: " + this.basePoint.toString());
        this.entities.stream().forEach((e) -> {
            //if(e.getClass()==DXFtext.class)
            System.out.println(e.toString());
        });
        System.out.println(this.entities.size() + " entités.");
    }

    /**
     * @return
     */
    public final BufferedImage draw() {
        double z = this.zoom * mmToPxScreen(1) * this.scale;
        int h = (int) (this.limMax.getY() * z);
        int w = (int) (this.limMax.getX() * z);
        BufferedImage img = new BufferedImage(w, h, TYPE_INT_RGB);
        Graphics2D g2 = img.createGraphics();

        g2.setColor(Color.WHITE);
        g2.clearRect(0, 0, w, h);
        g2.fillRect(0, 0, w, h);

        this.entities.stream().forEach((e) -> {
            e.draw(g2, z, this.basePoint);
        });
        return img;

    }

    /**
     * @param aFileName
     * @throws java.io.IOException
     */
    public final void saveToJPG(String aFileName) throws IOException {
        BufferedImage img = this.draw();
        File outputfile = new File(aFileName);
        Iterator iter = ImageIO.getImageWritersByFormatName("jpeg");
        ImageWriter writer = (ImageWriter) iter.next();
        ImageWriteParam iwp = writer.getDefaultWriteParam();
        iwp.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
        iwp.setCompressionQuality(1);
        FileImageOutputStream output = new FileImageOutputStream(outputfile);
        writer.setOutput(output);
        IIOImage image = new IIOImage(img, null, null);
        writer.write(null, image, iwp);
        writer.dispose();
    }

    /**
     * @param aFileName
     * @throws java.io.IOException
     */
    public final void saveToPNG(String aFileName) throws IOException {
        BufferedImage img = this.draw();
        File outputfile = new File(aFileName);
        ImageIO.write(img, "png", outputfile);

    }

    //
    @Deprecated
    private DXFpoint getPoint() {
        double x = 0;
        double y = 0;
        double z = 0;
        while (this.read2()) {
            switch (this.ligneCmd) {
                case "10":
                    x = Double.parseDouble(this.ligneVal);
                    break;
                case "20":
                    y = Double.parseDouble(this.ligneVal);
                    break;
                case "30":
                    z = Double.parseDouble(this.ligneVal);
                    break;
            }
        }
        return new DXFpoint(x, y, z);
    }

    //
    private DXFpoint getPoint2() {
        double x = 0;
        double y = 0;
        double z = 0;
        while (this.read2FromLines()) {
            switch (this.ligneCmd) {
                case "10":
                    x = Double.parseDouble(this.ligneVal);
                    break;
                case "20":
                    y = Double.parseDouble(this.ligneVal);
                    break;
                case "30":
                    z = Double.parseDouble(this.ligneVal);
                    break;
            }
        }
        return new DXFpoint(x, y, z);
    }

    /**
     * @return
     */
    public final double getAngleBase() {
        return angleBase;
    }

    public String getLigneVal() {
        return ligneVal;
    }

    /**
     * @return
     */
    public final int getAngleDir() {
        return angleDir;

    }

    /**
     * @return
     */
    public final int getAngleUnit() {
        return angleUnit;

    }

    /**
     * @return
     */
    public final DXFpoint getLimMin() {
        return limMin;
    }

    /**
     * @return
     */
    public final DXFpoint getLimMax() {
        return limMax;
    }

    /**
     * @return
     */
    public final DXFpoint getBasePoint() {
        return basePoint;
    }
}
