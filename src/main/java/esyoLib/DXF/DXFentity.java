/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.DXF;

import java.awt.*;

/**
 * @author rpetit
 */
public abstract class DXFentity {

    protected DXFdocument document = null;
    private boolean visible = true;
    private DXFpoint vector = DXFpoint.defaultVector();
    private String layer = "";
    private DXFcolor color = DXFcolor.get(0);
    private double color24 = 0;
    private double transparency = 1;

    /**
     * @param aDocument
     * @throws java.io.IOException
     */
    public DXFentity(DXFdocument aDocument) throws Exception {
        if (aDocument == null) {
            throw new Exception("Le document DXF est null, une entitée " + this.getClass().getName() + " ne peut être créée!");
        }
        //défini le document parent
        this.setDocument(aDocument);
        //marque la position de départ dans le buffer
        int c = this.getDocument().getFileCursor();
        //this.getDocument().getBuffer().mark(256);
        //lit les lignes du buffer 2 par 2
        while (this.getDocument().read2FromLines()) {
            switch (this.getDocument().getCmd()) {
                //nom du layer
                case "8":
                    setLayer(this.getDocument().getVal());
                    break;
                //couleur
                case "62":
                    setColor(Integer.parseInt(this.getDocument().getVal()));
                    break;
                //couleur 24bits
                case "420":
                    setColor24(Double.parseDouble(this.getDocument().getVal()));
                    break;
                //transparence de 0 à 1
                case "440":
                    setTransparency(Double.parseDouble(this.getDocument().getVal()));
                    break;
                //visible 1 / Non visible 0
                case "60":
                    setVisible(Integer.parseInt(this.getDocument().getVal()));
                    break;
                //X du vector
                case "220":
                    this.getVector().setX(Double.parseDouble(this.getDocument().getVal()));
                    break;
                //Y du vector
                case "221":
                    this.getVector().setY(Double.parseDouble(this.getDocument().getVal()));
                    break;
                //Z du vector
                case "222":
                    this.getVector().setZ(Double.parseDouble(this.getDocument().getVal()));
                    break;
            }
        }
        //retourne à la position de départ dans le buffer
        //this.getDocument().getBuffer().reset();
        this.getDocument().setFileCursor(c);
    }

    /**
     * @return
     */
    public final DXFdocument getDocument() {
        return this.document;
    }

    public final void setDocument(DXFdocument aDocument) {
        this.document = aDocument;
    }

    /**
     * @return
     */
    public final boolean isVisible() {
        return visible;
    }

    /**
     * @param visible
     */
    public final void setVisible(boolean visible) {
        this.visible = visible;
    }

    /**
     * @param i
     */
    public final void setVisible(int i) {
        if (i == 0) {
            this.visible = false;
        }
    }

    /**
     * @return
     */
    public final DXFpoint getVector() {
        return vector;
    }

    /**
     * @param vector
     */
    public final void setVector(DXFpoint vector) {
        this.vector = vector;
    }

    /**
     * @param x
     * @param y
     * @param z
     */
    public final void setVector(double x, double y, double z) {
        this.vector = new DXFpoint(x, y, z);
    }

    /**
     * @return
     */
    public final String getLayer() {
        return layer;
    }

    /**
     * @param layer
     */
    public final void setLayer(String layer) {
        this.layer = layer;
    }

    /**
     * @return
     */
    public final DXFcolor getColor() {
        if (this.color == null) {
            // ? System.out.println("getColor est null -> getColor24="+this.getColor24());
            return DXFcolor.get(0);
        }
        return color;
    }

    /**
     * @param color
     */
    public final void setColor(DXFcolor color) {
        this.color = color;
    }

    /**
     * @param i
     */
    public final void setColor(int i) {
        this.color = DXFcolor.get(i);
    }

    /**
     * @return
     */
    public final double getColor24() {
        return color24;
    }

    /**
     * @param color24
     */
    public void setColor24(double color24) {
        this.color24 = color24;
    }

    /**
     * @return
     */
    public final double getTransparency() {
        return transparency;
    }

    /**
     * @param transparency
     */
    public final void setTransparency(double transparency) {
        this.transparency = transparency;
    }

    /**
     * @param g2
     * @param zoom
     * @param base
     */
    public abstract void draw(Graphics2D g2, double zoom, DXFpoint base);

    @Override
    public String toString() {
        return "====\nVisible: " + visible + " Calque: " + layer + " Couleur: " + color + " Transparence: " + transparency + "\n";
    }

}
