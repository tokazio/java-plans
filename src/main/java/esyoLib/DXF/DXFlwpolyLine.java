/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.DXF;

import java.awt.*;
import java.util.ArrayList;

/**
 * @author RomainPETIT
 */
public class DXFlwpolyLine extends DXFentity {

    protected ArrayList<DXFquart> quarts;

    public DXFlwpolyLine(DXFdocument aDocument) throws Exception {
        super(aDocument);
        loadFromFile();
    }

    @Override
    public void draw(Graphics2D g2, double zoom, DXFpoint base) {
        double y = this.getDocument().getLimMax().getY();
        g2.setColor(this.getColor().asColor());
        this.quarts.stream().forEach((q) -> {
            g2.drawLine((int) (q.getA().getX() * zoom), (int) ((y - q.getA().getY()) * zoom), (int) (q.getB().getX() * zoom), (int) ((y - q.getB().getY()) * zoom));
        });
    }

    /**
     *
     */
    public void loadFromFile() {
        quarts = new ArrayList();
        ArrayList<Double> x = new ArrayList();
        ArrayList<Double> y = new ArrayList();
        ArrayList<Double> z = new ArrayList();
        while (this.getDocument().read2FromLines()) {
            switch (this.getDocument().getCmd()) {
                case "10":
                    x.add(Double.parseDouble(this.getDocument().getVal()));
                    break;
                case "20":
                    y.add(Double.parseDouble(this.getDocument().getVal()));
                    break;
                case "30":
                    z.add(Double.parseDouble(this.getDocument().getVal()));
                    break;
            }
        }
        double x0, y0, z0, x1, y1, z1;
        for (int i = 1; i < x.size(); i++) {
            try {
                x0 = x.get(i - 1);
            } catch (Exception e) {
                x0 = 0;
            }
            try {
                y0 = y.get(i - 1);
            } catch (Exception e) {
                y0 = 0;
            }
            try {
                z0 = z.get(i - 1);
            } catch (Exception e) {
                z0 = 0;
            }
            try {
                x1 = x.get(i);
            } catch (Exception e) {
                x1 = 0;
            }
            try {
                y1 = y.get(i);
            } catch (Exception e) {
                y1 = 0;
            }
            try {
                z1 = z.get(i);
            } catch (Exception e) {
                z1 = 0;
            }
            this.quarts.add(new DXFquart(new DXFpoint(x0, y0, z0), new DXFpoint(x1, y1, z1)));
        }
    }

    @Override
    public String toString() {
        return super.toString() + " POLYLINE ";
    }

}
