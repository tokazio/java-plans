/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.DXF;

/**
 * @author rpetit
 */
public class DXFpoint {

    private double x = 0;
    private double y = 0;
    private double z = 0;

    public DXFpoint(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public static DXFpoint zero() {
        return new DXFpoint(0, 0, 0);
    }

    public static DXFpoint defaultVector() {
        return new DXFpoint(0, 0, 1);
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }

    @Override
    public String toString() {
        return "(" + x + "," + y + "," + z + ")";
    }

    /**
     * @return La longueur d'un vecteur
     */
    public final double length() {
        double v = Math.sqrt((this.x * this.x) + (this.y * this.y) + (this.z * this.z));
        if (this.x < 0) {
            v = -v;
        }
        return v;
    }

    /**
     * @return Angle en radian formé par rapport à l'horizontale
     */
    public final double angleXY() {
        if (this.getX() == 0) {
            if (this.getY() == 0) {
                return 0;
            } else {
                return Math.PI / 2;
            }
        }
        double v = -Math.atan(this.getY() / this.getX());
        if (this.getX() < 0) {
            v = -v;
        }
        return v;
    }

}
