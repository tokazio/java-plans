/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.DXF;

import java.awt.*;

/**
 * @author rpetit
 */
public class DXFarc extends DXFentity {

    private DXFpoint center;
    private double radius = 0;
    private double startAngle = 0;//deg
    private double endAngle = 0;//deg

    public DXFarc(DXFdocument aDocument) throws Exception {
        super(aDocument);
        loadFromFile();
    }

    @Override
    public String toString() {
        return super.toString() + " ARC " + this.center.toString() + " rayon " + this.radius + " de " + this.startAngle + "° à " + this.endAngle + "°";
    }

    @Override
    public void draw(Graphics2D g2, double zoom, DXFpoint base) {
        double y = this.getDocument().getLimMax().getY();
        double sweep = Math.abs((this.endAngle - this.startAngle));
        if (this.endAngle < this.startAngle)
            sweep = (360 + this.endAngle) - this.startAngle;
        //System.out.println("start "+(int)this.startAngle+" longueur "+(int)(this.endAngle-this.startAngle) +" (fin "+(int)this.endAngle+")");
        //clockWise
        //if(this.vector.getZ()>=0) CounterClockwise;
        g2.setColor(this.getColor().asColor());
        g2.drawArc((int) ((this.center.getX() - this.radius) * zoom), (int) ((y - this.center.getY() - this.radius) * zoom), (int) (this.radius * 2 * zoom), (int) (this.radius * 2 * zoom), (int) this.startAngle, (int) sweep);
    }


    public void loadFromFile() {
        this.center = DXFpoint.zero();
        while (this.getDocument().read2FromLines()) {
            switch (this.getDocument().getCmd()) {
                case "10":
                    this.center.setX(Double.parseDouble(this.getDocument().getVal()));
                    break;
                case "20":
                    this.center.setY(Double.parseDouble(this.getDocument().getVal()));
                    break;
                case "30":
                    this.center.setZ(Double.parseDouble(this.getDocument().getVal()));
                    break;
                case "40":
                    this.radius = Double.parseDouble(this.getDocument().getVal());
                    break;
                case "50":
                    this.startAngle = Double.parseDouble(this.getDocument().getVal());
                    break;
                case "51":
                    this.endAngle = Double.parseDouble(this.getDocument().getVal());
                    break;
            }
        }
    }

}
