/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.DXF;

import java.awt.*;

/**
 * @author rpetit
 */
public class DXFcircle extends DXFentity {

    private DXFpoint center;
    private double radius = 0;

    public DXFcircle(DXFdocument aDocument) throws Exception {
        super(aDocument);
        loadFromFile();
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public void draw(Graphics2D g2, double zoom, DXFpoint base) {
        double y = this.getDocument().getLimMax().getY();
        g2.setColor(this.getColor().asColor());
        g2.drawOval((int) ((this.center.getX() - this.radius) * zoom), (int) ((y - this.center.getY() - this.radius) * zoom), (int) (2 * this.radius * zoom), (int) (2 * this.radius * zoom));
    }


    public void loadFromFile() {
        this.center = DXFpoint.zero();
        while (this.getDocument().read2FromLines()) {
            switch (this.getDocument().getCmd()) {
                case "10":
                    this.center.setX(Double.parseDouble(this.getDocument().getVal()));
                    break;
                case "20":
                    this.center.setY(Double.parseDouble(this.getDocument().getVal()));
                    break;
                case "30":
                    this.center.setZ(Double.parseDouble(this.getDocument().getVal()));
                    break;
                case "40":
                    this.radius = Double.parseDouble(this.getDocument().getVal());
                    break;
            }
        }
    }

}
