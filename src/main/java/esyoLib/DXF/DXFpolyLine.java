/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.DXF;

import java.awt.*;
import java.util.ArrayList;

/**
 * @author RomainPETIT
 */
public class DXFpolyLine extends DXFentity {

    protected ArrayList<DXFquart> quarts;

    public DXFpolyLine(DXFdocument aDocument) throws Exception {
        super(aDocument);
        loadFromFile();
    }

    @Override
    public void draw(Graphics2D g2, double zoom, DXFpoint base) {
        double y = this.getDocument().getLimMax().getY();
        g2.setColor(this.getColor().asColor());
        this.quarts.stream().forEach((q) -> {
            g2.drawLine((int) (q.getA().getX() * zoom), (int) ((y - q.getA().getY()) * zoom), (int) (q.getB().getX() * zoom), (int) ((y - q.getB().getY()) * zoom));
        });
    }

    /**
     *
     */
    public void loadFromFile() {
        quarts = new ArrayList();
        ArrayList<Double> x = new ArrayList();
        ArrayList<Double> y = new ArrayList();
        ArrayList<Double> z = new ArrayList();
        loop:
        while (this.getDocument().read1FromLines()) {
            switch (this.getDocument().getCmd()) {
                case "VERTEX":
                    while (this.getDocument().read2FromLines()) {
                        switch (this.getDocument().getCmd()) {
                            case "10":
                                x.add(Double.parseDouble(this.getDocument().getVal()));
                                break;
                            case "20":
                                y.add(Double.parseDouble(this.getDocument().getVal()));
                                break;
                            case "30":
                                z.add(Double.parseDouble(this.getDocument().getVal()));
                                break;
                        }
                    }
                    break;
                case "SEQEND":
                    break loop;
            }
        }
        for (int i = 1; i < x.size(); i++) {
            this.quarts.add(new DXFquart(new DXFpoint(x.get(i - 1), y.get(i - 1), z.get(i - 1)), new DXFpoint(x.get(i), y.get(i), z.get(i))));
        }
    }

    @Override
    public String toString() {
        return super.toString() + " POLYLINE ";
    }

}
