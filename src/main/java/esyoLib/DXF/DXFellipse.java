/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.DXF;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.io.IOException;

/**
 * @author rpetit
 */
public class DXFellipse extends DXFentity {

    private DXFpoint center;
    private DXFpoint major;
    private double ratio = 1;
    private double startAngle = 0;//radian
    private double endAngle = Math.PI * 2;//radian

    /**
     * @param aDocument
     * @throws IOException
     */
    public DXFellipse(DXFdocument aDocument) throws Exception {
        super(aDocument);
        loadFromFile();
    }

    /**
     * @param g2
     * @param zoom
     * @param base
     */
    @Override
    public void draw(Graphics2D g2, double zoom, DXFpoint base) {
        double y = this.getDocument().getLimMax().getY();
        double a = this.major.length();
        double b = a * this.ratio;
        double xc;
        double yc;
        double lxc = 0;
        double lyc = 0;
        g2.setColor(this.getColor().asColor());
        AffineTransform t = g2.getTransform();
        g2.rotate(this.major.angleXY(), this.center.getX() * zoom, (y - this.center.getY()) * zoom);
        double g = this.startAngle;
        while (g < this.endAngle) {
            xc = a * Math.cos(g);
            yc = b * Math.sin(g);
            if (lxc != 0 && lyc != 0) {
                g2.drawLine((int) (((int) this.center.getX() + lxc) * zoom), (int) ((y - ((int) this.center.getY() + lyc)) * zoom), (int) (((int) this.center.getX() + xc) * zoom), (int) ((y - ((int) this.center.getY() + yc)) * zoom));
            }
            lxc = xc;
            lyc = yc;
            g += 0.1;
        }
        g2.setTransform(t);

    }

    /**
     * @return
     */
    @Override
    public String toString() {
        return super.toString() + " ELLIPSE " + this.center.toString() + " major " + this.major.toString() + " " + this.ratio + " de " + Math.toDegrees(this.startAngle) + "° à " + Math.toDegrees(this.endAngle) + "°";
    }

    /**
     *
     */
    public void loadFromFile() {
        this.center = DXFpoint.zero();
        this.major = DXFpoint.zero();
        while (this.getDocument().read2FromLines()) {
            switch (this.getDocument().getCmd()) {
                case "10"://Center point X
                    this.center.setX(Double.parseDouble(this.getDocument().getVal()));
                    break;
                case "20"://Center point Y
                    this.center.setY(Double.parseDouble(this.getDocument().getVal()));
                    break;
                case "30"://Center point Z
                    this.center.setZ(Double.parseDouble(this.getDocument().getVal()));
                    break;
                case "11"://Endpoint of major axis, relative to the center (in WCS) DXF: X value
                    this.major.setX(Double.parseDouble(this.getDocument().getVal()));
                    break;
                case "21"://DXF: Y  values of endpoint of major axis, relative to the center (in WCS)
                    this.major.setY(Double.parseDouble(this.getDocument().getVal()));
                    break;
                case "31"://DXF: Z values of endpoint of major axis, relative to the center (in WCS)
                    this.major.setZ(Double.parseDouble(this.getDocument().getVal()));
                    break;
                case "40"://Ratio of minor axis to major axis
                    this.ratio = Double.parseDouble(this.getDocument().getVal());
                    break;
                case "41"://Start parameter (this value is 0.0 for a full ellipse)
                    this.startAngle = Double.parseDouble(this.getDocument().getVal());
                    break;
                case "42"://End parameter 2pi for a full ellipse
                    this.endAngle = Double.parseDouble(this.getDocument().getVal());
                    break;
            }
        }
    }

}
