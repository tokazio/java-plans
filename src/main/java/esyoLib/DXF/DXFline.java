/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.DXF;

import java.awt.*;

/**
 * @author rpetit
 */
public class DXFline extends DXFentity {

    private DXFpoint start;
    private DXFpoint end;

    public DXFline(DXFdocument aDocument) throws Exception {
        super(aDocument);
        loadFromFile();
    }

    @Override
    public String toString() {
        return super.toString() + " LINE " + start.toString() + "," + end.toString();
    }

    @Override
    public void draw(Graphics2D g2, double zoom, DXFpoint base) {
        double y = this.getDocument().getLimMax().getY();
        g2.setColor(this.getColor().asColor());
        g2.drawLine((int) (this.start.getX() * zoom), (int) ((y - this.start.getY()) * zoom), (int) (this.end.getX() * zoom), (int) ((y - this.end.getY()) * zoom));
    }

    public void loadFromFile() {
        this.start = DXFpoint.zero();
        this.end = DXFpoint.zero();
        while (this.getDocument().read2FromLines()) {
            switch (this.getDocument().getCmd()) {
                case "10":
                    this.start.setX(Double.parseDouble(this.getDocument().getVal()));
                    break;
                case "20":
                    this.start.setY(Double.parseDouble(this.getDocument().getVal()));
                    break;
                case "30":
                    this.start.setZ(Double.parseDouble(this.getDocument().getVal()));
                    break;
                case "11":
                    this.end.setX(Double.parseDouble(this.getDocument().getVal()));
                    break;
                case "21":
                    this.end.setY(Double.parseDouble(this.getDocument().getVal()));
                    break;
                case "31":
                    this.end.setZ(Double.parseDouble(this.getDocument().getVal()));
                    break;
            }
        }
    }

}
