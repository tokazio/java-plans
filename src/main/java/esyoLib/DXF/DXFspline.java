/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.DXF;

import java.awt.*;

/**
 * @author romainpetit
 */
public class DXFspline extends DXFentity {

    private double deg = 0;
    private Spline2D spline;

    public DXFspline(DXFdocument aDocument) throws Exception {
        super(aDocument);
        loadFromFile();
    }

    @Override
    public void draw(Graphics2D g2, double zoom, DXFpoint base) {
        double y = this.getDocument().getLimMax().getY();
        g2.setColor(this.getColor().asColor());

    }

    @Override
    public String toString() {
        return super.toString() + " SPLINE ";
    }

    public void loadFromFile() {

        while (this.getDocument().read2FromLines()) {
            switch (this.getDocument().getCmd()) {
                case "70":
		    /*
		     1 = Closed spline
		     2 = Periodic spline
		     4 = Rational spline
		     8 = Planar
		     16 = Linear (planar bit is also set)
		     */
                    //Integer.parseInt(this.getDocument().getVal()));
                    break;
                case "71":
                    this.deg = Double.parseDouble(this.getDocument().getVal());
                    break;
                case "72"://nbr of knots
                    //this.start.setZ(Double.parseDouble(this.getDocument().getVal()));
                    break;
                case "73"://nbr of ctrl point
                    //this.end.setX(Double.parseDouble(this.getDocument().getVal()));
                    break;
                case "74"://nbr of fit point
                    //this.end.setY(Double.parseDouble(this.getDocument().getVal()));
                    break;
                case "42"://knot tolerance
                    //this.end.setZ(Double.parseDouble(this.getDocument().getVal()));
                    break;
                case "43"://ctrl point tolerance
                    break;
                case "44"://fit tolerance
                    break;
                case "12"://X start tangent
                    break;
                case "22"://Y start tangent
                    break;
                case "32"://Z start tangent
                    break;
                case "13"://X end tangent
                    break;
                case "23"://Y end tangent
                    break;
                case "33"://Z end tangent
                    break;
                case "40"://knot value (1 par knot)
                    break;
                case "41"://Weight (if not 1); with multiple group pairs, they are present if all are not 1
                    break;
                case "10"://X ctrl point (1 par point)
                    break;
                case "20"://Y ctrl point (1 par point)
                    break;
                case "30"://Z ctrl point (1 par point)
                    break;
                case "11"://X fit point (1 par point)
                    break;
                case "21"://Y fit point (1 par point)
                    break;
                case "31"://Z fit point (1 par point)
                    break;
            }
        }


        //spline=new Spline2D();
    }

}
