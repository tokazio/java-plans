/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.DXF;

import java.awt.*;
import java.awt.geom.AffineTransform;

import static java.awt.Font.PLAIN;

/**
 * @author rpetit
 */
public class DXFmtext extends DXFentity {

    private DXFpoint position;
    private DXFpoint angleVector;
    private double taille = 10;
    private int attache = 1;//1 = Top left; 2 = Top center; 3 = Top right 4 = Middle left; 5 = Middle center; 6 = Middle right 7 = Bottom left; 8 = Bottom center; 9 = Bottom right
    private int direction = 1;//1 = Left to right 3 = Top to bottom 5 = By style (the flow direction is inherited from the associated text style)
    private String text = "";
    private double angle = 999;//radians
    private String textStyle = "";

    public DXFmtext(DXFdocument aDocument) throws Exception {
        super(aDocument);
        loadFromFile();
    }

    @Override
    public String toString() {
        return super.toString() + " TEXT '" + this.text + "' @ " + this.position + " Angle: " + this.angle + "rad Attache: " + this.attache;
    }

    @Override
    public void draw(Graphics2D g2, double zoom, DXFpoint base) {
        double y = this.getDocument().getLimMax().getY();

        Font f = new Font("Arial", PLAIN, (int) (this.taille * zoom));
        g2.setFont(f);
        double xp = 0;
        double yp = 0;
        //
        FontMetrics fm = g2.getFontMetrics(f);
        int textHeight = fm.getHeight();
        int textWidth = fm.stringWidth(this.text);

        //1 = Top left; 2 = Top center; 3 = Top right 4 = Middle left; 5 = Middle center; 6 = Middle right 7 = Bottom left; 8 = Bottom center; 9 = Bottom right
        switch (this.attache) {
            case 1://1 = Top left
                xp = this.position.getX();
                yp = this.position.getY() - this.taille;
                break;
            case 2://2 = Top center
                xp = this.position.getX() + (textWidth / 2);
                yp = this.position.getY() - this.taille;
                break;
            case 3://3 = Top right
                xp = this.position.getX() + textWidth;
                yp = this.position.getY() - this.taille;
                break;
            case 4://middle left
                xp = this.position.getX();
                yp = this.position.getY() + (this.taille / 2);
                break;
            case 5://midlle center
                xp = this.position.getX() + (textWidth / 2);
                yp = this.position.getY() + (this.taille / 2);
                break;
            case 6://midlle right
                xp = this.position.getX() + textWidth;
                yp = this.position.getY() + (this.taille / 2);
                break;
            case 7://Bottom left
                xp = this.position.getX();
                yp = this.position.getY();
                break;
            case 8://Bottom center
                xp = this.position.getX() + (textWidth / 2);
                yp = this.position.getY();
                break;
            case 9://Bottom right
                xp = this.position.getX() + textWidth;
                yp = this.position.getY();
                break;
        }

        //g2.setColor(Color.ORANGE);
        //this.angle=Math.PI/4;
        //g2.drawRect((int) (this.position.getX() * zoom), (int) ((y - this.position.getY()) * zoom), textWidth, textHeight);
        g2.setColor(this.getColor().asColor());
        AffineTransform t = g2.getTransform();
        g2.rotate(this.angle, this.position.getX() * zoom, (y - this.position.getY()) * zoom);
        //g2.drawRect((int) (this.position.getX() * zoom), (int) ((y - this.position.getY()) * zoom), textWidth, textHeight);
        g2.drawString(this.text, (int) (this.position.getX() * zoom), (int) ((y - yp) * zoom));
        g2.setTransform(t);
    }

    public void loadFromFile() {
        this.position = DXFpoint.zero();
        this.angleVector = DXFpoint.zero();
        //System.out.println("==");
        while (this.getDocument().read2FromLines()) {
            //System.out.println(this.getDocument().getCmd()+"="+this.getDocument().getVal());
            switch (this.getDocument().getCmd()) {
                case "10":
                    this.position.setX(Double.parseDouble(this.getDocument().getVal()));
                    break;
                case "20":
                    this.position.setY(Double.parseDouble(this.getDocument().getVal()));
                    break;
                case "30":
                    this.position.setZ(Double.parseDouble(this.getDocument().getVal()));
                    break;
                case "40":
                    this.taille = Double.parseDouble(this.getDocument().getVal());
                    break;
                case "71":
                    this.attache = Integer.parseInt(this.getDocument().getVal());
                    break;
                case "72":
                    this.direction = Integer.parseInt(this.getDocument().getVal());
                    break;
                case "1":
                    this.text += this.getDocument().getVal();
                    break;
                case "3":
                    this.text += this.getDocument().getVal();
                    break;
                case "50":
                    this.angle = Double.parseDouble(this.getDocument().getVal());
                    break;
                case "11":
                    this.angleVector.setX(Double.parseDouble(this.getDocument().getVal()));
                    break;
                case "21":
                    this.angleVector.setY(Double.parseDouble(this.getDocument().getVal()));
                    break;
                case "31":
                    this.angleVector.setZ(Double.parseDouble(this.getDocument().getVal()));
                    break;
                case "7":
                    this.textStyle = this.getDocument().getVal();
                    break;
            }
        }
        //symboles
        this.text = this.text.replace("%%c", "\u2300");
        //utilise le vecteur angle si pas d'angle au code 50
        if (this.angle == 999) {
            this.angle = this.angleVector.angleXY();
        }
    }

}
