/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.DXF;

import java.awt.*;
import java.awt.geom.AffineTransform;

import static java.awt.Font.PLAIN;

/**
 * @author rpetit
 */
public class DXFtext extends DXFentity {

    private DXFpoint position;
    private DXFpoint angleVector;
    private double taille = 10;
    private String text = "";
    private double angle = 999;//radians
    private String textStyle = "";

    public DXFtext(DXFdocument aDocument) throws Exception {
        super(aDocument);
        loadFromFile();
    }

    @Override
    public String toString() {
        return super.toString() + " TEXT '" + this.text + "' @ " + this.position + " Angle: " + this.angle + "rad";
    }

    @Override
    public void draw(Graphics2D g2, double zoom, DXFpoint base) {
        double y = this.getDocument().getLimMax().getY();

        Font f = new Font("Arial", PLAIN, (int) (this.taille * zoom));
        g2.setFont(f);
        double xp = 0;
        double yp = 0;
        //
        FontMetrics fm = g2.getFontMetrics(f);
        int textHeight = fm.getHeight();
        int textWidth = fm.stringWidth(this.text);

        xp = this.position.getX();
        yp = this.position.getY();

        //g2.setColor(Color.ORANGE);
        //this.angle=Math.PI/4;
        //g2.drawRect((int) (this.position.getX() * zoom), (int) ((y - this.position.getY()) * zoom), textWidth, textHeight);
        g2.setColor(this.getColor().asColor());
        AffineTransform t = g2.getTransform();
        g2.rotate(this.angle, this.position.getX() * zoom, (y - this.position.getY()) * zoom);
        //g2.drawRect((int) (this.position.getX() * zoom), (int) ((y - this.position.getY()) * zoom), textWidth, textHeight);
        g2.drawString(this.text, (int) (this.position.getX() * zoom), (int) ((y - yp) * zoom));
        g2.setTransform(t);
    }

    public void loadFromFile() {
        this.position = DXFpoint.zero();
        this.angleVector = DXFpoint.zero();
        //System.out.println("==");
        while (this.getDocument().read2FromLines()) {
            //System.out.println(this.getDocument().getCmd()+"="+this.getDocument().getVal());
            switch (this.getDocument().getCmd()) {
                case "10":
                    this.position.setX(Double.parseDouble(this.getDocument().getVal()));
                    break;
                case "20":
                    this.position.setY(Double.parseDouble(this.getDocument().getVal()));
                    break;
                case "30":
                    this.position.setZ(Double.parseDouble(this.getDocument().getVal()));
                    break;
                case "40":
                    this.taille = Double.parseDouble(this.getDocument().getVal());
                    break;
                case "1":
                    this.text = this.getDocument().getVal();
                    break;

                case "50":
                    this.angle = Double.parseDouble(this.getDocument().getVal());
                    break;
                case "11":
                    this.angleVector.setX(Double.parseDouble(this.getDocument().getVal()));
                    break;
                case "21":
                    this.angleVector.setY(Double.parseDouble(this.getDocument().getVal()));
                    break;
                case "31":
                    this.angleVector.setZ(Double.parseDouble(this.getDocument().getVal()));
                    break;
                case "7":
                    this.textStyle = this.getDocument().getVal();
                    break;
            }
        }
        //symboles
        this.text = this.text.replace("%%c", "\u2300");
        //utilise le vecteur angle si pas d'angle au code 50
        if (this.angle == 999) {
            this.angle = this.angleVector.angleXY();
        }
    }

}
