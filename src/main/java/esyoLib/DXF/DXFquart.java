/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.DXF;

/**
 * @author romainpetit
 */
public class DXFquart {
    private DXFpoint a = DXFpoint.zero();
    private DXFpoint b = DXFpoint.zero();

    public DXFquart(DXFpoint a, DXFpoint b) {
        this.a = a;
        this.b = b;
    }

    public DXFpoint getA() {
        return a;
    }

    public void setA(DXFpoint a) {
        this.a = a;
    }

    public DXFpoint getB() {
        return b;
    }

    public void setB(DXFpoint b) {
        this.b = b;
    }

    @Override
    public String toString() {
        return "(" + a.getX() + "," + a.getY() + "," + a.getZ() + ") à (" + b.getX() + "," + b.getY() + "," + b.getZ() + ")";
    }
}
