/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.Graphics;

import esyoLib.Formsfx.Tcontroller;
import javafx.fxml.Initializable;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * FXML Controller class
 *
 * @author rpetit
 */
public class C3dForm extends Tcontroller implements Initializable {

    /**
     *
     */
    private Tvue3D vue3D = null;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public final void initialize(URL url, ResourceBundle rb) {

    }

    /**
     * Redessine
     *
     * @return
     */
    @Override
    public final C3dForm invalidate() {
        if (vue3D != null) {
            try {
                vue3D.draw();
            } catch (Exception ex) {
                Logger.getLogger(C3dForm.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return this;
    }

    @Override
    public void refresh() {

    }

    /**
     * @param v
     * @return
     */
    public C3dForm setVue3D(Tvue3D v) {
        this.vue3D = v;
        return this;
    }

}
