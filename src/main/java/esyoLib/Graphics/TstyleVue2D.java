/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.Graphics;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.effect.Blend;
import javafx.scene.effect.BlendMode;
import javafx.scene.effect.DropShadow;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;

/**
 * Définir le style de la vue (Tvue2D)
 *
 * @author rpetit
 */
public class TstyleVue2D {

    //
    private final Tvue2D vue;
    //
    private Paint fontColor;

    /**
     * Constructeur
     *
     * @param v
     */
    public TstyleVue2D(Tvue2D v) {
        this.vue = v;
    }

    //Retourne le contexte graphique
    private GraphicsContext getGC() {
        return this.vue.getGC();
    }

    /**
     * Style de dessin du profilé
     */
    public final void profile() {
        getGC().setStroke(Color.BLACK);
        getGC().setFill(Color.DARKGRAY);
    }

    /**
     * Style de dessin du verre
     */
    public final void verre() {
        getGC().setStroke(Color.BLACK);
        getGC().setFill(Color.AQUAMARINE);
    }

    /**
     * Style de dessin des axes
     */
    public final void axe() {
        getGC().setStroke(Color.BLUE);
        getGC().setFill(Color.TRANSPARENT);
        setFontColor(Color.BLACK);
    }

    /**
     * Style de dessin du sol
     */
    public final void solfini() {
        getGC().setStroke(Color.ORANGE);
        getGC().setFill(Color.TRANSPARENT);
        setFontColor(Color.ORANGE);
    }

    /**
     * Style de dessin des perçages
     */
    public final void percage() {
        getGC().setStroke(Color.VIOLET);
        setFontColor(Color.VIOLET);
    }

    /**
     * Style de dessin des côtes
     */
    public final void cote() {
        getGC().setStroke(Color.GRAY);
        getGC().setFill(Color.TRANSPARENT);
        setFontColor(Color.GRAY);
    }

    /**
     * Style de dessin normal
     */
    public final void normal() {
        getGC().setStroke(Color.BLACK);
        getGC().setFill(Color.TRANSPARENT);
        setFontColor(Color.BLACK);
    }

    /**
     * Style de police
     */
    public final void fontColor() {
        getGC().setFill(this.fontColor);
    }

    /**
     * Défini la couleur de la police
     *
     * @param p couleur de la police (Paint)
     */
    public final void setFontColor(Paint p) {
        this.fontColor = p;
    }

    /**
     * Active l'effet de bordure blanche
     */
    public final void whiteBorderEffet() {
        Blend blend = new Blend();
        blend.setMode(BlendMode.MULTIPLY);
        DropShadow ds = new DropShadow();
        ds.setOffsetY(-1);
        ds.setOffsetX(-1);
        ds.setColor(Color.color(1, 1, 1));
        ds.setSpread(0.8);
        ds.setRadius(2);
        blend.setBottomInput(ds);
        DropShadow ds2 = new DropShadow();
        ds2.setOffsetY(1);
        ds2.setOffsetX(1);
        ds2.setColor(Color.color(1, 1, 1));
        ds2.setSpread(0.8);
        ds2.setRadius(2);
        blend.setTopInput(ds2);
        getGC().setEffect(blend);
    }

    /**
     * Retire les effets
     */
    public final void noEffect() {
        getGC().setEffect(null);
    }

}
