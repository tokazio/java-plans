package esyoLib.Graphics;


import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TableColumn;
import javafx.util.Callback;

/*
 * Here comes the text of your license
 * Each line should be prefixed with  *
 */

/**
 * @param <S>
 * @param <T>
 * @author RomainPETIT
 */
public class IcoValueFactory<S, T> implements Callback<TableColumn.CellDataFeatures<S, T>, ObservableValue<T>> {

    @Override
    public ObservableValue<T> call(TableColumn.CellDataFeatures<S, T> param) {
        return new ReadOnlyObjectWrapper(param.getValue());
    }

}
