/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package esyoLib.Graphics;

import esyoLib.XML.TxmlObject;
import esyoLib.XML.TxmlTag;

/**
 * Point 2D (x,y)
 *
 * @author rpetit
 */
public class Tpoint2D extends TxmlObject {

    /**
     * valeur x
     */
    public double x;
    /**
     * valeur y
     */
    public double y;

    /**
     * Constructeur
     *
     * @param x
     * @param y
     */
    public Tpoint2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    /**
     * @return nouveau point 2D (0,0)
     */
    public static final Tpoint2D zero() {
        return new Tpoint2D(0.0, 0.0);
    }

    /**
     * @return (x, y)
     */
    @Override
    public String toString() {
        return "(" + x + "," + y + ")";
    }

    /**
     * @param aTag
     * @return
     */
    @Override
    public TxmlTag saveToXML(TxmlTag aTag) {
        aTag.addTag("x").text(x + "");
        aTag.addTag("y").text(y + "");
        return aTag;
    }

    @Override
    public void onLoaded() {

    }
}
