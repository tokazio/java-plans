/*
 * Here comes the text of your license
 * Each line should be prefixed with  *
 */

package esyoLib.Graphics;

import javafx.beans.NamedArg;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Callback;

/**
 * @param <S>
 * @param <T>
 * @author RomainPETIT
 */
public class IcoCellFactory<S, T> implements Callback<TableColumn<S, T>, TableCell<S, T>> {

    private Image img = null;

    /**
     * @param i Une image
     */
    public IcoCellFactory(@NamedArg("img") Image i) {
        this.img = i;
    }

    @Override
    public TableCell<S, T> call(TableColumn<S, T> param) {
        return new TableCell<S, T>() {
            final ImageView buttonGraphic = new ImageView();

            @Override
            public void updateItem(final T param, boolean empty) {
                super.updateItem(param, empty);
                if (param != null) {
                    buttonGraphic.setImage(img);
                    setGraphic(buttonGraphic);
                } else setGraphic(null);
            }
        };
    }

}
