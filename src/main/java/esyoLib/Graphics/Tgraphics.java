/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.Graphics;

import javafx.scene.image.Image;

import javax.xml.bind.DatatypeConverter;
import java.io.ByteArrayInputStream;

/**
 * @author rpetit
 */
public class Tgraphics {

    /**
     * Renvoi un objet de type Image depuis une chaîne (String) base64
     *
     * @param img Chaîne représentant l'image en base64
     * @return Objet Image
     */
    public static Image image(String img) {
        byte[] b = DatatypeConverter.parseBase64Binary(img);
        ByteArrayInputStream s = new ByteArrayInputStream(b);
        return new Image(s);
    }

}
