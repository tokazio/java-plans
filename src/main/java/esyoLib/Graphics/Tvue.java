/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.Graphics;

import esyoLib.Formsfx.Tform;
import esyoLib.Formsfx.TformException;

/**
 * @author rpetit
 */
public abstract class Tvue {

    //Fenêtre lié
    private Tform form;

    /**
     * Constructeur
     *
     * @throws esyoLib.Formsfx.TformException
     */
    public Tvue() throws TformException {

    }

    /**
     * Titre de la vue
     *
     * @return
     */
    public abstract String titleView();

    /**
     * Fonction abstraite appelée à la création de la fenêtre
     *
     * @param f
     * @throws esyoLib.Formsfx.TformException
     */
    public abstract void onCreateForm(Tform f) throws TformException;

    /**
     * Création de la fenêtre
     *
     * @return
     * @throws esyoLib.Formsfx.TformException
     */
    public abstract Tform createForm() throws TformException;

    /**
     * Réduit la fenêtre
     */
    public final void minimize() {
        this.form.minimize();
    }

    /**
     * Agrandis la fenêtre
     */
    public final void maximize() {
        this.form.maximize();
    }

    /**
     * @return la fenêtre (Tform)
     */
    public final Tform getForm() {
        return this.form;
    }

    /**
     * @param aForm
     */
    public void setForm(Tform aForm) {
        this.form = aForm;
    }

    /**
     * Fonction abstraite permettant de dessiner sur le canvas.
     *
     * @throws java.lang.Exception
     */
    public abstract void draw() throws Exception;

    /**
     * Place la fenêtre lié en avant plan
     */
    public final void toFront() {
        if (this.form != null) {
            this.form.toFront();
        }
    }

    /**
     * Ferme la fenêtre lié
     */
    public final void close() {
        this.form.close();
    }

    /**
     * Affiche la fenêtre
     */
    public final void show() {
        this.form.show();
    }
}
