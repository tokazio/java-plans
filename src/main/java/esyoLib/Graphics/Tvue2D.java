/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.Graphics;

import esyoLib.Files.TfileUtils;
import esyoLib.Formsfx.Tform;
import esyoLib.Formsfx.TformException;
import esyoLib.Formsfx.TformFmxlLoadException;
import esyoLib.Formsfx.TformFmxlNotFoundException;
import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.VPos;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;

import static plansevrfx.PlansEVRfx.application;

/**
 * @author rpetit
 */
public abstract class Tvue2D extends Tvue {

    /**
     * inch en mm
     */
    public static double INCHPERMM = 0.0393701;
    /**
     * résolution de l'écran
     */
    public static double RESOLUTION = 96.0;
    /*
    Style de la vue
    */
    protected final TstyleVue2D style = new TstyleVue2D(this);

    /*
    Canvas
    */
    private Canvas canvas;
    //Contexte graphique
    private GraphicsContext gc;
    //Texte du zoom
    private String zoomText = "1/20";
    //Zoom
    private double zoom = 0.05;


    /**
     * Constructeur
     *
     * @throws esyoLib.Formsfx.TformException
     */
    public Tvue2D() throws TformException {
        super();
    }

    /**
     * Constructeur
     *
     * @param aZoomText Texte de zoom (A/B)
     * @throws esyoLib.Formsfx.TformException
     */
    public Tvue2D(String aZoomText) throws TformException {
        super();
        setZoom(aZoomText);
    }

    /**
     * Passe d'une valeur en mm à une valeur d'écran La résolution de l'écran
     * est définie par une constante. Elle devrait être trouvé par Java!
     * <p>
     * Toolkit.getDefaultToolkit().getScreenResolution() ne semble pas retourner
     * la bonne valeur
     *
     * @param value valeur en mm à convertir
     * @return valeur convertie à la résolution d'écran
     */
    public static final double mmToPxScreen(double value) {
        return INCHPERMM * value * RESOLUTION;
    }

    /**
     * Passe d'une valeur d'écran à une valeur en mm La résolution de l'écran
     * est définie par une constante. Elle devrait être trouvé par Java!
     * <p>
     * Toolkit.getDefaultToolkit().getScreenResolution() ne semble pas retourner
     * la bonne valeur
     *
     * @param value à la résolution d'écran
     * @return valeur convertie en mm
     */
    public static final double pxScreenToMm(double value) {
        if (value == 0) {
            return 0;
        }
        return Math.round(value / INCHPERMM / RESOLUTION);
    }

    /**
     * Retourne la fraction sous format texte d'une valeur
     *
     * @param d valeur
     * @return fraction sous format texte
     */
    public static final String frac(double d) {
        double e = 1;
        double f = 0;
        while (e > d) {
            f++;
            e = 1 / f;
        }
        return "1/" + (f + 1);
    }

    /**
     * Calcul la largeur d'un texte
     *
     * @param s texte
     * @return largeur du texte
     */
    public static final double textWidth(String s) {
        final Text text = new Text(s);
        return text.getLayoutBounds().getWidth();
    }

    /**
     * Calcul la hauteur d'un texte
     *
     * @param s texte
     * @return hauteur du texte
     */
    public static final double textHeight(String s) {
        final Text text = new Text(s);
        return text.getLayoutBounds().getHeight();
    }

    /**
     * Enregistre la vue au format PNG
     *
     * @param aFileName Fichier à enregistrer
     * @throws IOException
     * @throws java.lang.NoSuchFieldException
     * @throws java.lang.IllegalAccessException
     */
    public void exportAsPng(String aFileName) throws IOException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        int w = (int) this.canvas.getWidth();
        int h = (int) this.canvas.getHeight();
        WritableImage wim = new WritableImage(w, h);
        this.canvas.snapshot(null, wim);
        String ext = TfileUtils.getExtension(aFileName);
        //String n = (String) getClass().getField("titleView").get(this);
        String n = titleView();
        String a = TfileUtils.removeExtension(aFileName) + "-" + n + ext;
        File file = new File(a);
        ImageIO.write(SwingFXUtils.fromFXImage(wim, null), "png", file);
    }

    /**
     * @return la valeur du zoom
     */
    public final String getZoom() {
        return frac(this.zoom);
    }

    /**
     * Défini le zoom d'après un texte
     *
     * @param s Texte de zoom (A/B)
     * @return Chaînage
     */
    public final Tvue2D setZoom(String s) {
        this.zoomText = s;
        if (this.zoomText.equals("auto")) {
            autoZoom();
        } else {
            this.zoom = calcZoom(s);
        }
        return this;
    }

    /**
     * @return la valeur du zoom
     */
    public final double getZoomVal() {
        return this.zoom;
    }

    //Calcul le zoom d'après un texte A/B
    private double calcZoom(String s) {
        double i, j;
        try {
            String[] a = s.split("\\/");
            i = Double.parseDouble(a[0]);
            j = Double.parseDouble(a[1]);
        } catch (Exception e) {
            System.out.println(s + " n'est pas une valeur de zoom correcte");
            return 0.05;
        }
        return i / j;
    }

    /**
     * @return la largeur du canvas
     */
    public final double getCanvasWidth() {
        return this.canvas.getWidth();
    }

    /**
     * @return la hauteur du canvas
     */
    public final double getCanvasHeight() {
        return this.canvas.getHeight();
    }

    /**
     * @return le canvas (Canvas)
     */
    public final Canvas getCanvas() {
        return this.canvas;
    }

    /**
     * @param aCanvas
     */
    public final void setCanvas(Canvas aCanvas) {
        this.canvas = aCanvas;
    }

    /**
     * @return le contexte graphique
     */
    public final GraphicsContext getGC() {
        return this.gc;
    }

    /**
     * @param aGC
     */
    public final void setGC(GraphicsContext aGC) {
        this.gc = aGC;
    }

    //Efface le canvas
    protected final void clear(Color c) {
        getGC().clearRect(0, 0, getCanvasWidth(), getCanvasHeight());
        getGC().setFill(c);
        getGC().fillRect(0, 0, getCanvasWidth(), getCanvasHeight());
    }

    /**
     * Converti la valeur réelle à l'échelle du dessin (zoom)
     *
     * @param realVal valeur de la taille réelle
     * @return valeur avec le coeff de zoom appliqué
     */
    public final double z(double realVal) {
        return mmToPxScreen(realVal * this.zoom);
    }

    /**
     * Calcul du zoom en automatique
     */
    public void autoZoom() {

    }

    /**
     * Trace une ligne
     *
     * @param x1 x du 1er point
     * @param y1 y du 1er point
     * @param x2 x du 2ème point
     * @param y2 y du 2ème point
     */
    public final void line(double x1, double y1, double x2, double y2) {
        getGC().strokeLine(x1, y1, x2, y2);
    }

    public final void circle(double x1, double y1, double radius) {
        getGC().strokeOval(x1 - radius / 2, y1 - radius / 2, radius, radius);
    }

    /**
     * Trace un rectangle (fond puis contour)
     *
     * @param x1 x du 1er point
     * @param y1 y du 1er point
     * @param x2 x du 2ème point
     * @param y2 y du 2ème point
     */
    public final void rectangle(double x1, double y1, double x2, double y2) {
        getGC().fillRect(x1, y1, Math.abs(x2 - x1), Math.abs(y2 - y1));
        getGC().strokeRect(x1, y1, Math.abs(x2 - x1), Math.abs(y2 - y1));
    }

    /**
     * Trace une côte verticale
     *
     * @param p1  1er point (rpoint)
     * @param p2  2ème point (rpoint)
     * @param val Valeur à afficher
     */
    public final void cotevt(Tpoint2D p1, Tpoint2D p2, double val) {
        if (val == 0) {
            return;
        }
        Tpoint2D p = p2;
        if (p1.y > p2.y) {
            p2 = p1;
            p1 = p;
        }
        String v = new DecimalFormat("0.00").format(val);
        if (v.contains(".0")) {
            v = (int) val + "";
        }
        double h = textHeight(v);
        double w = textWidth(v);
        this.style.cote();
        getGC().strokeLine(p1.x, p1.y, p2.x, p2.y);
        this.style.cote();
        this.style.whiteBorderEffet();
        centerText(v, p1.x, p1.y + (p2.y - p1.y) / 2);
        this.style.noEffect();
        arrowUp(p1);
        arrowDown(p2);
    }

    /**
     * Trace une côte horizontale
     *
     * @param p1  1er point (rpoint)
     * @param p2  2ème point (rpoint)
     * @param val Valeur à afficher
     */
    public final void cotehz(Tpoint2D p1, Tpoint2D p2, double val) {
        if (val == 0) {
            return;
        }
        Tpoint2D p = p2;
        if (p1.x > p2.x) {
            p2 = p1;
            p1 = p;
        }
        String v = new DecimalFormat("0.00").format(val);
        if (v.contains(".0")) {
            v = (int) val + "";
        }
        double h = textHeight(v);
        double l = Math.abs(p2.x - p1.x);
        double w = textWidth(v);
        this.style.cote();
        this.style.whiteBorderEffet();
        centerText(v, p1.x + l / 2, p2.y - h / 2);
        this.style.noEffect();
        arrowLeft(p1);
        getGC().strokeLine(p1.x, p1.y, p2.x, p2.y);
        arrowRight(p2);
    }

    /**
     * Trace une flèche vers la gauche
     *
     * @param p point (rpoint) de la pointe de la flèche
     */
    public final void arrowLeft(Tpoint2D p) {
        getGC().strokeLine(p.x + 5, p.y - 5, p.x, p.y);
        getGC().strokeLine(p.x, p.y, p.x + 5, p.y + 5);
    }

    /**
     * Trace une flèche vers le bas
     *
     * @param p point (rpoint) de la pointe de la flèche
     */
    public final void arrowDown(Tpoint2D p) {
        getGC().strokeLine(p.x - 5, p.y - 5, p.x, p.y);
        getGC().strokeLine(p.x, p.y, p.x + 5, p.y - 5);
    }

    /**
     * Trace une flêche vers le haut
     *
     * @param p point (rpoint) de la pointe de la flèche
     */
    public final void arrowUp(Tpoint2D p) {
        getGC().strokeLine(p.x - 5, p.y + 5, p.x, p.y);
        getGC().strokeLine(p.x, p.y, p.x + 5, p.y + 5);
    }

    /**
     * trace une flêche vers la droite
     *
     * @param p point (rpoint) de la pointe de la flèche
     */
    public final void arrowRight(Tpoint2D p) {
        getGC().strokeLine(p.x - 5, p.y - 5, p.x, p.y);
        getGC().strokeLine(p.x, p.y, p.x - 5, p.y + 5);
    }

    /**
     * Trace un texte par son centre
     *
     * @param t texte
     * @param x x de la position (centre)
     * @param y y de la position (centre)
     */
    public final void centerText(String t, double x, double y) {
        this.gc.setTextAlign(TextAlignment.CENTER);
        Paint p = getGC().getFill();
        this.style.fontColor();
        getGC().fillText(t, x, y);
        getGC().setFill(p);
    }

    /**
     * Trace un texte par sa gauche
     *
     * @param t texte
     * @param x x de la position (gauche)
     * @param y y de la position (gauche)
     */
    public final void leftText(String t, double x, double y) {
        this.gc.setTextAlign(TextAlignment.LEFT);
        Paint p = getGC().getFill();
        this.style.fontColor();
        getGC().fillText(t, x, y);
        getGC().setFill(p);
    }

    /**
     * Création de la fenêtre
     *
     * @return
     * @throws esyoLib.Formsfx.TformException
     */
    @Override
    public Tform createForm() throws TformException {

        //crée la fenêtre de dessin
        Tform f = null;
        try {
            f = new Tform("", "fxmls/2dForm.fxml", application);// Tapplication.nullApplication());
        } catch (IOException | TformException | TformFmxlNotFoundException | TformFmxlLoadException ex) {
            throw new TformException(ex.getClass().getName() + ": " + ex.getMessage());
        }
        setForm(f);
        getForm().getController().afterCreate();
        //
        getForm().setSize(mmToPxScreen(297), mmToPxScreen(210));
        setCanvas(((C2dForm) getForm().getController()).getCanvas());
        setGC(getCanvas().getGraphicsContext2D());
        //centrer le texte verticalement
        getGC().setTextBaseline(VPos.CENTER);
        //
        onCreateForm(f);
        //
        return f;

    }

    /**
     * @param e
     */
    public abstract void onDragged(MouseEvent e);

    /**
     * @param e
     */
    public abstract void onClick(MouseEvent e);

    /**
     * @param e
     */
    public abstract void onDblClick(MouseEvent e);

    /**
     * @param e
     */
    public abstract void onPressed(MouseEvent e);

    /**
     * @param e
     */
    public abstract void onReleased(MouseEvent e);

    /**
     * @param e
     */
    public abstract void onMove(MouseEvent e);
}
