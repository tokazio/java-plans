/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.Graphics;

import esyoLib.Formsfx.Tcontroller;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * FXML Controller class
 *
 * @author rpetit
 */
public class C2dForm extends Tcontroller implements Initializable {

    private Tvue2D vue2D = null;

    @FXML
    private Canvas canvas;
    @FXML
    private TextField zoom;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public final void initialize(URL url, ResourceBundle rb) {
        zoom.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            try {
                changeZoom(newValue);
            } catch (Exception ex) {
                Logger.getLogger(C2dForm.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

        canvas.addEventHandler(MouseEvent.MOUSE_DRAGGED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                if (vue2D != null) {
                    vue2D.onDragged(e);
                }
            }
        });

        canvas.addEventHandler(MouseEvent.MOUSE_MOVED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                if (vue2D != null) {
                    vue2D.onMove(e);
                }
            }
        });

        canvas.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                if (e.getClickCount() >= 1) {
                    if (vue2D != null) {
                        vue2D.onClick(e);
                    }
                }
                if (e.getClickCount() >= 2) {
                    if (vue2D != null) {
                        vue2D.onDblClick(e);
                    }
                }
            }
        });

        canvas.addEventHandler(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                if (vue2D != null) {
                    vue2D.onPressed(e);
                }
            }
        });

        canvas.addEventHandler(MouseEvent.MOUSE_RELEASED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                if (vue2D != null) {
                    vue2D.onReleased(e);
                }
            }
        });
    }

    //Change le zoom de la vue
    private void changeZoom(String s) throws Exception {
        vue2D.setZoom(s);
        invalidate();
    }

    /**
     * @return le canvas (Canvas)
     */
    public final Canvas getCanvas() {
        return canvas;
    }

    // Change la largeur
    private void resizeW(Number w) throws Exception {
        if (canvas != null) {
            canvas.setWidth(w.doubleValue());
            invalidate();
        }
    }

    // Change la hauteur
    private void resizeH(Number h) throws Exception {
        if (canvas != null) {
            canvas.setHeight(h.doubleValue());
            invalidate();
        }
    }

    /**
     * Redessine
     *
     * @return
     */
    @Override
    public final C2dForm invalidate() {
        if (vue2D != null && canvas != null) {
            try {
                vue2D.draw();
            } catch (Exception ex) {
                Logger.getLogger(C2dForm.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return this;
    }

    @Override
    public void refresh() {

    }

    /**
     * Défini les fonctions de redimenssionnement
     */
    @Override
    public final void afterCreate() {
        Scene scene = getForm().getStage().getScene();
        //on resize------------------
        scene.widthProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            try {
                resizeW(newValue);
            } catch (Exception ex) {
                Logger.getLogger(C2dForm.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        scene.heightProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            try {
                resizeH(newValue);
            } catch (Exception ex) {
                Logger.getLogger(C2dForm.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        //----------------------------
    }

    /**
     * @param v
     * @return
     */
    public C2dForm setVue2D(Tvue2D v) {
        this.vue2D = v;
        return this;
    }

}
