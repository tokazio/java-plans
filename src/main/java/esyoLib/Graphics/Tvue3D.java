/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.Graphics;

import esyoLib.Formsfx.Tform;
import esyoLib.Formsfx.TformException;
import esyoLib.Formsfx.TformFmxlLoadException;
import esyoLib.Formsfx.TformFmxlNotFoundException;
import esyoLib.jfx3d.T3dWorld;

import java.io.IOException;

import static plansevrfx.PlansEVRfx.application;

/**
 * @author rpetit
 */
public abstract class Tvue3D extends Tvue {

    //
    private T3dWorld w;

    /**
     * @throws esyoLib.Formsfx.TformException
     */
    public Tvue3D() throws TformException {
        super();
    }

    /**
     * @return
     */
    public T3dWorld getWorld() {
        return this.w;
    }

    /**
     * @param a3Dworld
     */
    public void setWorld(T3dWorld a3Dworld) {
        this.w = a3Dworld;
    }

    /**
     * Création de la fenêtre
     *
     * @return
     * @throws esyoLib.Formsfx.TformException
     */
    @Override
    public Tform createForm() throws TformException {

        //crée la fenêtre de 3D
        Tform f = null;
        try {
            f = new Tform("", "fxmls/3dForm.fxml", application);
        } catch (IOException | TformException | TformFmxlNotFoundException | TformFmxlLoadException ex) {
            throw new TformException(ex.getClass().getName() + ": " + ex.getMessage());
        }
        setForm(f);
        getForm().getController().afterCreate();
        //
        onCreateForm(f);
        //
        return f;

    }

}
