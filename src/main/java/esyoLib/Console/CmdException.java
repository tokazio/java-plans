/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.Console;

/**
 * Exception du package esyoLib.Console
 *
 * @author RomainPETIT
 */
public class CmdException extends Exception {

    /**
     * Lève l'exception
     *
     * @param s Texte du message de l'exception
     */
    public CmdException(String s) {
        super(s);
    }

}
