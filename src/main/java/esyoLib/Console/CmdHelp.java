/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.Console;

import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author RomainPETIT
 */
public class CmdHelp extends Cmd {

    /**
     *
     */
    //public static String defs[] = {"mp3"+Cmd.ANSI_YELLOW+" >Pour avoir plus d'infos sur 'mp3'", "dir"+Cmd.ANSI_YELLOW+" >Pour avoir plus d'infos sur 'dir'", "info"+Cmd.ANSI_YELLOW+" >Pour avoir plus d'infos sur 'info'", "db"+Cmd.ANSI_YELLOW+" >Pour avoir plus d'infos sur 'db'", "bass"+Cmd.ANSI_YELLOW+" >Pour avoir plus d'infos sur 'bass'"};

    /**
     *
     */
    public CmdHelp() throws CmdParamException {
        super("help");
    }

    /**
     * @return
     */
    public static String[] defs() {
        String[] r = new String[CmdApp.cmds.size()];
        int i = 0;
        for (Entry<String, Class<? extends Cmd>> e : CmdApp.cmds.entrySet()) {
            if (e.getValue() != CmdHelp.class) {
                r[i] = Cmd.ANSI_PURPLE + "help " + e.getKey() + Cmd.ANSI_YELLOW + " >Pour avoir plus d'infos sur '" + e.getKey() + "'" + Cmd.ANSI_RESET;
                i++;
            }
        }
        return r;
    }

    /**
     * @throws CmdException
     */
    @Override
    public void dispatch(String[] args) throws CmdException {
        String s = "";
        try {
            s = super.getCmd(args);
        } catch (CmdException ex) {
            print(defs());
            return;
        }

        Class<? extends Cmd> c = CmdApp.getCommand(s);
        try {
            print(c.newInstance().getDefs());
        } catch (InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(CmdHelp.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * @return
     */
    @Override
    public String[] getDefs() {
        return null;
    }

}
