/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.Console;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Gestion d'une application en ligne de commande
 *
 * @author RomainPETIT
 */
public abstract class CmdApp {

    /**
     * Liste des commandes associé à leurs classes
     */
    protected static final HashMap<String, Class<? extends Cmd>> cmds = new HashMap();
    /**
     * Historique des commandes
     */
    private static final ArrayList<String> histo = new ArrayList();
    /**
     * Nom de l'application
     */
    private static String name = "Application's name";

    /**
     * @param name Nom de l'application
     * @param args Argument de l'application
     */
    public CmdApp(String name, String[] args) {
        CmdApp.name = name;
        addCommand("help", CmdHelp.class);
    }

    /**
     * @param s
     * @return
     */
    public static final Class<? extends Cmd> getCommand(String s) {
        return cmds.get(s);
    }

    /**
     * Si des commandes sont passé en arguments, je les executes de suite et je
     * quitte Sinon je démarre l'invite de commande
     *
     * @param args Argument de l'application
     */
    public final void start(String[] args) {
        if (args.length > 0) {
            tryExecute(args);
        } else {
            go();
        }
    }

    /**
     * Boucle d'invite de commande
     *
     * @throws IOException Erreur de lecture de la commande
     */
    private void go() {
        String s = "";
        while (!s.equals("quit")) {
            try {
                s = input();
                tryExecute(s);
            } catch (IOException ex) {
                go();
            }
        }
    }

    /**
     * Invite de commande
     *
     * @return @throws IOException Erreur de lecture de la commande
     */
    private String input() throws IOException {
        System.out.print(Cmd.ANSI_BLUE + name + ">" + Cmd.ANSI_RESET);
        String s = new Scanner(System.in).nextLine();
        return s;
    }

    /**
     * Essai d'éxécuter la commande texte (Message d'erreur en cas d'échec)
     *
     * @param s Commande complète à éxécuter
     */
    private void tryExecute(String s) {
        try {
            execute(s);
        } catch (Exception ex) {
            System.out.println(Cmd.ANSI_BLUE + name + ">" + Cmd.ANSI_RED + ex.getMessage() + "(" + ex.getClass().getName() + ")" + Cmd.ANSI_RESET);
        }
    }

    /**
     * Transforme la commande texte en commande + arguments et l'éxécute
     *
     * @param s Commande complète à éxécuter
     */
    public final void execute(String s) throws Exception {
        //ajoute la commande à l'historique
        histo.add(s);
        //découpe de la commande en:
        //cmd [args]
        List<String> matchList = new ArrayList<String>();
        Pattern regex = Pattern.compile("[^\\s\"']+|\"[^\"]*\"|'[^']*");
        Matcher regexMatcher = regex.matcher(s);
        while (regexMatcher.find()) {
            matchList.add(regexMatcher.group());
        }
        String[] args = new String[matchList.size()];
        for (int i = 0; i < matchList.size(); i++) {
            String m = matchList.get(i);
            if (m.startsWith("\"")) {
                m = m.substring(1);
            }
            if (m.endsWith("\"")) {
                m = m.substring(0, m.length() - 1);
            }
            args[i] = m;
        }
        //vers le dispatch
        execute(args);
    }

    /**
     * Essai d'éxécuter la commande selon les arguments données (Message
     * d'erreur en cas d'échec)
     *
     * @param args Argument de l'application
     */
    private void tryExecute(String[] args) {
        try {
            execute(args);
        } catch (Exception ex) {
            System.out.println(Cmd.ANSI_BLUE + name + ">" + Cmd.ANSI_RED + ex.getMessage() + "(" + ex.getClass().getName() + ")" + Cmd.ANSI_RESET);
        }
    }

    /**
     * Exécute la commande selon les arguments données
     *
     * @param args Argument de l'application
     * @throws Exception Erreur lors de l'éxécution de la commande
     */
    public final void execute(String[] args) throws Exception {
        /*
         for (int i = 0; i < args.length; i++) {
         System.out.println(i + ": " + args[i]);
         }
         */
        if (cmds.containsKey(args[0])) {//cmd!=null) {
            Class myClass = cmds.get(args[0]);//Class.forName("CmdHelp");
            Class[] types = {};
            Constructor constructor = myClass.getConstructor(types);
            Object[] objargs = new Object[0];
            //objargs[0] = args;
            Object instanceOfMyClass = constructor.newInstance(objargs);
            ((Cmd) instanceOfMyClass).dispatch(args);
        } else {
            switch (args[0]) {
                case "quit":
                    System.out.println(Cmd.ANSI_BLUE + "Jukebox>Buy buy!" + Cmd.ANSI_RESET);
                    onExit(args);
                    System.exit(0);
                    return;
                default:
                    System.out.println(Cmd.ANSI_BLUE + "Jukebox>" + Cmd.ANSI_RED + "Commande '" + args[0] + "' non reconnue!" + Cmd.ANSI_RESET);
                    onHelp(args);
            }
        }
    }

    /**
     * Appelé lors d'une erreur dans la commande Affiche la liste des arguments
     * de cette commande
     * <p>
     * Cette liste est définie dans la classe de la commande par la constante
     * 'public static final String[] defs = {}'
     *
     * @param args Argument de l'application
     */
    public abstract void onHelp(String[] args);

    /**
     * Appelé lors de la sortie de l'application
     *
     * @param args Argument de l'application
     */
    public abstract void onExit(String[] args);

    /**
     * Ajoute la commande à l'application
     *
     * @param s      Nom de la commande
     * @param aClass Classe de la commande
     */
    public final void addCommand(String s, Class<? extends Cmd> aClass) {
        cmds.put(s, aClass);
    }

}
