/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.Console;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;

import static java.nio.file.LinkOption.NOFOLLOW_LINKS;

/**
 * Fonctions de système de fichier utiles au package esyoLib.Console
 *
 * @author RomainPETIT
 */
public class CmdFileUtils {

    /**
     * Traduit le ~/ en dossier (remplace par le user.dir du système)
     *
     * @param s Chemin du dossier à traduire
     * @return Chemin du dossier traduit
     */
    public static final String fullDir(String s) {
        if (s.startsWith("~/")) {
            s = System.getProperty("user.dir") + "/" + s.substring(2);
        }
        return s;
    }

    /**
     * Crée le dossier
     * Exception si échec (le dossier existe déjà ou erreur lors de la création)
     *
     * @param dir File représentant le dossier
     * @throws java.io.IOException Erreur lors de la création du dossier
     */
    public static final void createDir(File dir) throws IOException {
        System.out.println(Cmd.ANSI_CYAN + "Création du dossier '" + dir.getAbsolutePath() + "'..." + Cmd.ANSI_RESET);
        if (dir.exists()) {
            throw new IOException("Le dossier '" + dir.getAbsolutePath() + "' existe déjà!");
        }
        if (!dir.mkdir()) {
            throw new IOException("Impossible de créer le dossier '" + dir.getAbsolutePath() + "'");
        }
    }

    /**
     * Liste les sous-dossiers et fichiers du dossier
     * Filtre les fichiers par l'extention donnée
     *
     * @param folder File représentant le dossier
     * @param aExt   Extention de fichier à filtrer
     */
    public static final void lister(File folder, String aExt) {
        FilenameFilter textFilter = (File dir, String name) -> {
            String lowercaseName = name.toLowerCase();
            return lowercaseName.endsWith(aExt) || dir.isDirectory();
        };
        System.out.println(Cmd.ANSI_CYAN + "Liste des dossiers/fichiers '" + aExt + "' dans " + folder.getAbsolutePath() + "..." + Cmd.ANSI_RESET);
        File[] children = folder.listFiles(textFilter);
        if (children != null) {
            for (File child : children) {
                System.out.println(child.getName());
            }
        }
    }

    /**
     * Copie les fichiers d'un dossier à l'autre
     * Filtre les fichiers par leurs extention
     *
     * @param aSourceFolder File représentant le dossier source
     * @param aDestPath     File représentant le dossier destination
     * @param aExt          Extention des fichiers à filtrer
     * @throws java.io.IOException          Erreur avec les dossiers (n'existent pas ou protégés)
     * @throws esyoLib.Console.CmdException Pas de fichier avec l'extention dans le dossier source
     */
    public static final void copyFiles(File aSourceFolder, File aDestPath, String aExt) throws IOException, CmdException {
        FilenameFilter textFilter = (File dir, String name) -> {
            String lowercaseName = name.toLowerCase();
            return lowercaseName.endsWith(aExt);
        };
        double size = 0;
        double pos = 0;
        if (!aSourceFolder.exists()) {
            throw new IOException("Le dossier source '" + aSourceFolder.getAbsolutePath() + "' n'existe pas.");
        }
        if (!aDestPath.exists()) {
            throw new IOException("Le dossier destination '" + aDestPath.getAbsolutePath() + "' n'existe pas.");
        }
        if (!aSourceFolder.canRead()) {
            throw new IOException("Le dossier source '" + aSourceFolder.getAbsolutePath() + "' ne peut pas être lu.");
        }
        if (!aDestPath.canWrite()) {
            throw new IOException("Le dossier destination '" + aDestPath.getAbsolutePath() + "' ne peut pas être modifié.");
        }
        File[] children = aSourceFolder.listFiles(textFilter);
        if (children != null) {
            if (children.length == 0) {
                throw new CmdException("Le dossier source ne contient aucun '" + aExt + "'.");
            }
            size = children.length;
            pos = 0;
            System.out.println(Cmd.ANSI_CYAN + "Copie de " + size + " fichier(s) de '" + aSourceFolder.getAbsolutePath() + "' vers '" + aDestPath.getAbsolutePath() + "'..." + Cmd.ANSI_RESET);
            for (File child : children) {
                pos++;
                try {
                    Files.copy(child.toPath(), new File(aDestPath.getAbsolutePath() + "/" + child.getName()).toPath(), NOFOLLOW_LINKS);
                } catch (FileAlreadyExistsException ex) {
                    System.out.println(Cmd.ANSI_RED + "Non copié, le fichier existe déjà. (" + ex.getMessage() + ")" + Cmd.ANSI_RESET);
                } catch (IOException ex) {
                    System.out.println(Cmd.ANSI_RED + ex.getClass().getName() + ": " + ex.getMessage() + Cmd.ANSI_RESET);
                }
                System.out.println(Cmd.ANSI_CYAN + ((pos / size) * 100) + "% | " + pos + "/" + size + " copie de " + child.getName() + "..." + Cmd.ANSI_RESET);
            }
        }
    }

    /**
     * Vide et supprimer le dossier
     *
     * @param folder File représentant le dossier à vider/supprimer
     * @throws java.io.IOException Erreur lors du vidage ou de la suppression du dossier
     */
    public static final void deleteDir(final File folder) throws IOException {
        System.out.println(Cmd.ANSI_CYAN + "Efface le dossier '" + folder.getAbsolutePath() + "' et ses sous-dossiers/fichiers..." + Cmd.ANSI_RESET);
        // check if folder file is a real folder
        if (folder.isDirectory()) {
            File[] list = folder.listFiles();
            if (list != null) {
                for (File tmpF : list) {
                    if (tmpF.isDirectory()) {
                        deleteDir(tmpF);
                    }
                    tmpF.delete();
                }
            }
            if (!folder.delete()) {
                throw new IOException("Impossible d'effacer le dossier: '" + folder.getAbsolutePath() + "'");
            }
        }
    }

}
