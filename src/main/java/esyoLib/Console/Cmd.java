/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.Console;

import java.io.File;
import java.io.FileNotFoundException;

/**
 * Modèle de commande pour une application en ligne de commande
 *
 * @author RomainPETIT
 */
public abstract class Cmd {

    /**
     * Codes couleurs pour la console
     */
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";
    /**
     * Nom de la commande
     */
    private String name = "Command name";

    /**
     * @param name Nom de la commande
     */
    public Cmd(String name) throws CmdParamException {
        this.name = name;
    }

    /**
     * Récupère le paramètre n°[id] Si celui ci n'existe pas (non précisé,
     * manquant), affiche un message d'erreur
     *
     * @param args Arguments de l'application
     * @param id   Position du paramètre (à partir de 0)
     * @param s    Message en cas d'erreur de paramètre (description du paramètre
     *             manquant)
     * @return L'argument n°[id]
     * @throws CmdParamException Erreur paramètre manquant
     */
    public static final String getParam(String[] args, int id, String s) throws CmdParamException {
        /*
        for (int i = 0; i < args.length; i++) {
            System.out.println(i + "/" + args.length + ": " + args[i]);
        }
        */
        if (args.length < id + 1 || args[id].isEmpty()) {
            throw new CmdParamException("Indiquer un paramètre [" + id + "] : " + s);
        }
        return args[id];
    }

    /**
     * Retourne un File depuis son nom (String) Si le dossier ou le fichier
     * n'existe pas ou n'est pas lisible (protégè), lève une exception
     *
     * @param s Nom du dossier/fichier
     * @return File du dossier/fichier
     * @throws FileNotFoundException Le dossier/fichier est introuvable ou
     *                               protégé
     */
    public static final File getFile(String s) throws FileNotFoundException {
        File f = new File(CmdFileUtils.fullDir(s));
        if (!f.canRead()) {
            String t;
            if (f.isDirectory()) {
                t = "dossier";
            } else {
                t = "fichier";
            }
            throw new FileNotFoundException("Impossible de trouver/lire le " + t + " '" + f.getAbsolutePath() + "'");
        }
        return f;
    }

    /**
     * Affiche la liste de String passé en paramètre (Couleur violette) Utilisé
     * pour afficher la définition des commandes (defs)
     *
     * @param s Liste à afficher
     */
    public static final void print(String[] s) {
        for (String d : s) {
            if (d != null) {
                System.out.println("Utilisez: " + Cmd.ANSI_PURPLE + d + Cmd.ANSI_RESET);
            }
        }
    }

    /**
     * Affiche la liste de String passé en paramètre en ajoutant d au début de chaque lignes (Couleur violette) Utilisé
     * pour afficher la définition des commandes (defs)
     *
     * @param s Liste à afficher
     */
    public static final void print(String[] t, String d) {

    }

    /**
     * Affiche le message terminé (Couleur verte)
     */
    public static final void termine() {
        System.out.println(Cmd.ANSI_GREEN + "Terminé." + Cmd.ANSI_RESET);
    }

    /**
     * @param args
     */
    public abstract void dispatch(String[] args) throws Exception;

    /**
     * @param args Arguments de l'application
     * @return Paramètre n°[1]
     */
    protected String getCmd(String[] args) throws CmdParamException {
        return getParam(args, 1, "Indiquer une sous commande pour '" + name + "'.");
    }

    /**
     * @return
     */
    public abstract String[] getDefs();

}
