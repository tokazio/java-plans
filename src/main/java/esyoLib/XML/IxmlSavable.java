/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.XML;

import org.w3c.dom.Node;

/**
 * @author rpetit
 */
public interface IxmlSavable {

    /**
     * @param aTag
     * @return
     */
    TxmlTag saveToXML(TxmlTag aTag);

    /**
     * @param noeud
     * @return
     * @throws java.lang.Exception
     */
    Object loadFromXML(Node noeud) throws Exception;

}
