/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.XML;

import esyoLib.Utils.Tobject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Objet pouvant être enregistré dans un fichier XML
 * <p>
 * saveToFile(String aFileName) / loadFromFile(String aFileName)
 *
 * @author rpetit
 */
public abstract class TxmlObject extends Tobject implements IxmlSavable {

    /**
     * Constructeur
     */
    public TxmlObject() {
        super();
    }

    //
    private void saveFields(Class aClass, TxmlTag t) throws IllegalArgumentException, IllegalAccessException {
        String v;
        Field[] fs = aClass.getDeclaredFields();
        for (Field f : fs) {
            try {
                //si transient, je n'enregistre pas
                if (Modifier.isTransient(f.getModifiers())) {
                    continue;
                }
                f.setAccessible(true);
                switch (f.getType().getName()) {
                    case "int":
                        v = "" + f.getInt(this);
                        break;
                    case "double":
                        v = "" + f.getDouble(this);
                        break;
                    case "boolean":
                        v = "" + f.getBoolean(this);
                        break;
                    case "java.lang.String":
                        v = "" + f.get(this);
                        break;
                    default:
                        TxmlTag t2 = t.addTag(f.getName());
                        Class[] types = {TxmlTag.class};
                        Object[] params = {t2};
                        try {
                            Method m = f.getType().getMethod("saveToXML", types);
                            Object o = f.get(this);
                            if (o != null) {
                                m.invoke(o, params);
                            } else {
                                System.out.println("(i) TxmlObject::saveFields ->" + f.getName() + " est null!");
                            }
                        } catch (NoSuchMethodException | SecurityException | IllegalArgumentException | IllegalAccessException ex) {
                            System.out.println("/!\\ " + f.getName() + ": Error (pas de fonction saveToXML() " + ex.getClass().getName() + "\n" + ex.getMessage());
                            //v = fs[i].get(this).toString();
                            //v = "?" + f.getType().getName() + "?";
                            //t.addTag(fs[i].getName()).text(v);
                        } catch (InvocationTargetException ex) {
                            System.out.println("/!\\ " + f.getName() + ": Error InvocationTargetException " + ex.getTargetException().getClass().getName() + "\n" + ex.getMessage());
                        }
                        continue;
                        //System.out.println("Save object " + aClass.getName() + "::" + fs[i].getName() + "(" + fs[i].getType().getName() + ") ->toString()");
                        //System.out.println(fs[i].getName() + " = " + v);
                }
                //System.out.println(fs[i].getName() + " = " + v);
                t.addTag(f.getName()).text(v);
            } catch (SecurityException | IllegalArgumentException | IllegalAccessException ex) {
                //System.out.println("Save object " + ex.getMessage());
            }
        }
        if (aClass.getSuperclass() != null) {
            saveFields(aClass.getSuperclass(), t);
        }
    }

    /**
     * Enregistre tout les champs (DeclaredFields) hors transient et leurs
     * valeur dans le fichier XML 'aFileName'. Enregistre depuis la classe
     * actuelle jusqu'à la classe 'Object'.
     *
     * @param aFileName Fichier où enregistrer
     * @return Chaînage
     * @throws IOException              Erreur de lecture/écriture
     * @throws IllegalArgumentException Erreur de lecture de champ de la classe
     * @throws IllegalAccessException   Erreur de lecture de champ de la classe
     */
    public Object saveToFile(String aFileName) throws IOException, IllegalArgumentException, IllegalAccessException {
        Txml x = new Txml();
        saveFields(getClass(), x.addTag(getClass().getName()));
        x.saveToFile(aFileName);
        return this;
    }

    /**
     * Lit tout les champs (DeclaredFields) hors transient et leurs valeur
     * depuis le fichier 'aFileName'. Ouvre depuis la classe actuelle jusqu'à la
     * classe 'Object'.
     *
     * @param aFileName Fichier à ouvrir
     * @return Chaînage
     * @throws IOException                                    Erreur de lecture/écriture
     * @throws javax.xml.parsers.ParserConfigurationException Erreur XML
     * @throws org.xml.sax.SAXException                       Erreur XML
     */
    public Object loadFromFile(String aFileName) throws IOException, ParserConfigurationException, SAXException {
        Document x = Txml.loadFromFile(aFileName);
        Element r = x.getDocumentElement();
        NodeList racineNoeuds = r.getChildNodes();
        int nbRacineNoeuds = racineNoeuds.getLength();
        for (int i = 0; i < nbRacineNoeuds; i++) {
            if (racineNoeuds.item(i).getNodeType() == Node.ELEMENT_NODE) {
                try {
                    loadField(this.getClass(), racineNoeuds.item(i));
                } catch (SecurityException | IllegalArgumentException ex) {

                }
            }
        }
        onLoaded();
        return this;
    }

    public abstract void onLoaded() throws IOException, SAXException, ParserConfigurationException;

    //
    private void loadField(Class aClass, Node noeud) {
        String aFieldName = noeud.getNodeName();
        try {
            Field f = aClass.getDeclaredField(aFieldName);
            f.setAccessible(true);
            switch (f.getType().getName()) {
                case "int":
                    f.setInt(this, Integer.parseInt(noeud.getTextContent()));
                    break;
                case "double":
                    f.setDouble(this, Double.parseDouble(noeud.getTextContent()));
                    break;
                case "boolean":
                    f.setBoolean(this, Boolean.parseBoolean(noeud.getTextContent()));
                    break;
                case "java.lang.String":
                    f.set(this, noeud.getTextContent());
                    break;
                default:
                    //System.out.println("Objet " + f.getType().getName() + " pour le champ '" + aFieldName + "' -> recherche de la fonction loadFromXML()...");
                    Class[] types = {Node.class};
                    try {
                        Method m = f.getType().getMethod("loadFromXML", types);
                        f.set(this, m.invoke(f.get(this), noeud));
                    } catch (NoSuchMethodException ex) {
                        System.out.println("/!\\ Impossible de trouver la méthode loadFromXML du champ '" + aFieldName + "' de la classe '" + f.getType().getName() + "'");
                    } catch (InvocationTargetException ex) {
                        System.out.println("/!\\ Erreur lors de l'appel à loadFromXML pour le champ '" + aFieldName + "' de la classe " + f.getType().getName());
                    } catch (SecurityException | IllegalAccessException | IllegalArgumentException ex) {
                        System.out.println("/!\\ '" + aFieldName + "' ERROR " + ex.getClass().getName() + ": " + ex.getMessage());
                    }
            }
        } catch (NoSuchFieldException ex) {
            if (aClass.getSuperclass() != null) {
                loadField(aClass.getSuperclass(), noeud);
            } else {
                System.out.println("(i) Le champ '" + aFieldName + "' n'existe pas dans '" + aClass.getName() + "' et ses super classe...");
            }
        } catch (SecurityException | IllegalArgumentException | IllegalAccessException ex) {
            System.out.println("/!\\ loadField ERROR::" + ex.getClass().getName() + "\n" + ex.getMessage());
        }
    }

    /**
     * Enregistre au format XML
     *
     * @param aTag Tag parent dans lequel enregistrer les informations de la
     *             classe
     * @return Le tag dans lequel les informations de la classe ont étés
     * enregistrées
     */
    @Override
    public TxmlTag saveToXML(TxmlTag aTag) {
        try {
            saveFields(getClass(), aTag);
        } catch (IllegalArgumentException | IllegalAccessException ex) {
            Logger.getLogger(TxmlObject.class.getName()).log(Level.SEVERE, null, ex);
        }
        return aTag;
    }

    /**
     * Lecture du format XML
     *
     * @throws Exception Not supported yet
     */
    @Override
    public Object loadFromXML(Node noeud) throws Exception {
        //System.out.println(this.getClass().getName()+"::loadFromXML("+noeud.getNodeName()+")");
        for (int i = 0; i < noeud.getChildNodes().getLength(); i++) {
            if (noeud.getChildNodes().item(i).getNodeType() == Node.ELEMENT_NODE) {
                //System.out.println("noeud #"+i+"="+noeud.getChildNodes().item(i).getNodeName() +" classe="+this.getClass().getName());
                try {
                    loadField(this.getClass(), noeud.getChildNodes().item(i));
                } catch (NullPointerException ex) {
                    System.out.println("(i) TxmlObject::loadFromXML ->" + noeud.getChildNodes().item(i).getNodeName() + " est null!");
                } catch (Exception ex) {
                    System.out.println("/!\\ " + ex.getClass().getName() + " Impossible de charger le champ " + noeud.getChildNodes().item(i).getNodeName() + ": " + ex.getMessage());
                }
            }
        }
        return this;
    }

}
