/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.XML;

import com.sun.org.apache.xerces.internal.dom.DOMMessageFormatter;
import com.sun.org.apache.xerces.internal.dom.DocumentImpl;
import org.w3c.dom.DOMException;

/**
 * @author romainpetit
 */
public class TxmlDocument extends DocumentImpl {

    /**
     *
     */
    public TxmlDocument() {
        super();
    }

    /**
     * Crée un tag dans le document Généralement c'est la racine
     *
     * @param tagName Nom du tag
     * @return Le nouveau tag
     * @throws DOMException Erreur DOM
     */
    @Override
    public TxmlTag createElement(String tagName) throws DOMException {
        if (errorChecking && !isXMLName(tagName, false)) {
            String msg = DOMMessageFormatter.formatMessage(DOMMessageFormatter.DOM_DOMAIN, "INVALID_CHARACTER_ERR", null);
            throw new DOMException(DOMException.INVALID_CHARACTER_ERR, msg);
        }
        return new TxmlTag(this, tagName);
    }

}
