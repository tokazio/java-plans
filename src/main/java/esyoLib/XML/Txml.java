/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.XML;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * En écriture: Txml x= new Txml();
 * x.addTag("repertoire").addTag("personne").attr("sexe",
 * "masculin").addTag("nom").text("DOE").up().addTag("prenom").text("John").up().addTag("telephones").addTag("telephone").attr("type",
 * "fixe").text("01 02 03 04 05").up().addTag("telephone").attr("type",
 * "portable").text("06 07 08 09 10"); x.racine().addTag("test").attr("Doubi",
 * "Doowa"); x.print();
 * <p>
 * En lecture: Permet d'ouvrir un document XML de façon simplifié:
 * <p>
 * static Document loadFromFile(String aFileName)
 *
 * @author romainpetit
 */
public class Txml {

    //

    private TxmlDocument document;

    //=======================================================================
    //ECRITURE
    //=======================================================================

    /**
     * Crée un document XML vide
     */
    public Txml() {
        this.document = new TxmlDocument();//com.sun.org.apache.xerces.internal.dom.DocumentImpl();// builder.newDocument();
    }

    /**
     * Renvoi le Document XML ouvert depuis le fichier aFileName
     *
     * @param aFileName Fichier à ouvrir
     * @return 'Document' XML
     * @throws javax.xml.parsers.ParserConfigurationException Erreur XML
     * @throws org.xml.sax.SAXException                       Erreur XML
     * @throws java.io.IOException                            Erreur lecture/écriture du fichier
     */
    public static Document loadFromFile(String aFileName) throws ParserConfigurationException, SAXException, IOException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.parse(new File(aFileName));
        return doc;
    }

    //
    private void transform(StreamResult sortie) {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = null;
        try {
            transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.VERSION, "1.0");
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
        } catch (TransformerConfigurationException ex) {
            Logger.getLogger(TxmlDocument.class.getName()).log(Level.SEVERE, null, ex);
        }
        DOMSource source = new DOMSource(this.document);
        try {
            transformer.transform(source, sortie);
        } catch (TransformerException ex) {
            Logger.getLogger(TxmlDocument.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Affiche le document XML dans la console
     */
    public void print() {
        transform(new StreamResult(System.out));
    }

    /**
     * Enregistre le document XML dans aFileName
     *
     * @param aFileName Fichier où enregistrer
     */
    public void saveToFile(String aFileName) {
        transform(new StreamResult(new File(aFileName)));
    }

    /**
     * Ajoute un tag au document Normalement uniquement la racin
     *
     * @param s Nom du tag
     * @return Le tag
     */
    public TxmlTag addTag(String s) {
        TxmlTag e = this.document.createElement(s);
        e.setDocument(this.document);
        this.document.appendChild(e);
        return e;
    }

    //=======================================================================
    //LECTURE
    //=======================================================================

    /**
     * Ajoute un tag au tag choisi
     *
     * @param s        Nom du tag
     * @param aElement Tag dans lequel ajouter le nouveau tag
     * @return Le nouveau tag
     */
    public TxmlTag addTagTo(String s, TxmlTag aElement) {
        TxmlTag e = this.document.createElement(s);
        e.setDocument(this.document);
        aElement.appendChild(e);
        return e;
    }

}
