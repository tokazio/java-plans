/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.XML;

import com.sun.org.apache.xerces.internal.dom.ElementImpl;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * @author romainpetit
 */
public class TxmlTag extends ElementImpl {

    //

    private Document document;
    //
    private TxmlTag parent;

    /**
     * Crée un tag dans le document
     *
     * @param aThis   Le document
     * @param tagName Nom du tag
     */
    public TxmlTag(TxmlDocument aThis, String tagName) {
        super(aThis, tagName);
    }

    /**
     * Défini le document auquel appartient ce tag
     *
     * @param aDocument Le Document
     * @return Chaînage
     */
    public TxmlTag setDocument(Document aDocument) {
        this.document = aDocument;
        return this;
    }

    /**
     * Ajoute un attribut au tag
     *
     * @param aName  Le nom de l'attribut
     * @param aValue La valeur de l'attribut
     * @return Chaînage
     */
    public TxmlTag attr(String aName, String aValue) {
        this.setAttribute(aName, aValue);
        return this;
    }

    /**
     * Défini le texte du tag (Entre le tag ouvrant et le tag fermant)
     *
     * @param aText Texte
     * @return Chaînage
     */
    public TxmlTag text(String aText) {
        this.appendChild(this.document.createTextNode(aText));
        return this;
    }

    /**
     * Ajoute un tag au tag
     *
     * @param s Nom du tag
     * @return Le nouveau tag
     */
    public TxmlTag addTag(String s) {
        Element e = this.document.createElement(s);
        ((TxmlTag) e).setDocument(this.document);
        this.appendChild(e);
        ((TxmlTag) e).setParent(this);
        return (TxmlTag) e;
    }

    /**
     * Défini le tag parent
     *
     * @param aTag Tag parent
     */
    protected void setParent(TxmlTag aTag) {
        this.parent = aTag;
    }

    /**
     * Remonte au tag parent
     *
     * @return Le tag parent
     */
    public TxmlTag up() {
        return this.parent;
    }
}
