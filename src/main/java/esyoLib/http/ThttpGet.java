/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.http;

import com.sun.net.httpserver.HttpExchange;

import java.io.IOException;
import java.util.Map;

/**
 * Utilisé par le onGet() du ThttpHandler
 *
 * @author romainpetit
 */
public abstract class ThttpGet {

    /**
     * @param he
     * @param resp
     * @param params
     * @return
     * @throws java.io.IOException
     */
    public abstract String onGet(HttpExchange he, String resp, Map<String, String> params) throws IOException;

}
