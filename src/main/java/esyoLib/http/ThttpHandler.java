/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.http;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.*;
import java.nio.charset.Charset;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.GZIPOutputStream;

import static esyoLib.Files.TfileUtils.getExtension;

/**
 * http://fr.wikipedia.org/wiki/Liste_des_codes_HTTP
 *
 * @author romainpetit
 */
public abstract class ThttpHandler implements HttpHandler {

    /**
     *
     */
    private static final boolean trace = true;
    /**
     * Liste des paramètres GET de l'URL
     */
    private final Map<String, String> params;
    /**
     *
     */
    private HttpExchange he;

    /**
     * Constructor
     */
    public ThttpHandler() {
        this.params = new HashMap<>();
    }

    /**
     * @param file
     * @return
     */
    private static String getEtag(File file) {
        return file.lastModified() + file.length() + "";
    }

    /**
     * Converti un InputStream en String
     *
     * @param is InputStream
     * @return String
     */
    private static String inputStreamToString(java.io.InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is, "UTF-8").useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

    /**
     * Place les paramètres de l'URL dans une liste
     */
    public final void getParams() {
        this.params.clear();
        String query = he.getRequestURI().getQuery();
        if (query == null) {
            if (ThttpHandler.trace) {
                System.out.println("HTTP query NULL");
            }
        } else {
            if (ThttpHandler.trace) {
                System.out.println("HTTP query: " + query);
                if (query.indexOf("&") > 0) {
                    for (String param : query.split("&")) {
                        String[] pair = param.split("=");
                        if (pair.length > 1) {
                            this.params.put(pair[0], pair[1]);
                        } else {
                            this.params.put(pair[0], "");
                        }
                    }
                }
            }
        }
        printParams();
    }

    /**
     * Affiche les paramètres dans la console
     */
    public final void printParams() {
        if (ThttpHandler.trace) {
            System.out.println("HTTP params: ");
            this.params.entrySet().stream().forEach((e) -> {
                System.out.println(e.getKey() + "=" + e.getValue());
            });
            System.out.println();
        }
    }

    /**
     * Requête GET
     *
     * @throws IOException
     */
    private void get() throws IOException {
        if (ThttpHandler.trace) {
            System.out.println("Requête GET");
        }
        getParams();
        getHttpFile();
    }

    /**
     * Trouve le fichier demandé et réponds selon le type MIME
     *
     * @throws IOException
     */
    private void getHttpFile() throws IOException {
        //récupère le fichier/dossier sur le chemin local
        File file = new File(System.getProperty("user.dir") + "/html/" + this.he.getRequestURI().getPath());
        //si ce n'est qu'un dossier, utilise le fichier index.html
        if (file.isDirectory()) {
            file = new File(file.getAbsolutePath() + "/index.html");
        }
        if (ThttpHandler.trace) {
            System.out.println("Demande du fichier: " + file.getAbsolutePath());
        }
        //si le fichier n'existe pas
        if (!file.exists()) {
            httperror(404, "Introuvable: " + file.getAbsolutePath());
            return;
        }
        //vérifie le Etag
        if (this.he.getRequestHeaders().containsKey("If-none-match")) {
            //System.out.println(this.he.getRequestHeaders().get("If-none-match")+" / "+"["+getEtag(file)+"]");
            if (this.he.getRequestHeaders().get("If-none-match").toString().trim().equals("[" + getEtag(file) + "]")) {
                //System.out.println("Etag de '"+file.getAbsolutePath()+"' n'as pas changé, retourne 304");
                //le Etag n'a pas changé, renvoi 304
                this.he.sendResponseHeaders(304, -1);
                this.he.close();
                return;
            }
        }
        //MIME
        String mime = getMIME(file);
        //récup le fichier local selon le type MIME
        String[] st = mime.split("/");
        byte[] bytearray;
        switch (st[0]) {
            case "text":
                bytearray = getTextFile(file, mime);
                if (file.getName().equals("index.html")) {
                    System.out.println("INDEX");
                    everCache(file);
                }
                break;
            default:
                bytearray = getFile(file, mime);
        }
        //header de réponse
        this.he.getResponseHeaders().set("Server", "Esyo");
        this.he.getResponseHeaders().set("Content-Type", mime);
        this.he.getResponseHeaders().set("Content-Encoding", "gzip");
        this.he.getResponseHeaders().set("Access-Control-Expose-Headers", "ETag");
        //compresse (gzip) et termine la transaction en fermant
        close(bytearray);
    }

    /**
     *
     */
    private void printRequestHeaders() {
        this.he.getRequestHeaders().entrySet().stream().forEach((e) -> {
            System.out.println(e.getKey() + ":" + e.getValue());
        });
    }

    /**
     * Récupère le contenu du fichier, l'envois dans onGet(), récupère et répond
     */
    private byte[] getTextFile(File file, String mime) throws IOException {
        if (ThttpHandler.trace) {
            System.out.println("getTextFile(" + mime + ")");
        }
        InputStream is = new FileInputStream(file.getAbsolutePath());
        String r = onGet(he, inputStreamToString(is), this.params);
        return r.getBytes(Charset.forName("UTF-8"));
    }

    /**
     * Récupère le fichier et l'envois en direct
     */
    private byte[] getFile(File file, String mime) throws IOException {
        if (ThttpHandler.trace) {
            System.out.println("getFile(" + mime + ")");
        }
        byte[] bytearray = new byte[(int) file.length()];
        InputStream is = new FileInputStream(file.getAbsolutePath());
        is.read(bytearray, 0, bytearray.length);
        //cache infini pour les fichiers à envoi direct (basé sur le changement de l'Etag donc la date de dernière modif et la taille)
        everCache(file);
        //
        return bytearray;
    }

    /**
     * @param file
     */
    private void everCache(File file) {
        //Ajoute un an
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.YEAR, 1);
        this.he.getResponseHeaders().set("Expires", c.getTime().toString());
        //cache de 1 an (31536000 secondes)
        this.he.getResponseHeaders().set("Cache-control", "private,max-age=31536000");
        //Etag selon la date de dernière modif + la taille
        this.he.getResponseHeaders().set("ETag", "" + getEtag(file));
        //Date de dernière modif
        this.he.getResponseHeaders().set("Last-modified", new Date(file.lastModified()).toString());
    }

    /**
     * @param file
     */
    private void standardCache(File file) {
        //Etag selon la date de dernière modif + la taille
        this.he.getResponseHeaders().set("ETag", "" + file.lastModified() + file.length());
        //Date de dernière modif
        this.he.getResponseHeaders().set("Last-modified", new Date(file.lastModified()).toString());

    }

    /**
     * @param file
     */
    private void noCache(File file) {
        this.he.getResponseHeaders().set("Cache-control", "no-transform,private,no-cache");
        this.he.getResponseHeaders().set("Pragma", "no-cache");
    }

    /**
     * Commande POST au serveur
     *
     * @throws IOException
     */
    private void post() throws IOException {
        if (ThttpHandler.trace) {
            System.out.println("Requête POST");
        }
        getParams();
        getHttpFile();
    }

    /**
     * @param f
     * @return
     */
    private String getMIME(File f) {
        String t = "text/plain; charset=utf-8";
        switch (getExtension(f.getAbsolutePath())) {
            case ".php":
            case ".html":
                t = "text/html; charset=UTF-8";
                break;
            case ".js":
                t = "application/javascript; charset=UTF-8";
                break;
            case ".png":
                t = "image/png";
                break;
            case ".jpg":
            case ".jpeg":
                t = "image/jpeg";
                break;
            case ".gif":
                t = "image/gif";
                break;
            case ".pdf":
                t = "application/pdf";
                break;
        }
        return t;
    }

    /**
     * @param code Code d'erreur HTTP
     * @param desc Description de l'erreur
     * @throws java.io.IOException
     */
    public void httperror(int code, String desc) throws IOException {
        this.he.sendResponseHeaders(code, 0);
        this.he.getResponseHeaders().set("Content-Type", "text/html");
        OutputStream os = this.he.getResponseBody();
        String s = "<html><head></head><body><h1>Erreur " + code + "</h1><p>" + desc + "</p></body></html>";
        os.write(s.getBytes());
        this.he.close();
    }

    /**
     * Termine la transcation (envoi la réponse)
     *
     * @param bytearray
     * @throws IOException
     */
    public void close(byte[] bytearray) throws IOException {
        this.he.sendResponseHeaders(200, 0);
        GZIPOutputStream gz = new GZIPOutputStream(this.he.getResponseBody());
        gz.write(bytearray, 0, bytearray.length);
        gz.finish();
        this.he.close();
        if (ThttpHandler.trace) {
            System.out.println("Envois de la réponse");
        }
    }

    /**
     * @param he
     * @throws java.io.IOException
     */
    @Override
    public void handle(HttpExchange he) throws IOException {
        this.he = he;
        if (this.he.getRequestMethod().equalsIgnoreCase("GET")) {
            get();
        }
        if (this.he.getRequestMethod().equalsIgnoreCase("POST")) {
            post();
        }
        throw new IOException("Commande (requestMethod) non reconnue (ni GET ni POST)");
    }

    /**
     * @param he
     * @param resp
     * @param params
     * @return
     * @throws java.io.IOException
     */
    public abstract String onGet(HttpExchange he, String resp, Map<String, String> params) throws IOException;
}
