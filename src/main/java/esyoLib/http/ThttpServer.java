/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.http;

import com.sun.net.httpserver.BasicAuthenticator;
import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.net.BindException;
import java.net.InetSocketAddress;
import java.util.Map;
import java.util.concurrent.Executors;

/**
 * @author romainpetit
 */
public class ThttpServer {

    /**
     * Le port d'écoute du serveur HTTP
     */
    private int port = 0;

    /**
     * Le server HTTP
     */
    private HttpServer server = null;

    /**
     * @param port Port d'écoute du serveur HTTP
     */
    public ThttpServer(int port) {
        this.port = port;
    }

    /**
     * Démarre le serveur
     *
     * @return @throws java.io.IOException
     * @throws java.net.BindException
     */
    public ThttpServer start() throws IOException {
        try {
            this.server = HttpServer.create(new InetSocketAddress(this.port), 0);
            this.server.setExecutor(Executors.newCachedThreadPool());
            this.server.start();
            System.out.println("Serveur HTTP en écoute sur le port " + this.port);
        } catch (BindException ex) {
            throw new BindException("Le port " + this.port + " est déjà utilisé.");
        }

        return this;
    }

    /**
     * Ajoute un contexte sécurisé
     *
     * @param aPath    Un chemin de l'URL à gérer par le serveur HTTP
     * @param callback Une fonction ThhtpGet appelé lors d'une commander get
     *                 (modifier la réponse)
     */
    public void addPath(String aPath, ThttpGet callback) {
        this.server.createContext(aPath, new ThttpHandler() {

            @Override
            public String onGet(HttpExchange he, String resp, Map<String, String> params) throws IOException {
                return callback.onGet(he, resp, params);
            }
        });
    }

    /**
     * Ajoute un contexte sécurisé
     *
     * @param aPath     Un chemin de l'URL à gérer par le serveur HTTP
     * @param callback  Une fonction ThhtpGet appelé lors d'une commander get
     *                  (modifier la réponse)
     * @param aUser     Un nom d'uitlisateur
     * @param aPassword Un mot de passe
     */
    public void addSecurePath(String aPath, ThttpGet callback, String aUser, String aPassword) {
        HttpContext hc1 = server.createContext(aPath, new ThttpHandler() {

            @Override
            public String onGet(HttpExchange he, String resp, Map<String, String> params) throws IOException {
                return callback.onGet(he, resp, params);
            }
        });
        hc1.setAuthenticator(new BasicAuthenticator("get") {
            @Override
            public boolean checkCredentials(String user, String pwd) {
                return user.equals(aUser) && pwd.equals(aPassword);
            }
        });
    }

    /**
     *
     */
    public void stop() {
        server.stop(0);
    }
}
