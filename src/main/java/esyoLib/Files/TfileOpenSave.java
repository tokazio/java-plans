package esyoLib.Files;


import esyoLib.Formsfx.*;
import javafx.stage.FileChooser;

import java.io.File;
import java.io.IOException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * @author rpetit
 */
public abstract class TfileOpenSave extends FileObj {

    /**
     * edited?
     */
    private boolean Fedited = false;
    /**
     * fenêtre
     */
    private Tform Fform;
    /**
     * chemin du fichier
     */
    private String FfileName = "";
    /**
     *
     */
    private String FfilterName;
    /**
     *
     */
    private String Ffilter;
    /**
     *
     */
    private Tapplication application = null;

    /**
     * Crée un nouveau (pas de fichier en paramètre)
     *
     * @param aApplication
     */
    public TfileOpenSave(Tapplication aApplication) {
        doIni(aApplication);
        doOpen();
        //devra être enregistré
        setEdited();
    }

    /**
     * Ouvre (fichier en paramètre)
     *
     * @param aFileName
     * @param aApplication
     */
    public TfileOpenSave(String aFileName, Tapplication aApplication) {
        doIni(aApplication);
        FfileName = aFileName;
        doOpen();
        //pas besoin d'enregistré si pas de modif
        notEdited();
    }

    /**
     * Appelé dans le constructeur pour l'initialisation
     */
    public abstract void onIni();

    /**
     * Permet de changer les infos du fileChooser avant l'ouverture
     *
     * @param theFileChooser
     */
    public abstract void beforeOpen(FileChooser theFileChooser);

    /**
     * Effectue l'ouverture du fichier
     *
     * @return
     */
    public abstract Tform onOpen();

    /**
     * @param theForm
     */
    public abstract void afterOpen(Tform theForm);

    /**
     * Effectue la fermeture du fichier
     */
    public abstract void onClose();

    /**
     * Permet de changer les infos du fileChooser avant l'enregistrement
     *
     * @param theFileChooser
     */
    public abstract void beforeSave(FileChooser theFileChooser);

    /**
     * Effectue l'enregistrement
     */
    public abstract void onSave();

    /**
     * @param aApplication
     */
    private void doIni(Tapplication aApplication) {
        this.application = aApplication;
        onIni();
    }

    /**
     * @return
     * @throws IOException
     */
    public final String openFromFile() throws IOException {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Ouvrir...");
        //Set extension filter
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter(FfilterName, Ffilter);
        fileChooser.getExtensionFilters().add(extFilter);
        //
        beforeOpen(fileChooser);
        //Show open file dialog
        File file = fileChooser.showOpenDialog(getForm().getStage());
        //récup file
        if (file != null) {
            FfileName = file.getPath();
            doOpen();
        }
        return FfileName;
    }

    /**
     * Indique qu'un enregistrement devra être fait
     */
    public final void setEdited() {
        Fedited = true;
        Fform.setTitle("*" + getName());
        Fform.canClose = false;
    }

    /**
     * @return
     */
    public final boolean getEdited() {
        return Fedited;
    }

    /**
     *
     */
    private void notEdited() {
        Fedited = false;
        Fform.setTitle(getName());
        Fform.canClose = true;
    }

    /**
     *
     */
    private void doOpen() {
        Fform = onOpen();
        if (FfileName.isEmpty()) {
            Fform.setTitle("Nouveau");
        } else {
            Fform.setTitle(getName());
        }
        afterOpen(Fform);
    }

    /**
     * @return
     */
    public final String getName() {
        if (FfileName.isEmpty()) {
            return "Nouveau";
        }
        String[] s = FfileName.split("\\\\");
        return s[s.length - 1];
    }

    /**
     *
     */
    public final void close() {
        Fform.close();
    }

    /**
     *
     */
    public final void doClose() {
        onClose();
    }

    /**
     * @return @throws IOException
     * @throws esyoLib.Formsfx.TformException
     * @throws java.lang.IllegalAccessException
     * @throws esyoLib.Formsfx.TformFmxlNotFoundException
     */
    public final boolean confirmClose() throws IOException, TformException, IllegalArgumentException, IllegalAccessException, TformFmxlNotFoundException, TformFmxlLoadException {
        //si à enregistrer, demande?
        if (Fedited) {
            TconfirmCancelForm f = application.newConfirmCancelForm(Fform.getName() + "\nEnregistrer avant de fermer '" + getName() + "' ?");
            switch (f.isConfirmed()) {
                case crCancel:
                    return false;
                case crYes:
                    save();
                    return true;
                case crNo:
                    return true;
            }
        }
        return true;
    }

    /**
     * @return
     * @throws IOException
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     */
    public final String save() throws IOException, IllegalArgumentException, IllegalAccessException {
        if (getFileName().isEmpty()) {
            saveToFile();
        } else {
            notEdited();
            onSave();
        }
        return FfileName;
    }

    /**
     * @return
     */
    public final String getFileName() {
        return FfileName;
    }

    /**
     * @return @throws IOException
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     */
    public final String saveToFile() throws IOException, IllegalArgumentException, IllegalAccessException {
        FileChooser fileChooser = new FileChooser();
        //titre
        fileChooser.setTitle("Enregistrer-sous...");
        //défini le nom du fichier
        fileChooser.setInitialFileName(getName());
        //extensions
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter(FfilterName, Ffilter);
        fileChooser.getExtensionFilters().add(extFilter);
        //
        beforeSave(fileChooser);
        //affiche
        File file = fileChooser.showSaveDialog(getForm().getStage());
        //récup file
        if (file != null) {
            FfileName = file.getPath();
            save();
        }
        return FfileName;
    }

    /**
     * @return
     */
    public final Tform getForm() {
        return Fform;
    }

    /**
     * @param aFilterName
     * @param aFilterExtName
     */
    public final void setFilter(String aFilterName, String aFilterExtName) {
        FfilterName = aFilterName;
        Ffilter = aFilterExtName;
    }

    /**
     *
     */
    public final void toFront() {
        Fform.toFront().setFocus();
    }

}
