/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.Files;

import esyoLib.Utils.Tobject;
import esyoLib.Utils.TstringList;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author rpetit
 */
public class TstringObject extends Tobject implements IstringSavable {

    public TstringObject() {
        super();
    }

    //
    private void loadField(Class aClass, String aFieldName, String aFieldValue) {
        try {
            Field f = aClass.getDeclaredField(aFieldName);
            f.setAccessible(true);
            //System.out.println(aFieldName + " est de type " + f.getType().getName());
            switch (f.getType().getName()) {
                case "int":
                    f.setInt(this, Integer.parseInt(aFieldValue));
                    break;
                case "double":
                    f.setDouble(this, Double.parseDouble(aFieldValue));
                    break;
                case "boolean":
                    f.setBoolean(this, Boolean.parseBoolean(aFieldValue));
                    break;
                case "java.lang.String":
                    f.set(this, aFieldValue);
                    break;
                default:
                    System.out.println("Objet " + f.getType().getName() + " pour le champ '" + aFieldName + "' = '" + aFieldValue + "' recherche de la fonction loadFromString()...");
                    Class[] types = {String.class};
                    try {
                        Method m = f.getType().getMethod("loadFromString", types);
                        f.set(this, m.invoke(f, aFieldValue));
                        System.out.println(aFieldName + " = " + aFieldValue);
                    } catch (NoSuchMethodException ex) {
                        System.out.println("Impossible de trouver la méthode loadFromString(String) du champ '" + aFieldName + "' de la classe " + f.getType().getName());
                    } catch (InvocationTargetException ex) {
                        System.out.println("Erreur lors de l'appel à loadFromString(" + aFieldValue + ") avec le champ '" + aFieldName + "' de la classe " + f.getType().getName());
                    } catch (SecurityException | IllegalAccessException | IllegalArgumentException ex) {
                        System.out.println(aFieldName + "' ERROR::" + ex.getClass().getName() + "\n" + ex.getMessage());
                    }
            }
        } catch (NoSuchFieldException ex) {
            if (aClass.getSuperclass() != null) {
                loadField(aClass.getSuperclass(), aFieldName, aFieldValue);
            } else {
                System.out.println("Le champ '" + aFieldName + "' n'existe pas dans '" + aClass.getName() + "' et ses super classe...");
            }
        } catch (SecurityException | IllegalArgumentException | IllegalAccessException ex) {
            System.out.println("setField ERROR::" + ex.getClass().getName() + "\n" + ex.getMessage());
        }
    }

    /**
     * Lit tout les champs (DeclaredFields) hors transient et leurs valeur
     * depuis le fichier aFileName Prend en compte les: int, double, boolean
     * String et autres objet par défaut avec set() Ouvre depuis la classe
     * actuelle jusqu'à la classe Object
     *
     * @param aFileName
     * @return
     * @throws IOException
     */
    public Object loadFromFile(String aFileName) throws IOException {
        //System.out.println("Load obj from file '" + aFileName + "'...");
        TstringList l = new TstringList(aFileName);
        //l.print();
        for (int i = 0; i < l.size(); i++) {
            try {
                loadField(this.getClass(), l.nameFromIndex(i), l.valueFromIndex(i));
            } catch (SecurityException | IllegalArgumentException ex) {
                //System.out.println("Error '" + ex.getClass().getName() + ": " + ex.getMessage());
            }
        }
        return this;
    }

    public Object loadFromStream(InputStream s) throws IOException {
        //System.out.println("Load obj from file '" + aFileName + "'...");
        TstringList l = new TstringList(s, "UTF-8");
        //l.print();
        for (int i = 0; i < l.size(); i++) {
            try {
                loadField(this.getClass(), l.nameFromIndex(i), l.valueFromIndex(i));
            } catch (SecurityException | IllegalArgumentException ex) {
                //System.out.println("Error '" + ex.getClass().getName() + ": " + ex.getMessage());
            }
        }
        return this;
    }

    //
    private void saveFields(Class aClass, TstringList l) throws IllegalArgumentException, IllegalAccessException {
        String v;
        Field[] fs = aClass.getDeclaredFields();
        for (int i = 0; i < fs.length; i++) {
            try {
                //si transient, je n'enregistre pas
                if (Modifier.isTransient(fs[i].getModifiers())) {
                    continue;
                }
                fs[i].setAccessible(true);
                switch (fs[i].getType().getName()) {
                    case "int":
                        v = "" + fs[i].getInt(this);
                        break;
                    case "double":
                        v = "" + fs[i].getDouble(this);
                        break;
                    case "boolean":
                        v = "" + fs[i].getBoolean(this);
                        break;
                    case "java.lang.String":
                        v = "" + fs[i].get(this);
                        break;
                    default:
                        Class[] types = {};
                        Object[] params = {};
                        try {
                            Method m = fs[i].getType().getMethod("saveToString", types);
                            Object o = fs[i].get(this);
                            v = (String) m.invoke(o, params);
                        } catch (Exception ex) {
                            System.out.println(fs[i].getName() + " Error (pas de fonction saveToString() " + ex.getClass().getName() + "\n" + ex.getMessage());
                            v = fs[i].get(this).toString();
                        }


                        //System.out.println("Save object " + aClass.getName() + "::" + fs[i].getName() + "(" + fs[i].getType().getName() + ") ->toString()");
                        //System.out.println(fs[i].getName() + " = " + v);
                }
                //System.out.println(fs[i].getName() + " = " + v);
                l.add(fs[i].getName(), v);
            } catch (Exception ex) {
                //System.out.println("Save object " + ex.getMessage());
            }
        }
        if (aClass.getSuperclass() != null) {
            saveFields(aClass.getSuperclass(), l);
        }
    }


    /**
     * Enregistre tout les champs (DeclaredFields) hors transient et leurs
     * valeur dans le fichier aFileName. Prend en compte les: int, double,
     * boolean String et autres objet par défaut avec get(). Enregistre depuis la
     * classe actuelle jusqu'à la classe 'Object'.
     *
     * @param aFileName
     * @return
     * @throws IOException
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     */
    public Object saveToFile(String aFileName) throws IOException, IllegalArgumentException, IllegalAccessException {
        TstringList l = new TstringList();
        saveFields(getClass(), l);
        l.add("class", getClass().getName());
        l.saveToFile(aFileName);
        return this;
    }

    @Override
    public void saveToString(TstringList l) {
        try {
            saveFields(getClass(), l);
        } catch (IllegalArgumentException | IllegalAccessException ex) {
            Logger.getLogger(TstringObject.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void loadFromString(String s) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
