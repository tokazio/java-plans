/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.Files;

import java.io.File;

/**
 * @author rpetit
 */
public class TfileUtils {

    private static final char EXTENSION_SEPARATOR = '.';
    private static final char DIRECTORY_SEPARATOR = '/';

    /**
     * Remove the file extension from a filename, that may include a path.
     * <p>
     * e.g. /path/to/myfile.jpg donne /path/to/myfile
     *
     * @param filename
     * @return
     */
    public static String removeExtension(String filename) {
        if (filename == null) {
            return null;
        }

        int index = indexOfExtension(filename);

        if (index == -1) {
            return filename;
        } else {
            return filename.substring(0, index);
        }
    }

    /**
     * Return the file extension from a filename, including the "."
     * <p>
     * e.g. /path/to/myfile.jpg donne .jpg
     *
     * @param filename
     * @return
     */
    public static String getExtension(String filename) {
        if (filename == null) {
            return null;
        }

        int index = indexOfExtension(filename);

        if (index == -1) {
            return filename;
        } else {
            return filename.substring(index);
        }
    }

    /**
     * @param filename
     * @return
     */
    public static int indexOfExtension(String filename) {
        if (filename == null) {
            return -1;
        }
        // Check that no directory separator appears after the
        // EXTENSION_SEPARATOR
        int extensionPos = filename.lastIndexOf(EXTENSION_SEPARATOR);
        int lastDirSeparator = filename.lastIndexOf(DIRECTORY_SEPARATOR);
        if (lastDirSeparator > extensionPos) {
            System.out.println("A directory separator appears after the file extension, assuming there is no file extension");
            return -1;
        }
        return extensionPos;
    }

    /**
     * @param aPath
     * @param aFolderSeparator
     * @return
     */
    public static String folderName(String aPath, String aFolderSeparator) {
        String[] s = aPath.split(aFolderSeparator);
        return s[s.length - 1];
    }

    /**
     * @param aPath
     * @param aFolderSeparator
     * @return
     * @deprecated Remplacer par extractFilePath(String);
     */
    @Deprecated
    public static String getPath(String aPath, String aFolderSeparator) {
        String[] s = aPath.split(aFolderSeparator);
        String r = "";
        for (int i = 0; i < s.length - 1; i++)
            r += s[i] + aFolderSeparator;
        return r;
    }

    /**
     * @param aFileName
     * @param aFolderSeparator
     * @return
     */
    public static String extractFilePath(String aFileName, String aFolderSeparator) {
        return aFileName.substring(0, aFileName.lastIndexOf(aFolderSeparator));
    }

    /**
     * @param aPath
     * @return
     */
    public static boolean isValidFolder(String aPath) {
        File f = new File(aPath);
        return f.exists() && f.isDirectory();
    }
}
