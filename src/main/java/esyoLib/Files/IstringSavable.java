/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.Files;

import esyoLib.Utils.TstringList;

/**
 * @author rpetit
 */
public interface IstringSavable {

    /**
     * @param l
     */
    void saveToString(TstringList l);


    /**
     * @param s Anonymous function of type TfromString to decode and get params from
     * @throws java.lang.Exception
     */
    void loadFromString(String s) throws Exception;

}
