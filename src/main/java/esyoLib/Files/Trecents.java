/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.Files;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;

import java.io.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author rpetit
 */
public abstract class Trecents {
    private final int MAX = 10;
    private final ArrayList<TrecentItem> Ffiles;
    private String Ffile = "";

    /**
     * @param aFileName
     */
    public Trecents(String aFileName) {
        Ffiles = new ArrayList();
        Ffile = aFileName;
        load();
    }

    //
    private int has(ArrayList<TrecentItem> l, String p) {
        for (int i = 0; i < l.size(); i++)
            if (l.get(i).path.equals(p)) return i;
        return -1;
    }

    /**
     * @param aFileName
     */
    public final void add(String aFileName) {
        if (aFileName.isEmpty()) return;
        TrecentItem i = new TrecentItem(aFileName);
        int k;
        do {
            k = has(Ffiles, i.path);
            if (k != -1)
                Ffiles.remove(k);
        } while (k != -1);
        Ffiles.add(0, i);
        while (Ffiles.size() > MAX) {
            Ffiles.remove(MAX - 1);
        }
        save();
    }

    //
    private void save() {
        FileWriter fw = null;
        try {
            fw = new FileWriter(Ffile);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter pw = new PrintWriter(bw);
            Ffiles.stream().forEach((i) -> {
                pw.println(i.path);
            });
            pw.close();
        } catch (IOException ex) {
            Logger.getLogger(Trecents.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fw.close();
            } catch (IOException ex) {
                Logger.getLogger(Trecents.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    //
    private void load() {
        Ffiles.clear();
        FileReader fr;
        try {
            fr = new FileReader(Ffile);
            BufferedReader br = new BufferedReader(fr);
            String ligne = br.readLine();
            while (ligne != null) {
                Ffiles.add(new TrecentItem(ligne));
                ligne = br.readLine();
            }
            br.close();
        } catch (FileNotFoundException ex) {

        } catch (IOException ex) {
            Logger.getLogger(Trecents.class.getName()).log(Level.SEVERE, null, ex);
        }
        clean();
    }

    /**
     * @param menu
     */
    public final void majMenu(Menu menu) {
        menu.getItems().clear();
        EventHandler clic = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                onClick(e);
            }
        };
        for (TrecentItem f : Ffiles) {
            MenuItem m = new MenuItem(f.nom);
            m.addEventHandler(ActionEvent.ANY, clic);
            m.setId(f.path);
            menu.getItems().add(m);
        }
    }

    private void clean() {
        int i = 0;
        while (i < Ffiles.size()) {
            File f = new File(Ffiles.get(i).path);
            if (!f.exists()) {
                Ffiles.remove(i);
                i = 0;
                continue;
            }
            i++;
        }
        save();
    }

    public abstract void onClick(ActionEvent e);

}

class TrecentItem {
    private static final String PATHSEPARATOR = "\\\\";
    public String nom;
    public String path;

    /**
     * @param path
     */
    public TrecentItem(String path) {
        nom = getNom(path);
        this.path = path;
    }

    //
    private String getNom(String nom) {
        String[] s = nom.split(PATHSEPARATOR);
        return s[s.length - 1];
    }
}
