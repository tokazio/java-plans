/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.jfx3d;

import javafx.geometry.Point3D;
import javafx.scene.Parent;
import javafx.scene.shape.Cylinder;

/**
 * @author RomainPETIT
 */
public class T3dCylinder extends Cylinder implements I3d {

    public T3dCylinder() {
        super();
    }


    public T3dCylinder(double radius, double height) {
        super(radius, height);
    }


    public T3dCylinder(double radius, double height, int divisions) {
        super(radius, height, divisions);
    }

    /**
     * @return
     */
    @Override
    public final Point3D positionInWorld() {
        double x = this.getTranslateX();
        double y = this.getTranslateY();
        double z = this.getTranslateZ();
        Parent p = this.getParent();
        while (p.getClass() != T3dRoot.class) {
            x += p.getTranslateX();
            y += p.getTranslateX();
            z += p.getTranslateX();
            p = p.getParent();
        }
        return new Point3D(x, y, z);
    }

}
