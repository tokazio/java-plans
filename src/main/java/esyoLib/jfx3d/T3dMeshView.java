/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.jfx3d;

import javafx.geometry.Point3D;
import javafx.scene.Parent;
import javafx.scene.shape.Mesh;
import javafx.scene.shape.MeshView;

import java.io.Serializable;

/**
 * @author rpetit
 */
public class T3dMeshView extends MeshView implements Serializable, I3d {

    /**
     * Creates a new instance of {@code MeshView} class.
     */
    public T3dMeshView() {
        super();
    }

    /**
     * Creates a new instance of {@code MeshView} class with the specified {@code Mesh}
     * surface.
     */
    public T3dMeshView(Mesh mesh) {
        super(mesh);
    }

    /**
     * @return
     */
    @Override
    public Point3D positionInWorld() {
        double x = this.getTranslateX();
        double y = this.getTranslateY();
        double z = this.getTranslateZ();
        Parent p = this.getParent();
        while (p.getClass() != T3dRoot.class) {
            x += p.getTranslateX();
            y += p.getTranslateX();
            z += p.getTranslateX();
            p = p.getParent();
        }
        return new Point3D(x, y, z);
    }

}
