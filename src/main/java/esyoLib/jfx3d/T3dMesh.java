/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.jfx3d;

import com.interactivemesh.jfx.importer.stl.StlMeshImporter;
import javafx.geometry.Point3D;
import javafx.scene.Parent;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.DrawMode;
import javafx.scene.shape.TriangleMesh;
import javafx.scene.transform.Rotate;

import java.io.File;
import java.io.Serializable;

/**
 * @author rpetit
 */
public class T3dMesh extends T3dGroup implements Serializable, I3d {

    protected TriangleMesh mesh = null;
    protected T3dMeshView[] meshes = null;
    private T3dGroup x;
    private T3dGroup y;
    private T3dGroup z;
    private PhongMaterial material = null;

    /**
     * @param aFilename
     */
    public T3dMesh(String aFilename) {
        super();
        x = new T3dGroup(loadMesh(aFilename));
        y = new T3dGroup(x);
        z = new T3dGroup(y);
        getChildren().addAll(z);
    }

    /**
     * @param aFilename
     * @param transX
     * @param transY
     * @param transZ
     * @param rotX
     * @param rotY
     * @param rotZ
     */
    public T3dMesh(String aFilename, double transX, double transY, double transZ, double rotX, double rotY, double rotZ) {
        super();
        x = new T3dGroup(loadMesh(aFilename));
        y = new T3dGroup(x);
        z = new T3dGroup(y);
        getChildren().addAll(z);
        setRotateX(rotX);
        setRotateY(rotY);
        setRotateZ(rotZ);
        setTranslateX(transX);
        setTranslateY(transY);
        setTranslateZ(transZ);
    }

    /**
     * @param original
     * @throws java.lang.Exception
     */
    public T3dMesh(T3dMesh original) throws Exception {
        super();
        if (original == null) {
            //throw new Exception("T3dMesh(original): original is null!");
            System.out.println("T3dMesh(original): original is null!");
        }
        this.x = new T3dGroup(copyMesh(original));
        this.y = new T3dGroup(this.x);
        this.z = new T3dGroup(this.y);
        this.copy(original);
        getChildren().addAll(z);
    }

    //
    private T3dMeshView[] loadMesh(String aFilename) {
        File file = new File(aFilename);
        StlMeshImporter importer = new StlMeshImporter();
        importer.read(file);
        this.mesh = importer.getImport();
        this.meshes = new T3dMeshView[]{new T3dMeshView(this.mesh)};
        return this.meshes;
    }

    //
    private T3dMeshView[] copyMesh(T3dMesh original) {
        this.meshes = new T3dMeshView[]{new T3dMeshView(original.mesh)};
        return this.meshes;
    }

    //
    private void copy(T3dMesh original) {
        this.setMaterial(original.getMaterial());
        this.x.copyRotationX(original.x);
        this.y.copyRotationY(original.y);
        this.z.copyRotationZ(original.z);
        this.setTranslateX(original.getTranslateX());
        this.setTranslateY(original.getTranslateY());
        this.setTranslateZ(original.getTranslateZ());
        this.mesh = original.mesh;
    }

    /**
     * @return
     */
    public final PhongMaterial getMaterial() {
        return this.material;
    }

    /**
     * @param aMaterial
     */
    public final void setMaterial(PhongMaterial aMaterial) {
        this.material = aMaterial;
        for (T3dMeshView mesh1 : meshes) {
            mesh1.setMaterial(aMaterial);
        }
    }

    /**
     * @param d
     */
    public final void translateY(double d) {
        setTranslateY(getTranslateY() + d);
    }

    /**
     * @param d
     */
    public final void translateX(double d) {
        setTranslateX(getTranslateX() + d);
    }

    /**
     * @param d
     */
    public final void translateZ(double d) {
        setTranslateZ(getTranslateZ() + d);
    }

    /**
     * @param d
     */
    public final void rotateX(double d) {
        setRotateX(getRotateX() + d);
    }

    /**
     * @param d
     */
    public final void rotateY(double d) {
        setRotateY(getRotateY() + d);
    }

    /**
     * @param d
     */
    public final void rotateZ(double d) {
        setRotateZ(getRotateZ() + d);
    }

    /**
     * @return
     */
    public final double getRotateX() {
        setRotationAxis(Rotate.X_AXIS);
        return getRotate();
    }

    /**
     * @param d
     */
    public final void setRotateX(double d) {
        x.setRotationAxis(Rotate.X_AXIS);
        x.setRotate(d);
    }

    /**
     * @return
     */
    public final double getRotateY() {
        setRotationAxis(Rotate.Y_AXIS);
        return getRotate();
    }

    /**
     * @param d
     */
    public final void setRotateY(double d) {
        y.setRotationAxis(Rotate.Y_AXIS);
        y.setRotate(d);
    }

    /**
     * @return
     */
    public final double getRotateZ() {
        setRotationAxis(Rotate.Z_AXIS);
        return getRotate();
    }

    /**
     * @param d
     */
    public final void setRotateZ(double d) {
        z.setRotationAxis(Rotate.Z_AXIS);
        z.setRotate(d);
    }

    /**
     *
     */
    public final void viewAsLine() {
        for (T3dMeshView mv : this.meshes) {
            mv.setDrawMode(DrawMode.LINE);
        }
    }

    /**
     *
     */
    public final void viewAsFill() {
        for (T3dMeshView mv : this.meshes) {
            mv.setDrawMode(DrawMode.FILL);
        }
    }

    /**
     * @return
     */
    @Override
    public Point3D positionInWorld() {
        double xx = this.getTranslateX();
        double yy = this.getTranslateY();
        double zz = this.getTranslateZ();
        Parent p = this.getParent();
        while (p.getClass() != T3dRoot.class) {
            xx += p.getTranslateX();
            yy += p.getTranslateX();
            zz += p.getTranslateX();
            p = p.getParent();
        }
        return new Point3D(xx, yy, zz);
    }
}
