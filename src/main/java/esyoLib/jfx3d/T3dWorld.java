/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.jfx3d;

import javafx.event.EventHandler;
import javafx.scene.*;
import javafx.scene.image.WritableImage;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Box;
import javafx.scene.shape.Shape3D;
import javafx.stage.Stage;

/**
 * @author rpetit
 */
public abstract class T3dWorld {

    //scene

    //
    private static final double CAMERA_INITIAL_DISTANCE = -5000;
    private static final double CAMERA_INITIAL_X_ANGLE = 0.0;
    private static final double CAMERA_INITIAL_Y_ANGLE = 0.0;
    private static final double CAMERA_NEAR_CLIP = 0.1;
    private static final double CAMERA_FAR_CLIP = 10000.0;
    //axes
    private static final double AXIS_LENGTH = 1000.0;
    //
    private static final double CONTROL_MULTIPLIER = 0.1;
    private static final double SHIFT_MULTIPLIER = 10.0;
    private static final double MOUSE_SPEED = 0.1;
    private static final double ROTATION_SPEED = 2.0;
    private static final double TRACK_SPEED = 0.3;
    //camera
    final PerspectiveCamera camera = new PerspectiveCamera(true);
    //
    final T3dForme cameraXform = new T3dForme();
    final T3dForme cameraXform2 = new T3dForme();
    final T3dForme cameraXform3 = new T3dForme();
    private final T3dRoot root = new T3dRoot();
    private final T3dForme axisGroup = new T3dForme();
    private final T3dForme world = new T3dForme();
    //
    double mousePosX;
    double mousePosY;
    double mouseOldX;
    double mouseOldY;
    double mouseDeltaX;
    double mouseDeltaY;
    //
    double modifierFactor = 1;
    //

    /**
     * @param aStage
     */
    public T3dWorld(Stage aStage) {
        root.getChildren().add(world);
        buildCamera();
        buildAxes();
        //
        Scene scene = new Scene(root, 1024, 768, true);
        scene.setFill(Color.GREY);
        //
        handleKeyboard(scene, world);
        handleMouse(scene, world);
        //
        aStage.setScene(scene);
        //
        scene.setCamera(camera);
        //
        System.out.println("S: vue de face D: vue de dessus F: vue de droite");
        System.out.println("X: affichaer/masquer axes");
        System.out.println("Z: init caméra");
    }

    /**
     * @param aFileName
     */
    public void saveTofile(String aFileName) {

    }

    /**
     * @param params
     * @param aWritableImage
     * @return
     */
    public final WritableImage snapShot(SnapshotParameters params, WritableImage aWritableImage) {
        return root.snapshot(params, aWritableImage);
    }

    private void buildCamera() {
        root.getChildren().add(cameraXform);
        cameraXform.getChildren().add(cameraXform2);
        cameraXform2.getChildren().add(cameraXform3);
        cameraXform3.getChildren().add(camera);
        cameraXform3.setRotateZ(180.0);
        camera.setNearClip(CAMERA_NEAR_CLIP);
        camera.setFarClip(CAMERA_FAR_CLIP);
        camera.setTranslateZ(CAMERA_INITIAL_DISTANCE);
        //camera.setTranslateY(-1000);//CAMERA_INITIAL_DISTANCE);
        camera.setFieldOfView(35);//30 par défaut
        cameraXform.ry.setAngle(CAMERA_INITIAL_Y_ANGLE);
        cameraXform.rx.setAngle(CAMERA_INITIAL_X_ANGLE);

    }

    public final void setFarClip(double d) {
        camera.setFarClip(d);
    }

    private void buildAxes() {
        final PhongMaterial redMaterial = new PhongMaterial();
        redMaterial.setDiffuseColor(Color.DARKRED);
        redMaterial.setSpecularColor(Color.RED);
        final PhongMaterial greenMaterial = new PhongMaterial();
        greenMaterial.setDiffuseColor(Color.DARKGREEN);
        greenMaterial.setSpecularColor(Color.GREEN);
        final PhongMaterial blueMaterial = new PhongMaterial();
        blueMaterial.setDiffuseColor(Color.DARKBLUE);
        blueMaterial.setSpecularColor(Color.BLUE);
        final Box xAxis = new Box(AXIS_LENGTH, 0.2, 0.2);
        final Box yAxis = new Box(0.2, AXIS_LENGTH, 0.2);
        final Box zAxis = new Box(0.2, 0.2, AXIS_LENGTH);
        xAxis.setTranslateX(0.1);
        yAxis.setTranslateY(0.1);
        zAxis.setTranslateZ(0.1);
        xAxis.setMaterial(redMaterial);
        yAxis.setMaterial(greenMaterial);
        zAxis.setMaterial(blueMaterial);
        System.out.println("Rouge: X");
        System.out.println("Vert: Y");
        System.out.println("Bleu: Z");
        axisGroup.getChildren().addAll(xAxis, yAxis, zAxis);
        axisGroup.setVisible(true);
        world.getChildren().addAll(axisGroup);
    }

    public double getAngleY() {
        return cameraXform.ry.getAngle();
    }

    public double getAngleX() {
        return cameraXform.rx.getAngle();
    }

    public double getAngleZ() {
        return cameraXform.rz.getAngle();
    }

    private void handleMouse(Scene scene, final Node root) {
        //onZoom
        scene.setOnScroll(new EventHandler<ScrollEvent>() {
            @Override
            public void handle(ScrollEvent event) {
                onZoom(event);
                camera.setTranslateZ(camera.getTranslateZ() + event.getDeltaY());
            }
        });
        //onSelect
        scene.setOnMouseClicked((event) -> {
            switch (event.getClickCount()) {

                case 1:
                    onSelect((Shape3D) event.getPickResult().getIntersectedNode());
                    break;
                case 2:
                    onDblClick(event);
                    break;
            }
        });
        //
        scene.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                mousePosX = me.getSceneX();
                mousePosY = me.getSceneY();
                mouseOldX = me.getSceneX();
                mouseOldY = me.getSceneY();
            }
        });
        //
        scene.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                mouseOldX = mousePosX;
                mouseOldY = mousePosY;
                mousePosX = me.getSceneX();
                mousePosY = me.getSceneY();
                mouseDeltaX = (mousePosX - mouseOldX);
                mouseDeltaY = (mousePosY - mouseOldY);
                double modifier = 1.0;
                if (me.isControlDown()) {
                    modifier = CONTROL_MULTIPLIER;
                }
                if (me.isShiftDown()) {
                    modifier = SHIFT_MULTIPLIER;
                }
                if (me.isPrimaryButtonDown()) {
                    cameraXform.ry.setAngle(cameraXform.ry.getAngle()
                            - mouseDeltaX * modifierFactor * modifier * ROTATION_SPEED);  //
                    if (me.isAltDown()) {
                        cameraXform.rz.setAngle(cameraXform.rz.getAngle()
                                + mouseDeltaY * modifierFactor * modifier * ROTATION_SPEED);  // -
                    } else {
                        cameraXform.rx.setAngle(cameraXform.rx.getAngle()
                                + mouseDeltaY * modifierFactor * modifier * ROTATION_SPEED);  // -
                    }
                } else if (me.isSecondaryButtonDown()) {
                    double x = camera.getTranslateX();
                    double newX = x - mouseDeltaX * modifier;
                    camera.setTranslateX(newX);
                    double y = camera.getTranslateY();
                    double newY = y - mouseDeltaY * modifier;
                    camera.setTranslateY(newY);
                } else if (me.isMiddleButtonDown()) {
                    cameraXform2.t.setX(cameraXform2.t.getX()
                            + mouseDeltaX * MOUSE_SPEED * modifier * TRACK_SPEED);  // -
                    cameraXform2.t.setY(cameraXform2.t.getY()
                            + mouseDeltaY * MOUSE_SPEED * modifier * TRACK_SPEED);  // -
                }
            }
        }); // setOnMouseDragged
    } //handleMouse

    private void handleKeyboard(Scene scene, final Node root) {
        scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                switch (event.getCode()) {
                    case LEFT:
                        //camera.setTranslateX(camera.getTranslateX() + 1);
                        break;
                    case RIGHT:
                        //camera.setTranslateX(camera.getTranslateX() - 1);
                        break;
                    case DOWN:
                        //camera.setTranslateY(camera.getTranslateY() - 1);
                        break;
                    case UP:
                        //camera.setTranslateY(camera.getTranslateY() + 1);
                        break;
                    case Z:
                        System.out.println("Z");
                        cameraXform2.t.setX(0.0);
                        cameraXform2.t.setY(0.0);
                        cameraXform.ry.setAngle(CAMERA_INITIAL_Y_ANGLE);
                        cameraXform.rx.setAngle(CAMERA_INITIAL_X_ANGLE);


                        break;
                    case X:
                        System.out.println("X: afficher/masquer les axes");
                        axisGroup.setVisible(!axisGroup.isVisible());
                        break;
                    case S:
                        System.out.println("S: vue de face");
                        cameraXform.ry.setAngle(0);
                        cameraXform.rx.setAngle(0);
                        cameraXform.rz.setAngle(0);
                        camera.setTranslateY(0);
                        break;
                    case D:
                        System.out.println("D: vue de dessus");
                        cameraXform.ry.setAngle(90);
                        cameraXform.rx.setAngle(0);
                        cameraXform.rz.setAngle(0);
                        camera.setTranslateY(0);
                        camera.setTranslateX(0);
                        break;
                    case F:
                        System.out.println("F: vue de droite");
                        cameraXform.ry.setAngle(0);
                        cameraXform.rx.setAngle(90);
                        cameraXform.rz.setAngle(0);
                        break;
                } // switch
                onKey(event);
            } // handle()
        });  // setOnKeyPressed
    }  //  handleKeyboard()

    /**
     * @param aKeyEvent
     */
    public abstract void onKey(KeyEvent aKeyEvent);

    /**
     * @param aShape3D
     */
    public abstract void onSelect(Shape3D aShape3D);

    /**
     * @param me
     */
    public abstract void onDblClick(MouseEvent me);


    /**
     * @param aScrollEvent
     */
    public abstract void onZoom(ScrollEvent aScrollEvent);

    /**
     * @return
     */
    public Camera getCamera() {
        return this.camera;
    }

    /**
     * @return
     */
    public T3dForme getWorld() {
        return this.world;
    }

    /**
     * @param aXform
     * @return
     */
    public T3dWorld add(T3dForme aXform) {
        this.world.getChildren().addAll(aXform);
        return this;
    }

    /**
     *
     */
    public abstract void clear();

    /**
     * @param aT3dForm
     * @param transX
     * @param transY
     * @param transZ
     * @param xRot
     * @param yRot
     * @param zRot
     * @return
     */
    public abstract T3dWorld put(T3dForme aT3dForm, double transX, double transY, double transZ, double xRot, double yRot, double zRot);

    /**
     * @param aT3dMesh
     * @param transX
     * @param transY
     * @param transZ
     * @param xRot
     * @param yRot
     * @param zRot
     * @return
     */
    public abstract T3dWorld put(T3dMesh aT3dMesh, double transX, double transY, double transZ, double xRot, double yRot, double zRot);

    /**
     * @param aShape3D
     * @param transX
     * @param transY
     * @param transZ
     * @param xRot
     * @param yRot
     * @param zRot
     * @return
     */
    public abstract T3dWorld put(Shape3D aShape3D, double transX, double transY, double transZ, double xRot, double yRot, double zRot);

}
