/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.jfx3d;

import javafx.geometry.Point3D;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.transform.Rotate;

import java.io.Serializable;
import java.util.Collection;

/**
 * @author rpetit
 * @deprecated à remplacer par T3dForm
 */
@Deprecated
public class T3dGroup extends Group implements Serializable, I3d {

    public T3dGroup() {
        super();
    }

    public T3dGroup(Node... children) {
        super(children);
    }

    public T3dGroup(Collection<Node> children) {
        super(children);
    }

    /**
     * Copie les informations de translation X,Y,Z depuis 'original'
     *
     * @param original original
     * @return Chaînage
     */
    public final T3dGroup copyPosition(T3dGroup original) {
        this.setTranslateX(original.getTranslateX());
        this.setTranslateY(original.getTranslateY());
        this.setTranslateZ(original.getTranslateZ());
        return this;
    }

    /**
     * Copie les informations de rotation X,Y,Z depuis 'original'
     *
     * @param original original
     * @return Chaînage
     */
    public final T3dGroup copyRotations(T3dGroup original) {
        copyRotationX(original);
        copyRotationY(original);
        copyRotationZ(original);
        return this;
    }

    /**
     * Copie les informations de rotation X depuis 'original'
     *
     * @param original original
     * @return Chaînage
     */
    public final T3dGroup copyRotationX(T3dGroup original) {
        this.setRotationAxis(Rotate.X_AXIS);
        original.setRotationAxis(Rotate.X_AXIS);
        this.setRotate(original.getRotate());
        return this;
    }

    /**
     * Copie les informations de rotation Y depuis 'original'
     *
     * @param original original
     * @return Chaînage
     */
    public final T3dGroup copyRotationY(T3dGroup original) {
        this.setRotationAxis(Rotate.Y_AXIS);
        original.setRotationAxis(Rotate.Y_AXIS);
        this.setRotate(original.getRotate());
        return this;
    }

    /**
     * Copie les informations de rotation Z depuis 'original'
     *
     * @param original original
     * @return Chaînage
     */
    public final T3dGroup copyRotationZ(T3dGroup original) {
        this.setRotationAxis(Rotate.Z_AXIS);
        original.setRotationAxis(Rotate.Z_AXIS);
        this.setRotate(original.getRotate());
        return this;
    }

    /**
     * @return
     */
    public Point3D positionInWorld() {
        double x = this.getTranslateX();
        double y = this.getTranslateY();
        double z = this.getTranslateZ();
        Parent p = this.getParent();
        while (p.getClass() != T3dRoot.class) {
            x += p.getTranslateX();
            y += p.getTranslateX();
            z += p.getTranslateX();
            p = p.getParent();
        }
        return new Point3D(x, y, z);
    }

}
