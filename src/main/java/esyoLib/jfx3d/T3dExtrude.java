/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.jfx3d;

import esyoLib.STL.STLaxis;

/**
 * @author rpetit
 */
public class T3dExtrude extends T3dMesh {

    /**
     * @param aFilename
     * @param transX
     * @param transY
     * @param transZ
     * @param rotX
     * @param rotY
     * @param rotZ
     */
    public T3dExtrude(String aFilename, double transX, double transY, double transZ, double rotX, double rotY, double rotZ) {
        super(aFilename, transX, transY, transZ, rotX, rotY, rotZ);
    }

    /**
     * @param original
     * @param depth
     * @param aAxis    java.lang.Exception
     * @throws java.lang.Exception
     */
    public T3dExtrude(T3dMesh original, STLaxis aAxis, double depth) throws Exception {
        super(original);
        switch (aAxis) {
            case X_AXIS:
                extrudeX(depth);
                break;
            case Y_AXIS:
                extrudeY(depth);
                break;
            case Z_AXIS:
                extrudeZ(depth);
                break;
        }
    }

    //
    private void extrudeY(double depth) {
        setTranslateY(depth / 2);
        for (int i = 0; i < mesh.getPoints().size(); i = i + 3) {
            mesh.getPoints().set(i + 1, (float) (mesh.getPoints().get(i + 1) * depth));
        }

    }

    //
    private void extrudeX(double depth) {
        setTranslateX(depth / 2);
        for (int i = 0; i < mesh.getPoints().size(); i = i + 3) {
            //x
            mesh.getPoints().set(i, (float) (mesh.getPoints().get(i) * depth));
        }
        //--------------------------
    }

    //
    private void extrudeZ(double depth) {
        setTranslateZ(depth / 2);
        for (int i = 0; i < mesh.getPoints().size(); i = i + 3) {
            //z
            mesh.getPoints().set(i + 2, (float) (mesh.getPoints().get(i + 2) * depth));
        }
    }

}
