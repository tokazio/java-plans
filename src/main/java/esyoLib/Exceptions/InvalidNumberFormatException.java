/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esyoLib.Exceptions;

/**
 * @author RomainPETIT
 */
public class InvalidNumberFormatException extends Exception {

    public InvalidNumberFormatException(String s) {
        super(s);
    }

}
