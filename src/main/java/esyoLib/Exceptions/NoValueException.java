/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package esyoLib.Exceptions;

/**
 * @author RomainPETIT
 */
public class NoValueException extends Exception {

    public NoValueException(String s) {
        super("Impossible de trouver value: " + s);
    }

}
