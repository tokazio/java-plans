/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controllers;

import esyoLib.Formsfx.Tcontroller;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * FXML Controller class
 *
 * @author RomainPETIT
 */
public class CaProposForm extends Tcontroller implements Initializable {
    @FXML
    private Label version;
    @FXML
    private Label label1;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public final void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @Override
    public final void refresh() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        version.setText("v" + getForm().getApplication().getVersion());

        label1.setText("Antialiasing ? supporté!?");

        /*if(Toolkit.getToolkit().isAntiAliasingSupported()){
            label1.setText("Antialiasing supporté.");
        }else{
            label1.setText("Antialiasing NON supporté!");
        }*/
    }

    @Override
    public final void manualIni() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
