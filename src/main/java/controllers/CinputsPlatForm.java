/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import classes.TconfigUI;
import esyoLib.Formsfx.Tcontroller;
import esyoLib.Formsfx.TformException;
import esyoLib.Formsfx.TformFmxlLoadException;
import esyoLib.Formsfx.TformFmxlNotFoundException;
import evrLib.enumeration.Egamme;
import evrLib.gc.Tconfiguration;
import evrLib.gc.TconfigurationPlat;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import org.xml.sax.SAXException;
import plansevrfx.PlansEVRfx;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * FXML Controller class
 *
 * @auth or rpetit
 */
public class CinputsPlatForm extends Tcontroller implements Initializable {

    //

    ObservableList<Egamme> FgammesList = FXCollections.observableArrayList();
    //
    @FXML
    private TextField nomChantier;
    @FXML
    private TextField longueur;
    @FXML
    private TextField debordGauche;
    @FXML
    private TextField debordDroite;
    @FXML
    private TextField espacementPotMax;
    @FXML
    private CheckBox sansPotelet;
    @FXML
    private TextField hauteurFinie;
    @FXML
    private RadioButton coterDebGauche;
    @FXML
    private ToggleGroup cotation;
    @FXML
    private RadioButton coterDebDroite;
    @FXML
    private CheckBox fixMurGauche;
    @FXML
    private CheckBox fixMurDroite;
    @FXML
    private CheckBox angleGauche;
    @FXML
    private CheckBox angleDroite;
    @FXML
    private CheckBox poteletGauche;
    @FXML
    private CheckBox poteletDroite;
    @FXML
    private CheckBox tournerPot;
    @FXML
    private CheckBox tournerPotGauche;
    @FXML
    private CheckBox tournerPotDroite;
    @FXML
    private ComboBox<Egamme> gamme;
    @FXML
    private CheckBox isBal;
    @FXML
    private TextField espMaxBal;
    @FXML
    private CheckBox inversePotBalGauche;
    @FXML
    private CheckBox inversePotBalDroite;
    @FXML
    private CheckBox noprem;
    @FXML
    private CheckBox noder;
    @FXML
    private CheckBox hasMC;
    @FXML
    private TextField nbrTrav;
    @FXML
    private TextField espSolTrav;
    @FXML
    private RadioButton travIsApplique;
    @FXML
    private ToggleGroup postrav;
    @FXML
    private RadioButton travIsProfil;
    @FXML
    private ToggleGroup typtrav;
    @FXML
    private RadioButton travIsAxe;
    @FXML
    private RadioButton travIsCable;
    @FXML
    private TextField debHautVerreTole;
    @FXML
    private TextField posVerreTole;
    @FXML
    private TextField espSolVerreTole;
    @FXML
    private TextField axePinceHaut;
    @FXML
    private TextField axePinceBas;
    @FXML
    private CheckBox isVerreTole;
    @FXML
    private CheckBox isComplet;
    @FXML
    private TextField hauteurBalustre;
    @FXML
    private TextField axeBalTravHaut;
    @FXML
    private TextField axeBalTravBas;
    @FXML
    private TextField lettreA;
    @FXML
    private TextField lettreB;
    @FXML
    private RadioButton isSol;
    @FXML
    private ToggleGroup fix;
    @FXML
    private RadioButton isFacade;
    @FXML
    private RadioButton isScellement;
    @FXML
    private TextField scellPosZ;
    @FXML
    private TextField facPosY;
    @FXML
    private TextField solPosZ;
    @FXML
    private TextField epDalle;
    @FXML
    private TextField decalSol;
    @FXML
    private TextField nbrTravSerre;
    @FXML
    private TextField espTravSerre;
    @FXML
    private CheckBox muret;
    @FXML
    private TitledPane panelPotelet;
    @FXML
    private CheckBox mcMurale;
    @FXML
    private RadioButton gaucheDirHaut;
    @FXML
    private ToggleGroup gaucheDir;
    @FXML
    private RadioButton gaucheDirBas;
    @FXML
    private RadioButton droiteDirHaut;
    @FXML
    private ToggleGroup droiteDir;
    @FXML
    private RadioButton droiteDirBas;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        FgammesList.addAll(Egamme.values());
        gamme.setItems(FgammesList);
        gamme.getSelectionModel().selectFirst();

        gamme.valueProperty().addListener(new ChangeListener<Egamme>() {
            @Override
            public void changed(ObservableValue<? extends Egamme> observable, Egamme oldValue, Egamme newValue) {
                ((TconfigUI) Fobj).changeGamme(newValue);
            }
        });
    }

    /**
     *
     */
    @Override
    public void onActivate() {
        PlansEVRfx.selectChantier((TconfigUI) Fobj);
    }

    @Override
    public void onMinimized() {
        ((TconfigUI) Fobj).minimize();
    }

    @Override
    public void onMaximized() {
        ((TconfigUI) Fobj).maximize();
    }

    /**
     *
     */
    @Override
    public void onShow() {
//        ((TconfigUI) Fobj).refresh();
    }

    /**
     *
     */
    @Override
    public void onClose() {
        PlansEVRfx.selectChantier(null);
        ((TconfigUI) Fobj).doClose();
    }

    /**
     *
     */
    @Override
    public void onCloseQuery() {
        try {
            Fform.canClose = ((TconfigUI) Fobj).confirmClose();
        } catch (TformFmxlLoadException | IOException | TformException | IllegalArgumentException | IllegalAccessException | TformFmxlNotFoundException ex) {
            Logger.getLogger(CinputsPlatForm.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void refresh() {
        //TconfigurationPlat config=((Tchantier)Fobj).getConfigPlat();
        TconfigurationPlat config = (TconfigurationPlat) ((TconfigUI) Fobj).getConfig();
        //Général
        nomChantier.setText(config.getNom());
        longueur.setText("" + config.getLongueur());
        hauteurFinie.setText("" + config.getHauteurFinie());
        decalSol.setText("" + config.getDecalSol());
        muret.setSelected(config.getIsMuret());
        //Gamme
        if (config.getGamme() != null) {
            for (int i = 0; i < FgammesList.size(); i++) {
                if (FgammesList.get(i).getNom().equals(config.getGamme().getNom())) {
                    gamme.getSelectionModel().select(i);
                    break;
                }
            }
        }
        epDalle.setText("" + config.getEpdalle());
        //Potelets
        debordDroite.setText("" + config.getDebordPotDroite());
        debordGauche.setText("" + config.getDebordPotGauche());
        espacementPotMax.setText("" + config.getEspacementPotMax());
        sansPotelet.setSelected(config.getSansPotelet());
        poteletDroite.setSelected(config.getAnglePotDroite());
        poteletGauche.setSelected(config.getAnglePotGauche());
        tournerPot.setSelected(config.getTournerPot());
        tournerPotDroite.setSelected(config.getTournerPotDroite());
        tournerPotGauche.setSelected(config.getTournerPotGauche());
        noprem.setSelected(config.getNoPremPot());
        noder.setSelected(config.getNoDerPot());
        //cotation
        coterDebGauche.setSelected(config.getCoterDebGauche());
        coterDebDroite.setSelected(config.getCoterDebDroite());
        //mc
        hasMC.setSelected(config.getMainCourante());
        mcMurale.setSelected(config.getMCmurale());
        //traverse
        nbrTrav.setText("" + config.getNbrTraverses());
        espSolTrav.setText("" + config.getDebordTravBas());
        travIsApplique.setSelected(config.getTraversesApplique());
        travIsAxe.setSelected(config.getTraversesAxe());
        travIsCable.setSelected(config.getTraversesCable());
        espTravSerre.setText("" + config.getEspTravSerre());
        nbrTravSerre.setText("" + config.getNbrTravSerre());
        //verre/tole
        isVerreTole.setSelected(config.getIsVerre());
        isComplet.setSelected(config.getVerreIsComplet());
        debHautVerreTole.setText("" + config.getDebordVerreHaut());
        posVerreTole.setText("" + config.getPosVerre());
        espSolVerreTole.setText("" + config.getDebordVerreSol());
        axePinceBas.setText("" + config.getAxePinceBas());
        axePinceHaut.setText("" + config.getAxePinceHaut());
        //bal
        axeBalTravHaut.setText("" + config.getPosTravHaute());
        hauteurBalustre.setText("" + config.getHauteurBal());
        axeBalTravBas.setText("" + config.getPosTravBasse());
        isBal.setSelected(config.getIsBal());
        espMaxBal.setText("" + config.getEspacementBalMax());
        inversePotBalGauche.setSelected(config.getInversePotBalGauche());
        inversePotBalDroite.setSelected(config.getInversePotBalDroite());
        //Fixation au mur
        fixMurGauche.setSelected(config.getFixMurGauche());
        fixMurDroite.setSelected(config.getFixMurDroite());
        //angle
        poteletGauche.setSelected(config.getAnglePotGauche());
        poteletDroite.setSelected(config.getAnglePotDroite());
        angleDroite.setSelected(config.getAngleDroite());
        angleGauche.setSelected(config.getAngleGauche());
        lettreA.setText(config.getLettreA());
        lettreB.setText(config.getLettreB());
        gaucheDirHaut.setSelected(config.getAngleGaucheDir() > 0);
        gaucheDirBas.setSelected(config.getAngleGaucheDir() < 0);
        droiteDirHaut.setSelected(config.getAngleDroiteDir() > 0);
        droiteDirBas.setSelected(config.getAngleDroiteDir() < 0);
        //fixation au sol
        solPosZ.setText("" + config.getDecalZ());
        isFacade.setSelected(config.getIsFacade());
        facPosY.setText("" + config.getFacAxeVertPlatine());
        isScellement.setSelected(config.getIsScellement());
        scellPosZ.setText("" + config.getHauteurScellement());
    }

    public Tconfiguration getConfig() {
        return ((TconfigUI) Fobj).getConfig();
        //return ((Tchantier)Fobj).getConfigPlat();
    }

    @FXML
    private void calculer() {
        ((TconfigUI) Fobj).setEdited();
        //TconfigurationPlat config=((Tchantier)Fobj).getConfigPlat();
        TconfigurationPlat config = (TconfigurationPlat) ((TconfigUI) Fobj).getConfig();
        //Général
        config.setNom(nomChantier.getText());
        config.setLongueur(longueur.getText());
        config.setHauteurFinie(hauteurFinie.getText());
        try {
            config.setGamme(gamme.getValue());
        } catch (IOException | ParserConfigurationException | SAXException ex) {
            PlansEVRfx.application.showError(ex.getClass().getName(), ex.getMessage());
        }
        config.setEpdalle(epDalle.getText());
        config.setDecalSol(decalSol.getText());
        config.setIsMuret(muret.isSelected());
        //Potelets
        config.setDebordPotGauche(debordGauche.getText());
        config.setEspacementPotMax(espacementPotMax.getText());
        config.setDebordPotDroite(debordDroite.getText());
        config.setSansPotelet(sansPotelet.isSelected());
        config.setTournerPot(tournerPot.isSelected());
        config.setNoPremPot(noprem.isSelected());
        config.setNoDerPot(noder.isSelected());
        config.setTournerPotDroite(tournerPotDroite.isSelected());
        config.setTournerPotGauche(tournerPotGauche.isSelected());
        //cotation
        config.setCoterDebGauche(coterDebGauche.isSelected());
        config.setCoterDebDroite(coterDebDroite.isSelected());
        //mc
        config.setMainCourante(hasMC.isSelected());
        config.setMCmurale(mcMurale.isSelected());
        //traverse
        config.setNbrTraverses(nbrTrav.getText());
        config.setDebordTravBas(espSolTrav.getText());
        config.setTraversesApplique(travIsApplique.isSelected());
        config.setTraversesAxe(travIsAxe.isSelected());
        config.setTraversesCable(travIsCable.isSelected());
        config.setEspTravSerre(espTravSerre.getText());
        config.setNbrTravSerre(nbrTravSerre.getText());
        //verre/tole
        config.setIsVerre(isVerreTole.isSelected());
        config.setVerreIscomplet(isComplet.isSelected());
        config.setDebordVerreHaut(debHautVerreTole.getText());
        config.setPosVerre(posVerreTole.getText());
        config.setDebordVerreSol(espSolVerreTole.getText());
        config.setAxePinceHaut(axePinceHaut.getText());
        config.setAxePinceBas(axePinceBas.getText());
        //bal
        config.setIsBal(isBal.isSelected());
        config.setInversePotBalGauche(inversePotBalGauche.isSelected());
        config.setInversePotBalDroite(inversePotBalDroite.isSelected());
        config.setEspacementBalMax(espMaxBal.getText());
        config.setPosTravHaute(axeBalTravHaut.getText());
        config.setHauteurBal(hauteurBalustre.getText());
        config.setPosTravBasse(axeBalTravBas.getText());
        //Fixation au mur
        config.setFixMurGauche(fixMurGauche.isSelected());
        config.setFixMurDroite(fixMurDroite.isSelected());
        //angle
        config.setAngleGauche(angleGauche.isSelected());
        config.setAngleDroite(angleDroite.isSelected());
        config.setAnglePotGauche(poteletGauche.isSelected());
        config.setAnglePotDroite(poteletDroite.isSelected());
        config.setLettreA(lettreA.getText());
        config.setLettreB(lettreB.getText());
        if (gaucheDirHaut.isSelected()) {
            config.setAngleGaucheDir(1);
        }
        if (gaucheDirBas.isSelected()) {
            config.setAngleGaucheDir(-1);
        }
        if (droiteDirHaut.isSelected()) {
            config.setAngleDroiteDir(1);
        }
        if (droiteDirBas.isSelected()) {
            config.setAngleDroiteDir(-1);
        }
        //fixation au sol
        config.setDecalZ(solPosZ.getText());
        config.setIsFacade(isFacade.isSelected());
        config.setFacAxeVertPlatine(facPosY.getText());
        config.setIsScellement(isScellement.isSelected());
        config.setHauteurScellement(scellPosZ.getText());
        //config.print();
        try {
            ((TconfigUI) Fobj).refresh();
        } catch (Exception ex) {
            Logger.getLogger(CinputsPlatForm.class.getName()).log(Level.SEVERE, null, ex);
        }

        panelPotelet.setText("Potelet (" + config.getCodePotelet() + ")");
        //
        //config.comptePotelets();
        //System.out.println(config.getPotelets().saveToString());
    }

    @Override
    public void manualIni() {

    }

}
