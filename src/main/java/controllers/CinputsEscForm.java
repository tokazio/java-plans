/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import classes.TconfigUI;
import esyoLib.Formsfx.Tcontroller;
import esyoLib.Formsfx.TformException;
import esyoLib.Formsfx.TformFmxlLoadException;
import esyoLib.Formsfx.TformFmxlNotFoundException;
import evrLib.enumeration.Egamme;
import evrLib.gc.Tconfiguration;
import evrLib.gc.TconfigurationEscalier;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import org.xml.sax.SAXException;
import plansevrfx.PlansEVRfx;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * FXML Controller class
 *
 * @author rpetit
 */
public class CinputsEscForm extends Tcontroller implements Initializable {

    //
    ObservableList<Egamme> FgammesList = FXCollections.observableArrayList();
    @FXML
    private TextField nomChantier;
    @FXML
    private TextField espacementPotMax;
    @FXML
    private CheckBox hasMC;
    @FXML
    private TextField nbrTrav;
    @FXML
    private TextField espSolTrav;
    @FXML
    private TextField nbrTravSerre;
    @FXML
    private TextField espTravSerre;
    @FXML
    private RadioButton travIsApplique;
    @FXML
    private ToggleGroup postrav;
    @FXML
    private RadioButton travIsProfil;
    @FXML
    private ToggleGroup typtrav;
    @FXML
    private RadioButton travIsAxe;
    @FXML
    private RadioButton travIsCable;
    @FXML
    private TextField debHautVerreTole;
    @FXML
    private TextField posVerreTole;
    @FXML
    private TextField espSolVerreTole;
    @FXML
    private TextField axePinceHaut;
    @FXML
    private TextField axePinceBas;
    @FXML
    private CheckBox isVerreTole;
    @FXML
    private CheckBox isComplet;
    @FXML
    private CheckBox isBal;
    @FXML
    private TextField espMaxBal;
    @FXML
    private TextField hauteurBalustre;
    @FXML
    private CheckBox inversePotBalGauche;
    @FXML
    private CheckBox inversePotBalDroite;
    @FXML
    private TextField axeBalTravHaut;
    @FXML
    private TextField axeBalTravBas;
    @FXML
    private CheckBox angleGauche;
    @FXML
    private CheckBox angleDroite;
    @FXML
    private TextField lettreA;
    @FXML
    private TextField lettreB;
    @FXML
    private RadioButton isSol;
    @FXML
    private ToggleGroup fix;
    @FXML
    private RadioButton isFacade;
    @FXML
    private RadioButton isScellement;
    @FXML
    private TextField scellPosZ;
    @FXML
    private TextField facPosY;
    @FXML
    private TextField solPosZ;
    @FXML
    private TextField hauteurFinie;
    @FXML
    private ComboBox<Egamme> gamme;
    @FXML
    private TextArea marches;
    @FXML
    private TextField longueur;
    @FXML
    private CheckBox dev;
    @FXML
    private TextField debPotelet;
    @FXML
    private Button calcMarche;
    @FXML
    private Label reel;
    @FXML
    private Button calclongueur;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        FgammesList.addAll(Egamme.values());
        gamme.setItems(FgammesList);
        gamme.getSelectionModel().selectFirst();

        gamme.valueProperty().addListener(new ChangeListener<Egamme>() {
            @Override
            public void changed(ObservableValue<? extends Egamme> observable, Egamme oldValue, Egamme newValue) {
                ((TconfigUI) Fobj).changeGamme(newValue);
            }
        });
    }

    @Override
    public void refresh() {
        TconfigurationEscalier config = (TconfigurationEscalier) ((TconfigUI) Fobj).getConfig();
        //Général
        nomChantier.setText(config.getNom());
        hauteurFinie.setText("" + config.getHauteurFinie());
        //Gamme
        if (config.getGamme() != null) {
            for (int i = 0; i < FgammesList.size(); i++) {
                if (FgammesList.get(i).getNom().equals(config.getGamme().getNom())) {
                    gamme.getSelectionModel().select(i);
                    break;
                }
            }
        }
        //marches
        marches.setText(config.getMarchesString());
        longueur.setText("" + config.getLongueur());
        dev.setSelected(config.getIsDevelop());
        //Potelets
        espacementPotMax.setText("" + config.getEspacementPotMax());
        debPotelet.setText("" + config.getDebPotelet());
        //mc
        hasMC.setSelected(config.getMainCourante());
        //traverse
        nbrTrav.setText("" + config.getNbrTraverses());
        espSolTrav.setText("" + config.getDebordTravBas());
        travIsApplique.setSelected(config.getTraversesApplique());
        travIsAxe.setSelected(config.getTraversesAxe());
        travIsCable.setSelected(config.getTraversesCable());
        espTravSerre.setText("" + config.getEspTravSerre());
        nbrTravSerre.setText("" + config.getNbrTravSerre());
        //verre/tole
        isVerreTole.setSelected(config.getIsVerre());
        isComplet.setSelected(config.getVerreIsComplet());
        //debHautVerreTole.setText(""+config.getDebordVerreHaut());
        posVerreTole.setText("" + config.getPosVerre());
        espSolVerreTole.setText("" + config.getDebordVerreSol());
        axePinceBas.setText("" + config.getAxePinceBas());
        axePinceHaut.setText("" + config.getAxePinceHaut());
        //bal
        axeBalTravHaut.setText("" + config.getPosTravHaute());
        hauteurBalustre.setText("" + config.getHauteurBal());
        axeBalTravBas.setText("" + config.getPosTravBasse());
        isBal.setSelected(config.getIsBal());
        espMaxBal.setText("" + config.getEspacementBalMax());
        inversePotBalGauche.setSelected(config.getInversePotBalGauche());
        inversePotBalDroite.setSelected(config.getInversePotBalDroite());
        //angle
        //angleDroite.setSelected(config.getAngleDroite());
        //angleGauche.setSelected(config.getAngleGauche());
        //lettreA.setText(config.getLettreA());
        //lettreB.setText(config.getLettreB());
        //fixation au sol
        solPosZ.setText("" + config.getDecalZ());
        isFacade.setSelected(config.getIsFacade());
        facPosY.setText("" + config.getFacAxeVertPlatine());
        isScellement.setSelected(config.getIsScellement());
        //scellPosZ.setText(""+config.getHauteurScellement());
    }

    @Override
    public void manualIni() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *
     */
    @Override
    public void onActivate() {
        PlansEVRfx.selectChantier((TconfigUI) Fobj);
    }

    @Override
    public void onMinimized() {
        ((TconfigUI) Fobj).minimize();
    }

    @Override
    public void onMaximized() {
        ((TconfigUI) Fobj).maximize();
    }

    /**
     *
     */
    @Override
    public void onShow() {
//        ((TconfigUI) Fobj).refresh();
    }

    /**
     *
     */
    @Override
    public void onClose() {
        PlansEVRfx.selectChantier(null);
        ((TconfigUI) Fobj).doClose();
    }

    /**
     *
     */
    @Override
    public void onCloseQuery() {
        try {
            Fform.canClose = ((TconfigUI) Fobj).confirmClose();
        } catch (TformFmxlLoadException | IOException | TformException | IllegalArgumentException | IllegalAccessException | TformFmxlNotFoundException ex) {
            Logger.getLogger(CinputsPlatForm.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Tconfiguration getConfig() {
        return ((TconfigUI) Fobj).getConfig();
        //return ((Tchantier)Fobj).getConfigPlat();
    }

    @FXML
    private void calculer() {
        ((TconfigUI) Fobj).setEdited();
        //TconfigurationPlat config=((Tchantier)Fobj).getConfigPlat();
        TconfigurationEscalier config = (TconfigurationEscalier) ((TconfigUI) Fobj).getConfig();
        //Général
        config.setNom(nomChantier.getText());
        config.setHauteurFinie(hauteurFinie.getText());
        try {
            config.setGamme(gamme.getValue());
        } catch (IOException | ParserConfigurationException | SAXException ex) {
            PlansEVRfx.application.showError(ex.getClass().getName(), ex.getMessage());
        }

        //marches
        config.setMarchesString(marches.getText());
        config.setLongueur(longueur.getText());
        config.setIsDevelop(dev.isSelected());
        //Potelets
        config.setEspacementPotMax(espacementPotMax.getText());
        config.setDebPotelet(debPotelet.getText());
        //mc
        config.setMainCourante(hasMC.isSelected());
        //traverse
        config.setNbrTraverses(nbrTrav.getText());
        config.setDebordTravBas(espSolTrav.getText());
        config.setTraversesApplique(travIsApplique.isSelected());
        config.setTraversesAxe(travIsAxe.isSelected());
        config.setTraversesCable(travIsCable.isSelected());
        config.setEspTravSerre(espTravSerre.getText());
        config.setNbrTravSerre(nbrTravSerre.getText());
        //verre/tole
        config.setIsVerre(isVerreTole.isSelected());
        config.setVerreIscomplet(isComplet.isSelected());
        //config.setDebordVerreHaut(debHautVerreTole.getText());
        config.setPosVerre(posVerreTole.getText());
        config.setDebordVerreSol(espSolVerreTole.getText());
        config.setAxePinceHaut(axePinceHaut.getText());
        config.setAxePinceBas(axePinceBas.getText());
        //bal
        config.setIsBal(isBal.isSelected());
        config.setInversePotBalGauche(inversePotBalGauche.isSelected());
        config.setInversePotBalDroite(inversePotBalDroite.isSelected());
        config.setEspacementBalMax(espMaxBal.getText());
        config.setPosTravHaute(axeBalTravHaut.getText());
        config.setHauteurBal(hauteurBalustre.getText());
        config.setPosTravBasse(axeBalTravBas.getText());
        //angle
        //config.setAngleGauche(angleGauche.isSelected());
        //config.setAngleDroite(angleDroite.isSelected());
        //config.setLettreA(lettreA.getText());
        //config.setLettreB(lettreB.getText());
        //fixation au sol
        config.setDecalZ(solPosZ.getText());
        config.setIsFacade(isFacade.isSelected());
        config.setFacAxeVertPlatine(facPosY.getText());
        config.setIsScellement(isScellement.isSelected());
        //config.setHauteurScellement(scellPosZ.getText());
        //config.print();
//        ((TconfigUI) Fobj).refresh();
    }

    @FXML
    private void docalcmarches(ActionEvent event) {
        //g + 2h = 630 / g=630-2h avec h=180 -> g=270
        //TconfigurationEscalier config = (TconfigurationEscalier) ((TconfigUI) Fobj).getConfig();
        double l = Double.parseDouble(longueur.getText());

        if (dev.isSelected()) {
            l = l * Math.cos(Math.PI / 6);
            reel.setText("(" + l + ")");
        } else {
            reel.setText("(réelle)");
        }

        //config.setLongueur(l);        
        int n = (int) Math.ceil(l / 270);
        //if(dev.isSelected()) n++;
        double g = l / n;
        double h = 180;
        String s = "";
        for (int i = 0; i < n; i++)
            s += (int) h + "=" + g + "\n";
        marches.setText(s);
    }

    @FXML
    private void docalclongueur(ActionEvent event) {
        TconfigurationEscalier config = (TconfigurationEscalier) ((TconfigUI) Fobj).getConfig();
        longueur.setText("" + config.getCalculatedLongueurTotMarches());
        dev.setSelected(false);
        reel.setText("(réelle)");
    }

}
