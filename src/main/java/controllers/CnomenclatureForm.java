/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import classes.TvueNomenclature;
import esyoLib.Formsfx.Tcontroller;
import evrLib.gc.Tconfiguration;
import evrLib.gc.Tnomenclature;
import evrLib.gc.TnomenclatureRef;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * FXML Controller class
 *
 * @author rpetit
 */
public class CnomenclatureForm extends Tcontroller implements Initializable {
    @FXML
    private TableView<TnomenclatureRef> table;
    @FXML
    private TableColumn<TnomenclatureRef, String> refCol;
    @FXML
    private TableColumn<TnomenclatureRef, Double> qttCol;
    @FXML
    private TableColumn<TnomenclatureRef, String> designationCol;
    @FXML
    private TableColumn<TnomenclatureRef, Double> prixVenteCol;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO        
        refCol.setCellValueFactory(new PropertyValueFactory("ref"));
        qttCol.setCellValueFactory(new PropertyValueFactory("qtt"));
        prixVenteCol.setCellValueFactory(new PropertyValueFactory("prixVente"));
        designationCol.setCellValueFactory(new PropertyValueFactory("designation"));
    }

    @Override
    public void refresh() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void manualIni() {
        Tconfiguration c = ((TvueNomenclature) Fobj).getConfig();
        if (c != null) {
            Tnomenclature n = ((TvueNomenclature) Fobj).getConfig().getNomenclature();
            if (n != null) {
                table.setItems(n.getRefs());
            }
        }
    }

}
