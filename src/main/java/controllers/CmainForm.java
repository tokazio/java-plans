/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import classes.TconfigUI;
import classes.TconfigUIEscalier;
import classes.TconfigUIPlat;
import esyoLib.Files.TfileUtils;
import esyoLib.Files.Trecents;
import esyoLib.Formsfx.Tcontroller;
import esyoLib.Formsfx.Tform;
import esyoLib.Formsfx.TformException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.stage.FileChooser;
import plansevrfx.PlansEVRfx;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author rpetit
 */
public class CmainForm extends Tcontroller implements Initializable {

    //éléments de l'IHM

    //Liste des chantiers
    private static final ArrayList<TconfigUI> Fchantiers = new ArrayList();
    //Chantier selectionné
    private static TconfigUI FselectedChantier;
    //Liste des fichiers récement utilisés
    private static Trecents Frecents;
    @FXML
    private MenuItem nouveauaplatMenuBut;
    @FXML
    private MenuItem enregistrerMenuBut;
    @FXML
    private MenuItem enregistrersousMenuBut;
    @FXML
    private MenuItem exporterMenuBut;
    @FXML
    private MenuItem ouvrirMenuBut;
    @FXML
    private MenuItem fermerMenuBut;
    @FXML
    private MenuItem quitterMenuBut;
    @FXML
    private Menu recentsMenu;
    @FXML
    private MenuItem exporterPDFBut;
    @FXML
    private Button enregistrerToolBut;
    @FXML
    private Button showfolderToolBut;
    @FXML
    private MenuItem nouveauescalierBut;
    @FXML
    private Button suivantToolBut;
    @FXML
    private Button binpackBut;
    @FXML
    private Button progBut;
    @FXML
    private Button chantier3dBut;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Frecents = new Trecents("recents.txt") {
            @Override
            public void onClick(ActionEvent e) {
                try {
                    open(((MenuItem) e.getSource()).getId());
                } catch (Exception ex) {
                    Logger.getLogger(CmainForm.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        };
    }

    /**
     * Actualise la fenêtre
     */
    @Override
    public void refresh() {
        Frecents.majMenu(recentsMenu);
        enregistrerMenuBut.setDisable(FselectedChantier == null);
        enregistrersousMenuBut.setDisable(FselectedChantier == null);
        exporterMenuBut.setDisable(FselectedChantier == null);
        fermerMenuBut.setDisable(FselectedChantier == null);
        enregistrerToolBut.setDisable(FselectedChantier == null);
        showfolderToolBut.setDisable(FselectedChantier == null);
        exporterPDFBut.setDisable(FselectedChantier == null);
        suivantToolBut.setDisable(FselectedChantier == null);
        binpackBut.setDisable(FselectedChantier == null);
        progBut.setDisable(FselectedChantier == null);
        chantier3dBut.setDisable(FselectedChantier == null);
    }

    /**
     * initialisation après création de la fenêtre
     */
    @Override
    public final void manualIni() {
        getForm().setPos(0, 0);
        getForm().setSize(java.awt.GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds().width, 100);
    }

    @FXML
    private void nouveauaplat(ActionEvent event) throws IOException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, ClassNotFoundException {
        FselectedChantier = new TconfigUIPlat();
        Fchantiers.add(FselectedChantier);
        refresh();
    }

    @FXML
    private void enregistrer(ActionEvent event) throws IOException, IllegalArgumentException, IllegalAccessException {
        if (FselectedChantier == null) {
            return;
        }
        String f = FselectedChantier.save();
        Frecents.add(f);
        refresh();
    }

    @FXML
    private void enregistrersous(ActionEvent event) throws IOException, IllegalArgumentException, IllegalAccessException {
        if (FselectedChantier == null) {
            return;
        }
        String f = FselectedChantier.saveToFile();
        Frecents.add(f);
        getForm().getApplication().setActualFolder(new File(f).getPath());
        refresh();
    }

    @FXML
    private void exporter(ActionEvent event) throws IOException, IllegalArgumentException, IllegalAccessException {
        if (FselectedChantier == null) {
            return;
        }
        try {
            FselectedChantier.exportAsPng();
        } catch (NoSuchFieldException ex) {
            PlansEVRfx.application.showError(ex);
        }
        refresh();
    }

    @FXML
    private void ouvrir(ActionEvent event) throws Exception {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Ouvrir...");
        //Pré selection du dossier
        String f = getForm().getApplication().getActualFolder();
        if (!TfileUtils.isValidFolder(f)) {
            f = PlansEVRfx.options.defaultPath;
        }
        if (TfileUtils.isValidFolder(f)) {
            fileChooser.setInitialDirectory(new File(f));
        }
        //Set extension filter
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("PLA files (*.pla)", "*.pla");
        fileChooser.getExtensionFilters().add(extFilter);
        //Show open file dialog
        File file = fileChooser.showOpenDialog(getForm().getStage());
        //récup file
        if (file != null) {
            open(file.getPath());
        }
        //Rafraîchit l'UI principale
        refresh();
    }

    //ouvre un chantier
    private void open(String aFileName) throws Exception {
        getForm().getApplication().setActualFolder(new File(aFileName).getParent());
        FselectedChantier = TconfigUI.createFrom(aFileName);
        Fchantiers.add(FselectedChantier);
        Frecents.add(aFileName);
        //Rafraîchit l'UI principale
        refresh();
        //Rafraîchit configUI
        FselectedChantier.refresh();
    }

    @FXML
    private void fermer(ActionEvent event) throws TformException, IOException {
        if (FselectedChantier == null) {
            return;
        }
        FselectedChantier.close();
        Fchantiers.remove(FselectedChantier);
        FselectedChantier = null;
        refresh();
    }

    /**
     * Demande de fermeture de l'application
     */
    @Override
    public void onCloseQuery() {
        while (Fchantiers.size() > 0) {
            Fchantiers.get(Fchantiers.size() - 1).close();
            Fchantiers.remove(Fchantiers.get(Fchantiers.size() - 1));
        }
    }

    @FXML
    private void quitter(ActionEvent event) throws TformException, IOException {
        getForm().close();
        //Platform.exit();
    }

    @FXML
    private void apropos(ActionEvent event) throws Exception {
        //crée la fenêtre d'inputs
        Tform f = PlansEVRfx.application.newForm("", "fxmls/aProposForm.fxml", null).setTitle("A propos de Plan");
        //lie l'objet à la fenêtre
        //f.setObj(FselectedProjet);
        //met à jour la fenêtre,affiche la fenêtre,place le fenêtre en 1er
        f.refresh().show().toFront();
    }

    /**
     * Selection du chantier
     *
     * @param c
     */
    public final void selectChantier(TconfigUI c) {
        //si déjà sélèctionné, je sors
        if (FselectedChantier == c) {
            return;
        }
        //nouvelle selection
        FselectedChantier = c;
        //si pas null
        if (FselectedChantier != null) //place devant
        {
            c.toFront();
        }
        //Rafraîchit l'UI principale
        refresh();
    }

    /**
     * A l'agrandissement, agrandis les fenêtres de config dépendantes
     */
    @Override
    public void onMaximized() {
        Fchantiers.stream().forEach((Fchantier) -> {
            Fchantier.maximize();
        });
    }

    /**
     * A la réduction, agrandis les fenêtres de config dépendantes
     */
    @Override
    public void onMinimized() {
        Fchantiers.stream().forEach((Fchantier) -> {
            Fchantier.minimize();
        });
    }

    @FXML
    private void exporterPDF(ActionEvent event) {
        if (FselectedChantier == null) {
            return;
        }
        FselectedChantier.exportPDF();
        refresh();
    }

    @FXML
    private void showFolder(ActionEvent event) {
        if (FselectedChantier == null) {
            return;
        }
        FselectedChantier.openFolder();
    }

    @FXML
    private void nouveauescalier(ActionEvent event) throws IOException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, ClassNotFoundException {
        FselectedChantier = new TconfigUIEscalier();
        Fchantiers.add(FselectedChantier);
        //Rafraîchit l'UI principale
        refresh();
    }

    @FXML
    private void suivant(ActionEvent event) throws Exception {
        String file = FselectedChantier.getFileName();
        String nom = FselectedChantier.getConfig().getNom();
        FselectedChantier.close();
        FselectedChantier = null;
        if (new File(nextFile(file)).exists()) {
            open(nextFile(file));
            return;
        }
        TconfigUI c = TconfigUI.createFrom(file);
        c.getConfig().setNom(nextNom(nom));
        c.setFileName(nextFile(file));
        c.save();
        Fchantiers.add(c);
        FselectedChantier = c;
        //Rafraîchit l'UI principale
        refresh();
        //Rafraîchit configUI
        c.refresh();
    }

    /**
     * Crée le fichier suivant
     *
     * @param s chemin du fichier de la config actuelle
     * @return chemin du fichier de la config suivante
     */
    private String nextFile(String s) {
        String path = new File(s).getParent();
        String filename = new File(s).getName();
        String ext = TfileUtils.getExtension(filename);
        String noext = TfileUtils.removeExtension(filename);
        String n = nextNom(noext);
        return path + "\\" + n + ext;
    }

    /**
     * Compose le nom de la config suivante
     *
     * @param s Nom de la config actuelle
     * @return Nom de la config suivante
     */
    private String nextNom(String s) {
        int i = s.lastIndexOf("-");
        if (i >= 0) {
            String n = s.substring(0, i);
            int j;
            try {
                j = Integer.parseInt(s.substring(i + 1));
                j++;
                return n + "-" + j;
            } catch (Exception ex) {
                return s;
            }
        }
        return s;
    }

    @FXML
    private void goBinpack(ActionEvent event) {
        ProcessBuilder pb = new ProcessBuilder("java.exe", "-jar", "C:\\Users\\rpetit\\Documents\\NetBeansProjects\\binpackEVRfx\\dist\\binpackEVRfx.jar", "\"" + FselectedChantier.getFileName() + "\"");
        System.out.println("Arg1=\"" + FselectedChantier.getFileName() + "\"");
        try {
            Process p = pb.start();
        } catch (IOException ex) {
            Logger.getLogger(CmainForm.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void program(ActionEvent event) throws Exception {
        //crée la fenêtre d'inputs
        Tform f = PlansEVRfx.application.newForm("", "fxmls/programForm.fxml", null).setTitle("Programme de découpe de " + FselectedChantier.getName());
        //lie l'objet à la fenêtre
        f.setObj(FselectedChantier);
        //met à jour la fenêtre,affiche la fenêtre,place le fenêtre en 1er
        f.refresh().show().toFront();
    }

    @FXML
    private void goChantier3d(ActionEvent event) {
        ProcessBuilder pb = new ProcessBuilder("java.exe", "-jar", "C:\\Users\\rpetit\\Documents\\NetBeansProjects\\chantier3dEVRfx\\dist\\chantier3dEVRfx.jar", "\"" + FselectedChantier.getFileName() + "\"");
        System.out.println("Arg1=\"" + FselectedChantier.getFileName() + "\"");
        try {
            Process p = pb.start();
        } catch (IOException ex) {
            Logger.getLogger(CmainForm.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
