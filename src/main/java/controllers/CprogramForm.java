package controllers;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import classes.TconfigUI;
import esyoLib.Formsfx.Tcontroller;
import esyoLib.Utils.TarrayList;
import evrLib.gc.Tconfiguration;
import evrLib.gc.Tpercage;
import evrLib.gc.Tpotelet;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

/**
 * FXML Controller class
 *
 * @author rpetit
 */
public class CprogramForm extends Tcontroller implements Initializable {
    @FXML
    private TextArea progText;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @Override
    public void refresh() {
        String s = "";
        Tconfiguration config = ((TconfigUI) Fobj).getConfig();

        s += "POTELETS\n\n";
        TarrayList<Tpotelet> b = config.getPotelets();
        for (int i = 0; i < b.size(); i++) {
            s += "Potelet #" + (i + 1) + "(" + b.get(i).getType() + ")\n";
            ArrayList<Tpercage> p = b.get(i).getPercages();
            s = p.stream().map((p1) -> p1.toString() + "\n").reduce(s, String::concat);
            //fichiers des morceaux potelet pour editeur de barre---------------
            /*
            try {
                Tpotelet pot=((Tpotelet)b.get(i));
                pot.toFile(config.getNom()+" "+pot.getCode()+" "+pot.getType()+".txt");
            } catch (IOException ex) {
                Logger.getLogger(CprogramForm.class.getName()).log(Level.SEVERE, null, ex);
            }
                    */
            //------------------------------------------------------------------
            s += "\n";
        }
        /*
        s+="\n\nBALUSTRES\n\n";
        b=config.getBalustres();
        for(int i=0;i<b.size();i++){
            s+="Balustre #"+(i+1)+"\n";
            ArrayList<Tpercage> p=b.get(i).getPercages();
            for(int j=0;j<p.size();j++)
                s+=p.get(j).toString()+"\n";
        }
        s+="\n\nTRAVERSES\n\n";
        b=config.getTraverses();
        for(int i=0;i<b.size();i++){
            s+="Traverse #"+(i+1)+"\n";
            ArrayList<Tpercage> p=b.get(i).getPercages();
            for(int j=0;j<p.size();j++)
                s+=p.get(j).toString()+"\n";
        }
                */
        s += "\n\nFIN\n\n";
        progText.setText(s);
    }

    @Override
    public void manualIni() {

    }

}
