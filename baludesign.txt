<gamme>

<!-- Décalage entre le bas du potelet en façade et l'axe de fixation de la platine de façade-->
<FfacDecalHauteurPlatine>61</FfacDecalHauteurPlatine>

<FfacDecalZ>45.2</FfacDecalZ>

<FtravDecalZ>43.5</FtravDecalZ>

<LARGPINCEVERRE>23</LARGPINCEVERRE>
<FHBOUCHONPOT>5</FHBOUCHONPOT>
<FHCABLE>3</FHCABLE>
<FHMC>50</FHMC>
<FHLIAISON>45.74</FHLIAISON>
<FHLIAISONANGLE>45.74</FHLIAISONANGLE>
<FHTRAV>16</FHTRAV>
<FHFACADE>0</FHFACADE>
<FLARGPOT>40</FLARGPOT>
<FAXEMC>25</FAXEMC>
<FAXELIAISON>0</FAXELIAISON>
<FEPPOT>40</FEPPOT>
<FEPTRAV>16</FEPTRAV>
<FsolDesaxeVertAncrageA>-60</FsolDesaxeVertAncrageA>
<FsolDesaxeVertAncrageB>0</FsolDesaxeVertAncrageB>
<FsolDesaxeVertAncrageC>0</FsolDesaxeVertAncrageC>
<FsolDesaxeHoriAncrageA>0</FsolDesaxeHoriAncrageA>
<FsolDesaxeHoriAncrageB>0</FsolDesaxeHoriAncrageB>
<FsolDesaxeHoriAncrageC>0</FsolDesaxeHoriAncrageC>
<FfacDesaxeVertAncrageA>0</FfacDesaxeVertAncrageA>
<FfacDesaxeVertAncrageB>0</FfacDesaxeVertAncrageB>
<FfacDesaxeHoriAncrageA>0</FfacDesaxeHoriAncrageA>
<FfacDesaxeHoriAncrageB>40</FfacDesaxeHoriAncrageB>
<FrotPot>0</FrotPot>
<FrotPotAngleDroit>45</FrotPotAngleDroit>
<FrotPotAngleGauche>315</FrotPotAngleGauche>
<FHSABOT>28</FHSABOT>
<MINHFINIE>142.74</MINHFINIE>
<FtraverseRef>AL16</FtraverseRef>
<FmcRef>AL50</FmcRef>
<FpoteletRef>AL40</FpoteletRef>
<FcacheRef />
<FbalRef>AL16</FbalRef>
<FLGBARRE>6600</FLGBARRE>
</gamme>